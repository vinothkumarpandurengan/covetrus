/**
 * This function cuts the product name to a specific length and height.
 * @param productId
 */
function adjustProductName(obj, maxLength)
{
    var maxTextLength = maxLength;
    if (typeof(maxTextLength) == 'undefined') {
        maxTextLength = 30;
    }

    var elem = jQuery(obj);
    var text = elem.text().trim();

    if(text) {
        if (text.length && text.length > maxTextLength) {
            elem.empty();

            // cut keyword length
            stringLength = text.length - maxTextLength;
            text = text.substring(0, text.length - stringLength) + '...';

            elem.append(text);
        }
    }
}