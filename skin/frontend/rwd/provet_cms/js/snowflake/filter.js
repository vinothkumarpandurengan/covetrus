if (typeof Snowflake == 'undefined') {
    Snowflake = {};
}

Snowflake.MultipleFilter = Class.create({
    initialize: function () {
        var items, i, j, n,
            //classes = ['category', 'attribute', 'icon', 'price', 'clear', 'dt', 'clearall'];
            classes = ['attribute', 'clearall'];

        for (j = 0; j < classes.length; ++j) {
            items = $('narrow-by-list').select('.filter_layered_' + classes[j]);

            n = items.length;
            for (i = 0; i < n; ++i) {
                Event.observe(items[i], 'click', eval('this.run' + classes[j].capitalize() + 'Listener.bind(this)'));
            }
        }

        this.initToolbar();
    },

    initToolbar: function () {
        var items = $('filter_layered_container').select('.pages a', '.view-mode a', '.sort-by a');
        var i, n = items.length;
        for (i = 0; i < n; ++i) {
            Event.observe(items[i], 'click', this.runToolbarListener.bind(this));
        }
    },

    runToolbarListener: function (event) {
        this.toolbarRequest(Event.findElement(event, 'A').href);
        Event.stop(event);
    },

    runAttributeListener: function (event) {
        this.addParams('p', 1, 1);
        this.updateLinks(event, 'filter_attribute', 0);
    },

    runClearallListener: function (event) {
        var params = $('snowflake_filter_params').value.parseQuery();
        $('snowflake_filter_params').value = 'clearall=true';
        if (params['q'])
        {
            $('snowflake_filter_params').value += '&q=' + params['q'];
        }

        this.request();

        Event.stop(event);
    },

    addParams: function (key, val, isSingleVal) {
        var elem   = $('snowflake_filter_params');
        var params = elem.value.parseQuery();

        var strVal = params[key];
        if (typeof strVal == 'undefined' || !strVal.length) {
            params[key] = val;
        }
        else if ('clear' == val) {
            params[key] = 'clear';
        }
        else {
            if (key == 'price')
                var values = strVal.split(',');
            else
                var values = strVal.split('-');

            if (-1 == values.indexOf(val)) {
                if (isSingleVal)
                    values = [val];
                else
                    values.push(val);
            }
            else {
                values = values.without(val);
            }

            params[key] = values.join('-');
        }

        elem.value = Object.toQueryString(params).gsub('%2B', '+');
    },

    updateLinks: function (event, className, isSingleVal) {
        var link   = Event.findElement(event, 'A'),
            select = className + '-selected';

        if (link.hasClassName(select))
            link.removeClassName(select);
        else
            link.addClassName(select);

        this.addParams(link.id.split('-')[0], link.id.split('-')[1], isSingleVal);
        this.request();

        Event.stop(event);
    },

    hideProducts: function() {
        var items = $('narrow-by-list').select('a', 'input');
        n = items.length;
        for (i = 0; i < n; ++i) {
            items[i].addClassName('filter_layered_disabled');
        }

        var divs = $$('div.loading_filters');
        for (var i = 0; i < divs.length; ++i)
            divs[i].show();
    },

    showProducts: function(transport) {
        var resp = {};
        if (transport && transport.responseText) {
            try {
                resp = eval('(' + transport.responseText + ')');
            }
            catch (e) {
                resp = {};
            }
        }

        if (resp.products) {
            var ajaxUrl = $('snowflake_filter_ajax').value;

            // update Product
            $('filter_layered_container').update(resp.products.gsub(ajaxUrl, $('snowflake_filter_url').value));
            this.initToolbar();

            // update Filter
            $('catalog-filters').update(resp.layer.gsub(ajaxUrl, $('snowflake_filter_url').value));

            $('snowflake_filter_ajax').value = ajaxUrl;
        }

        var items = $('narrow-by-list').select('a', 'input');
        n = items.length;
        for (i = 0; i < n; ++i) {
            items[i].removeClassName('filter_layered_disabled');
        }
    },

    toolbarRequest: function (href) {
        var pos = href.indexOf('?');
        if (pos > -1) {
            $('snowflake_filter_params').value = href.substring(pos + 1, href.length);
        }

        this.request();
    },

    request: function() {
        this.hideProducts();

        var self = this;
        var params = $('snowflake_filter_params').value.parseQuery();
        if (!params['dir'])
        {
            $('snowflake_filter_params').value += '&dir=' + 'asc';
        }

        new Ajax.Request(
            $('snowflake_filter_ajax').value + '?' + $('snowflake_filter_params').value,
            {
                method: 'get',
                onSuccess: function (transport) {
                    self.showProducts(transport)
                }
            }
        );
    }
});