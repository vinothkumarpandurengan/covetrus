<?php

/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magegiant.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magegiant.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_Onestepcheckout
 * @copyright   Copyright (c) 2012 Magegiant (http://www.magegiant.com/)
 * @license     http://www.magegiant.com/license-agreement.html
 */
class Magegiant_Onestepcheckout_Model_Observer
{
    public function controllerActionPredispatchCheckout($observer)
    {
        $controllerInstance = $observer->getControllerAction();
        if (
            $controllerInstance instanceof Mage_Checkout_OnepageController &&
            $controllerInstance->getRequest()->getActionName() !== 'success' &&
            $controllerInstance->getRequest()->getActionName() !== 'failure' &&
            $controllerInstance->getRequest()->getActionName() !== 'saveOrder' &&
            Mage::helper('onestepcheckout/config')->isEnabled()
        ) {
            $controllerInstance->getResponse()->setRedirect(
                Mage::getUrl('onestepcheckout/index', array('_secure' => true))
            );
            $controllerInstance->setFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH, true);
        }
    }

    /**
     * @param $observer
     * submit order after
     */
    public function checkoutSubmitAllAfter($observer)
    {
        $oscOrderData = Mage::getSingleton('checkout/session')->getData('onestepcheckout_order_data');
        if (!is_array($oscOrderData)) {
            $oscOrderData = array();
        }
        // add customer comment
        if (array_key_exists('comments', $oscOrderData)) {
            $comment = $oscOrderData['comments'];
            if ($lastOrderId = Mage::getSingleton('checkout/type_onepage')->getCheckout()->getLastOrderId()) {
                $order = Mage::getModel('sales/order')->load($lastOrderId);
                $order
                    ->addStatusHistoryComment(Mage::helper('onestepcheckout')->__('Comment by customer: %s', $comment))
                    ->setIsVisibleOnFront(true)
                    ->save();
            }
        }
        // add delivery time
        if (array_key_exists('delivery', $oscOrderData) && !empty($oscOrderData['delivery'])) {
            $delivery_data = $oscOrderData['delivery'];
            if ($lastOrderId = Mage::getSingleton('checkout/type_onepage')->getCheckout()->getLastOrderId()) {
                $delivery = Mage::getModel('onestepcheckout/delivery');
                $delivery
                    ->setDeliveryTimeDate($delivery_data['date'] . ' ' . $delivery_data['time'])
                    ->setOrderId($lastOrderId)
                    ->save();
            }
        }
        // Add survey
        if (array_key_exists('onestepcheckout_survey_answer', $oscOrderData) && $oscOrderData['onestepcheckout_survey_answer']) {
            $survey_answer    = $oscOrderData['onestepcheckout_survey_answer'];
            $survey_quesition = $oscOrderData['onestepcheckout_survey_question'];
            if ($lastOrderId = Mage::getSingleton('checkout/type_onepage')->getCheckout()->getLastOrderId()) {
                $survey = Mage::getModel('onestepcheckout/survey');
                $survey
                    ->setQuestion($survey_quesition)
                    ->setAnswer($survey_answer)
                    ->setOrderId($lastOrderId)
                    ->save();
            }
        }
        // subscribe to newsletter
        if (array_key_exists('is_subscribed', $oscOrderData) && $oscOrderData['is_subscribed']) {
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            if ($customer->getId()) {
                $data = array(
                    'email'       => $customer->getEmail(),
                    'first_name'  => $customer->getFirstname(),
                    'last_name'   => $customer->getLastname(),
                    'customer_id' => $customer->getId(),
                );
            } else {
                $billing = $oscOrderData['billing'];
                $data    = array(
                    'email'      => $billing['email'],
                    'first_name' => $billing['firstname'],
                    'last_name'  => $billing['lastname'],
                );
            }
            if (array_key_exists('segments_select', $oscOrderData)) {
                $data['segments_codes'] = $oscOrderData['segments_select'];
            }
            $data['store_id'] = Mage::app()->getStore()->getId();
            Mage::helper('onestepcheckout/newsletter')->subscribeCustomer($data);
        }

        //clear saved values
        Mage::getSingleton('checkout/session')->setData('onestepcheckout_form_values', array());
        Mage::getSingleton('checkout/session')->setData('onestepcheckout_order_data', array());
        Mage::getSingleton('checkout/session')->setData('is_used_giftwrap', '');
    }

    /**
     * Compatibility with Paypal Hosted Pro
     *
     * @param $observer
     */
    public function controllerActionPostdispatchOnestepcheckoutAjaxPlaceOrder($observer)
    {
        $paypalObserver = Mage::getModel('paypal/observer');
        if (!method_exists($paypalObserver, 'setResponseAfterSaveOrder')) {
            return $this;
        }
        $controllerAction = $observer->getEvent()->getControllerAction();
        $result           = Mage::helper('core')->jsonDecode(
            $controllerAction->getResponse()->getBody(),
            Zend_Json::TYPE_ARRAY
        );
        if ($result['success']) {
            $paypalObserver->setResponseAfterSaveOrder($observer);
            $result                  = Mage::helper('core')->jsonDecode(
                $controllerAction->getResponse()->getBody(),
                Zend_Json::TYPE_ARRAY
            );
            $result['is_hosted_pro'] = true;
            $controllerAction->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }

    /**
     * Change template for Authorize.net Direct Post (DPM)
     *
     * @param $observer
     */
    public function coreLayoutBlockCreateAfterOnestepcheckout($observer)
    {
        if (Mage::app()->getRequest()->getControllerModule() !== 'Magegiant_Onestepcheckout') {
            return $observer;
        }

        $block = $observer->getBlock();
        if ($block instanceof Mage_Authorizenet_Block_Directpost_Form) {
            $block->setTemplate('magegiant/onestepcheckout/onestep/form/payment/authorizenet/directpost.phtml');
        }
    }

    /**
     * Fix for bug in Mage_Core_Model_Layout_Update::getFileLayoutUpdatesXml
     *
     * @param $observer
     *
     * @todo Move to Magegiant_Lib
     */
    public function coreLayoutUpdateUpdatesGetAfter($observer)
    {
        /* @var Mage_Core_Model_Config_Element $updateRoot */
        $updateRoot       = $observer->getUpdates();
        $nodeListToRemove = array();
        foreach ($updateRoot->children() as $updateKey => $updateNode) {
            if ($updateNode->file) {
                if (strpos($updateKey, 'onestepcheckout') !== false) {
                    $module = $updateNode->getAttribute('module');
                    if ($module && !Mage::helper('core')->isModuleOutputEnabled($module)) {
                        $nodeListToRemove[] = $updateKey;
                    }
                }
            }
        }

        foreach ($nodeListToRemove as $nodeKey) {
            unset($updateRoot->$nodeKey);
        }
    }

    public function paypalPrepareLineItems($observer)
    {
        if ($paypalCart = $observer->getPaypalCart()) {
            $salesEntity        = $paypalCart->getSalesEntity();
            $giftWrapBaseAmount = $salesEntity->getGiantGiftwrapBaseAmount();
            if ($giftWrapBaseAmount > 0.0001) {
                $paypalCart->updateTotal(
                    Mage_Paypal_Model_Cart::TOTAL_SUBTOTAL,
                    (float)$giftWrapBaseAmount,
                    Mage::helper('onestepcheckout')->__('Gift wrap')
                );
            }
        }

        return $this;
    }
}