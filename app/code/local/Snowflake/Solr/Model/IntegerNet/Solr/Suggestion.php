<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 10.12.2015
 * Time: 10:03
 */ 
class Snowflake_Solr_Model_IntegerNet_Solr_Suggestion extends IntegerNet_Solr_Model_Suggestion
{
    /**
     * Get Matrix Helper
     *
     * @return  Snowflake_Matrix_Helper_Data
     */
    protected function _getMatrixHelper()
    {
        return Mage::helper('snowflake_matrix');
    }

    protected function _getFilterQuery($storeId)
    {
        $filterQuery = 'store_id:' . $storeId;

        if (Mage::getStoreConfig('snowflake_matrix/settings/active') && Mage::getSingleton('customer/session')->isLoggedIn())
        {
            $productMatrixGroup = $this->_getMatrixHelper()->getProductMatrixGroup();

            if (is_array($productMatrixGroup)) {
                $filterQuery .= ' AND (';
                $filterQueryParts = array();
                foreach($productMatrixGroup as $singleValue) {
                    $filterQueryParts[] = Snowflake_Matrix_Helper_Data::PROVET_MATRIX_GROUP . '_s:' . $singleValue;
                }
                $filterQuery .= implode(' OR ', $filterQueryParts);
                $filterQuery .= ')';
            } else {
                $filterQuery .= ' AND ' . Snowflake_Matrix_Helper_Data::PROVET_MATRIX_GROUP . '_s:' . $value;
            }
        }

        return $filterQuery;
    }

    /**
     * @param $storeId
     * @return array
     */
    protected function _getParams($storeId)
    {
        $params = array(
            'fq' =>  $this->_getFilterQuery($storeId),
            'df' => 'text_autocomplete',
            'facet' => 'true',
            'facet.field' => 'text_autocomplete',
            'facet.sort' => 'count',
            'facet.limit' => intval(Mage::getStoreConfig('integernet_solr/autosuggest/max_number_searchword_suggestions')),
            'f.text_autocomplete.facet.prefix' => strtolower($this->_getQueryText()),
        );

        return $params;
    }
}