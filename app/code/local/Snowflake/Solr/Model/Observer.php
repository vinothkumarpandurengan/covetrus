<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 10.12.2014
 * Time: 14:42
 */

class Snowflake_Solr_Model_Observer
{
    protected $_productMatrixGroup = null;

    /**
     * Add 'provet_warenbezug' to product collection for Solr
     *
     * @param Varien_Event_Observer $observer
     */
    public function integernetSolrProductCollectionLoadBefore($observer)
    {
        /*$productCollection = $observer->getCollection();
        $productCollection
            ->addAttributeToSelect('provet_sort')
            ->addAttributeToSelect('provet_warenbezug');*/
    }

    /**
     * Add 'provet_matrix_group' to product data for Solr
     *
     * @param Varien_Event_Observer $observer
     */
    public function integernetSolrGetProductData($observer)
    {
        $product = $observer->getProduct();
        $productData = $observer->getProductData();

        $productData->setData('provet_sort_s', $product->getProvetSort());
        $productData->setData('provet_warenbezug_i', $product->getData('provet_warenbezug'));
    }

    public function integernetSolrBeforeSearchRequest($observer)
    {
        $transport = $observer->getTransport();
        $params = $transport->getParams();

        /*if (Mage::getStoreConfig('snowflake_matrix/settings/active') && Mage::getSingleton('customer/session')->isLoggedIn())
        {
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            $isCustomerLimited = $customer->getData('provet_warenbezug');

            if(!$isCustomerLimited){
                $params['fq'] .= ' AND provet_warenbezug_i:0';
            }
        }*/
        //$params['sort'] = 'provet_sort_s asc, ' . Mage::app()->getStore()->getConfig(Mage_Catalog_Model_Config::XML_PATH_LIST_DEFAULT_SORT_BY) . '_s asc';
        
        $params['sort'] = Mage::app()->getStore()->getConfig(Mage_Catalog_Model_Config::XML_PATH_LIST_DEFAULT_SORT_BY) . '_s asc';
        
        $transport->setParams($params);
    }
}