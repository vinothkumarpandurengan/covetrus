<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 15.12.2014
 * Time: 13:30
 */

class Snowflake_Breadcrumbs_Model_Observer
{
    /**
     * Retrieve current layout object
     *
     * @return Mage_Core_Model_Layout
     */
    public function getLayout()
    {
        return Mage::getSingleton('core/layout');
    }

    public function breadcrumbsRenderLayout(Varien_Event_Observer $observer)
    {
        $request = Mage::app()->getRequest();

        $controllerName = $request->getControllerName();
        $actionName     = $request->getActionName();
        $moduleName     = $request->getModuleName();

        /** @var $breadcrumbsBlock Mage_Page_Block_Html_Breadcrumbs */
        if ($breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs')) {
            $breadcrumbsBlock->addCrumb('home', array(
                'label' => Mage::helper('catalog')->__('Home'),
                'title' => Mage::helper('catalog')->__('Home'),
                'link'  => Mage::getUrl('/')
            ));

            if (
                $moduleName == 'checkout' && ($controllerName == 'onepage' || $controllerName == 'cart')
            ) {
                $breadcrumbsBlock->addCrumb('breadcrumb_checkout', array(
                    'label' => Mage::helper('checkout')->__('Checkout'),
                    'title' => Mage::helper('checkout')->__('Checkout'),
                    'link'  => Mage::getUrl('checkout/cart')
                ));
            }

            if (
                $moduleName == 'customer' || $moduleName == 'sales'
            ) {
                $breadcrumbsBlock->addCrumb('breadcrumb_account', array(
                    'label' => Mage::helper('customer')->__('My Account'),
                    'title' => Mage::helper('customer')->__('My Account'),
                    'link'  => Mage::getUrl('customer/account/')
                ));
            }

            if (
                $moduleName == 'customer' && $controllerName == 'account' && $actionName == 'edit'
            ) {
                $breadcrumbsBlock->addCrumb('breadcrumb_account_edit', array(
                    'label' => Mage::helper('customer')->__('Account Information'),
                    'title' => Mage::helper('customer')->__('Account Information'),
                    'link'  => Mage::getUrl('customer/account/edit')
                ));
            }

            if (
                $moduleName == 'customer' && $controllerName == 'address'
            ) {
                $breadcrumbsBlock->addCrumb('breadcrumb_address', array(
                    'label' => Mage::helper('customer')->__('Address Book'),
                    'title' => Mage::helper('customer')->__('Address Book'),
                    'link'  => Mage::getUrl('customer/address')
                ));
            }

            if (
                $moduleName == 'sales' && $controllerName == 'order' && ($actionName == 'history' || $actionName == 'view')
            ) {
                $breadcrumbsBlock->addCrumb('breadcrumb_sales_order', array(
                    'label' => Mage::helper('customer')->__('My Orders'),
                    'title' => Mage::helper('customer')->__('My Orders'),
                    'link'  => Mage::getUrl('sales/order/history')
                ));
            }

            if (
                $moduleName == 'wishlist'
            ) {
                $breadcrumbsBlock->addCrumb('breadcrumb_wishlist', array(
                    'label' => Mage::helper('wishlist')->__('My Wishlist'),
                    'title' => Mage::helper('wishlist')->__('My Wishlist'),
                    'link'  => Mage::getUrl('wishlist')
                ));
            }

            if (
                $moduleName == 'newsletter' && $controllerName == 'manage'
            ) {
                $breadcrumbsBlock->addCrumb('breadcrumb_newsletter', array(
                    'label' => Mage::helper('newsletter')->__('Newsletter Abonnements'),
                    'title' => Mage::helper('newsletter')->__('Newsletter Abonnements'),
                    'link'  => Mage::getUrl('newsletter')
                ));
            }
        }
    }
}