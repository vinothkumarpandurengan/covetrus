<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 15.09.14
 * Time: 15:08
 */
class Snowflake_CustomerAccountLinks_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * @param mixed $store
     * @return array
     */
    public function getNavigationLinksToRemove($store = null)
    {
        $items = Mage::getStoreConfig('customer/myaccount/items', $store);
        return explode(',', $items);
    }
}