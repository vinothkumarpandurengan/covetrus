<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 15.09.14
 * Time: 15:14
 */
class Snowflake_CustomerAccountLinks_Block_Navigation extends Mage_Customer_Block_Account_Navigation
{
    /**
     * Get all enabled links for my account menu
     *
     * @return array
     */
    public function getLinks(){

        $_links       = $this->_links;
        $_tempLinks   = array();
        $this->_links = array();

        foreach($_links as $_link) {
            $_linkName = $this->_getFormattedName($_link->getName());
            $_tempLinks[Mage::getStoreConfig('customer/myaccount/position_'.$_linkName)] = $_link;
        }

        // My account link sort order
        ksort($_tempLinks);
        foreach ($_tempLinks as $key => $_link) {
            $_linkName = $_link->getName();

            $this->addLink($_linkName, $_link->getPath(), $_link->getLabel());
        }

        return $this->_links;
    }

    /**
     * @return $this
     */
    public function removeLink()
    {
        foreach (Mage::helper('snowflake_customeraccountlinks')->getNavigationLinksToRemove() as $link) {
            unset($this->_links[$link]);
        }

        return $this;
    }

    /**
     * Get formatted link name
     *
     * @return string
     */
    protected function _getFormattedName($_linkName) {
        return strtolower(str_replace(' ', '_', $_linkName));
    }

    /**
     * @return mixed
     */
    protected function _toHtml()
    {
        $this->removeLink();
        return parent::_toHtml();
    }
}