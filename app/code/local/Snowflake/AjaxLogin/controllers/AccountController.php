<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 15.01.2015
 * Time: 12:40
 */
require_once Mage::getModuleDir('controllers', 'Snowflake_Customer').DS.'AccountController.php';

class Snowflake_AjaxLogin_AccountController extends Snowflake_Customer_AccountController {

    private $_url;

    public function preDispatch()
    {
        $this->_url = Mage::getBaseUrl().'?login';
        parent::preDispatch();
    }

    public function loginAction()
    {
        $this->setLocation();
    }

    private function setLocation()
    {
        Mage::app()->getFrontController()->getResponse()->setRedirect($this->_url);
    }
}