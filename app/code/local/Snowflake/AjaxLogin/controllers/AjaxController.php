<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 15.01.2015
 * Time: 15:25
 */
require_once Mage::getModuleDir('controllers', 'Magegiant_Onestepcheckout').DS.'AjaxController.php';

class Snowflake_AjaxLogin_AjaxController extends Magegiant_Onestepcheckout_AjaxController
{

    /**
     * action for customer login
     */
    public function customerLoginAction()
    {
        /*
        if ($this->_expireAjax()) {
            return;
        }
        */
        $customerSession = Mage::getSingleton('customer/session');
        $result          = array(
            'success'  => true,
            'messages' => array()
        ); 
        if (!$customerSession->isLoggedIn()) {
            $login = $this->getRequest()->getPost('login');
            if (!empty($login['username']) && !empty($login['password'])) {
                try {
                    $customerSession->login($login['username'], $login['password']);
                } catch (Mage_Core_Exception $e) {
                    switch ($e->getCode()) {
                        case Mage_Customer_Model_Customer::EXCEPTION_EMAIL_NOT_CONFIRMED:
                            $value   = Mage::helper('customer')->getEmailConfirmationUrl($login['username']);
                            $message = $this->__('This account is not confirmed. <a href="%s">Click here</a> to resend confirmation email.', $value);
                            break;
                        case Mage_Customer_Model_Customer::EXCEPTION_INVALID_EMAIL_OR_PASSWORD:
                            $message = $e->getMessage();
                            break;
                        default:
                            $message = $e->getMessage();
                    }
                    $result['success']    = false;
                    $result['messages'][] = $message;
                    $customerSession->setUsername($login['username']);
                } catch (Exception $e) {
                    $result['success']    = false;
                    $result['messages'][] = $this->__("Oops something's wrong: %s", $e->getMessage());
                    //TODO: think about redirect to login page
                }
            } else {
                $result['success']    = false;
                $result['messages'][] = $this->__('Login and password are required.');
            }
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }


    /**
     * action for customer forgot password
     */
    public function customerForgotPasswordAction()
    {
        $customerSession = Mage::getSingleton('customer/session');
        $result          = array(
            'success'  => true,
            'messages' => array()
        );

        $email = (string) $this->getRequest()->getPost('email');
        if ($email) {

            /** @var $customer Diglin_Username_Model_Customer */
            $customer = Mage::getModel('customer/customer')
                ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                ->loadByUsername($email);

            if (!$customer->getId() && !Zend_Validate::is($email, 'EmailAddress')) {
                $customerSession->setForgottenEmail($email);
                $result['success']    = false;
                $result['messages'][] = $this->__('Invalid email address or username.');
            } else if (!$customer->getId()) {
                // Load by Email if username not found and email seems to be valid
                $customer
                    ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                    ->loadByEmail($email);
            }

            if ($customer->getId()) {
                try {
                    Mage::helper('onestepcheckout/customer')->sendForgotPasswordForCustomer($customer);
                    $customerSession->addSuccess($this->__('You will receive an email with a link to reset your password.'));
                    $result['redirect_to'] = Mage::getBaseUrl();
                } catch (Exception $exception) {
                    $result['success']    = false;
                    $result['messages'][] = $exception->getMessage();
                }
            }
        } else {
            $result['success']    = false;
            $result['messages'][] = $this->__('Please enter your email or username.');
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }
}