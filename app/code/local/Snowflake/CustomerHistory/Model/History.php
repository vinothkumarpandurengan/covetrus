<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 08.04.2015
 * Time: 14:25
 */
class Snowflake_CustomerHistory_Model_History extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        $this->_init('snowflake_customerhistory/history');
    }
}
  