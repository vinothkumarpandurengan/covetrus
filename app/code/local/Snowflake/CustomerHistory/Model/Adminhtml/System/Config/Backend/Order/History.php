<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 08.04.2015
 * Time: 11:35
 */
class Snowflake_CustomerHistory_Model_Adminhtml_System_Config_Backend_Order_History extends Mage_Core_Model_Config_Data
{
    public function _afterSave()
    {
        Mage::getResourceModel('snowflake_customerhistory/history')->uploadAndImport($this);
    }
}
  