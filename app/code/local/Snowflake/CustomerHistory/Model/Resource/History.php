<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 08.04.2015
 * Time: 14:25
 */
class Snowflake_CustomerHistory_Model_Resource_History extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('snowflake_customerhistory/history', 'history_id');
    }

    public function uploadAndImport(Varien_Object $object)
    {
        $csvFile = $_FILES['groups']['tmp_name']['general']['fields']['import']['value'];

        if (!empty($csvFile)) {
            $csv   = trim(file_get_contents($csvFile));
            $table = Mage::getSingleton('core/resource')->getTableName('snowflake_customerhistory/history');

            if (!empty($csv)) {
                $exceptions = array();

                $csvLines = explode("\n", $csv);
                $csvLine = array_shift($csvLines);
                $csvLine = $this->_getCsvValues($csvLine);
                if (count($csvLine) < 2) {
                    $exceptions[0] = Mage::helper('snowflake_customerhistory')->__('Invalid csv seperator. Only ";" allowed.');
                }

                $customerCodes = array();
                foreach ($csvLines as $key => $csvLine) {
                    $csvLine = $this->_getCsvValues($csvLine);
                    if ($csvLine[0] != '') {
                        $customerCodes[] = $csvLine[0];
                    } else {
                        $exceptions[] = Mage::helper('snowflake_customerhistory')->__('Customer Number in the Row #%s is empty', ($key + 1));
                    }
                }

                if (empty($exceptions)) {
                    $data = array();
                    $customers = array();
                    $customerCodesToIds = array();

                    $customerCollection = Mage::getModel('customer/customer')
                        ->getCollection()
                        ->addFieldToFilter('customer_id', $customerCodes);

                    foreach ($customerCollection as $customer) {
                        if ($customer->getId()) {
                            $customers[] = $customer->getId();
                            $customerCodesToIds[$customer->getCustomerId()] = $customer->getId();
                        }
                    }

                    $historyCollection = Mage::getModel('snowflake_customerhistory/history')->getCollection();

                    foreach ($csvLines as $key => $csvLine) {
                        $csvLine    = $this->_getCsvValues($csvLine);
                        $customerId = $customerCodesToIds[$csvLine[0]];

                        for ($i = 1; $i < count($csvLine); $i++) {
                            if (empty($customerCodesToIds) || !array_key_exists($csvLine[0], $customerCodesToIds)) {
                                if ($csvLine[0] != '') {
                                    $exceptions[0] = Mage::helper('snowflake_customerhistory')->__('Invalid Customer Number "%s" in the Row #%s', $csvLine[0], ($key + 1));
                                }
                            } else {
                                if ($csvLine[$i] != '') {
                                    if ($productId = Mage::getModel('catalog/product')->getIdBySku($csvLine[$i])) {
                                        $historyCollection
                                            ->addFieldToFilter('customer_id', $customerId)
                                            ->addFieldToFilter('product_id', $productId)
                                            ->getData();

                                        // only new data imported
                                        if (count($historyCollection) == 0) {
                                            $data[] = array('customer_id' => $customerId, 'product_id' => $productId);
                                        }

                                    } else {
                                        continue;
                                        //$exceptions[0] = Mage::helper('snowflake_customerhistory')->__('Invalid Product SKU "%s" in the Row #%s', $csvLine[$i], ($key + 1));
                                    }
                                }
                            }
                        }
                    }
                }

                if (empty($exceptions)) {
                    $connection = $this->_getWriteAdapter();

                    /*
                    if (Mage::getStoreConfig('snowflake_customerhistory/general/import_behavior') == 'delete') {
                        $connection->delete($table, 1);
                    }
*/
                    foreach($data as $key => $dataLine) {
                        try {
                            $connection->insert($table, $dataLine);
                        } catch (Exception $e) {
                            $exceptions[] = Mage::helper('snowflake_customerhistory')->__('An error has occurred during import');
                        }
                    }
                }
                if (!empty($exceptions)) {
                    throw new Exception( "\n" . implode("\n", $exceptions) );
                } else {
                    Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('snowflake_customerhistory')->__('Data has been imported successfully'));
                }
            }
        }
    }

    private function _getCsvValues($string, $separator = ';')
    {
        $elements = explode($separator, trim($string));
        for ($i = 0; $i < count($elements); $i++) {
            $nquotes = substr_count($elements[$i], '"');
            if ($nquotes %2 == 1) {
                for ($j = $i+1; $j < count($elements); $j++) {
                    if (substr_count($elements[$j], '"') > 0) {
                        // Put the quoted string's pieces back together again
                        array_splice($elements, $i, $j-$i+1, implode($separator, array_slice($elements, $i, $j-$i+1)));
                        break;
                    }
                }
            }
            if ($nquotes > 0) {
                // Remove first and last quotes, then merge pairs of quotes
                $qstr =& $elements[$i];
                $qstr = substr_replace($qstr, '', strpos($qstr, '"'), 1);
                $qstr = substr_replace($qstr, '', strrpos($qstr, '"'), 1);
                $qstr = str_replace('""', '"', $qstr);
            }
            $elements[$i] = trim($elements[$i]);
        }
        return $elements;
    }
}
  