<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 08.04.2015
 * Time: 10:49
 */
require_once Mage::getModuleDir('controllers', 'Mage_Adminhtml') . DS . 'CustomerController.php';
class Snowflake_CustomerHistory_Adminhtml_CustomerController extends Mage_Adminhtml_CustomerController
{
    /**
     * Customer order history grid
     *
     */
    public function historyAction()
    {
        $this->_initCustomer();
        $this->loadLayout();
        $this->renderLayout();
    }
}