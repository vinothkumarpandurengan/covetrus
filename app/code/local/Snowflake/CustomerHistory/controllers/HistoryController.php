<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 09.04.2015
 * Time: 13:31
 */
class Snowflake_CustomerHistory_HistoryController extends Mage_Core_Controller_Front_Action
{

    /**
     * Display customer wishlist
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');
        $this->getLayout()->getBlock('head')->setTitle($this->__('My Items'));
        $this->renderLayout();
    }
}
  