<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 08.04.2015
 * Time: 10:32
 */
class Snowflake_CustomerHistory_Block_Adminhtml_Customer_Edit_Tab_History extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Set grid params
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('customer_history_grid');
        $this->setDefaultSort('history_id', 'desc');
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        $orderHistory = Mage::getSingleton('core/resource')->getTableName('snowflake_customerhistory/history');

        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('name');

        $collection
            ->getSelect()
            ->join($orderHistory, 'e.entity_id = ' . $orderHistory . '.product_id  AND ' . $orderHistory . '.customer_id = ' . Mage::registry('current_customer')->getId());

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('history_id', array(
            'header'    => Mage::helper('customer')->__('History #'),
            'index'     => 'history_id',
            'width'     => '100px',
        ));

        $this->addColumn('name', array(
            'header'    => Mage::helper('review')->__('Product Name'),
            'align'     =>'left',
            'type'      => 'text',
            'index'     => 'name',
            'escape'    => true
        ));

        $this->addColumn('sku', array(
            'header'    => Mage::helper('review')->__('Product SKU'),
            'align'     => 'right',
            'type'      => 'text',
            'width'     => '50px',
            'index'     => 'sku',
            'escape'    => true
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/history', array('_current' => true));
    }
}
  