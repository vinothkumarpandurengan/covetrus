<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 08.04.2015
 * Time: 09:44
 */
class Snowflake_CustomerHistory_Block_Adminhtml_Customer_Edit_Tab
    extends Mage_Adminhtml_Block_Widget
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    public function getTabLabel()
    {
        return $this->__('Most Frequently Ordered Items');
    }

    public function getTabTitle()
    {
        return $this->__('Most Frequently Ordered Items');
    }

    public function canShowTab()
    {
        if (Mage::getStoreConfig('snowflake_customerhistory/general/enabled')) {
            $customer = Mage::registry('current_customer');
            return (bool) $customer->getId();
        }
        return false;
    }

    public function isHidden()
    {
        return false;
    }

    public function getTabUrl()
    {
        return $this->getUrl('*/*/history', array('_current' => true));
    }

    public function getTabClass()
    {
        return 'ajax';
    }

    public function getAfter()
    {
        return 'addresses';
    }
}
  