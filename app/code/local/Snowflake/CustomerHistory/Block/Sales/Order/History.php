<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 23.04.2015
 * Time: 16:59
 */ 
class Snowflake_CustomerHistory_Block_Sales_Order_History extends Mage_Sales_Block_Order_History
{
    protected function _prepareLayout()
    {
        Mage_Core_Block_Template::_prepareLayout();

        $pager = $this->getLayout()->createBlock('page/html_pager', 'sales.order.history.pager');
        $pager->setTemplate('page/html/myaccount/pager.phtml');
        $pager->setAvailableLimit(array(50=>50,100=>100,150=>150));
        $pager->setCollection($this->getOrders());

        $this->setChild('pager', $pager);
        $this->getOrders()->load();
        return $this;
    }
}