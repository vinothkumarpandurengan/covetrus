<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 09.04.2015
 * Time: 13:49
 */
class Snowflake_CustomerHistory_Block_Order_History extends Mage_Core_Block_Template
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('snowflake/customer/history.phtml');

        $histories = Mage::getModel('snowflake_customerhistory/history')
            ->getCollection()
            ->addFieldToSelect('history_id')
            ->addFieldToSelect('product_id')
            ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId());

        $this->setHistories($histories);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $pager = $this->getLayout()->createBlock('page/html_pager', 'customer.order.history.pager');
        $pager->setTemplate('page/html/myaccount/pager.phtml');
        $pager->setAvailableLimit(array(50=>50,100=>100,150=>150));
        $pager->setCollection($this->getHistories());
        
        $this->setChild('pager', $pager);
        $this->getHistories()->load();

        return $this;
    }

    protected function _getProduct($productId)
    {
        return Mage::getModel('catalog/product')->load($productId);
    }

    public function getProductName($productId)
    {
        return $this->_getProduct($productId)->getName();
    }

    public function getProductSku($productId)
    {
        return $this->_getProduct($productId)->getSku();
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getViewUrl($productId)
    {
        return $this->_getProduct($productId)->getProductUrl();
    }

    public function getBackUrl()
    {
        return $this->getUrl('customer/account/');
    }
}
  