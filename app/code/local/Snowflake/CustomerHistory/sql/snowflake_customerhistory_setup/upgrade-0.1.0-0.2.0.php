<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 08.04.2015
 * Time: 13:07
 */

/* @var $installer Snowflake_Customer_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$this->run("
  DROP TABLE IF EXISTS {$this->getTable('snowflake_customerhistory/history')};
");

$table = $installer->getConnection()
    ->newTable($installer->getTable('snowflake_customerhistory/history'))
    ->addColumn('history_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'History Id')
    ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
    ), 'Customer Id')
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
    ), 'Product id')
    ->addForeignKey(
        'product_id',
        'product_id', $installer->getTable('catalog_product_entity'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Customer Order History');
$installer->getConnection()->createTable($table);

$installer->endSetup();