<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 10.12.2014
 * Time: 15:44
 */

class Snowflake_ReplacementItems_Model_Catalog_Product extends Mage_Catalog_Model_Product
{
    /**
     * Retrieve array of replacement products
     *
     * @return array
     */
    public function getReplacementProducts()
    {
        if (!$this->hasReplacementProducts()) {
            $products = array();
            $collection = $this->getReplacementProductCollection();
            foreach ($collection as $product) {
                $products[] = $product;
            }
            $this->setReplacementProducts($products);
        }
        return $this->getData('replacement_products');
    }

    /**
     * Retrieve replacement products identifiers
     *
     * @return array
     */
    public function getReplacementProductIds()
    {
        if (!$this->hasReplacementProductIds()) {
            $ids = array();
            foreach ($this->getReplacementProducts() as $product) {
                $ids[] = $product->getId();
            }
            $this->setReplacementProductIds($ids);
        }
        return $this->getData('replacement_product_ids');
    }

    /**
     * Retrieve collection replacement product
     *
     * @return Mage_Catalog_Model_Resource_Product_Link_Product_Collection
     */
    public function getReplacementProductCollection()
    {
        $collection = $this->getLinkInstance()->useReplacementLinks()
            ->getProductCollection()
            ->setIsStrongMode();
        $collection->setProduct($this);
        return $collection;
    }

    /**
     * Retrieve collection replacement link
     *
     * @return Mage_Catalog_Model_Resource_Product_Link_Collection
     */
    public function getReplacementLinkCollection()
    {
        $collection = $this->getLinkInstance()->useRelatedLinks()
            ->getLinkCollection();
        $collection->setProduct($this);
        $collection->addLinkTypeIdFilter();
        $collection->addProductIdFilter();
        $collection->joinAttributes();
        return $collection;
    }
}
  