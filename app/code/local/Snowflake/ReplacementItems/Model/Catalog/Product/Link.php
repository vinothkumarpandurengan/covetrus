<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 10.12.2014
 * Time: 15:09
 */

class Snowflake_ReplacementItems_Model_Catalog_Product_Link extends Mage_Catalog_Model_Product_Link
{
    const LINK_TYPE_REPLACEMENT = 6;

    /**
     * @return Mage_Catalog_Model_Product_Link
     */
    public function useReplacementLinks()
    {
        $this->setLinkTypeId(self::LINK_TYPE_REPLACEMENT);
        return $this;
    }

    /**
     * Save data for product relations
     *
     * @param Mage_Catalog_Model_Product $product
     * @return Mage_Catalog_Model_Product_Link
     */
    public function saveProductRelations($product)
    {
        parent::saveProductRelations($product);
        $data = $product->getReplacementLinkData();
        if (!is_null($data)) {
            $this->_getResource()->saveProductLinks($product, $data, self::LINK_TYPE_REPLACEMENT);
        }
    }
}
  