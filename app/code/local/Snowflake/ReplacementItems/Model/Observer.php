<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 10.12.2014
 * Time: 14:42
 */

class Snowflake_ReplacementItems_Model_Observer
{
    public function catalogProductPrepareSave($observer)
    {
        $event = $observer->getEvent();

        $product = $event->getProduct();
        $request = $event->getRequest();

        $links = $request->getPost('links');
        if (isset($links['replacement']) && !$product->getReplacementReadonly()) {
            $product->setReplacementLinkData(Mage::helper('adminhtml/js')->decodeGridSerializedInput($links['replacement']));
        }
    }

    public function catalogModelProductDuplicate($observer)
    {
        $event = $observer->getEvent();

        $currentProduct = $event->getCurrentProduct();
        $newProduct = $event->getNewProduct();

        $data = array();
        $currentProduct->getLinkInstance()->useReplacementLinks();
        $attributes = array();
        foreach ($currentProduct->getLinkInstance()->getAttributes() as $_attribute) {
            if (isset($_attribute['code'])) {
                $attributes[] = $_attribute['code'];
            }
        }
        foreach ($currentProduct->getReplacementLinkCollection() as $_link) {
            $data[$_link->getLinkedProductId()] = $_link->toArray($attributes);
        }
        $newProduct->setReplacementLinkData($data);
    }
}