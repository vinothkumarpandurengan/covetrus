<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 10.12.2014
 * Time: 14:33
 */

class Snowflake_ReplacementItems_Block_Adminhtml_Catalog_Product_Edit_Tab
    extends Mage_Adminhtml_Block_Widget
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    public function getTabLabel()
    {
        return $this->__('Replacement Items');
    }

    public function getTabTitle()
    {
        return $this->__('Replacement Items');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }

    public function getTabUrl()
    {
        return $this->getUrl('*/*/replacement', array('_current' => true));
    }

    public function getTabClass()
    {
        return 'ajax';
    }
}
  