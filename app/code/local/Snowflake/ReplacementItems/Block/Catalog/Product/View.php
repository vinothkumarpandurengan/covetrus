<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 11.12.2014
 * Time: 10:09
 */ 
class Snowflake_ReplacementItems_Block_Catalog_Product_View extends Mage_Catalog_Block_Product_View
{

    /**
     * Check if product has replacement items
     *
     * @return boolean
     */
    public function hasReplacementItems()
    {
        $replacement = false;
        foreach (Mage::registry('current_product')->getReplacementProductIds() as $id) {
            $replacement = true;
        }

        return $replacement;
    }
}