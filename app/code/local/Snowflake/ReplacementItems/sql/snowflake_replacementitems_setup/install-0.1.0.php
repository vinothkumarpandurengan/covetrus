<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 10.12.2014
 * Time: 15:05
 */

$installer = $this;
/**
 * Install product link types
 */
$data = array(
    array(
        'link_type_id' => Snowflake_ReplacementItems_Model_Catalog_Product_Link::LINK_TYPE_REPLACEMENT,
        'code' => 'replacement'
    )
);
foreach ($data as $bind) {
    $installer->getConnection()->insertForce($installer->getTable('catalog/product_link_type'), $bind);
}
/**
 * install product link attributes
 */
$data = array(
    array(
        'link_type_id' => Snowflake_ReplacementItems_Model_Catalog_Product_Link::LINK_TYPE_REPLACEMENT,
        'product_link_attribute_code' => 'position',
        'data_type' => 'int'
    )
);
$installer->getConnection()->insertMultiple($installer->getTable('catalog/product_link_attribute'), $data);