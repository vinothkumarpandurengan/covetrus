<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 10.12.2014
 * Time: 14:26
 */

require_once(Mage::getModuleDir('controllers','Mage_Adminhtml').DS.'Catalog'.DS.'ProductController.php');
class Snowflake_ReplacementItems_Adminhtml_Catalog_ProductController extends Mage_Adminhtml_Catalog_ProductController
{

    /**
     * Get replacement products grid and serializer block
     */
    public function replacementAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.edit.tab.replacement')
            ->setProductsReplacement($this->getRequest()->getPost('products_replacement', null));
        $this->renderLayout();
    }

    /**
     * Get replacement products grid
     */
    public function replacementGridAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.edit.tab.replacement')
            ->setProductsRelated($this->getRequest()->getPost('products_replacement', null));
        $this->renderLayout();
    }
}