<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 10.02.2015
 * Time: 17:02
 */

$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_category', 'snowflake_category_quickaccess', array(
    'group'         => 'General Information',
    'label'         => 'Show for quickaccess',
    'input'         => 'select',
    'type'          => 'int',
    'source'        => 'eav/entity_attribute_source_boolean',
    'user_defined'  => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'required'      => 0,
    'visible'       => 1,
    'default'       => 0,
    'sort_order'    => 60,
));

$installer->endSetup();