<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 10.02.2015
 * Time: 17:02
 */

$installer = $this;

$installer->startSetup();

$installer->updateAttribute('catalog_category', 'snowflake_category_quickaccess', 'frontend_label', 'Include in Quick-Access Menu');

$installer->endSetup();