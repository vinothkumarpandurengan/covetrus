<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 11.02.2015
 * Time: 17:18
 */
require_once Mage::getModuleDir('controllers', 'Mage_Adminhtml').DS.'Cms'.DS.'Wysiwyg'.DS.'ImagesController.php';

class Snowflake_QuickAccess_Adminhtml_Cms_Wysiwyg_ImagesController extends Mage_Adminhtml_Cms_Wysiwyg_ImagesController
{

    public function indexAction()
    {
        if ($widget = $this->getRequest()->getParam('snowflake_widget'))
        {
            $this->_getSession()->setData('snowflake_widget', 1);
        } else {
            $this->_getSession()->unsetData('snowflake_widget');
        }

        parent::indexAction();
    }

    /**
     * Fire when select image.
     *
     * @return void
     */
    public function onInsertAction()
    {
        if ($this->_getSession()->getData('snowflake_widget'))
        {
            $this->_getSession()->unsetData('snowflake_widget');

            $helper = Mage::helper('cms/wysiwyg_images');
            $storeId = $this->getRequest()->getParam('store');

            $filename = $this->getRequest()->getParam('filename');
            $filename = $helper->idDecode($filename);

            Mage::helper('catalog')->setStoreId($storeId);
            $helper->setStoreId($storeId);

            $fileUrl = $helper->getCurrentUrl() . $filename;
            $mediaPath = str_replace(Mage::getBaseUrl('media'), '', $fileUrl);

            $this->getResponse()->setBody($mediaPath);
        } else {
            parent::onInsertAction();
        }
    }
}
  