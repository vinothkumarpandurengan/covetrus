<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 12.02.2015
 * Time: 08:58
 */
class Snowflake_QuickAccess_Block_Widget_Block extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{

    protected function _toHtml()
    {
        $imgUrl = $this->getData('image');

        if( !preg_match("/^http\:\/\/|https\:\/\//", $imgUrl) ) {
            $imgUrl = Mage::getBaseUrl('media') . $imgUrl;
        }

        $contact = array(
            'image'  => $imgUrl,
            'name'   => $this->getData('name'),
            'email'  => $this->getData('email'),
            'street' => $this->getData('street'),
            'zip'    => $this->getData('zip'),
            'city'   => $this->getData('city'),
            'phone'  => $this->getData('phone'),
            'fax'    => $this->getData('fax'),
        );

        $this->assign('contact', $contact);
        return parent::_toHtml();
    }
}