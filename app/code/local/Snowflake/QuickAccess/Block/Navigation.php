<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 10.02.2015
 * Time: 14:10
 */
class Snowflake_QuickAccess_Block_Navigation extends Mage_Catalog_Block_Navigation
{
    public function getQuickAccessCategories()
    {
        $collection = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToFilter('is_active', 1)
            ->addAttributeToFilter('snowflake_category_quickaccess', 1)
            ->addAttributeToSelect('name')
            ->setCurPage(1);

        return $collection;
    }
}
  