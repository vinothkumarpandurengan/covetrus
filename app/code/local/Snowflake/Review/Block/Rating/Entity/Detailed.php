<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 17.04.2015
 * Time: 12:46
 */ 
class Snowflake_Review_Block_Rating_Entity_Detailed extends Mage_Rating_Block_Entity_Detailed
{
    protected function _getReviewPage()
    {
        $request = $this->getRequest();

        $controllerName = $request->getControllerName();
        $actionName     = $request->getActionName();
        $moduleName     = $request->getModuleName();

        if (
            $moduleName == 'review' && $controllerName == 'product' && $actionName == 'list'
        ) {
            return true;
        }

        return false;
    }

    protected function _toHtml()
    {
        $entityId = Mage::app()->getRequest()->getParam('id');
        if (intval($entityId) <= 0) {
            return '';
        }

        $reviewsCount = Mage::getModel('review/review')
            ->getTotalReviews($entityId, true);
        if ($reviewsCount == 0 && !$this->_getReviewPage()) {
            #return Mage::helper('rating')->__('Be the first to review this product');
            $this->setTemplate('rating/empty.phtml');
            return $this->_getTemplate();
        }

        $ratingCollection = Mage::getModel('rating/rating')
            ->getResourceCollection()
            ->addEntityFilter('product') # TOFIX
            ->setPositionOrder()
            ->setStoreFilter(Mage::app()->getStore()->getId())
            ->addRatingPerStoreName(Mage::app()->getStore()->getId())
            ->load();

        if ($entityId) {
            $ratingCollection->addEntitySummaryToItem($entityId, Mage::app()->getStore()->getId());
        }

        $this->assign('collection', $ratingCollection);
        return $this->_getTemplate();
    }

    protected function _getTemplate()
    {
        if (!$this->getTemplate()) {
            return '';
        }
        $html = $this->renderView();
        return $html;
    }
}