<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 08.12.2014
 * Time: 14:25
 */ 
class Snowflake_AWCatalogPermissions_Model_Catalogpermissions_Observer extends AW_Catalogpermissions_Model_Observer {

    /****************************************************************************************************************/
    /**
     * Block [Block 3] of methods below observe blocks and add specific info to product data $Product->setAWDisableOutOfStock(true);
     * Event - core_block_abstract_to_html_before
     *
     * @param Varien_Event_Observer $event
     */
    public function blockAbstractToHtmlBefore($event)
    {
        if (!parent::_validateProcess()) {
            return;
        }
        if ($product = $event->getData('block')->getProduct()) {
            /** @var Mage_Catalog_Model_Product $product */
            $class = get_class($event->getData('block'));
            if ($class == 'Mage_Wishlist_Block_Render_Item_Price' || $class == 'Mage_Catalog_Block_Product_Price'
                || $class == 'Mage_Bundle_Block_Catalog_Product_Price'
                || $class == 'AW_Sarp_Block_Catalog_Product_Price'
                || $class == 'AW_Sarp_Block_Product_Price'
                || $class == 'FireGento_MageSetup_Block_Catalog_Product_Price'
            ) {
                $disabled = Mage::registry(AW_Catalogpermissions_Helper_Data::DISABLED_PRICE_PROD_SCOPE);
                $disabled || $disabled = array();
                $isDisabledPrice = true;
                if ($product->getTypeId() === Mage_Catalog_Model_Product_Type_Grouped::TYPE_CODE) {
                    $prices = array();
                    foreach ($product->getTypeInstance()->getAssociatedProducts() as $child) {
                        /** @var Mage_Catalog_Model_Product $child */
                        if (!in_array($child->getId(), $disabled)) {
                            $prices[] = $child->getFinalPrice();
                        }
                    }
                    $product->setData('minimal_price', sizeof($prices) ? min($prices) : false);
                    $isDisabledPrice = (bool)$product->getData('minimal_price');
                }
                $currentGroupId = AW_Catalogpermissions_Helper_Data::getCustomerGroup();
                $globalRestrictedGroups = AW_Catalogpermissions_Helper_Data::getHidePriceGroupConfig();
                $isDisabledPrice = (($isDisabledPrice && in_array($product->getId(), $disabled))
                    || in_array(
                        $currentGroupId, $globalRestrictedGroups
                    ));
                if ($isDisabledPrice) {
                    AW_Catalogpermissions_Helper_Data::setVisibility($product, false);
                    self::$temp[
                    "aw_catalogpermissions_block_{$product->getId()}_" . get_class($event->getData('block'))]
                        = 1;
                }
            }
        }
    }
}