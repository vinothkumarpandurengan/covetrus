<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 18.11.2014
 * Time: 13:26
 */
class Snowflake_ImportExport_Block_System_Config_Import extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $html = $this->_getButtonHtml(
            Mage::helper('adminhtml')->getUrl('adminhtml/importexport/importProducts'),
            $this->__('Import Products (Do not use, is only for testing with small arikel.csv - otherwise it will run into timeout)')
        );

        $html .= '<br /><br />';

        $html .= $this->_getButtonHtml(
            Mage::helper('adminhtml')->getUrl('adminhtml/importexport/importProductStocks'),
            $this->__('Import Product Stocks')
        );

        $html .= '<br /><br />';

        $html .= $this->_getButtonHtml(
            Mage::helper('adminhtml')->getUrl('adminhtml/importexport/importTierPrices'),
            $this->__('Import Tier Price')
        );

        $html .= '<br /><br />';

        $html .= $this->_getButtonHtml(
            Mage::helper('adminhtml')->getUrl('adminhtml/importexport/importWebGroupCodes'),
            $this->__('Import Categories (Do not use, is only for testing with small webgroupcode.csv - otherwise it will run into timeout)')
        );

        $html .= '<br /><br />';

        /*$html .= $this->_getButtonHtml(
            Mage::helper('adminhtml')->getUrl('adminhtml/importexport/importReplacementItems'),
            $this->__('Import Replacement Items')
        );*/

        $html .= $this->_getButtonHtml(
            Mage::helper('adminhtml')->getUrl('adminhtml/importexport/importCustomers'),
            $this->__('Import Customers')
        );

        $html .= '<br /><br />';


        $html .= $this->_getButtonHtml(
            Mage::helper('adminhtml')->getUrl('adminhtml/importexport/exportupload'),
            $this->__('Upload Export files to FTP Server')
        );

        $html .= '<br /><br />';

        $html .= $this->_getButtonHtml(
            Mage::helper('adminhtml')->getUrl('adminhtml/importexport/downloadfiles'),
            $this->__('Download files from FTP Server')
        );


        return $html;
    }

    /**
     * @param $url
     * @param $buttonText
     * @return string
     */
    protected function _getButtonHtml($url, $buttonText)
    {
        $html = '<button type="button" onclick="window.location.href=\'' . $url . '\'"><span><span>';
        $html .= $buttonText;
        $html .= '</span></span></button>';
        return $html;
    }
}
  