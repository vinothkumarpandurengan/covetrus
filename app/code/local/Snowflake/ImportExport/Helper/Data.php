<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 18.11.2014
 * Time: 13:00
 */
class Snowflake_ImportExport_Helper_Data extends Mage_Customer_Helper_Data
{
    /**
     * Retrieve name prefix dropdown options
     *
     * @return array|bool
     */
    public function getNameOptions($attr, $store = null)
    {
        return $this->_prepareNamePrefixSuffixOptions(
            Mage::helper('customer/address')->getConfig($attr.'_options', $store)
        );
    }

    /**
     * Send welcome mail to customer
     */
    public function sendWelcomeEmail($email)
    {
        /** @var Mage_Customer_Model_Customer $customer */
        $customer = mage::getModel('customer/customer')
            ->setWebsiteId(Mage::app()->getWebsite(true)->getId())
            ->loadByEmail($email);

        if ($customer->getId()) {
            $password = Mage::helper('core')->getRandomString($length = 12);
            $customer->setPassword($password);

            $customer->save();

            $customer->sendNewAccountEmail('registered', '', Mage::app()->getStore()->getId());
        }
    }
}
  