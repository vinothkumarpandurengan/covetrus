<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 06.03.2015
 * Time: 09:32
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$importDir = Mage::getBaseDir('var') . DS . 'import' . DS;
$installer->setConfigData('snowflake_importexport/general/import_csv_directory', $importDir);

$importDir = Mage::getBaseDir('media') . DS . 'import' . DS;
$installer->setConfigData('snowflake_importexport/general/import_pdf_directory', $importDir);
$installer->setConfigData('snowflake_importexport/general/import_images_directory', $importDir);

$installer->endSetup();