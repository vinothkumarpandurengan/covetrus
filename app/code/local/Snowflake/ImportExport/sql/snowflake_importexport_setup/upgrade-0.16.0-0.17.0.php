<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 06.03.2015
 * Time: 09:32
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_product', 'provet_commercial_form', array(
    'group'                 => 'General',
    'label'                 => 'Handelsform',
    'input'                 => 'text',
    'type'                  => 'varchar',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible_on_front'      => 1,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 0,
    'filterable'            => 0,
    'filterable_in_search'  => 0,
    'searchable'            => 0,
    'default'               => '',
    'sort_order'            => 20,
));

$installer->endSetup();