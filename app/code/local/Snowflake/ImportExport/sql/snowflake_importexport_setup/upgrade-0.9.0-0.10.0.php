<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 06.03.2015
 * Time: 09:32
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_product', 'provet_webgrpcode', array(
    'group'                 => 'General',
    'label'                 => 'Web Gruppe Code',
    'input'                 => 'multiselect',
    'type'                  => 'varchar',
    'user_defined'          => 1,
    'backend'               => 'eav/entity_attribute_backend_array',
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'required'              => 1,
    'filterable'            => 2,
    'filterable_in_search'  => 1,
    'searchable'            => 0,
    'sort_order'            => 300,
));

$installer->endSetup();