<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 06.03.2015
 * Time: 09:32
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->removeAttribute('catalog_product', 'provet_matrix_group');
$installer->addAttribute('catalog_product', 'provet_matrix_group', array(
    'group'                 => 'General',
    'label'                 => 'MatrixGrp',
    'input'                 => 'select',
    'type'                  => 'varchar',
    'user_defined'          => 1,
    'source'                => 'eav/entity_attribute_source_table',
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'required'              => 1,
    'filterable'            => 0,
    'filterable_in_search'  => 0,
    'searchable'            => 0,
    'sort_order'            => 90,
));

$installer->endSetup();