<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 06.03.2015
 * Time: 09:32
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_product', 'provet_stock', array(
    'group'                 => 'General',
    'label'                 => 'Verfügbarkeit Ampelsystem',
    'input'                 => 'select',
    'type'                  => 'int',
    'user_defined'          => 1,
    'source'                => 'snowflake_importexport/product_attribute_source_trafficlights',
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'required'              => 0,
    'filterable'            => 0,
    'filterable_in_search'  => 0,
    'searchable'            => 0,
    'sort_order'            => 4,
));

$installer->endSetup();