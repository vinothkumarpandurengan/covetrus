<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 06.03.2015
 * Time: 09:32
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_product', 'provet_expiration_date', array(
    'group'                      => 'General',
    'label'                      => 'Verfallsdatum',
    'input'                      => 'date',
    'type'                       => 'datetime',
    'backend'                    => 'eav/entity_attribute_backend_datetime',
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
    'visible'                    => 1,
    'required'                   => 0,
    'visible_on_front'           => 1,
    'filterable'                 => 0,
    'filterable_in_search'       => 0,
    'searchable'                 => 0,
    'sort_order'                 => 6,
));

$installer->endSetup();