<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 22.01.2015
 * Time: 09:41
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn(
    $this->getTable('customer/customer_group'),
    'price_group_id',
    array(
        'type'      =>  Varien_Db_Ddl_Table::TYPE_INTEGER,
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
        'comment'   => 'Navision Price Group Id'
    )
);

$installer->endSetup();