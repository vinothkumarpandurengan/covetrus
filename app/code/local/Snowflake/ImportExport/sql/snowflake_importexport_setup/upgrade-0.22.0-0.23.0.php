<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 10.02.2015
 * Time: 17:02
 */

$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_category', 'snowflake_webgroupcode', array(
    'group'         => 'General Information',
    'label'         => 'WebGroupCode is ID for Category',
    'input'         => 'text',
    'type'          => 'varchar',
    'source'        => 'eav/entity_attribute_source_boolean',
    'user_defined'  => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'required'      => 0,
    'visible'       => 1,
    'default'       => '',
    'sort_order'    => 80,
));

$installer->endSetup();