<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 06.03.2015
 * Time: 09:32
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$exportDir = Mage::getBaseDir('var') . DS . 'export' . DS;
$installer->setConfigData('snowflake_importexport/general/export_csv_directory', $exportDir);

$installer->endSetup();