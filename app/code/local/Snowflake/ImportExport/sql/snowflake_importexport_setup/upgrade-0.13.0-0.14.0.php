<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 06.03.2015
 * Time: 09:32
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->updateAttribute('catalog_category', 'available_sort_by', 'is_required', 0);
$installer->updateAttribute('catalog_category', 'default_sort_by', 'is_required', 0);

$installer->endSetup();