<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 18.11.2014
 * Time: 16:44
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_product', 'provet_net_weight', array(
    'group'                 => 'General',
    'label'                 => 'Nettogewicht',
    'input'                 => 'text',
    'type'                  => 'decimal',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 0,
    'filterable'            => 0,
    'filterable_in_search'  => 0,
    'searchable'            => 0,
    'default'               => '',
    'sort_order'            => 6,
));

$installer->addAttribute('catalog_product', 'provet_substitute', array(
    'group'                 => 'General',
    'label'                 => 'Substitute',
    'input'                 => 'text',
    'type'                  => 'varchar',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 0,
    'filterable'            => 0,
    'filterable_in_search'  => 0,
    'searchable'            => 0,
    'default'               => '',
    'sort_order'            => 50,
));

$installer->addAttribute('catalog_product', 'provet_art_no_op', array(
    'group'                 => 'General',
    'label'                 => 'ArtNrOp',
    'input'                 => 'text',
    'type'                  => 'varchar',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 0,
    'filterable'            => 0,
    'filterable_in_search'  => 0,
    'searchable'            => 0,
    'default'               => '',
    'sort_order'            => 60,
));

$installer->addAttribute('catalog_product', 'provet_own_brand', array(
    'group'                 => 'General',
    'label'                 => 'Eigenmarke',
    'input'                 => 'boolean',
    'type'                  => 'int',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 0,
    'filterable'            => 0,
    'filterable_in_search'  => 0,
    'searchable'            => 0,
    'default'               => '',
    'source'                => 'eav/entity_attribute_source_boolean',
    'sort_order'            => 70,
));

$installer->addAttribute('catalog_product', 'provet_stock_items', array(
    'group'                 => 'General',
    'label'                 => 'Lagerartikel',
    'input'                 => 'boolean',
    'type'                  => 'int',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 0,
    'filterable'            => 0,
    'filterable_in_search'  => 0,
    'searchable'            => 0,
    'default'               => '',
    'source'                => 'eav/entity_attribute_source_boolean',
    'sort_order'            => 80,
));

$installer->addAttribute('catalog_product', 'provet_matrix_group', array(
    'group'                 => 'General',
    'label'                 => 'MatrixGrp',
    'input'                 => 'text',
    'type'                  => 'varchar',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 1,
    'filterable'            => 0,
    'filterable_in_search'  => 0,
    'searchable'            => 0,
    'default'               => '',
    'sort_order'            => 90,
));

$installer->addAttribute('catalog_product', 'provet_vet_point_cat', array(
    'group'                 => 'General',
    'label'                 => 'VetPointKat',
    'input'                 => 'text',
    'type'                  => 'varchar',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 1,
    'filterable'            => 1,
    'filterable_in_search'  => 1,
    'searchable'            => 1,
    'default'               => '',
    'sort_order'            => 100,
));

$installer->addAttribute('catalog_product', 'provet_reg_no', array(
    'group'                 => 'General',
    'label'                 => 'RegNr',
    'input'                 => 'text',
    'type'                  => 'varchar',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 0,
    'filterable'            => 0,
    'filterable_in_search'  => 0,
    'searchable'            => 0,
    'default'               => '',
    'sort_order'            => 110,
));

$installer->addAttribute('catalog_product', 'provet_atc_vet', array(
    'group'                 => 'General',
    'label'                 => 'ATCVet',
    'input'                 => 'text',
    'type'                  => 'varchar',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 0,
    'filterable'            => 0,
    'filterable_in_search'  => 0,
    'searchable'            => 0,
    'default'               => '',
    'sort_order'            => 120,
));

$installer->addAttribute('catalog_product', 'provet_ref_vet_cat', array(
    'group'                 => 'General',
    'label'                 => 'RefVetKat',
    'input'                 => 'text',
    'type'                  => 'varchar',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 0,
    'filterable'            => 0,
    'filterable_in_search'  => 0,
    'searchable'            => 0,
    'default'               => '',
    'sort_order'            => 130,
));

$installer->addAttribute('catalog_product', 'provet_ref_hum_cat', array(
    'group'                 => 'General',
    'label'                 => 'RefHumKat',
    'input'                 => 'text',
    'type'                  => 'varchar',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 0,
    'filterable'            => 0,
    'filterable_in_search'  => 0,
    'searchable'            => 0,
    'default'               => '',
    'sort_order'            => 140,
));

$installer->addAttribute('catalog_product', 'provet_hills_cat', array(
    'group'                 => 'General',
    'label'                 => 'HillsKat',
    'input'                 => 'text',
    'type'                  => 'varchar',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 0,
    'filterable'            => 1,
    'filterable_in_search'  => 1,
    'searchable'            => 1,
    'default'               => '',
    'sort_order'            => 150,
));

$installer->addAttribute('catalog_product', 'provet_spezies', array(
    'group'                 => 'General',
    'label'                 => 'Spezies',
    'input'                 => 'text',
    'type'                  => 'varchar',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 0,
    'filterable'            => 1,
    'filterable_in_search'  => 1,
    'searchable'            => 1,
    'default'               => '',
    'sort_order'            => 160,
));

$installer->addAttribute('catalog_product', 'provet_size', array(
    'group'                 => 'General',
    'label'                 => 'Size',
    'input'                 => 'text',
    'type'                  => 'varchar',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 0,
    'filterable'            => 1,
    'filterable_in_search'  => 1,
    'searchable'            => 1,
    'default'               => '',
    'sort_order'            => 5,
));

$installer->addAttribute('catalog_product', 'provet_designation', array(
    'group'                 => 'General',
    'label'                 => 'Designation',
    'input'                 => 'text',
    'type'                  => 'varchar',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 0,
    'filterable'            => 1,
    'filterable_in_search'  => 1,
    'searchable'            => 1,
    'default'               => '',
    'sort_order'            => 180,
));

$installer->addAttribute('catalog_product', 'provet_variety', array(
    'group'                 => 'General',
    'label'                 => 'Variety',
    'input'                 => 'text',
    'type'                  => 'varchar',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 0,
    'filterable'            => 1,
    'filterable_in_search'  => 1,
    'searchable'            => 1,
    'default'               => '',
    'sort_order'            => 190,
));

$installer->addAttribute('catalog_product', 'provet_feedays', array(
    'group'                 => 'General',
    'label'                 => 'Feedays',
    'input'                 => 'text',
    'type'                  => 'decimal',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 0,
    'filterable'            => 1,
    'filterable_in_search'  => 1,
    'searchable'            => 1,
    'default'               => '',
    'sort_order'            => 200,
));

$installer->addAttribute('catalog_product', 'provet_hills_units', array(
    'group'                 => 'General',
    'label'                 => 'HillsEinheiten',
    'input'                 => 'text',
    'type'                  => 'decimal',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 0,
    'filterable'            => 0,
    'filterable_in_search'  => 0,
    'searchable'            => 0,
    'default'               => '',
    'sort_order'            => 210,
));

$installer->addAttribute('catalog_product', 'provet_rabgruppep', array(
    'group'                 => 'General',
    'label'                 => 'RabGruppeP',
    'input'                 => 'text',
    'type'                  => 'varchar',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 0,
    'filterable'            => 0,
    'filterable_in_search'  => 0,
    'searchable'            => 0,
    'default'               => '',
    'sort_order'            => 220,
));

$installer->addAttribute('catalog_product', 'provet_rabgruppec', array(
    'group'                 => 'General',
    'label'                 => 'RabGruppeC',
    'input'                 => 'text',
    'type'                  => 'varchar',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 0,
    'filterable'            => 0,
    'filterable_in_search'  => 0,
    'searchable'            => 0,
    'default'               => '',
    'sort_order'            => 230,
));

$installer->addAttribute('catalog_product', 'provet_rabgruppeh', array(
    'group'                 => 'General',
    'label'                 => 'RabGruppeH',
    'input'                 => 'text',
    'type'                  => 'varchar',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 0,
    'filterable'            => 0,
    'filterable_in_search'  => 0,
    'searchable'            => 0,
    'default'               => '',
    'sort_order'            => 240,
));

$installer->addAttribute('catalog_product', 'provet_in_action', array(
    'group'                 => 'General',
    'label'                 => 'InAktion',
    'input'                 => 'boolean',
    'type'                  => 'int',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 0,
    'filterable'            => 0,
    'filterable_in_search'  => 0,
    'searchable'            => 0,
    'source'                => 'eav/entity_attribute_source_boolean',
    'sort_order'            => 250,
));

$installer->addAttribute('catalog_product', 'provet_action_label', array(
    'group'                 => 'General',
    'label'                 => 'AktionBez',
    'input'                 => 'text',
    'type'                  => 'varchar',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => '',
    'required'              => 0,
    'filterable'            => 0,
    'filterable_in_search'  => 0,
    'searchable'            => 0,
    'sort_order'            => 260,
));

$installer->addAttribute('catalog_product', 'provet_vksperrep', array(
    'group'                 => 'General',
    'label'                 => 'VkSperreP',
    'input'                 => 'boolean',
    'type'                  => 'int',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 0,
    'filterable'            => 0,
    'filterable_in_search'  => 0,
    'searchable'            => 0,
    'source'                => 'eav/entity_attribute_source_boolean',
    'sort_order'            => 270,
));

$installer->addAttribute('catalog_product', 'provet_vksperrec', array(
    'group'                 => 'General',
    'label'                 => 'VkSperreC',
    'input'                 => 'boolean',
    'type'                  => 'int',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 0,
    'filterable'            => 0,
    'filterable_in_search'  => 0,
    'searchable'            => 0,
    'source'                => 'eav/entity_attribute_source_boolean',
    'sort_order'            => 280,
));

$installer->addAttribute('catalog_product', 'provet_vksperreh', array(
    'group'                 => 'General',
    'label'                 => 'VkSperreH',
    'input'                 => 'boolean',
    'type'                  => 'int',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 0,
    'filterable'            => 0,
    'filterable_in_search'  => 0,
    'searchable'            => 0,
    'source'                => 'eav/entity_attribute_source_boolean',
    'sort_order'            => 290,
));

$installer->addAttribute('catalog_product', 'provet_webgrpcode', array(
    'group'                 => 'General',
    'label'                 => 'WebGrpCode',
    'input'                 => 'text',
    'type'                  => 'varchar',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => '',
    'required'              => 1,
    'filterable'            => 0,
    'filterable_in_search'  => 0,
    'searchable'            => 0,
    'sort_order'            => 300,
));

$installer->addAttribute('catalog_product', 'provet_short_description_2', array(
    'group'                 => 'General',
    'label'                 => 'Kurzbeschrieb 2',
    'input'                 => 'textarea',
    'type'                  => 'varchar',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'               => 1,
    'default'               => '',
    'required'              => 0,
    'filterable'            => 0,
    'filterable_in_search'  => 0,
    'searchable'            => 0,
    'sort_order'            => 3,
));

$installer->updateAttribute('catalog_product', 'weight', 'frontend_label', 'Bruttogewicht');

$installer->endSetup();
  