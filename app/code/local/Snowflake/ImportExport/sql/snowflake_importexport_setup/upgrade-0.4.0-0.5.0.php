<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 02.02.2015
 * Time: 15:03
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()->dropColumn(
    $this->getTable('customer/customer_group'),
    'price_group_id'
);

$installer->endSetup();