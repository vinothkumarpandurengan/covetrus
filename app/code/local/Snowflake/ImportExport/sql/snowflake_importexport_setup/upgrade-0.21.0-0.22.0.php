<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 06.03.2015
 * Time: 09:32
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
$installer->addAttribute('catalog_product', 'provet_nettoprice', array(
    'group'                 => 'General',
    'label'                 => 'Nettopreis (Wenn "Ja" dann keine Rabattierung auf das Produkt)',
    'input'                 => 'boolean',
    'type'                  => 'int',
    'user_defined'          => 1,
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => 1,
    'default'               => 0,
    'required'              => 0,
    'filterable'            => 0,
    'filterable_in_search'  => 0,
    'searchable'            => 0,
    'default'               => '',
    'source'                => 'eav/entity_attribute_source_boolean',
    'sort_order'            => 170,
));

$installer->endSetup();