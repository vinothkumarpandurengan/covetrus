<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 05.03.2015
 * Time: 09:10
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$attributes = array(
    /* Pos. 7 */
    'provet_art_no_op' => array(
        'frontend_label'           => 'Grosspackung',
        'is_visible_on_front'      => 1,
        'is_filterable'            => 0,
        'is_filterable_in_search'  => 0,
        'is_searchable'            => 0,
    ),

    /* Pos. 8 */
    'provet_own_brand' => array(
        'frontend_label'           => 'Eigenmarke',
        'is_visible_on_front'      => 1,
        'is_filterable'            => 0,
        'is_filterable_in_search'  => 0,
        'is_searchable'            => 1,
    ),

    /* Pos. 9 */
    'provet_stock_items' => array(
        'frontend_label'           => 'Lagerartikel',
        'is_visible_on_front'      => 1,
        'is_filterable'            => 0,
        'is_filterable_in_search'  => 0,
        'is_searchable'            => 0,
    ),

    /* Pos. 10 */
    'provet_matrix_group' => array(
        'frontend_label'           => 'Matrix Gruppe',
        'is_visible_on_front'      => 0,
        'is_filterable'            => 0,
        'is_filterable_in_search'  => 0,
        'is_searchable'            => 0,
    ),

    /* Pos. 11 */
    'provet_vet_point_cat' => array(
        'frontend_label'           => 'VetPointKat',
        'is_visible_on_front'      => 1,
        'is_filterable'            => 0,
        'is_filterable_in_search'  => 0,
        'is_searchable'            => 0,
    ),

    /* Pos. 12 */
    'provet_reg_no' => array(
        'frontend_label'           => 'RegNr',
        'is_visible_on_front'      => 1,
        'is_filterable'            => 0,
        'is_filterable_in_search'  => 0,
        'is_searchable'            => 0,
    ),

    /* Pos. 13 */
    'provet_atc_vet' => array(
        'frontend_label'           => 'ATCVet',
        'is_visible_on_front'      => 1,
        'is_filterable'            => 0,
        'is_filterable_in_search'  => 0,
        'is_searchable'            => 0,
    ),

    /* Pos. 14 */
    'provet_ref_vet_cat' => array(
        'frontend_label'           => 'RefVetKat',
        'is_visible_on_front'      => 1,
        'is_filterable'            => 0,
        'is_filterable_in_search'  => 0,
        'is_searchable'            => 0,
    ),

    /* Pos. 15 */
    'provet_ref_hum_cat' => array(
        'frontend_label'           => 'RefHumKat',
        'is_visible_on_front'      => 1,
        'is_filterable'            => 0,
        'is_filterable_in_search'  => 0,
        'is_searchable'            => 0,
    ),

    /* Pos. 23 */
    'provet_rabgruppep' => array(
        'frontend_label'           => 'Rabattgruppe für Mandant P',
        'is_visible_on_front'      => 0,
        'is_filterable'            => 0,
        'is_filterable_in_search'  => 0,
        'is_searchable'            => 0,
    ),

    /* Pos. 24 */
    'provet_rabgruppec' => array(
        'frontend_label'           => 'Rabattgruppe für Mandant C',
        'is_visible_on_front'      => 0,
        'is_filterable'            => 0,
        'is_filterable_in_search'  => 0,
        'is_searchable'            => 0,
    ),

    /* Pos. 25 */
    'provet_rabgruppeh' => array(
        'frontend_label'           => 'Rabattgruppe für Mandant H',
        'is_visible_on_front'      => 0,
        'is_filterable'            => 0,
        'is_filterable_in_search'  => 0,
        'is_searchable'            => 0,
    ),

    /* Pos. 26 */
    'provet_in_action' => array(
        'frontend_label'           => 'In Aktion',
        'is_visible_on_front'      => 0,
        'is_filterable'            => 0,
        'is_filterable_in_search'  => 0,
        'is_searchable'            => 0,
    ),

    /* Pos. 27 */
    'provet_action_label' => array(
        'frontend_label'           => 'Bezeichnung Aktion',
        'is_visible_on_front'      => 1,
        'is_filterable'            => 0,
        'is_filterable_in_search'  => 0,
        'is_searchable'            => 0,
    ),

    /* Pos. 28 */
    'provet_vksperrep' => array(
        'frontend_label'           => 'Verkaufsperre für Mandant P',
        'is_visible_on_front'      => 0,
        'is_filterable'            => 0,
        'is_filterable_in_search'  => 0,
        'is_searchable'            => 0,
    ),

    /* Pos. 29 */
    'provet_vksperrec' => array(
        'frontend_label'           => 'Verkaufsperre für Mandant C',
        'is_visible_on_front'      => 0,
        'is_filterable'            => 0,
        'is_filterable_in_search'  => 0,
        'is_searchable'            => 0,
    ),

    /* Pos. 30 */
    'provet_vksperreh' => array(
        'frontend_label'           => 'Verkaufsperre für Mandant H',
        'is_visible_on_front'      => 0,
        'is_filterable'            => 0,
        'is_filterable_in_search'  => 0,
        'is_searchable'            => 0,
    ),
);

$installer->removeAttribute('catalog_product', 'provet_hills_units');
$installer->removeAttribute('catalog_product', 'provet_substitute');
$installer->removeAttribute('catalog_product', 'provet_webgrpcode');

foreach ($attributes as $attributeCode => $data) {
    foreach ($data as $key => $value) {
        $installer->updateAttribute('catalog_product', $attributeCode, $key, $value);
    }
}

$installer->endSetup();
  