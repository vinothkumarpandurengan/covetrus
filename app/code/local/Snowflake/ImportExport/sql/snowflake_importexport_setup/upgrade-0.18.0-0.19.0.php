<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 06.03.2015
 * Time: 09:32
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->updateAttribute('catalog_product', 'provet_expiration_date', 'is_user_defined', 1);
$installer->addAttribute('catalog_product', 'provet_sort', array(
    'group'                   => 'General',
    'label'                   => 'SortPrio',
    'note'                    => 'Reihenfolge: 1. Provet, 2. Andere',
    'input'                   => 'select',
    'type'                    => 'int',
    'user_defined'            => 1,
    'source'                  => 'snowflake_importexport/product_attribute_source_sortprio',
    'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'                 => 1,
    'required'                => 1,
    'filterable'              => 0,
    'filterable_in_search'    => 0,
    'searchable'              => 0,
    'used_in_product_listing' => 1,
    'sort_order'              => 17,
));

$installer->endSetup();