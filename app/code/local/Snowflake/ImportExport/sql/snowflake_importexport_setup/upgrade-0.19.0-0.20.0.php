<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 06.03.2015
 * Time: 09:32
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_product', 'provet_charge_cat', array(
    'group'                   => 'General',
    'label'                   => 'Abgabekategorie',
    'input'                   => 'text',
    'type'                    => 'varchar',
    'user_defined'            => 1,
    'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'                 => 1,
    'visible_on_front'        => 1,
    'required'                => 0,
    'filterable'              => 0,
    'filterable_in_search'    => 0,
    'searchable'              => 0,
    'used_in_product_listing' => 0,
    'sort_order'              => 26,
));

$installer->addAttribute('catalog_product', 'provet_ean', array(
    'group'                   => 'General',
    'label'                   => 'EAN-Code',
    'input'                   => 'text',
    'type'                    => 'varchar',
    'user_defined'            => 1,
    'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'                 => 1,
    'visible_on_front'        => 1,
    'required'                => 1,
    'filterable'              => 0,
    'filterable_in_search'    => 0,
    'searchable'              => 0,
    'used_in_product_listing' => 0,
    'sort_order'              => 28,
));


$installer->endSetup();