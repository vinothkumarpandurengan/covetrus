<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 20.01.2015
 * Time: 09:17
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->updateAttribute('catalog_product', 'provet_in_action', 'is_used_for_promo_rules', true);

$installer->endSetup();