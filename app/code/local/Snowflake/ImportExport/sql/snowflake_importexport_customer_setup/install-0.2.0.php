<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 09.12.2014
 * Time: 10:15
 */

/* @var $installer Mage_Customer_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

// update customer address user defined attributes data
$attributesCustomerAddress = array(
    'language_code' => array(
        'label' => 'Language code',
        'type' => 'varchar',
        'input' => 'text',
        'is_user_defined' => 1,
        'system' => 0,
        'visible' => true,
        'required' => false,
        'unique' => false,
        'sort_order' => 95,
        'multiline_count'   => 0,
        'validate_rules' => array(
            'max_text_length'   => 2,
            'min_text_length'   => 1
        ),
    ),
);

/* @var $eavConfig Mage_Eav_Model_Config */
$eavConfig = Mage::getSingleton('eav/config');

$store     = Mage::app()->getStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// attributes for customer address
foreach ($attributesCustomerAddress as $attributeCode => $data) {
    $installer->addAttribute('customer_address', $attributeCode, $data);

    $attribute = $eavConfig->getAttribute('customer_address', $attributeCode);
    $attribute->setWebsite((($store->getWebsite()) ? $store->getWebsite() : 0));

    $usedInForms = array(
        'adminhtml_customer_address',
        'customer_address_edit',
        'customer_register_address'
    );
    $attribute->setData('used_in_forms', $usedInForms);
    $attribute->save();
}

foreach ($attributesCustomerAddress as $attributeCode => $data) {
    if (isset($data['sort_order'])) {
        $installer->updateAttribute('customer_address', $attributeCode, 'sort_order', $data['sort_order']);
    }
    if (isset($data['label'])) {
        $installer->updateAttribute('customer_address', $attributeCode, 'frontend_label', $data['label']);
    }
    if (isset($data['visible'])) {
        $installer->updateAttribute('customer_address', $attributeCode, 'is_visible', $data['visible']);
    }
    if (isset($data['system'])) {
        $installer->updateAttribute('customer_address', $attributeCode, 'is_system', $data['system']);
    }
    if (isset($data['multiline_count'])) {
        $installer->updateAttribute('customer_address', $attributeCode, 'multiline_count', $data['multiline_count']);
    }
    if (isset($data['validate_rules'])) {
        $installer->updateAttribute('customer_address', $attributeCode, 'validate_rules', serialize($data['validate_rules']));
    }
}

$installer->endSetup();