<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 03.03.2015
 * Time: 16:29
 */

/* @var $installer Snowflake_Customer_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

/* @var $eavConfig Mage_Eav_Model_Config */
$eavConfig = Mage::getSingleton('eav/config');
$store     = Mage::app()->getStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// update customer address user defined attributes data
$attributesCustomerAddress = array(
    'provet_shipping_code' => array(
        'type'               => 'varchar',
        'input'              => 'text',
        'label'              => 'Lieferadresse Code',
        'is_user_defined'    => 1,
        'system'             => 0,
        'visible'            => true,
        'required'           => true,
        'unique'             => true,
        'sort_order'         => 150,
        'position'           => 150,
        'validate_rules' => array(
            'max_text_length'   => 20,
            'min_text_length'   => 1
        ),
    ),
);

// attributes for customer address
foreach ($attributesCustomerAddress as $attributeCode => $data) {
    $installer->addAttribute('customer_address', $attributeCode, $data);

    $attribute = $eavConfig->getAttribute('customer_address', $attributeCode);
    $attribute->setWebsite((($store->getWebsite()) ? $store->getWebsite() : 0));

    $usedInForms = array(
        'adminhtml_customer_address',
        'customer_address_edit',
        'customer_register_address'
    );
    $attribute->setData('used_in_forms', $usedInForms);
    $attribute->save();
}

foreach ($attributesCustomerAddress as $attributeCode => $data) {
    if (isset($data['sort_order'])) {
        $installer->updateAttribute('customer_address', $attributeCode, 'sort_order', $data['sort_order']);
    }
    if (isset($data['label'])) {
        $installer->updateAttribute('customer_address', $attributeCode, 'frontend_label', $data['label']);
    }
    if (isset($data['visible'])) {
        $installer->updateAttribute('customer_address', $attributeCode, 'is_visible', $data['visible']);
    }
    if (isset($data['system'])) {
        $installer->updateAttribute('customer_address', $attributeCode, 'is_system', $data['system']);
    }
    if (isset($data['validate_rules'])) {
        $installer->updateAttribute('customer_address', $attributeCode, 'validate_rules', serialize($data['validate_rules']));
    }
}
$installer->endSetup();
  