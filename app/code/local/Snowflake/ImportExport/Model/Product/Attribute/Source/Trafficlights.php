<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 27.03.2015
 * Time: 13:21
 */
class Snowflake_ImportExport_Model_Product_Attribute_Source_Trafficlights extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /**
     * Option values
     */
    const VALUE_RED = 0;
    const VALUE_YELLOW  = 1;
    const VALUE_GREEN   = 2;

    /**
     * Retrieve all options array
     *
     * @return array
     */
    public function getAllOptions()
    {
        if (is_null($this->_options)) {
            $this->_options = array(
                array(
                    'label' => Mage::helper('snowflake_importexport')->__('Red'),
                    'value' => self::VALUE_RED
                ),
                array(
                    'label' => Mage::helper('snowflake_importexport')->__('Yellow'),
                    'value' => self::VALUE_YELLOW
                ),
                array(
                    'label' => Mage::helper('snowflake_importexport')->__('Green'),
                    'value' => self::VALUE_GREEN
                ),
            );
        }
        return $this->_options;
    }
}
  