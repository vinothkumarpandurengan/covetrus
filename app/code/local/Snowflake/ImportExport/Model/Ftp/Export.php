<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Project: www
 * Date: 30.03.2015
 * Time: 12:22
 */

class Snowflake_ImportExport_Model_Ftp_Export
{
    public function transferFiles()
    {
        Mage::log("function Export->transferFiles() is called");
        $ftpServer    = Mage::getStoreConfig('snowflake_importexport/ftp_access/host');
        $ftpUserName  = Mage::getStoreConfig('snowflake_importexport/ftp_access/username');
        $ftpUserPass  = Mage::getStoreConfig('snowflake_importexport/ftp_access/password');
        $ftpExportDir = Mage::getStoreConfig('snowflake_importexport/ftp_access/export_csv_directory');

        $dir = Mage::getStoreConfig('snowflake_importexport/general/export_csv_directory');
        if (substr($dir, -1) != DS) {
            $dir .= DS;
        }

        $backupDir = $dir . 'backup_' . time() . DS;
        $files     = glob($dir . '*.*');

        if (count($files) > 0) {
            // Verbindung aufbauen
            $connId = ftp_connect($ftpServer);

            // Login mit Benutzername und Passwort
            if (!ftp_login($connId, $ftpUserName, $ftpUserPass)) {
                Mage::throwException('Attempted to connection.');
            }

            // Verzeichnis wechseln
            if (!ftp_chdir($connId, $ftpExportDir)) {
                Mage::throwException('Could not change dir.');
            }

            foreach ($files as $file) {
                $remoteFile = basename($file);
                
                $trackErrors = ini_get('track_errors');
                ini_set('track_errors', 1);
                
                if (!@ftp_put($connId, $remoteFile, $file, FTP_ASCII)) {
                    $msg = $php_errormsg;
                    ini_set('track_errors', $trackErrors);
                    
                    Mage::throwException($msg);
                }
                
                ini_set('track_errors', $trackErrors);

                if (!is_dir($backupDir)) {
                    mkdir($backupDir);
                }

                Mage::log('Uploading  ' . $file . ' to ' . $remoteFile . PHP_EOL, null, 'ftp_transfer.log');
                rename($file, $backupDir . basename($file));
            }

            // Verbindung schließen
            ftp_close($connId);
        }
    }
}
