<?php

/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Project: www
 * Date: 30.03.2015
 * Time: 12:22
 */
class Snowflake_ImportExport_Model_Ftp_Import {

    /**
     * Request config setting for delete Files
     *
     * @var boolean
     */
    protected $_deleteFiles = false;

    /**
     * Request config setting for ftp server
     *
     * @var string
     */
    protected $_ftpServer;

    /**
     * Request config setting for ftp username
     *
     * @var string
     */
    protected $_ftpUserName;

    /**
     * Request config setting for ftp password
     *
     * @var string
     */
    protected $_ftpUserPass;

    /**
     * Request config setting for ftp import csv directory
     *
     * @var string
     */
    protected $_ftpImportCsvDir;

    /**
     * Request config setting for ftp import pdf directory
     *
     * @var string
     */
    protected $_ftpImportPdfDir;

    /**
     * Request config setting for ftp import image directory
     *
     * @var string
     */
    protected $_ftpImportImagesDir;

    /**
     * Request config setting for local import csv directory
     *
     * @var string
     */
    protected $_localCsvImportDir;

    /**
     * Request config setting for local import pdf directory
     *
     * @var string
     */
    protected $_localPdfImportDir;

    /**
     * Request config setting for local import image directory
     *
     * @var string
     */
    protected $_localImagesImportDir;

    /**
     * Request config setting for local import image directory
     *
     * @var array
     */
    protected $_importTypes = array();

    /**
     * Allowed files for import types
     *
     * @var array
     */
    protected $_allowedFiles = array(
        'product' => array('Artikel', 'Artikel_Sp', 'artikel', 'artikel_Sp'),
        'stock' => array('Ampel', 'ampel'),
        'customer' => array('kunden', 'kunden_Ap', 'Kunden', 'Kunden_Ap'),
        'webgroupcode' => array('WebGrpCode', 'webGrpCode'),
    );
    protected $_archiveFiles = array(
        'product' => array('Artikel', 'artikel'),
        'tierprices' => array('Artikel_Sp', 'artikel_Sp'),
        'stock' => array('Ampel', 'ampel'),
        'customer' => array('kunden', 'kunden_Ap', 'Kunden', 'Kunden_Ap'),
        'webgroupcode' => array('WebGrpCode', 'webGrpCode'),
    );

    /**
     * Initialize config settings for directories
     */
    protected function _init() {        
        $this->_deleteFiles = Mage::getStoreConfig('snowflake_importexport/ftp_access/delete_files');

        $this->_ftpServer = Mage::getStoreConfig('snowflake_importexport/ftp_access/host');
        $this->_ftpUserName = Mage::getStoreConfig('snowflake_importexport/ftp_access/username');
        $this->_ftpUserPass = Mage::getStoreConfig('snowflake_importexport/ftp_access/password');

        $this->_ftpImportCsvDir = Mage::getStoreConfig('snowflake_importexport/ftp_access/import_csv_directory');
        $this->_ftpImportPdfDir = Mage::getStoreConfig('snowflake_importexport/ftp_access/import_pdf_directory');
        $this->_ftpImportImagesDir = Mage::getStoreConfig('snowflake_importexport/ftp_access/import_images_directory');

        $this->_localCsvImportDir = Mage::getStoreConfig('snowflake_importexport/general/import_csv_directory');
        $this->_localPdfImportDir = Mage::getStoreConfig('snowflake_importexport/general/import_pdf_directory');
        $this->_localImagesImportDir = Mage::getStoreConfig('snowflake_importexport/general/import_images_directory');
    }

    /**
     * Import products
     *
     * cronjob
     */
    public function importProduct() {
        Mage::log("function importProduct is called");
        $this->_init();
        $this->downloadFiles('product');

        try {
            Mage::getSingleton('snowflake_importexport/productimport')->importProducts(true);
            Mage::getSingleton('snowflake_importexport/productimport')->importTierPrices(true);
            //Mage::getSingleton('snowflake_importexport/productimport')->importReplacementItems(true);
        } catch (Exception $e) {
            Mage::throwException($e->getMessage());
            Mage::logException($e);
        }

        $this->transferFiles('product');
    }

    /**
     * Import product stocks
     *
     * cronjob
     */
    public function importStock() {
        Mage::log("function importStock is called");
        $this->_init();
        $this->downloadFiles('stock');

        try {
            Mage::getSingleton('snowflake_importexport/productimport')->importProductStocks(true);
        } catch (Exception $e) {
            Mage::throwException($e->getMessage());
            Mage::logException($e);
        }

        $this->transferFiles('stock');
    }

    /**
     * Import customers
     *
     * cronjob
     */
    public function importCustomer() {
        Mage::log("function importCustomer is called");
        $this->_init();
        $this->downloadFiles('customer');

        try {
            Mage::getSingleton('snowflake_importexport/customerimport')->importCustomer(true);
            Mage::getSingleton('snowflake_importexport/customerimport')->importCustomerPrices(true);
            Mage::getSingleton('snowflake_importexport/customerimport')->importQuantityDiscount(true);
        } catch (Exception $e) {
            Mage::throwException($e->getMessage());
            Mage::logException($e);
        }

        $this->transferFiles('customer');
    }

    /**
     * Import Categorie translate
     *
     * cronjob
     */
    public function importWebgroupcode() {
        Mage::log("function importWebgroupcode is called");
        $this->_init();
        $this->downloadFiles('webgroupcode');

        try {
            Mage::getSingleton('snowflake_importexport/productimport')->translateWebgroupCode(true);
        } catch (Exception $e) {
            Mage::throwException($e->getMessage());
            Mage::logException($e);
        }

        $this->transferFiles('webgroupcode');
    }

    /**
     * Set import types
     */
    protected function _setImportTypes() {
        $this->_importTypes = array(
            'product' => array(
                'csv' => array(
                    'ftp_dir' => $this->_ftpImportCsvDir,
                    'local_dir' => $this->_localCsvImportDir,
                    'type' => FTP_ASCII,
                ),
                'pdf' => array(
                    'ftp_dir' => $this->_ftpImportPdfDir,
                    'local_dir' => $this->_localPdfImportDir,
                    'type' => FTP_BINARY,
                ),
                'image' => array(
                    'ftp_dir' => $this->_ftpImportImagesDir,
                    'local_dir' => $this->_localImagesImportDir,
                    'type' => FTP_BINARY,
                ),
            ),
            'tierprices' => array(
                'csv' => array(
                    'ftp_dir' => $this->_ftpImportCsvDir,
                    'local_dir' => $this->_localCsvImportDir,
                    'type' => FTP_ASCII,
                ),
            ),
            'stock' => array(
                'csv' => array(
                    'ftp_dir' => $this->_ftpImportCsvDir,
                    'local_dir' => $this->_localCsvImportDir,
                    'type' => FTP_ASCII,
                ),
            ),
            'customer' => array(
                'csv' => array(
                    'ftp_dir' => $this->_ftpImportCsvDir,
                    'local_dir' => $this->_localCsvImportDir,
                    'type' => FTP_ASCII,
                ),
            ),
            'webgroupcode' => array(
                'csv' => array(
                    'ftp_dir' => $this->_ftpImportCsvDir,
                    'local_dir' => $this->_localCsvImportDir,
                    'type' => FTP_ASCII,
                ),
            ),
        );
    }

    public function reindexAll() {
        set_time_limit(0);
        ini_set('memory_limit', '2048M');
        ini_set('post_max_size', '2048M');
        ini_set('upload_max_filesize', '2048M');
        ini_set('default_socket_timeout', 12000);
        ini_set('max_execution_time', 12000);
        ini_set('max_input_time', 12000);

        $collection = Mage::getSingleton('index/indexer')->getProcessesCollection();
        foreach ($collection as $process) {
            if ($process->getIndexer()->isVisible() === false) {
                continue;
            }
            $process->reindexEverything();
        }
    }

    /**
     * Get import types from code (product, stock, customer)
     *
     * @param string $code
     * @return array
     */
    protected function _getImportTypes($code) {
        if (!$this->_importTypes) {
            $this->_setImportTypes();
        }
        return $this->_importTypes[$code];
    }

    /**
     * Get allowed cvs files from code (product, stock, customer)
     *
     * @param string $code
     * @return array
     */
    protected function _getAllowedFiles($code) {
        return $this->_allowedFiles[$code];
    }

    /**
     * Get FTP username
     *
     * @return string
     */
    protected function _getUsername() {
        return $this->_ftpUserName;
    }

    /**
     * Get FTP password
     *
     * @return string
     */
    protected function _getPassword() {
        return $this->_ftpUserPass;
    }

    /**
     * Transfer files to backup directory if them imported successfully
     *
     * @param string $type (product, stock, customer)
     */
    public function transferFiles($type) {
        Mage::log("function Import->transferFiles(" . $type . ") is called");
        $this->_init();
        foreach ($this->_getImportTypes($type) as $code => $dirData) {
            $localDir = $dirData['local_dir'];

            if (substr($localDir, -1) != DS) {
                $localDir .= DS;
            }

            $backupDir = $localDir . 'backup_' . time() . DS;

            if ($code == 'csv') {
                foreach ($this->_getAllowedFiles($type) as $file) {
                    if (file_exists($localDir . lcfirst($file) . '.csv')) {
                        if (!is_dir($backupDir)) {
                            mkdir($backupDir, 0777, true);
                        }

                        rename($localDir . lcfirst($file) . '.csv', $backupDir . lcfirst($file) . '.csv');
                    }
                }
            } else {
                $files = glob($localDir . '*.*');
                foreach ($files as $file) {
                    if (!is_dir($backupDir)) {
                        mkdir($backupDir, 0777, true);
                    }

                    //rename($file, $backupDir . basename($file));
                }
            }
        }
    }

    /**
     * Download files from ftp server
     *
     * @param string $type (product, stock, customer)
     */
    public function downloadFiles($type) {
        $this->_init();
        $ftpServer = $this->_ftpServer;

        // Verbindung aufbauen
        $connectionId = ftp_connect($ftpServer, 21, 120);

        // Login mit Benutzername und Passwort
        if (!ftp_login($connectionId, $this->_getUsername(), $this->_getPassword())) {
            Mage::throwException('Couldn\'t connect to ftp server.');
        }

        foreach ($this->_getImportTypes($type) as $code => $dirData) {
            $ftpDir = $dirData['ftp_dir'];
            $localDir = $dirData['local_dir'];

            if (substr($localDir, -1) != DS) {
                $localDir .= DS;
            }

            if (substr($ftpDir, -1) != '/') {
                $ftpDir .= '/';
            }

            $dirContents = ftp_nlist($connectionId, $ftpDir);
            foreach ($dirContents as $fileName) {
                $localFileName = basename($fileName);
                $localFileName = lcfirst($localFileName);
                $localFilename = $localDir . $localFileName;
                $remoteFilename = $fileName;

                $pathinfo = pathinfo($localFilename);
                if ($code == 'csv' && !in_array($pathinfo['filename'], $this->_getAllowedFiles($type))) {
                    continue;
                }

                //ftp_pasv($connectionId, true);

                $trackErrors = ini_get('track_errors');
                ini_set('track_errors', 1);
                
                if (file_exists($localFilename)) {
                    unlink($localFilename);
                }
                
                $this->downloadMyFile($connectionId, $localFilename, $remoteFilename, $dirData['type']);
                                
                ini_set('track_errors', $trackErrors);

                if ($this->_deleteFiles) {
                    ftp_delete($connectionId, $remoteFilename);
                }
            }
        }

        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('snowflake_importexport')->__('Files have been downloaded successfully.'));

        // Verbindung schließen
        ftp_close($connectionId);
    }
    
    protected function downloadMyFile($connectionId, $localFilename, $remoteFilename, $type, $try = 0) {
        if (!@ftp_get($connectionId, $localFilename, $remoteFilename, $type)) {
            //$msg = $php_errormsg . " in file " . $localFilename;
            //ini_set('track_errors', $trackErrors);

            //Mage::throwException($msg);
            
            if ($try < 3) {
                $try++;
                sleep(5);
            
                $this->downloadMyFile($connectionId, $localFilename, $remoteFilename, $type, $try);
            }
        }
    }

    public function archiveFile($type) {
        $this->_init();

        $remoteArchiveFolder = "archive/";

        if ($type && isset($this->_archiveFiles[$type]) && count($this->_archiveFiles[$type])) {
            $types = $this->_getImportTypes($type);

            if (isset($types["csv"]) && isset($types["csv"]["ftp_dir"]) && $types["csv"]["ftp_dir"]) {
                $ftpServer = $this->_ftpServer;

                $ftpDir = $types["csv"]["ftp_dir"];
                if (substr($ftpDir, -1) != '/') {
                    $ftpDir .= '/';
                }

                // Verbindung aufbauen
                $connectionId = ftp_connect($ftpServer);

                // Login mit Benutzername und Passwort
                if (!ftp_login($connectionId, $this->_getUsername(), $this->_getPassword())) {
                    Mage::throwException('Couldn\'t connect to ftp server.');
                }

                foreach ($this->_archiveFiles[$type] as $filename) {
                    $completeFilename = $filename . ".csv";

                    $this->_ftp_move($connectionId, $ftpDir, $remoteArchiveFolder, $completeFilename, $filename . "-" . date("Y-m-d-H-i-s") . ".csv");
                }

                ftp_close($connectionId);
            }
        }
    }
    
    public function archiveImages2() {
        $this->_init();
        
        $remoteArchiveFolder = "/apics";
        $removePictureFolder = "/pics";
        
        $ftpServer = $this->_ftpServer;
        
        $connectionId = ftp_connect($ftpServer);
        
        if (!ftp_login($connectionId, $this->_getUsername(), $this->_getPassword())) {
            Mage::throwException('Couldn\'t connect to ftp server.');
        }
        
        $importMediaDir = Mage::getBaseDir("media") . DS . "import" . DS;
        
        if ($handle = opendir($importMediaDir)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != ".." && !strstr($entry, '.php')) {
                    if (file_exists($importMediaDir . $entry) && is_readable($importMediaDir . $entry)) {
                        if (ftp_put($connectionId, $remoteArchiveFolder . '/' . $entry, $importMediaDir . $entry, FTP_BINARY)) {
                            if (@ftp_delete($connectionId, $removePictureFolder . "/" . $entry)) {
                                unlink($importMediaDir . $entry);
                            }
                        }
                    }
                }
            }
            closedir($handle);
        }
        
        ftp_close($connectionId);
    }

    public function archiveImages() {
        $this->_init();
        
        $remoteArchiveFolder = "/archive";
        $removePictureFolder = "/pics";
        
        $ftpServer = $this->_ftpServer;
        
        $connectionId = ftp_connect($ftpServer);
        
        if (!ftp_login($connectionId, $this->_getUsername(), $this->_getPassword())) {
            Mage::throwException('Couldn\'t connect to ftp server.');
        }
        
        // -- zip import folder --
        $importMediaDir = Mage::getBaseDir("media") . DS . "import" . DS;
        $importVarDir = Mage::getBaseDir("var") . DS . "import" . DS;

        $fileName = 'backup-images-' . date("Y-m-d-H-i-s") . '.zip';
        $zipName = $importVarDir . $fileName;
        $zip = new ZipArchive();

        $opened = $zip->open($zipName, ZipArchive::CREATE);
        
        if( $opened !== true ){
            die("Cannot open ".$zipName." for writing.");
        }

        $fileCount = 0;
        
        if ($handle = opendir($importMediaDir)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != ".." && !strstr($entry, '.php')) {
                    if (file_exists($importMediaDir . $entry) && is_readable($importMediaDir . $entry)) {
                        $zip->addFile($importMediaDir . $entry, $entry);
                        $fileCount++;
                    }
                }
            }
            closedir($handle);
        }

        if ($fileCount) {
            $zip->close();
            
            if (file_exists($zipName)) {
                if ($handle = opendir($importMediaDir)) {
                    while (false !== ($entry = readdir($handle))) {
                        if ($entry != "." && $entry != ".." && !strstr($entry, '.php')) {
                            if (ftp_delete($connectionId, $removePictureFolder . "/" . $entry)) {
                                unlink($importMediaDir . $entry);
                            }
                        }
                    }
                    closedir($handle);
                }
            }

            $backupVarDir = Mage::getBaseDir("var") . DS . "archive" . DS;
            $backupVarFile = $backupVarDir . $fileName;

            if (!file_exists($backupVarDir)) {
                mkdir($backupVarDir, 0777, true);
            }

            rename($zipName, $backupVarFile);

            ftp_put($connectionId, $remoteArchiveFolder . '/' . $fileName, $backupVarFile, FTP_BINARY);

            ftp_close($connectionId);
        }
    }

    protected function _ftp_move($conn_distant, $pathftp, $pathftpimg, $localName, $remoteName) {
        if (!file_exists(Mage::getBaseDir("var") . DS . "tmp" . DS)) {
            mkdir(Mage::getBaseDir("var") . DS . "tmp" . DS, 0777, true);
        }

        if (@ftp_get($conn_distant, Mage::getBaseDir("var") . DS . "tmp" . DS . $localName, $pathftp . '/' . $localName, FTP_BINARY)) {
            if (ftp_put($conn_distant, $pathftpimg . '/' . $remoteName, Mage::getBaseDir("var") . DS . "tmp" . DS . $localName, FTP_BINARY)) {
                unlink(Mage::getBaseDir("var") . DS . "tmp" . DS . $localName);
                if (!ftp_delete($conn_distant, $pathftp . '/' . $localName)) {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

}
