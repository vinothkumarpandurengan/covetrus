<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 17.03.2015
 * Time: 13:04
 */
class Snowflake_ImportExport_Model_FastSimpleImport_Import_Uploader extends Mage_ImportExport_Model_Import_Uploader
{
    /**
     * Initiate uploader default settings
     */
    public function init()
    {
        $this->setAllowRenameFiles(true);
        $this->setAllowCreateFolders(true);
        $this->setFilesDispersion(true);
        $this->setAllowedExtensions(Mage::getModel('snowflake_importexport/productimport')->getAllowedFileExtensions());
    }
}
  