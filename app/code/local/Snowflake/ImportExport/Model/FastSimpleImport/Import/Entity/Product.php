<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 04.02.2015
 * Time: 09:58
 */ 
class Snowflake_ImportExport_Model_FastSimpleImport_Import_Entity_Product
    extends AvS_FastSimpleImport_Model_Import_Entity_Product
{
    /**
     * Error - invalid tier price qty
     */
    const ERROR_INVALID_CUSTOMER_PRICE_QTY      = 'invalidCustomerPriceOrQty';

    /**
     * Error - invalid tier price site
     */
    const ERROR_INVALID_CUSTOMER_PRICE_SITE     = 'customerPriceWebsiteInvalid';

    /**
     * Error - tier data incomplete
     */
    const ERROR_CUSTOMER_PRICE_DATA_INCOMPLETE  = 'customerPriceDataIsIncomplete';

    /**
     * Column names that holds images files names
     *
     * @var array
     */
    protected $_filesArrayKeys = array(
        '_file_name'
    );

    /**
     * Product files uploader
     *
     * @var Snowflake_ImportExport_Model_FastSimpleImport_Import_Uploader
     */
    protected $_productFileUploader;

    /**
     * Links attribute name-to-link type ID.
     *
     * @var array
     */
    protected $_linkNameToId = array(
        '_links_related_'       => Mage_Catalog_Model_Product_Link::LINK_TYPE_RELATED,
        '_links_crosssell_'     => Mage_Catalog_Model_Product_Link::LINK_TYPE_CROSSSELL,
        '_links_upsell_'        => Mage_Catalog_Model_Product_Link::LINK_TYPE_UPSELL,
        '_links_replacement_'   => Snowflake_ReplacementItems_Model_Catalog_Product_Link::LINK_TYPE_REPLACEMENT
    );

    /**
     * Column names that holds values with particular meaning.
     *
     * @var array
     */
    protected $_particularAttributes = array(
        '_store', '_attribute_set', '_type', self::COL_CATEGORY, self::COL_ROOT_CATEGORY, '_product_websites',
        '_tier_price_website', '_tier_price_customer_group', '_tier_price_qty', '_tier_price_price', '_tier_price_start_date', '_tier_price_end_date',
        '_group_price_website', '_group_price_customer_group', '_group_price_price', '_group_price_start_date', '_group_price_end_date',
        '_customer_price_website', '_customer_price_customer', '_customer_price_qty', '_customer_price_price', '_customer_price_discount', '_customer_price_finalprice', '_customer_price_start_date', '_customer_price_end_date',
        '_links_related_sku', '_links_related_position', '_links_crosssell_sku', '_links_crosssell_position', '_links_upsell_sku', '_links_replacement_sku', '_links_replacement_position',
        '_links_upsell_position', '_custom_option_store', '_custom_option_type', '_custom_option_title',
        '_custom_option_is_required', '_custom_option_price', '_custom_option_sku', '_custom_option_max_characters',
        '_custom_option_sort_order', '_custom_option_file_extension', '_custom_option_image_size_x',
        '_custom_option_image_size_y', '_custom_option_row_title', '_custom_option_row_price',
        '_custom_option_row_sku', '_custom_option_row_sort', '_media_attribute_id', '_media_image', '_media_lable',
        '_media_position', '_media_is_disabled', '_file_type', '_file_title','_file_name', '_file_position', '_file_promotion'
    );

    /**
     * Common validation
     *
     * @param array $rowData
     * @param int $rowNum
     * @param string|false|null $sku
     */
    protected function _validate($rowData, $rowNum, $sku)
    {
        parent::_validate($rowData, $rowNum, $sku);
        $this->_isCustomerPriceValid($rowData, $rowNum);
    }

    /**
     * Check tier price data validity.
     *
     * @param array $rowData
     * @param int $rowNum
     * @return bool
     */
    protected function _isTierPriceValid(array $rowData, $rowNum)
    {
        if ((isset($rowData['_tier_price_website']) && strlen($rowData['_tier_price_website']))
            || (isset($rowData['_tier_price_customer_group']) && strlen($rowData['_tier_price_customer_group']))
            || (isset($rowData['_tier_price_qty']) && strlen($rowData['_tier_price_qty']))
            || (isset($rowData['_tier_price_price']) && strlen($rowData['_tier_price_price']))
        ) {
            if (!isset($rowData['_tier_price_website']) || !isset($rowData['_tier_price_customer_group'])
                || !isset($rowData['_tier_price_qty']) || !isset($rowData['_tier_price_price'])
                || !strlen($rowData['_tier_price_website']) || !strlen($rowData['_tier_price_customer_group'])
                || !strlen($rowData['_tier_price_qty']) || !strlen($rowData['_tier_price_price'])
            ) {
                $this->addRowError(self::ERROR_TIER_DATA_INCOMPLETE, $rowNum);
                return false;
            } elseif ($rowData['_tier_price_website'] != self::VALUE_ALL
                && !isset($this->_websiteCodeToId[$rowData['_tier_price_website']])) {
                $this->addRowError(self::ERROR_INVALID_TIER_PRICE_SITE, $rowNum);
                return false;
            } elseif ($rowData['_tier_price_customer_group'] != self::VALUE_ALL
                && !isset($this->_customerGroups[$rowData['_tier_price_customer_group']])) {
                $this->addRowError(self::ERROR_INVALID_TIER_PRICE_GROUP, $rowNum);
                return false;
            } elseif ($rowData['_tier_price_qty'] <= 0 || $rowData['_tier_price_price'] <= 0) {
                $this->addRowError(self::ERROR_INVALID_TIER_PRICE_QTY, $rowNum);
                return false;
            }
        }
        return true;
    }

    /**
     * Check group price data validity.
     *
     * @param array $rowData
     * @param int $rowNum
     * @return bool
     */
    protected function _isGroupPriceValid(array $rowData, $rowNum)
    {
        if ((isset($rowData['_group_price_website']) && strlen($rowData['_group_price_website']))
            || (isset($rowData['_group_price_customer_group']) && strlen($rowData['_group_price_customer_group']))
            || (isset($rowData['_group_price_price']) && strlen($rowData['_group_price_price']))
        ) {
            if (!isset($rowData['_group_price_website']) || !isset($rowData['_group_price_customer_group'])
                || !strlen($rowData['_group_price_website']) || !strlen($rowData['_group_price_customer_group'])
                || !strlen($rowData['_group_price_price'])
            ) {
                $this->addRowError(self::ERROR_GROUP_PRICE_DATA_INCOMPLETE, $rowNum);
                return false;
            } elseif ($rowData['_group_price_website'] != self::VALUE_ALL
                && !isset($this->_websiteCodeToId[$rowData['_group_price_website']])
            ) {
                $this->addRowError(self::ERROR_INVALID_GROUP_PRICE_SITE, $rowNum);
                return false;
            } elseif ($rowData['_group_price_customer_group'] != self::VALUE_ALL
                && !isset($this->_customerGroups[$rowData['_group_price_customer_group']])
            ) {
                $this->addRowError(self::ERROR_INVALID_GROUP_PRICE_GROUP, $rowNum);
                return false;
            }
        }
        return true;
    }

    /**
     * Check customer price data validity.
     *
     * @param array $rowData
     * @param int $rowNum
     * @return bool
     */
    protected function _isCustomerPriceValid(array $rowData, $rowNum)
    {
        if ((isset($rowData['_customer_price_website']) && strlen($rowData['_customer_price_website']))
            || (isset($rowData['_customer_price_customer']) && strlen($rowData['_customer_price_customer']))
            || (isset($rowData['_customer_price_qty']) && strlen($rowData['_customer_price_qty']))
            || (isset($rowData['_customer_price_price']) && strlen($rowData['_customer_price_price']))
        ) {
            if (!isset($rowData['_customer_price_website']) || !isset($rowData['_customer_price_customer'])
                || !strlen($rowData['_customer_price_website']) || !strlen($rowData['_customer_price_customer'])
                || !strlen($rowData['_customer_price_price'])
            ) {
                $this->addRowError(self::ERROR_CUSTOMER_PRICE_DATA_INCOMPLETE, $rowNum);
                return false;
            } elseif ($rowData['_customer_price_website'] != self::VALUE_ALL
                && !isset($this->_websiteCodeToId[$rowData['_customer_price_website']])) {
                $this->addRowError(self::ERROR_INVALID_CUSTOMER_PRICE_SITE, $rowNum);
                return false;
            } elseif ($rowData['_customer_price_qty'] <= 0 || $rowData['_customer_price_price'] <= 0) {
                $this->addRowError(self::ERROR_INVALID_CUSTOMER_PRICE_QTY, $rowNum);
                return false;
            }
        }

        return true;
    }

    protected function _saveProducts()
    {
        $priceIsGlobal  = Mage::helper('catalog')->isPriceGlobal();
        $productLimit   = null;
        $productsQty    = null;
        $rowSku         = null;

        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            $entityRowsIn   = array();
            $entityRowsUp   = array();
            $attributes     = array();
            $websites       = array();
            $categories     = array();
            $tierPrices     = array();
            $groupPrices    = array();
            $customerPrices = array();
            $mediaGallery = array();
            $uploadedGalleryFiles = array();
            $productFiles = array();
            $uploadedProductFiles = array();
            $previousType = null;
            $previousAttributeSet = null;

            foreach ($bunch as $rowNum => $rowData) {
                $this->_filterRowData($rowData);
                if (!$this->validateRow($rowData, $rowNum)) {
                    continue;
                }
                $rowScope = $this->getRowScope($rowData);

                if (self::SCOPE_DEFAULT == $rowScope) {
                    $rowSku = $rowData[self::COL_SKU];

                    // 1. Entity phase
                    if (isset($this->_oldSku[$rowSku])) { // existing row
                        $entityRowsUp[] = array(
                            'updated_at' => now(),
                            'entity_id'  => $this->_oldSku[$rowSku]['entity_id']
                        );
                    } else { // new row
                        if (!$productLimit || $productsQty < $productLimit) {
                            $entityRowsIn[$rowSku] = array(
                                'entity_type_id'   => $this->_entityTypeId,
                                'attribute_set_id' => $this->_newSku[$rowSku]['attr_set_id'],
                                'type_id'          => $this->_newSku[$rowSku]['type_id'],
                                'sku'              => $rowSku,
                                'created_at'       => now(),
                                'updated_at'       => now()
                            );
                            $productsQty++;
                        } else {
                            $rowSku = null; // sign for child rows to be skipped
                            $this->_rowsToSkip[$rowNum] = true;
                            continue;
                        }
                    }
                } elseif (null === $rowSku) {
                    $this->_rowsToSkip[$rowNum] = true;
                    continue; // skip rows when SKU is NULL
                } elseif (self::SCOPE_STORE == $rowScope) { // set necessary data from SCOPE_DEFAULT row
                    $rowData[self::COL_TYPE]     = $this->_newSku[$rowSku]['type_id'];
                    $rowData['attribute_set_id'] = $this->_newSku[$rowSku]['attr_set_id'];
                    $rowData[self::COL_ATTR_SET] = $this->_newSku[$rowSku]['attr_set_code'];
                }
                if (!empty($rowData['_product_websites'])) { // 2. Product-to-Website phase
                    $websites[$rowSku][$this->_websiteCodeToId[$rowData['_product_websites']]] = true;
                }

                // 3. Categories phase
                $categoryPath = empty($rowData[self::COL_CATEGORY]) ? '' : $rowData[self::COL_CATEGORY];
                if (!empty($rowData[self::COL_ROOT_CATEGORY])) {
                    $categoryId = $this->_categoriesWithRoots[$rowData[self::COL_ROOT_CATEGORY]][$categoryPath];
                    $categories[$rowSku][$categoryId] = true;
                } elseif (!empty($categoryPath)) {
                    $categories[$rowSku][$this->_categories[$categoryPath]] = true;
                } elseif (isset($rowData[self::COL_CATEGORY])) {
                    $categories[$rowSku] = array();
                }

                if (!empty($rowData['_tier_price_website'])) { // 4.1. Tier prices phase
                    $tierPrices[$rowSku][] = array(
                        'all_groups'        => $rowData['_tier_price_customer_group'] == self::VALUE_ALL,
                        'customer_group_id' => ($rowData['_tier_price_customer_group'] == self::VALUE_ALL)
                            ? 0 : $rowData['_tier_price_customer_group'],
                        'qty'               => $rowData['_tier_price_qty'],
                        'value'             => $rowData['_tier_price_price'],
                        'website_id'        => (self::VALUE_ALL == $rowData['_tier_price_website'] || $priceIsGlobal)
                            ? 0 : $this->_websiteCodeToId[$rowData['_tier_price_website']],
                        /* new */
                        'start_date'        => isset($rowData['_tier_price_start_date']) ? $rowData['_tier_price_start_date'] : NULL,
                        'end_date'          => isset($rowData['_tier_price_end_date']) ? $rowData['_tier_price_end_date'] : NULL
                    );
                }
                if (!empty($rowData['_group_price_website'])) { // 4.2. Group prices phase
                    $groupPrices[$rowSku][] = array(
                        'all_groups'        => $rowData['_group_price_customer_group'] == self::VALUE_ALL,
                        'customer_group_id' => ($rowData['_group_price_customer_group'] == self::VALUE_ALL)
                            ? 0 : $rowData['_group_price_customer_group'],
                        'value'             => $rowData['_group_price_price'],
                        'website_id'        => (self::VALUE_ALL == $rowData['_group_price_website'] || $priceIsGlobal)
                            ? 0 : $this->_websiteCodeToId[$rowData['_group_price_website']],
                        /* new */
                        'start_date'        => isset($rowData['_group_price_start_date']) ? $rowData['_group_price_start_date'] : NULL,
                        'end_date'          => isset($rowData['_group_price_end_date']) ? $rowData['_group_price_end_date'] : NULL
                    );
                }

                // New Snowflake customer price entry
                if (!empty($rowData['_customer_price_website'])) { // 4.3. Customer prices phase
                    $customerPrices[$rowSku][] = array(
                        'qty'               => $rowData['_customer_price_qty'],
                        'value'             => $rowData['_customer_price_price'],
                        'customer_id'       => $rowData['_customer_price_customer'],
                        'website_id'        => (self::VALUE_ALL == $rowData['_customer_price_website'] || $priceIsGlobal)
                            ? 0 : $this->_websiteCodeToId[$rowData['_customer_price_website']],
                        'discount'          => isset($rowData['_customer_price_discount']) ? $rowData['_customer_price_discount'] : NULL,
                        'final_price'       => $rowData['_customer_price_finalprice'],
                        'start_date'        => isset($rowData['_customer_price_start_date']) ? $rowData['_customer_price_start_date'] : NULL,
                        'end_date'          => isset($rowData['_customer_price_end_date']) ? $rowData['_customer_price_end_date'] : NULL
                    );
                }
                // End Snowflake customer price

                if (is_array($this->_imagesArrayKeys)  && count($this->_imagesArrayKeys) > 0) {
                    foreach ($this->_imagesArrayKeys as $imageCol) {
                        if (!empty($rowData[$imageCol])) { // 5. Media gallery phase
                            if (!array_key_exists($rowData[$imageCol], $uploadedGalleryFiles)) {
                                $uploadedGalleryFiles[$rowData[$imageCol]] = $this->_uploadMediaFiles($rowData[$imageCol]);
                            }
                            $rowData[$imageCol] = $uploadedGalleryFiles[$rowData[$imageCol]];
                        }
                    }
                }
                if (!empty($rowData['_media_image'])) {
                    $mediaGallery[$rowSku][] = array(
                        'attribute_id'      => $rowData['_media_attribute_id'],
                        'label'             => isset($rowData['_media_lable']) ? $rowData['_media_lable'] : '',
                        'position'          => isset($rowData['_media_position']) ? $rowData['_media_position'] : 0,
                        'disabled'          => isset($rowData['_media_is_disabled']) ? $rowData['_media_is_disabled'] : 0,
                        'value'             => $rowData['_media_image']
                    );
                }
                if (is_array($this->_filesArrayKeys)  && count($this->_filesArrayKeys) > 0) {
                    foreach ($this->_filesArrayKeys as $fileCol) {
                        if (!empty($rowData[$fileCol])) { // 6. Product file phase
                            if ($rowData['_file_type'] != 'file') {
                                continue;
                            }

                            if (!array_key_exists($rowData[$fileCol], $uploadedProductFiles)) {
                                $uploadedProductFiles[$rowData[$fileCol]] = $this->_uploadProductFiles($rowData[$fileCol]);
                            }
                            $rowData[$fileCol] = $uploadedProductFiles[$rowData[$fileCol]];
                        }
                    }
                }
                if (!empty($rowData['_file_name'])) {
                    $productFiles[$rowSku][] = array(
                        'type'              => $rowData['_file_type'],
                        'title'             => isset($rowData['_file_title']) ? $rowData['_file_title'] : '',
                        'promotion'         => isset($rowData['_file_promotion']) ? $rowData['_file_promotion'] : 0,
                        'position'          => isset($rowData['_file_position']) ? $rowData['_file_position'] : 0,
                        'value'             => $rowData['_file_name']
                    );
                }

                // 7. Attributes phase
                $rowStore     = self::SCOPE_STORE == $rowScope ? $this->_storeCodeToId[$rowData[self::COL_STORE]] : 0;
                $productType  = isset($rowData[self::COL_TYPE]) ? $rowData[self::COL_TYPE] : null;
                if (!is_null($productType)) {
                    $previousType = $productType;
                }
                if (isset($rowData[self::COL_ATTR_SET]) && !is_null($rowData[self::COL_ATTR_SET])) {
                    $previousAttributeSet = $rowData[Mage_ImportExport_Model_Import_Entity_Product::COL_ATTR_SET];
                }
                if (self::SCOPE_NULL == $rowScope) {
                    // for multiselect attributes only
                    if (!is_null($previousAttributeSet)) {
                        $rowData[Mage_ImportExport_Model_Import_Entity_Product::COL_ATTR_SET] = $previousAttributeSet;
                    }
                    if (is_null($productType) && !is_null($previousType)) {
                        $productType = $previousType;
                    }
                    if (is_null($productType)) {
                        continue;
                    }
                }
                $rowData = $this->_productTypeModels[$productType]->prepareAttributesForSave(
                    $rowData,
                    !isset($this->_oldSku[$rowSku])
                );
                try {
                    $attributes = $this->_prepareAttributes($rowData, $rowScope, $attributes, $rowSku, $rowStore);
                } catch (Exception $e) {
                    Mage::logException($e);
                    continue;
                }
            }
            $this->_saveProductEntity($entityRowsIn, $entityRowsUp)
                ->_saveProductWebsites($websites)
                ->_saveProductCategories($categories)
                ->_saveProductTierPrices($tierPrices)
                ->_saveProductGroupPrices($groupPrices)
                ->_saveProductCustomerPrices($customerPrices)
                ->_saveMediaGallery($mediaGallery)
                ->_saveProductAttributes($attributes)
                ->_saveProductFiles($productFiles);
        }
        if (method_exists($this,'_fixUrlKeys')) { // > EE 1.13.1.0
            $this->_fixUrlKeys();
        }
        return $this;
    }

    /**
     * Uploading files into the "product_files" media folder.
     * Return a new file name if the same file is already exists.
     *
     * @see https://github.com/avstudnitz/AvS_FastSimpleImport/issues/109
     * In some cases the moving of files doesn't work because it is already
     * moved in a previous entity. We try and find the product in the destination folder.
     *
     * @param  string $fileName ex: /abc.pdf
     * @return string           ex: /a/b/abc.pdf
     */
    protected function _uploadProductFiles($fileName)
    {
        try {
            $res = $this->_getProductFileUploader()->move($fileName);
            return $res['file'];
        } catch (Exception $e) {
            //added additional logging
            Mage::logException($e);

            //find new target
            $dispretionPath = Mage_ImportExport_Model_Import_Uploader::getDispretionPath(substr($fileName, 1));
            $destDir = $this->_getUploader()->getDestDir();

            if (file_exists($destDir.$dispretionPath.$fileName)) {
                return $dispretionPath.$fileName;
            }
            return '';
        }
    }

    /**
     * Returns an object for upload a product file
     */
    protected function _getProductFileUploader()
    {
        if (is_null($this->_productFileUploader)) {
            $this->_productFileUploader    = new Snowflake_ImportExport_Model_FastSimpleImport_Import_Uploader();
            $this->_productFileUploader->init();

            $tmpDir  = Mage::getConfig()->getOptions()->getMediaDir() . '/import';
            $destDir = Mage::getConfig()->getOptions()->getMediaDir() . '/product_files';

            if (!is_writable($destDir)) {
                @mkdir($destDir, 0777, true);
            }
            // diglin - add auto creation in case folder doesn't exist
            if (!file_exists($tmpDir)) {
                @mkdir($tmpDir, 0777, true);
            }
            if (!$this->_productFileUploader->setTmpDir($tmpDir)) {
                Mage::throwException("File directory '{$tmpDir}' is not readable.");
            }
            if (!$this->_productFileUploader->setDestDir($destDir)) {
                Mage::throwException("File directory '{$destDir}' is not writable.");
            }
        }
        return $this->_productFileUploader;
    }

    /**
     * Save product group prices.
     *
     * @param array $groupPriceData
     * @return Mage_ImportExport_Model_Import_Entity_Product
     */
    protected function _saveProductCustomerPrices(array $customerPriceData)
    {
        static $tableName = null;

        if (!$tableName) {
            $tableName = Mage::getModel('importexport/import_proxy_product_resource')
                ->getTable('snowflake_pricegroup/catalog_product_attribute_customer_price');
        }

        if ($customerPriceData) {
            $customerPriceIn = array();
            $delProductId = array();

            foreach ($customerPriceData as $delSku => $customerPriceRows) {
                $productId      = $this->_newSku[$delSku]['entity_id'];
                $delProductId[] = $productId;

                foreach ($customerPriceRows as $row) {
                    $row['entity_id'] = $productId;
                    $customerPriceIn[]  = $row;
                }
            }
            if (Mage_ImportExport_Model_Import::BEHAVIOR_APPEND != $this->getBehavior()) {
                $this->_connection->delete(
                    $tableName,
                    $this->_connection->quoteInto('entity_id IN (?)', $delProductId)
                );
            }
            if ($customerPriceIn) {
                $this->_connection->insertOnDuplicate($tableName, $customerPriceIn, array('value'));
            }
        }

        return $this;
    }

    /**
     * Save product attributes.
     *
     * @param array $attributesData
     * @return Mage_ImportExport_Model_Import_Entity_Product
     */
    protected function _saveProductFiles(array $productFilesData)
    {
        if (empty($productFilesData)) {
            return $this;
        }

        $model = Mage::getModel('snowflake_fileattachment/attachment');
        foreach ($productFilesData as $productSku => $productFilesRow) {
            $productId = $this->_newSku[$productSku]['entity_id'];

            foreach ($productFilesRow as $currentFile) {
                $model->setProductId($productId)
                    ->setFileTitle($currentFile['title'])
                    ->setFilePromotion($currentFile['promotion'])
                    ->setFileType($currentFile['type'])
                    ->setSortOrder($currentFile['position']);

                if ($currentFile['type'] == 'file') {
                    $model
                        ->setFileName($currentFile['value'])
                        ->setFileUrl(null);
                } else {
                    $model
                        ->setFileName(null)
                        ->setFileUrl($currentFile['value']);
                }

                $model->save();

                $model->unsetData();
                sleep(1);
            }
        }

        return $this;
    }
}