<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 23.01.2015
 * Time: 16:51
 */
class Snowflake_ImportExport_Model_Customerimport extends Snowflake_ImportExport_Model_Abstract
{
    public function importCustomer($cron = false)
    {
        $defaultCustomers  = $this->_getImportArray('kunden', $cron);
        if (count($defaultCustomers) > 0) {
            $this->setCustomerImportData($defaultCustomers);

            // FastSimpleImport
            $this->_customerImport($this->getCustomerData());
            
            $this->_archiveFtpFile("customer");
        }
    }

    /**
     * Import product customer price
     *
     * @return void
     */
    public function importCustomerPrices($cron = false)
    {
        $data = $this->_getImportArray('kunden_ap', $cron);
        if (count($data) > 0) {
            $this->setCustomerPriceImportData($data);
            unset($data);

            // FastSimpleImport
            $this->_productImport($this->getCustomerPriceData());
        }
    }

    /**
     * Import product customer price
     *
     * @return void
     */
    public function importQuantityDiscount($cron = false)
    {
        $data = $this->_getImportArray('mengenrabatte', $cron);
        if (count($data) > 0) {
            $this->setDiscountImportData($data);
            unset($data);

            $this->_discountImport($this->getDiscountImportData());
        }
    }

    /**
     * Prepare customers data for FastSimpleImport
     *
     * @param array $customers Customers from csv file
     * @return void
     */
    public function setCustomerImportData($customers)
    {
        $customerModel = Mage::getModel('customer/customer');
        $count = $customerModel->getCollection()->count();

        foreach($customers as $customer) {
            if ($customer['EMail'] != '') {
                $email = trim($customer['EMail']); // default
                //$email = $customer['KdNr'] . '@xyz.ch'; // still duplicated email exist
            } else {
                $email = null;
            }

            $customerModel->setWebsiteId(Mage::app()->getWebsite(true)->getId())
                ->loadByEmail($email);

            if ($count > 0 && !$customerModel->getId()) {
                // set customer as new to sent email
                //$this->_newCustomers[]['email'] = $customer['EMail'];
            }

            $street = $customer['Adresse'];
            if ($customer['Adresse2'] != '') {
                $street .= "\n".$customer['Adresse2'];
            }

            $data = array(
                'email' => $email,
                '_website' => ($customer['EMail'] != '') ? 'base' : null,
                '_address_language_code' => $customer['Sprachcode'],
                '_address_firstname' => ($customer['Name'] != '') ? $customer['Name'] : "",
                '_address_lastname' => ($customer['Name2'] != '') ? $customer['Name2'] : "",
                '_address_street' => $street,
                '_address_postcode' => $customer['PLZ'],
                '_address_city' => ($customer['Ort'] != '') ? $customer['Ort'] : 'n/a',
                //'_address_country_id' => 'CH',
                '_address_country_id' => $customer['Land'],
                //'_address_region' => $customer['Region'],
                '_address_telephone' => $customer['Telefon'],
                '_address_provet_shipping_code' =>  $customer['LfAdrCode'],
                '_address_default_shipping_' => ($customer['StdLfAdr'] == 1) ? $customer['StdLfAdr'] : 0,
                '_address_default_billing_' => ($customer['StdRgAdr'] == 1) ? $customer['StdRgAdr'] : 0,
            );

            if ($customer['EMail'] != '') {
                if ($customer['RabGruppe'] != '') {
                    $discountGroup = substr($customer['RabGruppe'], 1);
                } else {
                    $discountGroup = '';
                }

                if ($this->_getDiscountGroupId($discountGroup) == 0) {
                    $this->_setDiscountGroup($discountGroup);
                }

                $data = array_merge($data, array(
                    'group_id' => ($customer['PrGrp'] != '') ? $this->_getCustomerGroup($customer['PrGrp']) : $this->_getCustomerGroup(2),
                    'customer_id' => $customer['KdNr'],
                    'username' => $customer['KdNr'],
                    'firstname' => ($customer['Name'] != '') ? $customer['Name'] : "",
                    'lastname' => ($customer['Name2'] != '') ? $customer['Name2'] : "",
                    'read_only' => $this->_getBooleanAttribute($customer['ReadOnly']),
                    'credit_limit' => $customer['Kreditlimite'],
                    'provet_currency_code' => $customer['Währungscode'],
                    'provet_billing_discount' => $customer['RgRabatt'],
                    'provet_quantity_discount' => $this->_getBooleanAttribute($customer['MngRab']),
                    'provet_discount_group' => $discountGroup,
                    'provet_tax_group' => ($customer['MWStGrp'] != '') ? $customer['MWStGrp'] : 'n/a',
                    'provet_matrix_group' => $customer['WarenBezug'],
                    'provet_warenbezug' => $this->_getBooleanAttribute($customer['WarenBezug']),
                    'provet_block' => $this->_getBooleanAttribute($customer['Sperre']),
                    'allow_government_restriction' => $this->_getBooleanAttribute($customer['BezugRegProd']),
                ));
            }

            $this->_customers[] = $data;
        }
    }

    /**
     * Prepare customer price data for FastSimpleImport
     *
     * @param array $customerPrice Customer Price from csv file
     * @return void
     */
    public function setCustomerPriceImportData($customerPrice)
    {
        $sku = array();

        $customerModel = Mage::getModel('customer/customer');
        foreach($customerPrice as $data) {
            $isExist = (bool) Mage::getSingleton('catalog/product')->getIdBySku($data['ArtNr']);
            if($isExist) {
                if ($data['Währungscode'] != 'CHF') {
                    continue;
                }

                if (in_array($data['ArtNr'], $sku)) {
                    $data['ArtNr'] = null;
                } else {
                    $sku[] = $data['ArtNr'];
                }

                if ($data['KdNr'] != '') {
                    $customer = $customerModel->setWebsiteId(Mage::app()->getWebsite(true)->getId())
                        ->loadByCustomerId($data['KdNr']);
                } else {
                    continue;
                }

                $this->_customerPrice[] = array(
                    'sku' => $data['ArtNr'],
                    '_customer_price_customer' => $customer->getId(),
                    '_customer_price_website' => 'all',
                    '_customer_price_qty' => $data['AbMenge'],
                    '_customer_price_price' => $data['Preis'],
                    '_customer_price_discount' => $data['Zusatzrabatt'],
                    '_customer_price_finalprice' => $data['Nettopreis'],
                    '_customer_price_start_date' => (!empty($data['GültigAb'])) ? $this->_formatDate($data['GültigAb']) : NULL,
                    '_customer_price_end_date' => (!empty($data['GültigBis'])) ? $this->_formatDate($data['GültigBis']) : NULL,
                );
            }
        }
    }

    /**
     * Prepare quantity discount data for Import
     *
     * @return void
     */
    public function setDiscountImportData($discounts)
    {
        foreach($discounts as $data) {
            if ($data['Code'] != '') {
                $code   = $data['Code'];
                $client = $code{0};
                $sku    = substr($code, 1);
            } else {
                continue;
            }

            $isExist = (bool)Mage::getSingleton('catalog/product')->getIdBySku($sku);
            if($isExist) {
                $discountGroupId = $this->_getDiscountGroupId($data['KdRabGrp']);
                if ($discountGroupId != 0) {
                    $this->_discounts[] = array(
                        'client' => strtolower($client),
                        'product_id' => $this->_getProductId($sku),
                        'min_quantity' => $data['Schwelle'],
                        'is_active' => 1,
                        'simple_action' => $this->_getSimpleAction($data),
                        'discount_group_ids' => (array) $discountGroupId,
                        'discount_type' => strtolower($data['Rabattart']),
                        'discount_amount' => $data['Rabatt'],
                        'discount_qty' => $data['BonusFixMenge'],
                        'discount_percent' => $this->_formatPercent($data['BonusInProz']),
                        'discount_product_id' => (!empty($data['BonusArtikel'])) ? $this->_getProductId($data['BonusArtikel']) : $this->_getProductId($sku),
                        'from_date' => (!empty($data['Gültigvon'])) ? $this->_formatDate($data['Gültigvon']) : NULL,
                        'to_date' => (!empty($data['GültigBis'])) ? $this->_formatDate($data['GültigBis']) : NULL,
                    );
                }
            }
        }
    }

    /**
     * Get simple action
     *
     * @param   array $data
     * @return  string
     */
    protected function _getSimpleAction($data)
    {
        if ($data['BonusFixMenge'] > 0) {
            return Snowflake_QuantityDiscount_Model_Rule::BONUS_FIXED_QTY_ACTION;
        } else if ($data['BonusNachBest'] != 0) {
            return Snowflake_QuantityDiscount_Model_Rule::BONUS_AFTER_ORDER_ACTION;
        } else if ($data['BonusInProz'] > 0) {
            return Snowflake_QuantityDiscount_Model_Rule::BONUS_PERCENT_ACTION;
        } else {
            return '';
        }
    }
}
  