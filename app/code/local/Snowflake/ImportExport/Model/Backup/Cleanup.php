<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 08.02.2016
 * Time: 09:14
 */

class Snowflake_ImportExport_Model_Backup_Cleanup
{
    /**
     * Cronjob for cleanup directory older than 2 days
     *
     * cronjob
     */
    public function run()
    {
        $directories = array (
            Mage::getStoreConfig('snowflake_importexport/general/import_csv_directory'),
            Mage::getStoreConfig('snowflake_importexport/general/import_images_directory'),
            Mage::getStoreConfig('snowflake_importexport/general/import_pdf_directory')
        );

        foreach ($directories as $directory) {
            $this->_cleanup($directory);
        }
    }

    /**
     * Cleanup backup directory older than 2 days
     *
     * @param string $dir
     */
    protected function _cleanup($dir)
    {
        $days = 2;

        $subdirs = glob($dir . '*', GLOB_ONLYDIR);
        foreach ($subdirs as $subdir) {
            clearstatcache();
            $lastModified = filemtime($subdir);

            if((time() - $lastModified) > ($days * 86400))
            {
                $this->_deleteDir($subdir);
            }

        }
    }

    /**
     * Delete backup directory older than 2 days
     *
     * @param string $dir
     */
    protected function _deleteDir($dir)
    {
        $files = glob($dir . '*', GLOB_MARK);

        foreach($files as $file){
            if(substr($file, -1 ) == '/')
                $this->_deleteDir($file);
            else
                unlink($file);

        }

        if (is_dir($dir)) rmdir($dir);
    }
}