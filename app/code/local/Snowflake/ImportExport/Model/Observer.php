<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 30.03.2015
 * Time: 11:21
 */

class Snowflake_ImportExport_Model_Observer {

    public function checkoutTypeOnepageSaveOrderAfter($observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getOrder();

        $_request  = Mage::app()->getRequest();
        $_comments = strip_tags($_request->getParam('comments'));

        if(!empty($_comments)){
            $order->setCustomerNote($_comments);
        }

        Mage::getModel('snowflake_importexport/orderexport')->export($order);
    }
}