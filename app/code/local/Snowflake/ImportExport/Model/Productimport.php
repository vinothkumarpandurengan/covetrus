<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 23.01.2015
 * Time: 16:51
 */
class Snowflake_ImportExport_Model_Productimport extends Snowflake_ImportExport_Model_Abstract
{
    protected $_allowedImageExtensions = array('jpg','jpeg');
    protected $_allowedFileExtensions  = array('pdf');
    protected $newMwstTypes = array();
    
    protected $_parentCategory = "Shop";

    /**
     * Import product data
     *
     * @param bool $cron true = cronjob is used
     * @return void
     */
    public function importProducts($cron = false)
    {
        $data = $this->_getImportArray('artikel', $cron);

        //$this->_updateCategoryStatus();
        if (count($data) > 0) {
            $this->setProductImportData($data);

            unset($data);

            $this->_productImport($this->getProductData());
            
            $this->_archiveFtpFile("product");
            
            $this->_archiveImages();
            
            $this->_updateCategoryStatus();
        }
    }
    
    protected function _updateCategoryStatus() {
        /*$resource = Mage::getSingleton('core/resource');
        
        $sql = "DELETE FROM 
`".$resource->getTableName('catalog_category_product')."` 
where product_id NOT IN (SELECT entity_id FROM `".$resource->getTableName('catalog_product_entity')."`)";
        
        $writeConnection = $resource->getConnection('core_write');
        
        $writeConnection->query($sql);*/
        
        $categorieCollection = Mage::getModel('catalog/category')->getCollection();
        
        if (count($categorieCollection)) {
            foreach($categorieCollection as $category) {
                $category = Mage::getModel('catalog/category')->load($category->getId());
                if (strpos($category->getPath(), "1/2/13") !== FALSE && $category->getPath() != "1/2/13") {
                    if ($category->getProductCount() == 0 && $category->getIsActive() == 1) {
                        $category->setIsActive(0);
                        $category->save();
                    }
                    else if ($category->getProductCount() > 0 && $category->getIsActive() == 0) {
                        $category->setIsActive(1);
                        $category->save();
                    }
                }
                else {
                    //$category->setIsActive(1);
                    //$category->save();
                }
            }
        }
    }

    /**
     * Import product data
     *
     * @param bool $cron true = cronjob is used
     * @return void
     */
    public function importProductStocks($cron = false)
    {
        $data = $this->_getImportArray('ampel', $cron);
        if (count($data) > 0) {
            $this->setProductStockImportData($data);
            unset($data);

            // FastSimpleImport
            $this->_productImport($this->getProductStocksData());
            
            $this->_archiveFtpFile("stock");
        }
    }


    /**
     * Import product data
     *
     * @param bool $cron true = cronjob is used
     * @return void
     */
    /*public function importReplacementItems($cron = false)
    {
        $data = $this->_getImportArray('artikel', $cron);
        if (count($data) > 0) {
            $this->setReplacementItems($data);
            unset($data);

            // FastSimpleImport
            $this->_productImport($this->getReplacementItemsData());
        }
    }*/

    /**
     * Import product tier price data
     *
     * @param bool $cron true = cronjob is used
     * @return void
     */
    public function importTierPrices($cron = false)
    {
        $data = $this->_getImportArray('artikel_Sp', $cron);

        if (count($data) > 0) {
            $this->setTierPriceImportData($data);
            unset($data);

            $tierPriceData = $this->getTierPriceData();
            $netPriceData = $this->getNettoPriceData();
            
            if (count($tierPriceData)) {
                $tierPriceToImport = array();
                $lastTierPrice = "";
                foreach($tierPriceData as $tierPrice) {
                    if (!isset($tierPrice["sku"]) || !$tierPrice["sku"]) {
                        $tierPrice["sku"] = $lastTierPrice;
                    }
                    else {
                        $lastTierPrice = $tierPrice["sku"];
                    }
                    if (!isset($tierPriceToImport[$tierPrice["sku"]])) {
                        $tierPriceToImport[$tierPrice["sku"]] = array();
                    }

                    $tierPriceToImport[$tierPrice["sku"]][] = array(
                        "cust_group" => $tierPrice["_tier_price_customer_group"],
                        "price_qty" => $tierPrice["_tier_price_qty"],
                        "price" => $tierPrice["_tier_price_price"],
                        "website_id" => 0,
                        "tier_price_start_date" => $tierPrice["_tier_price_start_date"],
                        "tier_price_end_date" => $tierPrice["_tier_price_end_date"]
                    );
                }

                if (count($tierPriceToImport)) {
                    foreach($tierPriceToImport as $sku => $tierData) {
                        $product = Mage::getModel('catalog/product');
                        $product->load($product->getIdBySku($sku));
                        
                        if ($product->getId()) {
                            $product->setTierPrice($tierData);

                            if (count($netPriceData)) {
                                foreach($netPriceData as $netPrice) {
                                    if (isset($netPrice["sku"]) && $netPrice["sku"] && $netPrice["sku"] == $sku) {
                                        $product->setProvetNettoprice((isset($netPrice["provet_nettoprice"]) && $netPrice["provet_nettoprice"] == "yes" ? 1 : 0));
                                    }
                                }
                            }
                            
                            $product->save();
                        }
                    }
                }
            }
            
            
            
            
            
            $this->_updatedProducts = count($tierPriceToImport);

            // FastSimpleImport
            //$this->_productImport($this->getTierPriceData());
            //$this->_productImport($this->getNettoPriceData());
            
            //$this->_archiveFtpFile("tierprices");
        }
    }

    /**
     * Import matrix table
     *
     * @param bool $cron true = cronjob is used
     * @return void
     */
    public function translateWebgroupCode($cron = false)
    {
        $data = $this->_getImportArray('webGrpCode', $cron);

        if (count($data) > 0) {
            $this->_webgroupCodeTranslate($data);
            
            $this->_archiveFtpFile("webgroupcode");
        }
    }

    /**
     * Get all allowed file extensions
     *
     * @return array
     */
    public function getAllowedFileExtensions()
    {
        return $this->_allowedFileExtensions;
    }
    
    protected function _getMainCategory($data) {
        $mainCategory = $data['Hauptkategorie'];
        
        switch($mainCategory) {
            case "PHARMA":
                $mainCategory = $this->_parentCategory . "/Pharma / Ergänzungsfuttermittel";
                break;
            case "CONSUM":
                $mainCategory = $this->_parentCategory . "/Bedarfsartikel";
                break;
            case "EQUIP":
                $mainCategory = $this->_parentCategory . "/Equipment";
                break;
            case "ACCESSORIES":
                $mainCategory = $this->_parentCategory . "/Accessories";
                break;
            case "PETFOOD":
                $mainCategory = $this->_parentCategory . "/PetFood / Snacks";
                break;
            case "VARIA":
                $mainCategory = $this->_parentCategory . "/Varia";
                break;
        }
        
        return $mainCategory;
    }

    /**
     * Prepare products data for FastSimpleImport
     *
     * @param array $products Products from csv file
     * @return void
     */
    public function setProductImportData($products)
    {
        foreach ($products as $product) {
            if (isset($product['ArtNr'])) {
                $sku = $product['ArtNr'];
            
                $mainCategory = $this->_getMainCategory($product);

                $categories = $this->getCategories($product['WebGrpCode'], $mainCategory, $product);

                if ($categories) {
                    if (!in_array($mainCategory, $categories)) {
                        $categories = array_merge($categories, array($mainCategory));
                    }
                }
                else {
                    $categories = $mainCategory;
                }
                
                if ((int) $product['InAktion'] == 1) {
                    $categories = array_merge($categories, array($this->_parentCategory . "/Promo / Outlet"));
                }

                $isNew = !((bool)Mage::getSingleton('catalog/product')->getIdBySku($sku));

                $counter = count($this->_products);
                $this->_products[$counter] = array(
                    'sku' => $sku,
                    '_type' => 'simple',
                    '_attribute_set' => 'Default',
                    '_product_websites' => 'base',
                    '_category' => $categories,
                    'name' => $product['BezeichnungD'],
                    'description' => '',
                    'short_description' => !empty($product['Kurztext']) ? trim($product['Kurztext']) : ' ',
                    'price' => $product['VP'],
                    'weight' => $product['Bruttogewicht'],
                    'provet_net_weight' => $product['Nettogewicht'],
                    'provet_own_brand' => $this->_getBooleanAttribute($product['Eigenmarke']),
                    'provet_in_action' => $this->_getBooleanAttribute($product['InAktion']),
                    'provet_action_label' => $product['AktionBez'],
                    'provet_webgrpcode' => $product['WebGrpCode'],
                    'provet_sort' => $this->_getSortPrio($product['SortPrio']),
                    'provet_warenbezug' => $this->_getBooleanAttribute($product['WarenBezug']),
                    //if mwst != 8% (CH default), send mail to team.gecko@snowflake.ch so they do manually add this mwst
                    'tax_class_id' => $this->_getTaxClassId($product['MWStSatz']),

                    //Zusatzinformationen = no logic only text
                    'provet_reg_no' => $product['RegNr'],
                    'provet_atc_vet' => $product['ATCVEt'],
                    'provet_charge_cat' => !empty($product['AbgabeKat']) ? trim($product['AbgabeKat']) : '',
                    'provet_ean' => !empty($product['EAN_GTIN']) ? trim($product['EAN_GTIN']) : '',
                    'provet_ref_katalog' => trim($product['RefKatalog']),
                    'provet_stock_items' => $this->_getBooleanAttribute($product['Lagerartikel']),
                    'provet_vksperre' => $this->_getBooleanAttribute($product['VkSperre'])
                );

                // status
                if ($isNew) {
                    $this->_products[$counter]['status'] = Mage_Catalog_Model_Product_Status::STATUS_ENABLED;
                }

                // foreign language
                if ($product['BezeichnungF'] != '') {
                    $this->_products[$counter]['_store'] = array('de', 'fr');
                    $this->_products[$counter]['name'] = array($product['BezeichnungD'], $product['BezeichnungF']);
                }

                if (Mage::getStoreConfig('snowflake_importexport/ftp_access/delete_files')) {
                    // images
                    if (isset($product['Bilddatei'])) {
                        $this->_setMediaGallery($sku, $product['Bilddatei'], $counter);
                    }

                    // file attachment
                    if (isset($product['Textdatei'])) {
                        $this->_setProductFiles($sku, $product['Textdatei'], $counter);
                    }

                    // file attachment for promotion
                    if (isset($product['PDFAktion']) && $product['PDFAktion'] != '') {
                        $this->_setProductFiles($sku, $product['PDFAktion'], $counter, 1);
                    }
                } else {
                    if (isset($product['Bilddatei'])) {
                        $this->_setMediaGallery($sku, $product['Bilddatei'], $counter);
                    }
                    
                    // file attachment
                    if (($isNew) && isset($product['Textdatei'])) {
                        $this->_setProductFiles($sku, $product['Textdatei'], $counter);
                    }

                    // file attachment for promotion
                    if (($isNew) && isset($product['PDFAktion']) && $product['PDFAktion'] != '') {
                        $this->_setProductFiles($sku, $product['PDFAktion'], $counter, 1);
                    }
                }
            }
        }

        if ($this->newMwstTypes) {
            $mwstTypesArray = array_unique($this->newMwstTypes);
            $mwstTypesJson = json_encode($mwstTypesArray);

            $mail = new Zend_Mail('UTF8');
            $mail->setBodyHtml('Neue Steuerklasse beim Henryschein Shop, Prozentsatz ist: ' . $mwstTypesJson . ' bitte bei Funktion _getTaxClassId hinzufügen sowie auch im Magento Backend!');
            $mail->setFrom('info@henryschein.ch', 'Henryschein');
            $mail->addTo('team.gecko@snowflake.ch', 'Team Gecko');
            $mail->setSubject('Henryschein Shop, MwSt hinzufügen.');
            $mail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);

            try {
                $mail->send();
            } catch (Exception $ex) {
                Mage::getSingleton('core/session')->addError('Unable to send email. Sample of a custom notification error from Productimport');
            }
        }
    }

    /**
     * Prepare product stock data for FastSimpleImport
     *
     * @param array $products Products from csv file
     * @return void
     */
    public function setProductStockImportData($productStocks)
    {
        foreach($productStocks as $stock) {
            $sku     = $stock['ArtikelNr'];
            $isExist = ((bool) Mage::getSingleton('catalog/product')->getIdBySku($sku));

            if ($isExist) {
                $this->_productStocks[] = array(
                    'sku' => $sku,
                    '_type' => 'simple',
                    '_attribute_set' => 'Default',
                    '_product_websites' => 'base',
                    'delivery_time' => ($stock['Lieferzeit'] ? $stock['Lieferzeit'] : " "),
                    'provet_stock' => $this->_getTrafficLightAttribute($stock['Ampelwert']),
                    'provet_expiration_date' => $stock['ExpDate'],
                );
            }
        }
    }

    /**
     * Prepare products data for FastSimpleImport
     *
     * @param array $products Products from csv file
     * @return void
     */
    public function setReplacementItems($products)
    {
        foreach($products as $product) {
            $sku   = $product['ArtNr'];

            $this->_replacement[] = array(
                'sku' => $sku,
                '_type' => 'simple',
                '_attribute_set' => 'Default',
                '_product_websites' => 'base',
                '_links_replacement_sku' => $product['Substitute'],
            );
        }
    }

    /**
     * Prepare product tier price data for FastSimpleImport
     *
     * @param array $tierPrices Tierprice from csv file
     * @return void
     */
    public function setTierPriceImportData($tierPrices)
    {
        $sku  = array();

        foreach($tierPrices as $data) {
            $isExist = (bool) Mage::getSingleton('catalog/product')->getIdBySku($data['ArtNr']);

            //set nettopreis for product
            $this->_productNettoPrice[] = array(
                'sku' => $data['ArtNr'],
                '_type' => 'simple',
                '_attribute_set' => 'Default',
                '_product_websites' => 'base',
                'provet_nettoprice' => $this->_getBooleanAttribute($data['Nettopreis']),
            );


            if($isExist) {
                if (!$this->_isCustomerGroupExist($data['PrGrp'])) {
                    continue;
                }

                if ($data['Währung'] != 'CHF') {
                    continue;
                }

                if ($data['Preis'] == 0) {
                    continue;
                }

                if ($data['AbMenge'] == 0) {
                    continue;
                }

                if (in_array($data['ArtNr'], $sku)) {
                    $data['ArtNr'] = null;
                } else {
                    $sku[] = $data['ArtNr'];
                }

                $this->_tierPrice[] = array(
                    'sku' => $data['ArtNr'],
                    '_tier_price_website' => 'all',
                    '_tier_price_customer_group' => $this->_getCustomerGroup($data['PrGrp']),
                    '_tier_price_qty' => $data['AbMenge'],
                    '_tier_price_price' => $data['Preis'],
                    '_tier_price_start_date' => (!empty($data['GültigVon'])) ? $this->_formatDateExplode($data['GültigVon']) : NULL,
                    '_tier_price_end_date' => (!empty($data['GültigBis'])) ? $this->_formatDateExplode($data['GültigBis']) : NULL,
                );
            }
        }
    }

    protected function _formatDateExplode($date) {
        $date = explode(".", $date);

        return $date[2] . "-" . $date[1] . "-" . $date[0];
    }

    /**
     * Prepare product group price data for FastSimpleImport
     *
     * @param array $groupPrices Groupprice from csv file
     * @return void
     */
    public function setGroupPriceImportData($groupPrices)
    {
        $sku = array();

        foreach($groupPrices as $data) {
            $isExist = (bool)Mage::getSingleton('catalog/product')->getIdBySku($data['ArtNr']);
            if($isExist) {
                if (!$this->_isCustomerGroupExist($data['PrGrp'])) {
                    continue;
                }

                if ($data['Währung'] != 'CHF') {
                    continue;
                }

                if ($data['Preis'] == 0) {
                    continue;
                }

                if (in_array($data['ArtNr'], $sku)) {
                    $data['ArtNr'] = null;
                } else {
                    $sku[] = $data['ArtNr'];
                }

                $this->_groupPrice[] = array(
                    'sku' => $data['ArtNr'],
                    '_group_price_website' => 'all',
                    '_group_price_customer_group' => $this->_getCustomerGroup($data['PrGrp']),
                    '_group_price_price' => $data['Preis'],
                    '_group_price_start_date' => (!empty($data['GültigAb'])) ? $this->_formatDate($data['GültigAb']) : NULL,
                    '_group_price_end_date' => (!empty($data['GültigBis'])) ? $this->_formatDate($data['GültigBis']) : NULL,
                );
            }
        }
    }

    /**
     * Prepare matrix data for Import
     *
     * @param array $matrix Matrix from csv file
     * @return void
     */
    public function setMatrixData($matrix)
    {
        foreach($matrix as $data) {
            if ($this->_getMatrixProductGroupId($data['matrixGrpArtikel']) != '') {
                $this->_matrix[] = array(
                    //'matrixCustomerGroup' => $data['Mandant'] . $data['matrixGrpKunde'],
                    'matrixCustomerGroup' => $data['matrixGrpKunde'],
                    'matrixProdutGroup' => $this->_getMatrixProductGroupId($data['matrixGrpArtikel']),
                    'available' => $data['verfügbar'],
                );
            }
        }
    }

    /**
     * Prepare up-selling product data for FastSimpleImport
     *
     * @param array $upsell Generated array from _getUpsell()
     * @param int $counter
     * @return void
     */
    protected function _setUpsellData($upsell = array(), $counter = 0)
    {
        $this->_products[$counter]['_links_upsell_sku'] = $upsell;
        $this->_products[$counter]['_links_upsell_position'] = array_keys($upsell);
    }

    /**
     * Prepare product media gallery data for FastSimpleImport
     *
     * @param string $sku
     * @param string $media
     * @param int $key
     * @return void
     */
    protected function _setMediaGallery($sku, $media = '', $key = 0)
    {
        //$this->_emptyImages($sku);
        
        $images = array();
        if ($media != '') {
            $images = explode(',', $media);
        } else {
            //$images = $this->_getImages($sku);
            $this->_emptyImages($sku);
        }

        $counter = 0;
        if (count($images)) {
            foreach($images as $imgKey => $image) {
                $parts = explode('.', $image);
                if (!in_array(strtolower(array_pop($parts)), $this->_allowedImageExtensions)) {
                    continue;
                }

                if (!is_file($this->_getImportDir() . DS . $image)) {
                    continue;
                }

                $this->_deleteDuplicatePicture($image);

                $this->_products[$key]['_media_attribute_id'][] = Mage::getSingleton('catalog/product')->getResource()->getAttribute('media_gallery')->getAttributeId();
                $this->_products[$key]['_media_image'][] = $image;
                $this->_products[$key]['_media_is_disabled'][] = max(0, 1 - $counter);
                $this->_products[$key]['_media_position'][] = $counter;

                if ($counter == 0) {
                    $this->_products[$key]['image'] = $image;
                    $this->_products[$key]['small_image'] = $image;
                    $this->_products[$key]['thumbnail'] = $image;
                }

                $counter++;
            }
        }
    }
    
    protected function _emptyImages($sku) {
        if ($sku) {            
            $product = Mage::getModel('catalog/product');
            $product->load($product->getIdBySku($sku));

            if ($product->getId()) {
                $mediaApi = Mage::getModel("catalog/product_attribute_media_api");
                $items = $mediaApi->items($product->getId());
                foreach($items as $item) {
                    $mediaApi->remove($product->getId(), $item['file']);
                }
            }
        }
    }
    
    protected function _deleteDuplicatePicture($image) {
        $pictureFolder = Mage::getBaseDir("media") . DS . "catalog" . DS . "product" . DS;
        $importPicture = Mage::getBaseDir("media") . DS . "import" . DS . $image;
        
        // -- Build image path in catalog --
        $picture = $pictureFolder . $image[0] . DS . $image[1] . DS . $image;

        if (file_exists($picture) && file_exists($importPicture)) {
            unlink($picture);
        }
        
        return true;
    }

    /**
     * Prepare categories for category FastSimpleImport
     *
     * @param array $data Product data from csv file
     * @return array
     */
    protected function _setProductCategories($data)
    {
        $productCategories = array();

        //we need to make the kategories with de WebGrpCode and the labels CSV webgrpCode.csv

        if (trim($data['Hauptkategorie']) != '') {
            array_push($productCategories, $data['Hauptkategorie']);
        }

        if (trim($data['Hauptkategorie']) != '') {
            array_push($productCategories, $data['Hauptkategorie']);
        }

        if (trim($data['Hauptkategorie']) != '') {
            array_push($productCategories, $data['Hauptkategorie']);
        }



        $productCategories = array_values(array_unique($productCategories));

        $categories = array_diff($productCategories, $this->_categories);
        $this->_categories = array_merge($this->_categories, $categories);

        return $productCategories;
    }

    /**
     * Prepare product files data for FastSimpleImport
     *
     * @param string $sku
     * @param string $doc
     * @param int $key
     * @param bool $promotion true = it's a promotion file
     * @return void
     */
    protected function _setProductFiles($sku, $doc = '', $key = 0, $promotion = 0)
    {
         if ($doc != '') {
            $files = explode(',', $doc);
        } else {
            $files = $this->_getFiles($sku);
        }

        $counter = 0;
        foreach($files as $docKey => $file) {
            $urlParts = parse_url($file);
            if (!isset($urlParts["scheme"])) {
                if (strpos($file, 'www.') !== false) {
                    $url = 'http://' . $file;
                } else {
                    $url = '';
                }
            } else {
                $url = $file;
            }

            if ($url != '') {
                // url
                $url = filter_var($url, FILTER_SANITIZE_URL);

                if (!filter_var($url, FILTER_VALIDATE_URL)) {
                    continue;
                }

                $this->_products[$key]['_file_type'][] = 'url';
            } else {
                // file
                $parts = explode('.', $file);
                if (!in_array(strtolower(array_pop($parts)), $this->_allowedFileExtensions)) {
                    continue;
                }

                if (!is_file($this->_getImportDir() . DS . $file)) {
                    continue;
                }

                $this->_products[$key]['_file_type'][] = 'file';
            }

            $this->_products[$key]['_file_title'][] = '';
            $this->_products[$key]['_file_name'][] = $file;
            $this->_products[$key]['_file_position'][] = $counter;
            $this->_products[$key]['_file_promotion'][] = $promotion;

            $counter++;
        }
    }

    /**
     * Check if customer group from price group code exist
     *
     * @param string $priceGroupCode
     * @return bool
     */
    protected function _isCustomerGroupExist($priceGroupCode)
    {
        $group = Mage::getModel('customer/group');
        $group->load($priceGroupCode, 'customer_group_code');

        $result = false;
        if ($group->getId()) {
            $result = true;
        }

        return $result;
    }

    /**
     * Prepare product upsell data
     *
     * @param array $data Product data from csv file
     * @return array
     */
    protected function _getProductUpsell($data)
    {
        $productUpsell = array();

        if (trim($data['xArtNr1']) != '') {
            array_push($productUpsell, $data['xArtNr1']);
        }

        if (trim($data['xArtNr2']) != '') {
            array_push($productUpsell, $data['xArtNr2']);
        }

        if (trim($data['xArtNr3']) != '') {
            array_push($productUpsell, $data['xArtNr3']);
        }

        if (trim($data['xArtNr4']) != '') {
            array_push($productUpsell, $data['xArtNr4']);
        }

        $productUpsell = array_values(array_unique($productUpsell));

        return $productUpsell;
    }

    /**
     * Get tax class id from percentage value
     *
     * @param string $tax percentage tax value
     * @return integer
     */
    protected function _getTaxClassId($tax)
    {
        switch ($tax) {
            case '8':
                $taxId = 3;
                break;
            case '8.00':
                $taxId = 3;
                break;
            case '7.7':
                $taxId = 3;
                break;
            case '7.70':
                $taxId = 3;
                break;
            case '2.5':
                $taxId = 2;
                break;
            case '0':
                $taxId = 0;
                break;
            case '':
                $taxId = 0;
            default:
                $this->newMwstTypes[] = $tax;
                $taxId = 3;

                break;
        }

        return $taxId;
    }

    /**
     * Get matrix product group id
     *
     * @param string $value matrix product group name
     * @return integer
     */
    protected function _getMatrixProductGroupId($value)
    {
        $code = 'provet_matrix_group';

        $entityTypeId  = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();
        $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($code)->setEntityTypeFilter($entityTypeId)->getFirstItem();
        $attributeId   = $attributeInfo->getAttributeId();
        $attribute     = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);

        return $attribute->getSource()->getOptionId($value);
    }

    /**
     * Get images by sku for product import
     *
     * @param string $sku
     * @return array
     */
    protected function _getImages($sku)
    {
        $files    = glob($this->_getImportDir() . DS . $sku . '*.*');

        $images = array();
        foreach ($files as $file) {
            $file  = basename($file);
            $parts = explode('.', $file);

            if (!in_array(strtolower(array_pop($parts)), $this->_allowedImageExtensions)) {
                continue;
            }

            if (count($parts) > 2) {
                continue;
            }

            if (count($parts) == 1) {
                if ($parts[0] != $sku) {
                    continue;
                } else {
                    array_unshift($images, $file);
                }
            } else {
                $images[] = $file;
            }
        }

        return $images;
    }

    /**
     * Get files by sku for product import
     *
     * @param string $sku
     * @return array
     */
    protected function _getFiles($sku)
    {
        $files    = glob($this->_getImportDir() . DS . $sku . '*.*');

        $attachment = array();
        foreach ($files as $file) {
            $file  = basename($file);
            $parts = explode('.', $file);

            if (!in_array(strtolower(array_pop($parts)), $this->_allowedFileExtensions)) {
                continue;
            }

            if (count($parts) > 2) {
                continue;
            }

            if (count($parts) == 1) {
                if ($parts[0] != $sku) {
                    continue;
                } else {
                    array_unshift($attachment, $file);
                }
            } else {
                $attachment[] = $file;
            }
        }

        return $attachment;
    }

    protected function _getImportDir()
    {
        return Mage::getConfig()->getOptions()->getMediaDir() . '/import';
    }

    /**
     * @param $webGroupCode
     * @return mixed
     */
    protected function getCategories($webGroupCode, $mainCategory, $product){
        $categories = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect('*');

        foreach ($categories as $category) {
            

            //if no name for categorie, then skip
            if(!$category->getName()) continue;

            $categoryWebGroupCode = Mage::getResourceModel('catalog/category')
                                            ->getAttributeRawValue( $category->getId(),
                                                "snowflake_webgroupcode1",
                                                Mage::app()->getStore()->getId());

            if($categoryWebGroupCode == $webGroupCode) {
                $path = explode("/", $category->getPath());
                
                $checkCategory = Mage::getModel("catalog/category")->load($path[2]);
                
                $name = $checkCategory->getName();
                
                $checkCategory = Mage::getModel("catalog/category")->load($path[3]);
                
                $name = $name ."/".$checkCategory->getName();
                
                if ($name != $mainCategory) continue;

                $productCategory['path'] = $category->getPath();
                break;
            }
        }
        
        if (isset($productCategory['path']) && $productCategory['path']) {
            //remove the first three categories because there are some default categories
            $allProductCategoryIds = explode('/',$productCategory['path']);
            $productCategoryIds = array_slice($allProductCategoryIds, 2);

            $startCatregory = $this->getCategorieNameForFastSimpleImport($productCategoryIds[0]);
            $categoryPathForFastSimpleImport[] = $startCatregory . '/' . $this->getCategorieNameForFastSimpleImport($productCategoryIds[1]);

            if(isset($productCategoryIds[1]) && $productCategoryIds[1] && isset($productCategoryIds[2]) && $productCategoryIds[2]){
                $categoryPathForFastSimpleImport[]  = $categoryPathForFastSimpleImport[0] . '/' . $this->getCategorieNameForFastSimpleImport($productCategoryIds[2]);
            }
            if(isset($productCategoryIds[2]) && $productCategoryIds[2] && isset($productCategoryIds[3]) && $productCategoryIds[3]){
                $categoryPathForFastSimpleImport[]  = $categoryPathForFastSimpleImport[1] . '/' . $this->getCategorieNameForFastSimpleImport($productCategoryIds[3]);
            }
            if(isset($productCategoryIds[3]) && $productCategoryIds[3] && isset($productCategoryIds[4]) && $productCategoryIds[4]){
                $categoryPathForFastSimpleImport[]  = $categoryPathForFastSimpleImport[2] . '/' . $this->getCategorieNameForFastSimpleImport($productCategoryIds[4]);
            }

            return $categoryPathForFastSimpleImport;
        }
        
        return null;
    }

    /**
     * @param $categoryId
     * @return mixed
     */
    protected function getCategorieNameForFastSimpleImport($categoryId){
        $category = Mage::getModel('catalog/category')
                        ->setStoreId(0)
                        ->load($categoryId);
        $categoryName = $category->getName();

        return $categoryName;
    }
}
  
