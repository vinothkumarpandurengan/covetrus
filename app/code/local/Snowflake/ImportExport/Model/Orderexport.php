<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Project: www
 * Date: 30.03.2015
 * Time: 11:22
 */

class Snowflake_ImportExport_Model_Orderexport
{
    public function export(Mage_Sales_Model_Order $order)
    {
        $dir = Mage::getStoreConfig('snowflake_importexport/general/export_csv_directory');
        if (!substr($dir, -1) == DS) {
            $dir .= DS;
        }

        if (!is_dir($dir)) {
            mkdir($dir);
        }

        $filename = 'order_' . $order->getIncrementId() . '.csv';

        $handle = fopen($dir . $filename, 'w');

        $orderData = $this->_getOrderData($order);
        foreach($orderData as $data) {
            fputcsv($handle, $data, ";", '"');
        }

        fclose($handle);
    }


    protected function _getHeader()
    {
        return array(
            'order_number',
            'order_date',
            'item_sku',
            'item_name',
            'item_qty',
            'item_price',
            'item_row_price',
            'comment',
            'customer_number',
            'billing_firstname',
            'billing_lastname',
            'billing_street',
            'billing_postcode',
            'billing_city',
            'billing_region',
            'billing_land',
            'shipping_firstname',
            'shipping_lastname',
            'shipping_street',
            'shipping_postcode',
            'shipping_city',
            'shipping_region',
            'shipping_land',
            'payment_method',
            'payment_amount',
            'shipping_method',
            'shipping_amount',
        );
    }

    protected function _getOrderData(Mage_Sales_Model_Order $order)
    {
        $header = $this->_getHeader();

        $data = array(
            $header,
        );

        $billingAddress = $order->getBillingAddress();
        $shippingAddress = $order->getShippingAddress();
        foreach ($order->getAllItems() as $item) {
            if ($item->getParentItemId()) {
                continue;
            }
            
            $customerNote = $order->getCustomerNote();
            if ($customerNote) {
                $customerNote = explode("\n", $customerNote);
                if (count($customerNote)) {
                    foreach($customerNote as $i => $value) {
                        if (!$value) {
                            unset($customerNote[$i]);
                        }
                    }
                    $customerNote = implode(" - ", $customerNote);
                }
            }
            
            $data[] = array(
                $order->getIncrementId(),
                $order->getCreatedAt(),
                $item->getSku(),
                $item->getName(),
                $item->getQtyOrdered(),
                $item->getPrice(),
                $item->getRowTotal(),
                $customerNote,
                $order->getCustomerNumber(),
                $billingAddress->getFirstname(),
                $billingAddress->getLastname(),
                $billingAddress->getStreetFull(),
                $billingAddress->getPostcode(),
                $billingAddress->getCity(),
                $billingAddress->getRegion(),
                $billingAddress->getCountry(),
                $shippingAddress->getFirstname(),
                $shippingAddress->getLastname(),
                $shippingAddress->getStreetFull(),
                $shippingAddress->getPostcode(),
                $shippingAddress->getCity(),
                $shippingAddress->getRegion(),
                $shippingAddress->getCountry(),
                $order->getPayment()->getMethodInstance()->getTitle(),
                $order->getPayment()->getAmountOrdered(),
                $order->getShippingDescription(),
                $order->getShippingAmount(),
            );
        }

        return $data;
    }
}