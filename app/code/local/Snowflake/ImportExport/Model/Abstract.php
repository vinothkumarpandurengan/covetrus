<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 23.01.2015
 * Time: 16:52
 */
class Snowflake_ImportExport_Model_Abstract extends Mage_Core_Model_Abstract
{
    protected $_categories    = array();
    protected $_products      = array();
    protected $_productStocks = array();
    protected $_replacement   = array();
    protected $_tierPrice     = array();
    protected $_groupPrice    = array();
    protected $_customers     = array();
    protected $_customerPrice = array();
    protected $_discounts     = array();
    protected $_newCustomers  = array();
    protected $_matrix        = array();
    protected $_productNettoPrice   = array();

    protected $_createdProducts = 0;
    protected $_updatedProducts = 0;
    protected $_deletedProducts = 0;

    protected $_createdCustomers = 0;
    protected $_updatedCustomers = 0;
    protected $_deletedCustomers = 0;

    protected $_createdMatrixTables = 0;

    protected $replace = array(' ', '_', '.', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '=', '+', '?', '/', '<', '>', '|', '`', '~');
    const DEFAULTSHOPROOTCATREOGYPATH = "1/2/13";
    const DEFAULTSHOPROOTCATREOGYID = "13";
    const DEFAULTSTOREID = "0";

    protected function _construct()
    {
        // prevent program crash by over 10'000 products
        set_time_limit(0);
        ini_set('memory_limit', '1024M');
        ini_set('default_socket_timeout', 6000);
        ini_set('max_execution_time', 6000);
        ini_set('max_input_time', 6000);
    }
    
    /**
     * @return int
     */
    public function getUpdatedProducts()
    {
        return $this->_updatedProducts;
    }

    /**
     * @return int
     */
    public function getCreatedProducts()
    {
        return $this->_createdProducts;
    }

    /**
     * @return int
     */
    public function getDeletedProducts()
    {
        return $this->_deletedProducts;
    }

    /**
     * @return int
     */
    public function getUpdatedCustomers()
    {
        return $this->_updatedCustomers;
    }

    /**
     * @return int
     */
    public function getCreatedCustomers()
    {
        return $this->_createdCustomers;
    }

    /**
     * @return int
     */
    public function getDeletedCustomers()
    {
        return $this->_deletedCustomers;
    }

    /**
     * @return array
     */
    public function getNewCustomers()
    {
        return $this->_newCustomers;
    }

    /**
     * @return int
     */
    public function getCreatedMatrixTables()
    {
        return $this->_createdMatrixTables;
    }

    /**
     * @return array
     */
    public function getProductData()
    {
        return $this->_products;
    }

    /**
     * @return array
     */
    public function getProductStocksData()
    {
        return $this->_productStocks;
    }

    /**
     * @return array
     */
    public function getReplacementItemsData()
    {
        return $this->_replacement;
    }

    /**
     * @return array
     */
    public function getTierPriceData()
    {
        return $this->_tierPrice;
    }

    /**
     * @return array
     */
    public function getNettoPriceData()
    {
        return $this->_productNettoPrice;
    }

    /**
     * @return array
     */
    public function getGroupPriceData()
    {
        return $this->_groupPrice;
    }

    /**
     * @return array
     */
    public function getMatrixData()
    {
        return $this->_matrix;
    }

    /**
     * @return array
     */
    public function getCustomerData()
    {
        return $this->_customers;
    }

    /**
     * @return array
     */
    public function getCustomerPriceData()
    {
        return $this->_customerPrice;
    }

    /**
     * @return array
     */
    public function getDiscountImportData()
    {
        return $this->_discounts;
    }


    protected function _productImport($data)
    {
        // FastSimpleImport
        $import = Mage::getSingleton('fastsimpleimport/import');
        try {
            $import
                ->setUseNestedArrays(true)
//                ->setDropdownAttributes(array(
//                    'provet_matrix_group',
//                    'color', // Configurable Product
//                    'size', // Configurable Product
//                ))
                ->setMultiselectAttributes(array(
                    'provet_webgrpcode',
                ))
                //->dryrunProductImport($data);
                ->processProductImport($data);
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'fastsimpleimport.log');
            Mage::log($import->getErrorMessages(), null, 'fastsimpleimport.log');
            Mage::log($data, null, 'fastsimpleimport.log');
            throw $e;
        }
        $importedSkus = array_diff(
            array_keys($import->getEntityAdapter()->getNewSku()),
            array_keys($import->getEntityAdapter()->getOldSku())
        );

        $this->_createdProducts = sizeof($importedSkus);
        $this->_updatedProducts = sizeof($import->getEntityAdapter()->getNewSku()) - $this->_createdProducts;
    }

    protected function _customerImport($data)
    {
        // FastSimpleImport
        $import = Mage::getSingleton('fastsimpleimport/import');
        try {
            $import
                ->processCustomerImport($data);
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'fastsimpleimport.log');
            Mage::log($import->getErrorMessages(), null, 'fastsimpleimport.log');
            Mage::log($data, null, 'fastsimpleimport.log');
            throw $e;
        }

        $importedCustomers = array_diff(
            array_keys($import->getEntityAdapter()->getNewCustomers()),
            array_keys($import->getEntityAdapter()->getOldCustomers())
        );

        $this->_createdCustomers = sizeof($importedCustomers);
        $this->_updatedCustomers = sizeof($import->getEntityAdapter()->getNewCustomers()) - $this->_createdCustomers;
    }

    protected function _matrixImport($data)
    {
        if (is_array($data)) {
            Mage::getConfig()->saveConfig('snowflake_matrix/settings/config', serialize($data));

            $this->_createdMatrixTables = sizeof($data);
        }
    }

    /**
     * Save quantity discount settings to db
     *
     * @params array $discounts
     */
    protected function _discountImport($discounts)
    {
        try {
            foreach($discounts as  $rowNum => $rowData) {
                $discountModel = Mage::getModel('snowflake_quantitydiscount/rule');

                $validateResult = $discountModel->validateData(new Varien_Object($rowData));
                if ($validateResult) {
                    $discountCheck = Mage::getModel('snowflake_quantitydiscount/rule')->getCollection()
                        ->addFieldToFilter('client', $rowData['client'])
                        ->addFieldToFilter('product_id', $rowData['product_id'])
                        ->addFieldToFilter('discount_group_id', $rowData['discount_group_ids'])
                        ->load();

                    $discountCheckArr = $discountCheck->getData();
                    if(count($discountCheckArr) > 0) {
                        foreach($discountCheckArr as $data) {
                            $discountModel = $discountModel->load($data['rule_id']);
                            $discountModel->addData($rowData);
                        }
                    } else {
                        $discountModel->setData($rowData);
                    }

                    $discountModel->loadPost($discountModel->getData());
                    $discountModel->save();
                }

            }
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'fastsimpleimport.log');
            Mage::log($import->getErrorMessages(), null, 'fastsimpleimport.log');
            Mage::log($discounts, null, 'fastsimpleimport.log');
            throw $e;
        }
    }

    /**
     * import webgroupcode to categories
     *
     * @param $codes
     */
    protected function _webgroupCodeTranslate($codes)
    {
        $mainCategories = Mage::getModel("catalog/category")->getCollection()->addFieldToFilter("level", 3)->addFieldToFilter("parent_id", 13)->addFieldToFilter("snowflake_webgroupcode1", array("in" => array("PHARMA", "CONSUM", "EQUIP", "ACCESSORIES", "PETFOOD", "VARIA")));

        if (count($mainCategories)) {
            foreach($mainCategories as $mainCategory) {
                if (is_array($codes)) {
                    $firstLevelCategories    = array();
                    $secondLevelCategories   = array();
                    $thirdLevelCategories    = array();

                    foreach ($codes as $code){
                        $category = Mage::getModel('catalog/category');
                        $category->setStoreId(self::DEFAULTSTOREID);
                        $categoryNameOrigin =  $code['Bezeichnung'];

                        $urlKeyName = $this->cleanUrlName($categoryNameOrigin);

                        $existCategoryCollection = Mage::getResourceModel('catalog/category_collection')
                            ->addFieldToFilter('snowflake_webgroupcode1', $code['WebGrpCode'])->addFieldToFilter("path", array("like" => $mainCategory->getPath() . "%"));

                        if (count($existCategoryCollection)) {
                            foreach($existCategoryCollection as $existCategory) {
                                $existCategoryId = $existCategory->getId();

                                if($existCategoryId){
                                    $this->updateCategory($existCategoryId, $categoryNameOrigin, $urlKeyName);

                                }
                            }

                            continue;
                        }

                        if(strlen($code['WebGrpCode']) == 1) $firstLevelCategories[]    = $code;
                        if(strlen($code['WebGrpCode']) == 2) $secondLevelCategories[]   = $code;
                        if(strlen($code['WebGrpCode']) == 3) $thirdLevelCategories []   = $code;
                    }

                    //sort array alphabetical
                    usort($firstLevelCategories, function($a, $b) { return $a['WebGrpCode'] === $b['WebGrpCode'] ? 0 : $a['WebGrpCode'] > $b['WebGrpCode']; });
                    usort($secondLevelCategories, function($a, $b) { return $a['WebGrpCode'] === $b['WebGrpCode'] ? 0 : $a['WebGrpCode'] > $b['WebGrpCode']; });
                    usort($thirdLevelCategories, function($a, $b) { return $a['WebGrpCode'] === $b['WebGrpCode'] ? 0 : $a['WebGrpCode'] > $b['WebGrpCode']; });

                    foreach ($firstLevelCategories as $firstLevelCategory ){
                        $this->insertCategory($firstLevelCategory, $mainCategory);
                    }

                    foreach ($secondLevelCategories as $secondLevelCategory ){
                        $this->insertCategory($secondLevelCategory, $mainCategory);
                    }

                    foreach ($thirdLevelCategories as $thirdLevelCategory ){
                        $this->insertCategory($thirdLevelCategory, $mainCategory);
                    }
                }
            }
        }
    }

    protected function _formatDate($date)
    {
        $date = new Zend_Date($date);
        return $date->toString(Varien_Date::DATE_INTERNAL_FORMAT);
    }

    /**
     * Format percent: 1 = 100%
     *
     * @param   float $percent
     * @return  int
     */
    protected function _formatPercent($percent)
    {
        return intval(100 * $percent);
    }

    /**
     * Get attribute name from boolean value
     *
     * @param bool $value boolean value from Yes/No dropdown
     * @return string
     */
    protected function _getBooleanAttribute($value) {
        if ($value == 1) {
            return Mage::helper('snowflake_importexport')->__('yes');
        } else {
            return Mage::helper('snowflake_importexport')->__('no');
        }
    }

    /**
     * Get attribute name from boolean value
     *
     * @param bool $value boolean value from Yes/No dropdown
     * @return string
     */
    protected function _getSortPrio($value) {
        switch ($value) {
            case 0:
                $result = Mage::helper('snowflake_importexport')->__('Others');
                break;
            case 1:
            default:
                $result = Mage::helper('snowflake_importexport')->__('Provet');
        }

        return $result;
    }

    /**
     * Get attribute name from boolean value
     *
     * @param bool $value boolean value from Yes/No dropdown
     * @return string
     */
    protected function _getTrafficLightAttribute($value) {
        switch ($value) {
            case 3:
                $result = Mage::helper('snowflake_importexport')->__('Green');
                break;
            case 2:
                $result = Mage::helper('snowflake_importexport')->__('Yellow');
                break;
            case 1:
            default:
                $result = Mage::helper('snowflake_importexport')->__('Red');
        }

        return $result;
    }

    /**
     * Get product id by sku
     *
     * @param   string $sku
     * @return  int
     */
    protected function _getProductId($sku)
    {
        return Mage::getModel('catalog/product')->getIdBySku($sku);
    }

    /**
     * Get customer group id
     *
     * @param int $priceGroup attribute from csv-file
     * @return int
     */
    protected function _getCustomerGroup($priceGroup)
    {
        $group = Mage::getModel('customer/group');
        $group->load($priceGroup, 'customer_group_code');

        $result = 0;
        if ($group->getId()) {
            $result = $group->getId();
        }

        return $result;
    }

    /**
     * Set discount group to db
     *
     * @param   string $code
     * @return  int
     */
    protected function _setDiscountGroup($code)
    {
        $discountGroup = Mage::getModel('snowflake_quantitydiscount/discount_group');
        $discountGroup->setData('discount_group_code', $code);
        $discountGroup->save();

        return $discountGroup->getId();
    }

    /**
     * Get discount group id by code
     *
     * @param   string $code
     * @return  int
     */
    protected function _getDiscountGroupId($code)
    {
        $discountGroup = Mage::getModel('snowflake_quantitydiscount/discount_group');
        $discountGroup->load($code, 'discount_group_code');

        $result = 0;
        if ($discountGroup->getId()) {
            $result = $discountGroup->getId();
        }

        return $result;
    }

    /**
     * Read import csv file and transform to array format
     *
     * @param string $type Type of import (product, category, customer, category_product)
     * @return array
     */
    protected function _getImportArray($type, $cron = false)
    {
        ini_set('auto_detect_line_endings', true);

        $data = array();

        if ($filename = $this->_getFilename($type, $cron)) {
            $bom     = pack("CCC", 0xef, 0xbb, 0xbf);
            $string  = file_get_contents($filename, null, null, 0, 10);
            if (0 === strncmp($string, $bom, 3)) {
                Mage::throwException('BOM detected in file ' . $type .'.csv');
            }

            $lineDataWithKeys = array();
            $fieldNames = array();

            $handle = fopen($filename, 'r');
            while (($lineData = fgetcsv($handle, null, ';', '"')) !== FALSE) {
                if (!sizeof($fieldNames)) {
                    $fieldNames = $lineData;
                } else {
                    foreach($lineData as $key => $value) {
                        unset($lineData);
                        $lineData = null;

                        if (!isset($fieldNames[$key])) {
                            Mage::throwException('Data has more columns than the header.');
                        }
                        $lineDataWithKeys[$fieldNames[$key]] = $value;
                    }
                    $data[] = $lineDataWithKeys;
                }
            }

            fclose($handle);
        }

        return $data;
    }

    /**
     * Get CSV Filename
     *
     * @param string $type Type of import (product, category, customer, category_product)
     * @return string||boolean
     */
    protected function _getFilename($type, $cron)
    {
        $filename = Mage::getBaseDir('var') . DS . 'import' . DS . $type . '.csv';

        if (!is_file($filename)) {
            if ($cron) return false;
            Mage::throwException('File "' . $filename . '" does not exist.');
        }

        if (!is_readable($filename)) {
            if ($cron) return false;
            Mage::throwException('File "' . $filename . '" is not readable.');
        }

        if (!filesize($filename)) {
            if ($cron) return false;
            Mage::throwException('File "' . $filename . '" seems to be empty.');
        }

        return $filename;
    }

    /**
     * Searches a multidimensional array for a given value
     *
     * @param array $data Multidimensional array
     * @param array $searched
     * @return boolean | array
     */
    protected function _multiSearch($data, $searched) {
        if (empty($searched) || empty($data)) {
            return false;
        }

        foreach ($data as $key => $value) {
            $exists = true;
            foreach ($searched as $skey => $svalue) {
                $exists = ($exists && IsSet($data[$key][$skey]) && $data[$key][$skey] == $svalue);
            }
            if($exists){ return $value; }
        }

        return false;
    }

    /**
     * Filter post data
     *
     * @param array $data
     * @return array
     */
    protected function _filterPostData($data)
    {
        if ($data) {
            /** @var $helperCatalog Mage_Catalog_Helper_Data */
            $helperCatalog = Mage::helper('catalog');

            if (!empty($data['option']) && !empty($data['option']['value']) && is_array($data['option']['value'])) {
                foreach ($data['option']['value'] as $key => $values) {
                    $data['option']['value'][$key] = array_map(array($helperCatalog, 'stripTags'), $values);
                }
            }
        }
        return $data;
    }

    /**
     * make some clean url's
     *
     * @param $categoryName
     * @return string
     */
    protected function cleanUrlName($categoryName){
        $categoryNameWithoutSpecialChar = strtolower(str_replace($this->replace, '-', trim($categoryName)));
        $categoryNameWithoutSpecialChar = strtolower(str_replace('ä', 'ae', trim($categoryNameWithoutSpecialChar)));
        $categoryNameWithoutSpecialChar = strtolower(str_replace('ö', 'oe', trim($categoryNameWithoutSpecialChar)));
        $categoryNameWithoutSpecialChar = strtolower(str_replace('ü', 'ue', trim($categoryNameWithoutSpecialChar)));

        return $categoryNameWithoutSpecialChar;
    }

    /**
     * update categories
     *
     * @param $categoryId
     * @param $categoryNameOrigin
     * @param $urlKeyName
     */
    protected function updateCategory($categoryId, $categoryNameOrigin, $urlKeyName){
        $category = Mage::getModel('catalog/category')
            ->setStoreId(self::DEFAULTSTOREID)
            ->load($categoryId);

        $category->setName($categoryNameOrigin);
        $category->setUrlKey($urlKeyName);

        try{
            $category->save();
        }
        catch (Exception $e){
            echo "categories not found update";
        }
    }

    /**
     * this function only works, because we import the categories level wise
     *
     * @param $category
     */
    protected function insertCategory($category, $mainCategory) {
        $categoryPath = self::DEFAULTSHOPROOTCATREOGYPATH;
        $urlKeyName = $this->cleanUrlName($category['Bezeichnung']);

        //find the parent, remove the last char from string => this is the parent categorie
        $parentWebGroupCode = substr($category['WebGrpCode'],0,-1);
        if(strlen($parentWebGroupCode) >= 1){
            $parentCategory = Mage::getResourceModel('catalog/category_collection')
                ->addFieldToFilter('snowflake_webgroupcode1', $parentWebGroupCode)->addFieldToFilter("path", array("like" => $mainCategory->getPath() . "%"))
                ->getFirstItem();
            $categoryPath = $parentCategory->getPath();
        }

        $category = Mage::getModel('catalog/category')
            ->setStoreId(self::DEFAULTSTOREID)
            ->setName($category['Bezeichnung'])
            ->setSnowflakeWebgroupcode1($category['WebGrpCode'])
            ->setPath($categoryPath)
            ->setUrlKey($urlKeyName)
            ->setIncludeInMenu(true)
            ->setIsActive(true);

        try{
            $category->save();
        }
        catch (Exception $e){
            echo "categories not found insert";
        }
    }

    protected function _archiveFtpFile($type) {
        Mage::getModel('snowflake_importexport/ftp_import')->archiveFile($type);
    }
    
    protected function _archiveImages() {
        Mage::getModel('snowflake_importexport/ftp_import')->archiveImages2();
    }
}
  