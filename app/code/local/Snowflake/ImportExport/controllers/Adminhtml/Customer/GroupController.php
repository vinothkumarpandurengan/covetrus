<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 22.01.2015
 * Time: 10:52
 */

require_once Mage::getModuleDir('controllers', 'Mage_Adminhtml').DS.'Customer'.DS.'GroupController.php';

class Snowflake_ImportExport_Adminhtml_Customer_GroupController extends Mage_Adminhtml_Customer_GroupController
{
    /**
     * Create or save customer group.
     */
    public function saveAction()
    {
        $customerGroup = Mage::getModel('customer/group');
        $id = $this->getRequest()->getParam('id');
        if (!is_null($id)) {
            $customerGroup->load((int)$id);
        }

        $taxClass   = (int)$this->getRequest()->getParam('tax_class');
        $priceGroup = (int)$this->getRequest()->getParam('price_group');

        if ($taxClass) {
            try {
                $customerGroupCode = (string)$this->getRequest()->getParam('code');

                if (!empty($customerGroupCode)) {
                    $customerGroup->setCode($customerGroupCode);
                }

                $customerGroup
                    ->setTaxClassId($taxClass)
                    ->setPriceGroupId($priceGroup)
                    ->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('customer')->__('The customer group has been saved.'));
                $this->getResponse()->setRedirect($this->getUrl('*/customer_group'));
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setCustomerGroupData($customerGroup->getData());
                $this->getResponse()->setRedirect($this->getUrl('*/customer_group/edit', array('id' => $id)));
                return;
            }
        } else {
            $this->_forward('new');
        }
    }
}