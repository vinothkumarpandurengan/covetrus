<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 18.11.2014
 * Time: 13:45
 */
class Snowflake_ImportExport_Adminhtml_ImportexportController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Import products from csv file
     *
     * @return void
     */
    public function importProductsAction()
    {
        try {
            Mage::getSingleton('snowflake_importexport/productimport')->importProducts();
            if ($createdProducts = Mage::getSingleton('snowflake_importexport/productimport')->getCreatedProducts()) {
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    $this->__('%s Products created.', $createdProducts)
                );
            }
            if ($updatedProducts = Mage::getSingleton('snowflake_importexport/productimport')->getUpdatedProducts()) {
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    $this->__('%s Products updated.', $updatedProducts)
                );
            }
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::logException($e);
        }

        $this->_redirectReferer();
    }

    /**
     * Import product stocks from csv file
     *
     * @return void
     */
    public function importProductStocksAction()
    {
        try {
            Mage::getSingleton('snowflake_importexport/productimport')->importProductStocks();
            if ($createdProducts = Mage::getSingleton('snowflake_importexport/productimport')->getCreatedProducts()) {
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    $this->__('%s Products created.', $createdProducts)
                );
            }
            if ($updatedProducts = Mage::getSingleton('snowflake_importexport/productimport')->getUpdatedProducts()) {
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    $this->__('%s Products updated.', $updatedProducts)
                );
            }
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::logException($e);
        }

        $this->_redirectReferer();
    }

    /**
     * Import products from csv file
     *
     * @return void
     */
    /*public function importReplacementItemsAction()
    {
        try {
            Mage::getSingleton('snowflake_importexport/productimport')->importReplacementItems();
            if ($createdReplacementItems = Mage::getSingleton('snowflake_importexport/productimport')->getCreatedProducts()) {
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    $this->__('%s Products created.', $createdProducts)
                );
            }
            if ($updatedReplacementItems = Mage::getSingleton('snowflake_importexport/productimport')->getUpdatedProducts()) {
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    $this->__('%s Products updated.', $updatedProducts)
                );
            }
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::logException($e);
        }

        $this->_redirectReferer();
    }*/

    /**
     * Import product tier prices from csv file
     *
     * @return void
     */
    public function importTierPricesAction()
    {
        try {
            Mage::getSingleton('snowflake_importexport/productimport')->importTierPrices();
            if ($createdProducts = Mage::getSingleton('snowflake_importexport/productimport')->getCreatedProducts()) {
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    $this->__('%s Products created.', $createdProducts)
                );
            }
            if ($updatedProducts = Mage::getSingleton('snowflake_importexport/productimport')->getUpdatedProducts()) {
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    $this->__('%s Products updated.', $updatedProducts)
                );
            }
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::logException($e);
        }

        $this->_redirectReferer();
    }

    /**
     * Import webgroupcode from csv file
     *
     * @return void
     */
    public function importWebGroupCodesAction()
    {
        try {
            Mage::getSingleton('snowflake_importexport/productimport')->translateWebgroupCode();
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::logException($e);
        }

        $this->_redirectReferer();
    }


    /**
     * Import customers from csv file
     *
     * @return void
     */
    public function importCustomersAction()
    {
        try {
            Mage::getSingleton('snowflake_importexport/customerimport')->importCustomer();

            // send welcome email to new customers
            foreach(Mage::getSingleton('snowflake_importexport/customerimport')->getNewCustomers() as $customer) {
                Mage::helper('snowflake_importexport')->sendWelcomeEmail($customer['email']);
            }

            if ($createdCustomers = Mage::getSingleton('snowflake_importexport/customerimport')->getCreatedCustomers()) {
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    $this->__('%s Customer created.', $createdCustomers)
                );
            }
            if ($updatedCustomers = Mage::getSingleton('snowflake_importexport/customerimport')->getUpdatedCustomers()) {
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    $this->__('%s Customer updated.', $updatedCustomers)
                );
            }
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::logException($e);
        }

        $this->_redirectReferer();
    }

    public function exportuploadAction()
    {
        try {
            Mage::getSingleton('snowflake_importexport/ftp_export')->transferFiles();
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::logException($e);
        }

        $this->_redirectReferer();
    }

    public function downloadfilesAction()
    {
        try {
            //move csv files to backup folder if they exists
            Mage::getSingleton('snowflake_importexport/ftp_import')->transferFiles('product');
            Mage::getSingleton('snowflake_importexport/ftp_import')->transferFiles('stock');
            Mage::getSingleton('snowflake_importexport/ftp_import')->transferFiles('customer');
            Mage::getSingleton('snowflake_importexport/ftp_import')->transferFiles('webgroupcode');

            //download actuell import files
            Mage::getSingleton('snowflake_importexport/ftp_import')->downloadFiles('product');
            Mage::getSingleton('snowflake_importexport/ftp_import')->downloadFiles('stock');
            Mage::getSingleton('snowflake_importexport/ftp_import')->downloadFiles('customer');
            Mage::getSingleton('snowflake_importexport/ftp_import')->downloadFiles('webgroupcode');
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::logException($e);
        }

        $this->_redirectReferer();
    }
}

