<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 10.04.2015
 * Time: 13:33
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->updateAttribute('catalog_product', 'manufacturer', 'is_filterable', 0);
$installer->updateAttribute('catalog_product', 'manufacturer', 'is_filterable_in_search', 0);

$installer->endSetup();