<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 10.04.2015
 * Time: 13:33
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->updateAttribute('catalog_product', 'price', 'is_filterable', 0);
$installer->updateAttribute('catalog_product', 'price', 'is_filterable_in_search', 0);

$installer->updateAttribute('catalog_product', 'provet_feedays', 'is_filterable', 0);
$installer->updateAttribute('catalog_product', 'provet_feedays', 'is_filterable_in_search', 0);

$attribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', 'provet_webgrpcode');
$attribute->setStoreLabels( array( 1 => 'Unterkategorie' ) )->save();

$installer->endSetup();