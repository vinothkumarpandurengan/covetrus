<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 22.04.2015
 * Time: 09:08
 */
class Snowflake_Filter_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $_params = null;
    protected $_continueShoppingUrl = null;

    /**
     * Check if we are on the search page
     *
     * @return  boolean
     */
    public function isSearch()
    {
        $modul = Mage::app()->getRequest()->getModuleName();

        if ($modul === 'catalogsearch') {
            return true;
        }

        if ($modul === 'filter' && Mage::app()->getRequest()->getActionName() == 'search') {
            return true;
        }

        return false;
    }

    /**
     * Prepare params to a query for url
     *
     * @param   array $params
     * @param   string||array $without
     * @return  string
     */
    protected function _toQuery($params, $without = null)
    {
        if (!is_array($without))
            $without = array($without);

        $query = '?';
        foreach ($params as $key => $value) {
            if (!in_array($key, $without))
                $query .= $key . '=' . urlencode($value) . '&';
        }

        return substr($query, 0, -1);
    }

    /**
     * Remove unnecessary elements from the url
     *
     * @param   string $url
     * @return  string
     */
    public function stripQuery($url)
    {
        $pos = strpos($url, '?');
        if (false !== $pos)
            $url = substr($url, 0, $pos);
        return $url;
    }

    /**
     * Get continue shopping url
     *
     * @return  string
     */
    public function getContinueShoppingUrl()
    {
        if (is_null($this->_continueShoppingUrl)) {
            $url = '';

            $params = $this->getParams();
            $keys   = $this->_getNonFilteringParamKeys();

            $query = array();
            foreach ($params as $key => $value) {
                if (in_array($key, $keys))
                    $query[$key] = $value;
            }

            if ($this->isSearch()) {
                $url = Mage::getModel('core/url')->getUrl('catalogsearch/result/index', array('_query' => $query));
            } else {
                $category = Mage::registry('current_category');
                $rootId = Mage::app()->getStore()->getRootCategoryId();
                if ($category && $category->getId() != $rootId) {
                    $url = $category->getUrl();
                } else {
                    $url = Mage::app()->getStore()->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);
                }
                $url .= $this->_toQuery($query);
            }
            $this->_continueShoppingUrl = $url;
        }

        return $this->_continueShoppingUrl;
    }

    /**
     * Wrap product list in a div container
     *
     * @param   string $html
     * @return  string
     */
    public function wrapProducts($html)
    {
        $html = str_replace('onchange="setLocation', 'onchange="snowflakeMultipleFilter.toolbarRequest', $html);

        //$loaderHtml = '<div class="loading_filters" style="display:none"><img id="loading-image" src="' . Mage::getDesign()->getSkinUrl('images/ajax-loader.gif') . '" /></div>';
        $loaderHtml = '<div class="loading_filters" style="display:none">' . Mage::helper('snowflake_filter')->__('Please wait...') . '</div>';
        $html .= $loaderHtml;

        if (Mage::app()->getRequest()->isXmlHttpRequest()) {
            $html = str_replace('?___SID=U&amp;', '?', $html);
            $html = str_replace('?___SID=U', '', $html);
            $html = str_replace('&amp;___SID=U', '', $html);

            $key = Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED;
            $value = Mage::helper('core')->urlEncode($this->getContinueShoppingUrl());
            $html = preg_replace("#$key/[^/]+#", "$key/$value", $html);
        } else {
            $html = '<div id="filter_layered_container">'
                . $html
                . '</div>'
                . '';
        }

        return $html;
    }

    /**
     * Get clear all url
     *
     * @return  string
     */
    public function getClearAllUrl($baseUrl)
    {
        $baseUrl .= '?clearall=true';
        if ($this->isSearch()) {
            $baseUrl .= '&q=' . urlencode($this->getParam('q'));
        }
        return $baseUrl;
    }

    /**
     * Check if we need a clear all link
     *
     * @return  boolean
     */
    public function needClearAll()
    {
        if ($params = Mage::registry('current_session_params')) {
            $needClearAll = false;

            $nonFilteringParamKeys = $this->_getNonFilteringParamKeys();

            foreach ($params as $key => $val) {
                if (!in_array($key, $nonFilteringParamKeys)) {
                    $needClearAll = true;
                }
            }

            return $needClearAll;
        } else {
            return false;
        }
    }

    /**
     * Get Param from attributeCode
     *
     * @return  string||array
     */
    public function getParam($attributeCode)
    {
        $params = $this->getParams();
        $value  = isset($params[$attributeCode]) ? $params[$attributeCode] : null;
        return $value;
    }

    /**
     * Get all available params
     *
     * @param   boolean $asString
     * @param   string||array $without
     * @return  string||array
     */
    public function getParams($asString = false, $without = null)
    {
        if (is_null($this->_params)) {
            $session  = Mage::getSingleton('catalog/session');
            $clearAll = false;

            if (Mage::registry('new_category')) {
                $clearAll = true;
            }

            if ($this->isSearch()) {
                $session = Mage::getSingleton('catalogsearch/session');
                $query   = Mage::app()->getRequest()->getQuery();
                if (isset($query['q'])) {
                    if ($session->getData('layquery') && $session->getData('layquery') != $query['q']) {
                        $clearAll = true;
                    }
                    $session->setData('layquery', $query['q']);
                }
            }

            $query  = Mage::app()->getRequest()->getQuery();
            $params = (array) $session->getAdjNav();
            $this->_params = array_merge($params, $query);

            if (!empty($query['clearall']) || $clearAll) {
                $this->_params = array();
                if ($this->isSearch() && isset($query['q']))
                    $this->_params['q'] = $query['q'];
            }

            $params = array();
            foreach ($this->_params as $key => $value) {
                if ($value && 'clear' != $value)
                    $params[$key] = $value;
            }

            if (Mage::registry('new_category') && isset($params['p'])) {
                unset($params['p']);
            }

            $session->setAdjNav($params);
            $this->_params = $params;

            Mage::register('current_session_params', $params);
        }

        if ($asString) {
            return $this->_toQuery($this->_params, $without);
        }

        return $this->_params;
    }

    /**
     * Get cache key for use multiple filter ids
     *
     * @param   string $attributeCode
     * @return  string
     */
    public function getCacheKey($attributeCode)
    {
        $keys   = $this->_getNonFilteringParamKeys();
        $keys[] = $attributeCode;

        return md5($this->getParams(true, $keys));
    }

    /**
     * Get keys which shouldn't be filtered
     *
     * @return  array
     */
    protected function _getNonFilteringParamKeys()
    {
        return array('x', 'y', 'mode', 'p', 'order', 'dir', 'limit', 'q', '___store', '___from_store', 'sns');
    }
}
  