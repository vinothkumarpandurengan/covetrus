<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 22.04.2015
 * Time: 11:11
 */
class Snowflake_Filter_Model_Resource_Catalog_Layer_Filter_Attribute extends Mage_Catalog_Model_Resource_Layer_Filter_Attribute
{
    /**
     * Get Matrix Helper
     *
     * @return  Snowflake_Matrix_Helper_Data
     */
    protected function _getMatrixHelper()
    {
        return Mage::helper('snowflake_matrix');
    }

    /**
     * Retrieve array with products counts per attribute option
     *
     * @param Mage_Catalog_Model_Layer_Filter_Attribute $filter
     * @return array
     */
    public function getCount($filter)
    {
        // clone select from collection with filters
        $select = clone $filter->getLayer()->getProductCollection()->getSelect();
        // reset columns, order and limitation conditions
        $select->reset(Zend_Db_Select::COLUMNS);
        $select->reset(Zend_Db_Select::ORDER);
        $select->reset(Zend_Db_Select::LIMIT_COUNT);
        $select->reset(Zend_Db_Select::LIMIT_OFFSET);

        $connection = $this->_getReadAdapter();
        $attribute  = $filter->getAttributeModel();
        $tableAlias = sprintf('%s_idx', $attribute->getAttributeCode());
        $conditions = array(
            "{$tableAlias}.entity_id = e.entity_id",
            $connection->quoteInto("{$tableAlias}.attribute_id = ?", $attribute->getAttributeId()),
            $connection->quoteInto("{$tableAlias}.store_id = ?", $filter->getStoreId()),
        );

        $select
            ->join(
                array($tableAlias => $this->getMainTable()),
                join(' AND ', $conditions),
                array('value', 'count' => new Zend_Db_Expr("COUNT({$tableAlias}.entity_id)")))
            ->group("{$tableAlias}.value");



        $productMatrixGroup = $this->_getMatrixHelper()->getProductMatrixGroup();
        if (count($productMatrixGroup) > 0) {
            // Customer is logged in
            $matrixAttribute  = $this->_getMatrixHelper()->getMatrixAttribute();
            $tableAlias       = sprintf('%s_idx', $matrixAttribute->getAttributeCode());

            $conditions = array(
                "{$tableAlias}.entity_id = e.entity_id",
                $connection->quoteInto("{$tableAlias}.attribute_id = ?", $matrixAttribute->getAttributeId()),
                $connection->quoteInto("{$tableAlias}.store_id = ?", '0'),
            );

            $select
                ->join(
                    array($tableAlias => 'catalog_product_entity_varchar'),
                    join(' AND ', $conditions),
                    array())
                ->where("{$tableAlias}.value IN(" . implode(',', $productMatrixGroup) . ")");
        }

        return $connection->fetchPairs($select);
    }
}
  