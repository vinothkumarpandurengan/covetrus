<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 22.04.2015
 * Time: 10:05
 */
class Snowflake_Filter_Model_Layer_Filter_Attribute extends Mage_Catalog_Model_Layer_Filter_Attribute
{
    /**
     * Construct attribute filter
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get Filter Helper
     *
     * @return  Snowflake_Filter_Helper_Data
     */
    protected function _getHelper()
    {
        return Mage::helper('snowflake_filter');
    }

    /**
     * Get Matrix Helper
     *
     * @return  Snowflake_Matrix_Helper_Data
     */
    protected function _getMatrixHelper()
    {
        return Mage::helper('snowflake_matrix');
    }

    /**
     * Apply attribute option filter to product collection
     *
     * @param   Zend_Controller_Request_Abstract $request
     * @param   Varien_Object $filterBlock
     * @return  Snowflake_Filter_Model_Layer_Filter_Attribute
     */
    public function apply(Zend_Controller_Request_Abstract $request, $filterBlock)
    {
        $filter = Mage::helper('snowflake_filter')->getParam($this->_requestVar);
        $filter = explode('-', $filter);

        $ids = array();
        foreach ($filter as $id){
            $id = intVal($id);
            if ($id) {
                $ids[] = $id;
            }
        }
        if ($ids){
            $this->_applyMultipleValuesFilter($ids);
        }

        $this->setActiveState($ids);
        return $this;
    }

    /**
     * Apply attribute multiple filters to product collection
     *
     * @param   array $ids
     * @return  Snowflake_Filter_Model_Layer_Filter_Attribute
     */
    protected function _applyMultipleValuesFilter($ids)
    {
        $collection = $this->getLayer()->getProductCollection();
        $attribute  = $this->getAttributeModel();
        $table = Mage::getSingleton('core/resource')->getTableName('catalogindex/eav');

        $alias = 'attr_index_'.$attribute->getId();

        $collection->getSelect()->join(
            array($alias => $table),
            $alias.'.entity_id=e.entity_id',
            array()
        )
        ->where($alias.'.store_id = ?', Mage::app()->getStore()->getId())
        ->where($alias.'.attribute_id = ?', $attribute->getId())
        ->where($alias.'.value IN (?)', $ids);
        if (count($ids)>1){
            $collection->getSelect()->distinct(true);
        }

        return $this;
    }

    /**
     * Get data array for building attribute filter items
     *
     * @return array
     */
    protected function _getItemsData()
    {
        $attribute = $this->getAttributeModel();
        $this->_requestVar = $attribute->getAttributeCode();

        $key  = $this->getLayer()->getStateKey();
        $key .= $this->_getHelper()->getCacheKey($this->_requestVar);
        $data = $this->getLayer()->getAggregator()->getCacheData($key);

        if ($data === null) {
            $options = $attribute->getFrontend()->getSelectOptions();
            //$optionsCount = $this->_getResource()->getCount($this);
            $optionsCount = Mage::getSingleton('catalogindex/attribute')->getCount(
                $attribute,
                $this->_getBaseCollectionSql()
            );

            $data = array();
            foreach ($options as $option) {
                if (is_array($option['value'])) {
                    continue;
                }
                if (Mage::helper('core/string')->strlen($option['value'])) {
                    // Check filter type
                    if ($attribute->getIsFilterable() == self::OPTIONS_ONLY_WITH_RESULTS) {
                        if (!empty($optionsCount[$option['value']])) {
                            $data[] = array(
                                'label' => $option['label'],
                                'value' => $option['value'],
                                'count' => $optionsCount[$option['value']],
                            );
                        }
                    } else {
                        $data[] = array(
                            'label' => $option['label'],
                            'value' => $option['value'],
                            'count' => isset($optionsCount[$option['value']]) ? $optionsCount[$option['value']] : 0,
                        );
                    }
                }
            }

            $currentIds  = $this->_getHelper()->getParam($this->_requestVar);
            $tags = array(
                Mage_Eav_Model_Entity_Attribute::CACHE_TAG . ':' . $currentIds,
            );

            $tags = $this->getLayer()->getStateTags($tags);
            $this->getLayer()->getAggregator()->saveCacheData($data, $key, $tags);
        }

        return $data;
    }

    /**
     * Get base product collection query
     *
     * @return string
     */
    protected function _getBaseCollectionSql()
    {
        $attribute = $this->getAttributeModel();
        $alias = 'attr_index_' . $attribute->getId();

        // Varien_Db_Select
        $baseSelect = clone parent::_getBaseCollectionSql();

        // 1) remove from conditions
        $oldWhere = $baseSelect->getPart(Varien_Db_Select::WHERE);
        $newWhere = array();

        foreach ($oldWhere as $cond){
            if (!strpos($cond, $alias))
                $newWhere[] = $cond;
         }

        if ($newWhere && substr($newWhere[0], 0, 3) == 'AND')
            $newWhere[0] = substr($newWhere[0],3);

        $baseSelect->setPart(Varien_Db_Select::WHERE, $newWhere);

        // 2) remove from joins
        $oldFrom = $baseSelect->getPart(Varien_Db_Select::FROM);
        $newFrom = array();

        foreach ($oldFrom as $name=>$val){
            if ($name != $alias)
                $newFrom[$name] = $val;
        }

        $baseSelect->setPart(Varien_Db_Select::FROM, $newFrom);

        $productMatrixGroup = $this->_getMatrixHelper()->getProductMatrixGroup();
        if (count($productMatrixGroup) > 0) {
            // Customer is logged in
            $matrixAttribute  = $this->_getMatrixHelper()->getMatrixAttribute();
            $tableAlias       = sprintf('at_%s', $matrixAttribute->getAttributeCode());

            if (!isset($newFrom[$tableAlias])) {
                $conditions = array(
                    "{$tableAlias}.entity_id = e.entity_id",
                    "{$tableAlias}.attribute_id = " . $matrixAttribute->getAttributeId(),
                    "{$tableAlias}.store_id = 0",
                );

                $baseSelect
                    ->join(
                        array($tableAlias => 'catalog_product_entity_varchar'),
                        join(' AND ', $conditions),
                        array())
                    ->where("{$tableAlias}.value IN(" . implode(',', $productMatrixGroup) . ")");
            }
        }

        return $baseSelect;
    }
}