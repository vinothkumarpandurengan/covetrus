<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 22.04.2015
 * Time: 15:35
 */

class Snowflake_Filter_ProductController extends Mage_Core_Controller_Front_Action
{
    /**
     * Ajax action for load filtered products
     *
     * Ajax request
     */
    public function ajaxAction()
    {
        $categoryId = (int) $this->getRequest()->getParam('id');
        $category = Mage::getModel('catalog/category')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($categoryId);

        if (!$category->getId()) {
            $this->_redirectReferer();
            return;
        }

        Mage::register('current_category', $category);

        $this->loadLayout();

        $response = array();
        $response['layer'] = $this->getLayout()->getBlock('layer')->toHtml();
        $response['products'] = $this->getLayout()->getBlock('root')->toHtml();

        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($response));
    }

    public function searchAction()
    {
        $this->loadLayout();

        $response = array();
        $response['layer'] = $this->getLayout()->getBlock('layer')->toHtml();
        $response['products'] = $this->getLayout()->getBlock('root')->setIsSearchMode()->toHtml();

        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($response));
    }
}
  