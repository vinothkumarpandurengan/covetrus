<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 22.04.2015
 * Time: 16:16
 */
class Snowflake_Filter_Block_List extends Mage_Core_Block_Template
{
    protected $_productCollection;
    protected $_module = 'catalog';

    public function getListBlock()
    {
        return $this->getChild('product_list');
    }

    public function setListOrders()
    {
        if ('catalogsearch' != $this->_module)
            return $this;

        $category = Mage::getSingleton('catalog/layer')
            ->getCurrentCategory();

        /* @var $category Mage_Catalog_Model_Category */
        $availableOrders = $category->getAvailableSortByOptions();
        unset($availableOrders['position']);
        $availableOrders = array_merge(array(
            'relevance' => $this->__('Relevance')
        ), $availableOrders);

        $this->getListBlock()
            ->setAvailableOrders($availableOrders)
            ->setDefaultDirection('desc')
            ->setSortBy('relevance');

        return $this;
    }

    public function setIsSearchMode()
    {
        $this->_module = 'catalogsearch';
        return $this;
    }

    public function setListCollection()
    {
        $this->getListBlock()
            ->setCollection($this->_getProductCollection());
        return $this;
    }

    protected function _toHtml()
    {
        $this->setListOrders();
        $this->setListModes();
        $this->setListCollection();

        $html = $this->getChildHtml('product_list');
        $html = Mage::helper('snowflake_filter')->wrapProducts($html);

        return $html;
    }

    protected function _getProductCollection()
    {
        if (is_null($this->_productCollection)) {
            $this->_productCollection = Mage::getSingleton($this->_module . '/layer')
                ->getProductCollection()
                ->addAttributeToSort('provet_sort', 'ASC');
        }

        return $this->_productCollection;
    }
}
  