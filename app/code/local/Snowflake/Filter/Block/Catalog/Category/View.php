<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 22.04.2015
 * Time: 15:46
 */

class Snowflake_Filter_Block_Catalog_Category_View extends Mage_Catalog_Block_Category_View
{
    /**
     * Get product list template
     *
     * @return  string
     */
    public function getProductListHtml()
    {
        $html = parent::getProductListHtml();
        if ($this->getCurrentCategory()->getIsAnchor()){
            $html = Mage::helper('snowflake_filter')->wrapProducts($html);
        }
        return $html;
    }
}