<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 22.04.2015
 * Time: 09:59
 */
class Snowflake_Filter_Block_Layer_Filter_Attribute extends Mage_Catalog_Block_Layer_Filter_Attribute
{
    /**
     * Initialize filter item template
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->setTemplate('snowflake/filter/attribute.phtml');
        $this->_filterModelName = 'snowflake_filter/layer_filter_attribute';
    }

    /**
     * Get request variable name which is used for filter
     *
     * @return string
     */
    public function getVar()
    {
        return $this->_filter->getRequestVar();
    }

    /**
     * Get html id
     *
     * @param   $item
     * @return  array
     */
    protected function _getHtmlId($item)
    {
        return $this->getVar() . '-' . $item->getValueString();
    }

    /**
     * Check if item is selected
     *
     * @param   $item
     * @return  boolean
     */
    protected function _Selectedfilter($item)
    {
        $ids = (array) $this->_filter->getActiveState();
        return in_array($item->getValueString(), $ids);
    }

    /**
     * Get all filter items
     *
     * @return  array
     */
    public function getFiltersArray()
    {
        $_filtersArray = array();

        foreach ($this->getItems() as $_item) {
            if ($_item->getCount() > 0) {
                $_htmlFilters = 'id="' . $this->_getHtmlId($_item) . '" ';

                //Create URL
                $href = html_entity_decode(Mage::app()->getRequest()->getBaseUrl() . Mage::getSingleton('core/session')->getRequestPath());
                $_htmlFilters .= 'href="' . $href . '" ';

                $_htmlFilters .= 'class="filter_layered_attribute'
                    . ($this->_Selectedfilter($_item) ? ' filter_layered_attribute_selected' : '') . '" ';

                $_filtersArray[] = '<a ' . $_htmlFilters . '>' . $_item->getLabel() . '</a>';
            }
        }

        return $_filtersArray;
    }
}
  