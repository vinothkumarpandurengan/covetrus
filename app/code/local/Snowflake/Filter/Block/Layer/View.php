<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 22.04.2015
 * Time: 09:31
 */
class Snowflake_Filter_Block_Layer_View extends Mage_Catalog_Block_Layer_View
{
    protected $_filterBlocks = null;
    protected $_helper = null;

    /**
     * Initialize factory instance
     */
    public function __construct()
    {
        parent::__construct();
        $this->_helper = Mage::helper('snowflake_filter');
    }

    /**
     * Prepare child blocks
     *
     * @return Mage_Catalog_Block_Layer_View
     */
    protected function _prepareLayout()
    {
        // Return an object of current category
        $category = Mage::registry('current_category');

        if ($category) {
            $currentCategoryID = $category->getId();
        } else {
            $currentCategoryID = null;
        }

        // Return session object
        $session = Mage::getSingleton('catalog/session');
        if ($session && $lastCategoryID = $session->getLastCatgeoryID()) {
            if ($currentCategoryID != $lastCategoryID) {
                Mage::register('new_category', true);
            }
        }
        $session->setLastCatgeoryID($currentCategoryID);

        $filterableAttributes = $this->_getFilterableAttributes();
        foreach ($filterableAttributes as $attribute) {
            $filterBlockName = 'snowflake_filter/layer_filter_attribute';

            $this->setChild($attribute->getAttributeCode() . '_filter',
                $this->getLayout()->createBlock($filterBlockName)
                    ->setLayer($this->getLayer())
                    ->setAttributeModel($attribute)
                    ->init());
        }

        $this->getLayer()->apply();

        return Mage_Core_Block_Template::_prepareLayout();
    }

    /**
     * Get Filter Helper
     *
     * @return  Snowflake_Filter_Helper_Data
     */
    protected function _getHelper()
    {
        return Mage::helper('snowflake_filter');
    }

    /**
     * Get state info (url, params, ajaxUrl)
     *
     * @return  array
     */
    public function getStateInfo()
    {
        $_helper = $this->_getHelper();

        $ajaxUrl = '';
        if ($_helper->isSearch()) {
            $ajaxUrl = Mage::getUrl('filter/product/search');
        } else if ($category = $this->getLayer()->getCurrentCategory()) {
            $ajaxUrl = Mage::getUrl('filter/product/ajax', array('id' => $category->getId()));
        }

        $ajaxUrl = $_helper->stripQuery($ajaxUrl);
        $url     = $_helper->getContinueShoppingUrl();

        //Set the AJAX Pagination
        $pageKey = Mage::getBlockSingleton('page/html_pager')->getPageVarName();

        //Get parameters of search
        $query = $_helper->getParams(true, $pageKey);
        if ($query)
            $query = substr($query, 1);

        $this->setClearAllUrl($_helper->getClearAllUrl($url));

        if (false !== strpos($url, '?')) {
            $url = substr($url, 0, strpos($url, '?'));
        }

        return array($url, $query, $ajaxUrl);
    }

    /**
     * Check if we need a clear all link
     *
     * @return  boolean
     */
    public function needClearAll()
    {
        return $this->_getHelper()->needClearAll();
    }

    /**
     * Get all layer filters
     *
     * @return array
     */
    public function getFilters()
    {
        if (is_null($this->_filterBlocks)) {
            $this->_filterBlocks = parent::getFilters();
        }

        return $this->_filterBlocks;
    }

    protected function _toHtml()
    {
        $html = parent::_toHtml();
        if (!Mage::app()->getRequest()->isXmlHttpRequest()) {
            $html = '<div id="catalog-filters">' . $html . '</div>';
        }
        return $html;
    }
}
  