<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Project: feelbsz
 * Date: 11.02.2015
 * Time: 12:56
 */
class Snowflake_AttributeHidden_Block_Catalog_Product_Compare_List
    extends Mage_Catalog_Block_Product_Compare_List
{
    /**
     * Retrieve Product Attribute Value
     *
     * @param Mage_Catalog_Model_Product $product
     * @param Mage_Catalog_Model_Resource_Eav_Attribute $attribute
     * @return string
     */
    public function getProductAttributeValue($product, $attribute)
    {
        if (!$product->hasData($attribute->getAttributeCode())) {
            return '';
        }

        if ($attribute->getSourceModel()
            || in_array($attribute->getFrontendInput(), array('select','boolean','multiselect'))
        ) {
            //$value = $attribute->getSource()->getOptionText($product->getData($attribute->getAttributeCode()));
            $value = '';
            if ($product->getData($attribute->getAttributeCode())) {
                $value = $attribute->getFrontend()->getValue($product);
            }
        } else {
            $value = $product->getData($attribute->getAttributeCode());
        }
        return $value;
    }
}
  