﻿<?php

class Snowflake_FormHandler_IndexController extends Mage_Core_Controller_Front_Action{

    public function sendSeminarMailAction(){
        $params = $this->getRequest()->getParams();

        $storeEmailAdressName = Mage::getStoreConfig('trans_email/ident_general/name');
        $storeEmailAdress = Mage::getStoreConfig('trans_email/ident_general/email');

        /* Send Seminar registration info to Henryschein */
        $mail = new Zend_Mail('UTF8');
        $bodyText = '';
        $bodyText .= 'Seminar: ' . $params['seminar'] . '<br/>';
        if($params['praxis']) $bodyText .= 'Praxis: ' . $params['praxis'] . '<br/>';
        $bodyText .= 'Anrede: ' . $params['salutation'] . '<br/>';
        $bodyText .= 'Name: ' . $params['name'] . '<br/>';
        if($params['customer_number']) $bodyText .= 'Kundennummer: ' . $params['customer_number'] . '<br/>';
        $bodyText .= 'Strasse: ' . $params['street'] . '<br/>';
        $bodyText .= 'PLZ: ' . $params['plz'] . '<br/>';
        $bodyText .= 'Ort: ' . $params['location'] . '<br/>';
        if($params['fax']) $bodyText .= 'Fax: ' . $params['fax']  . '<br/>';
        $bodyText .= 'Telefon: ' . $params['telephone'] . '<br/>';
        $bodyText .= 'E-Mail: ' . $params['email'] . '<br/>';
        if($params['comment']) $bodyText .= 'Kommentar: ' . $params['comment'] . '<br/>';

        $mail->setBodyHtml( 'Kunde hat sich für ein Seminar angemeldet.' . '<br/>' . $bodyText);
        $mail->setFrom($params['email'], $params['name']);
//        $mail->addTo('Franziska.Goldinger@henryschein.com', $storeEmailAdressName);
//        $mail->addTo('Jessica.Wirz@henryschein.com', $storeEmailAdressName);
        $mail->addTo('seminare@covetrus.ch', $storeEmailAdressName);
        $mail->setSubject('Seminar Anmeldung');
        $mail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);

        try {
            $mail->send();
        }
        catch(Exception $ex) {
            Mage::getSingleton('core/session')->addError('Unable to send email. Sample of a custom notification error from Snowflake_FormHandler sendSeminarMailAction.');

        }

        /* Send Seminar registration info to applicant */
        $mail = new Zend_Mail('UTF8');
        $mail->setBodyText('
            Vielen Dank für die Anmeldung für das Seminar: "' . $params['seminar'] . '" bei Henry Schein Animal Health / Provet AG, Gewerbestrasse 1, CH-3421 Lyssach'
        );
        $mail->setFrom($storeEmailAdress, $storeEmailAdressName);
        $mail->addTo($params['email'], '');
        $mail->setSubject('Seminar Anmeldung');
        $mail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);

        try {
            $mail->send();
        }
        catch(Exception $ex) {
            Mage::getSingleton('core/session')->addError('Unable to send email. Sample of a custom notification error from Snowflake_FormHandler sendSeminarMailAction .');

        }

        $this->_redirectReferer();
    }

    public function sendNewsletterMailAction(){
        $params = $this->getRequest()->getParams();

        $storeEmailAdressName = Mage::getStoreConfig('trans_email/ident_general/name');
        $storeEmailAdress = Mage::getStoreConfig('trans_email/ident_general/email');

        $newsletterValue = $this->__('Newsletter_registrierung_generally');
        if($params['register'] == 1) $newsletterValue = $this->__('Newsletter_registrierung_accessories_petfood');


        /* Send Newsletter registration info to Henryschein */
        $mail = new Zend_Mail('UTF8');
        $bodyText = '';
        $bodyText .= $this->__('Registrierung: ') . $newsletterValue . '<br/>';
        if($params['date']) $bodyText .= $this->__('Datum: ') . $params['date'] . '<br/>';
        if($params['praxis']) $bodyText .= $this->__('Praxis: ') . $params['praxis'] . '<br/>';
        if($params['customer_number']) $bodyText .= $this->__('Kundennummer: ') . $params['customer_number'] . '<br/>';
        $bodyText .= $this->__('Name: ') . $params['name'] . '<br/>';
        if($params['street']) $bodyText .= $this->__('Strasse: ') . $params['street'] . '<br/>';
        if($params['plz']) $bodyText .= $this->__('PLZ: ') . $params['plz'] . '<br/>';
        if($params['location']) $bodyText .= $this->__('Ort: ') . $params['location'] . '<br/>';
        if($params['telephone']) $bodyText .= $this->__('Telefon: ') . $params['telephone'] . '<br/>';
        $bodyText .= $this->__('E-Mail: ') . $params['email'] . '<br/>';

        $mail->setBodyHtml( 'Kunde hat sich für den Newsletter angemeldet.' . '<br/>' . $bodyText);
        $mail->setFrom($params['email'], $params['name']);
//        $mail->addTo('Franziska.Goldinger@henryschein.com', $storeEmailAdressName);
//        $mail->addTo('Jessica.Wirz@henryschein.com', $storeEmailAdressName);
//        $mail->addTo('Urs.Rothen@henryschein.com', $storeEmailAdressName);
        $mail->addTo('newsletter@covetrus.ch', $storeEmailAdressName);

        $mail->setSubject('Newsletter Anmeldung');
        $mail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);

        try {
            $mail->send();
        }
        catch(Exception $ex) {
            Mage::getSingleton('core/session')->addError('Unable to send email. Sample of a custom notification error from Snowflake_FormHandler NewsletterMailAction.');

        }

        /* Send Newsletter registration info to applicant */
        $mail = new Zend_Mail('UTF8');
        $mail->setBodyText('Vielen Dank für die Anmeldung für den ' . $newsletterValue . ' Newsletter.');
        $mail->setFrom($storeEmailAdress, $storeEmailAdressName);
        $mail->addTo($params['email'], '');
        $mail->setSubject('Newsletter Anmeldung');
        $mail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);

        try {
            $mail->send();
        }
        catch(Exception $ex) {
            Mage::getSingleton('core/session')->addError('Unable to send email. Sample of a custom notification error from Snowflake_FormHandler NewsletterMailAction .');

        }

        $this->_redirectReferer();
    }

    public function sendOwnbrandMailAction(){
        $params = $this->getRequest()->getParams();

        $storeEmailAdressName = Mage::getStoreConfig('trans_email/ident_general/name');
        $storeEmailAdress = Mage::getStoreConfig('trans_email/ident_general/email');

        /* Send OwnBrand registration info to Henryschein */
        $mail = new Zend_Mail('UTF8');
        $bodyText = '';
        $bodyText .= $this->__('Registrierung: ') . $params['register'] . '<br/>';
        if($params['date']) $bodyText .= $this->__('Datum: ') . $params['date'] . '<br/>';
        if($params['praxis']) $bodyText .= $this->__('Praxis: ') . $params['praxis'] . '<br/>';
        if($params['customer_number']) $bodyText .= $this->__('Kundennummer: ') . $params['customer_number'] . '<br/>';
        $bodyText .= $this->__('Name: ') . $params['name'] . '<br/>';
        if($params['street']) $bodyText .= $this->__('Strasse: ') . $params['street'] . '<br/>';
        if($params['plz']) $bodyText .= $this->__('PLZ: ') . $params['plz'] . '<br/>';
        if($params['location']) $bodyText .= $this->__('Ort: ') . $params['location'] . '<br/>';
        if($params['telephone']) $bodyText .= $this->__('Telefon: ') . $params['telephone'] . '<br/>';
        $bodyText .= $this->__('E-Mail: ') . $params['email'] . '<br/>';

        $mail->setBodyHtml( 'Kunde hat sich für ein Prämienprogramm angemeldet.' . '<br/>' . $bodyText);
        $mail->setFrom($params['email'], $params['name']);
//        $mail->addTo('Franziska.Goldinger@henryschein.com', $storeEmailAdressName);
//        $mail->addTo('Jessica.Wirz@henryschein.com', $storeEmailAdressName);
        $mail->addTo('praxisbedarf@covetrus.ch', $storeEmailAdressName);
        $mail->setSubject('Prämienprogramm Anmeldung');
        $mail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);

        try {
            $mail->send();
        }
        catch(Exception $ex) {
            Mage::getSingleton('core/session')->addError('Unable to send email. Sample of a custom notification error from Snowflake_FormHandler sendOwnbrandMailAction.');

        }

        /* Send OwnBrand registration info to applicant */
        $mail = new Zend_Mail('UTF8');
        $mail->setBodyText('Vielen Dank für die Anmeldung für das Prämienprogramm: "' . $params['register'] . '"');
        $mail->setFrom($storeEmailAdress, $storeEmailAdressName);
        $mail->addTo($params['email'], '');
        $mail->setSubject('Anmeldung Prämienprogramm');
        $mail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);

        try {
            $mail->send();
        }
        catch(Exception $ex) {
            Mage::getSingleton('core/session')->addError('Unable to send email. Sample of a custom notification error from Snowflake_FormHandler sendOwnbrandMailAction .');

        }

        $this->_redirectReferer();
    }

    protected function _getCaptchaString($request, $formId)
    {
        $captchaParams = $request->getPost(Mage_Captcha_Helper_Data::INPUT_NAME_FIELD_VALUE);
        return $captchaParams[$formId];
    }
    
    public function sendordermailAction () {
        $params = $this->getRequest()->getParams();

        $formId = 'order_form';
        $captchaModel = Mage::helper('captcha')->getCaptcha($formId);
        if ($captchaModel->isRequired()) {
            if (!$captchaModel->isCorrect($this->_getCaptchaString($this->getRequest(), $formId))) {
                Mage::getSingleton('core/session')->addError(Mage::helper('captcha')->__('Incorrect CAPTCHA.'));
                $this->_redirectReferer();
                return;
            }
        }

        $storeEmailAdressName = Mage::getStoreConfig('trans_email/ident_general/name');
        $storeEmailAdress = "bestellung@covetrus.ch";

        /* Send OwnBrand registration info to Henryschein */
        $mail = new Zend_Mail('UTF8');
        $bodyText = '';
        $bodyText .= "<br />";
        $bodyText .= "<table>
                            <thead>
                                <tr>
                                    <th style='padding-right: 25px; text-align: left;' ><label></label></th>
                                    <th style='padding-right: 25px; text-align: left;'><label></label></th>
                                </tr>
                            </thead>
                            <tbody>";
        if($params['client_number'])    $bodyText .= "<tr><td style='padding-right: 25px;text-align: left;'>". Mage::helper('contacts')->__('Client Number').   ":</td><td>".  $params['client_number'] .   "</td></tr>";
        if($params['reference'])        $bodyText .= "<tr><td style='padding-right: 25px;text-align: left;'>". Mage::helper('contacts')->__('Reference').       ":</td><td>".  $params['reference'] .       "</td></tr>";
        if($params['company_name'])     $bodyText .= "<tr><td style='padding-right: 25px;text-align: left;'>". Mage::helper('contacts')->__('Company Name').    ":</td><td>".  $params['company_name'] .    "</td></tr>";
        if($params['name'])             $bodyText .= "<tr><td style='padding-right: 25px;text-align: left;'>". Mage::helper('contacts')->__('Name').            ":</td><td>".  $params['name'] .            "</td></tr>";
        if($params['address'])          $bodyText .= "<tr><td style='padding-right: 25px;text-align: left;'>". Mage::helper('contacts')->__('Address').         ":</td><td>".  $params['address'] .         "</td></tr>";
        if($params['place'])            $bodyText .= "<tr><td style='padding-right: 25px;text-align: left;'>". Mage::helper('contacts')->__('Postal Number').   ":</td><td>".  $params['postal_number'] .   "</td></tr>";
        if($params['place'])            $bodyText .= "<tr><td style='padding-right: 25px;text-align: left;'>". Mage::helper('contacts')->__('Place').           ":</td><td>".  $params['place'] .           "</td></tr>";
        if($params['phone'])            $bodyText .= "<tr><td style='padding-right: 25px;text-align: left;'>". Mage::helper('contacts')->__('Phone').           ":</td><td>".  $params['phone'] .           "</td></tr>";
        if($params['company_name'])     $bodyText .= "<tr><td style='padding-right: 25px;text-align: left;'>". Mage::helper('contacts')->__('Date').            ":</td><td>".  $params['date'] .            "</td></tr>";
        if($params['email'])            $bodyText .= "<tr><td style='padding-right: 25px;text-align: left;'>". Mage::helper('contacts')->__('Email').           ":</td><td>".  $params['email'] .           "</td></tr>";
        
        $bodyText .= "</tbody></table>";
        
        if (isset($params["order"]) && count($params["order"])) {
            $bodyText .= "<br />";
            $bodyText .= "<table>
                            <thead>
                                <tr>
                                    <th style='padding-right: 25px; text-align: left;' ><label>".Mage::helper('contacts')->__('SKU')."</label></th>
                                    <th style='padding-right: 25px; text-align: left;'><label>".Mage::helper('contacts')->__('Description')."</label></th>
                                    <th style='padding-right: 25px; text-align: left;'><label>".Mage::helper('contacts')->__('Qty')."</label></th>
                                </tr>
                            </thead>
                            <tbody>";
            foreach($params["order"] as $index => $info) {
                $bodyText .= "<tr><td style='padding-right: 25px;text-align: left;'>".$info["sku"]."</td><td style='padding-right: 25px;text-align: left;'>".$info["description"]."</td><td style='padding-right: 25px;text-align: left;'>".$info["qty"]."</td></tr>";
            }
            $bodyText .= "</tbody></table>";
        }
        
        if($params['comments']) $bodyText .= "<br /><br />" . Mage::helper('contacts')->__('Comments') . ": <br/>" . $params['comments'] . '<br/>';

        $mail->setBodyHtml($bodyText);
        $mail->setFrom($params['email'], $params['name']);
//        $mail->addTo('Franziska.Goldinger@henryschein.com', $storeEmailAdressName);
//        $mail->addTo('Jessica.Wirz@henryschein.com', $storeEmailAdressName);

        $mail->setSubject(Mage::helper("contacts")->__("Order"));
        $mail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);

	    $mail->addTo('bestellung@covetrus.ch', $storeEmailAdressName);

        try {
            $mail->send();
            
            Mage::getSingleton('core/session')->addSuccess(Mage::helper('contacts')->__('Thank you so much. We received your order!'));
        }
        catch(Exception $ex) {
            Mage::getSingleton('core/session')->addError('Unable to send email. Sample of a custom notification error from Snowflake_FormHandler sendOwnbrandMailAction.');
        }

        $this->_redirectReferer();
    }
    
    public function sendreturnmailAction () {
        $params = $this->getRequest()->getParams();

        $formId = 'return_form';
        $captchaModel = Mage::helper('captcha')->getCaptcha($formId);
        if ($captchaModel->isRequired()) {
            if (!$captchaModel->isCorrect($this->_getCaptchaString($this->getRequest(), $formId))) {
                Mage::getSingleton('core/session')->addError(Mage::helper('captcha')->__('Incorrect CAPTCHA.'));
                $this->_redirectReferer();
                return;
            }
        }

        $storeEmailAdressName = Mage::getStoreConfig('trans_email/ident_general/name');
        $storeEmailAdress = "bestellung@covetrus.ch";

        /* Send OwnBrand registration info to Henryschein */
        $mail = new Zend_Mail('UTF8');
        $bodyText = '';
        if($params['return_type']) $bodyText .= Mage::helper('contacts')->__('Return Return Type') . ": " . $params['return_type'] . '<br/>';
        if($params['name']) $bodyText .= Mage::helper('contacts')->__('Return Name') . ": " . $params['name'] . '<br/>';
        if($params['client_number']) $bodyText .= Mage::helper('contacts')->__('Return Client Number') . ": " . $params['client_number'] . '<br/>';
        if($params['phone']) $bodyText .= Mage::helper('contacts')->__('Return Phone') . ": " . $params['phone'] . '<br/>';
        if($params['email']) $bodyText .= Mage::helper('contacts')->__('Return Email') . ": " . $params['email'] . '<br/>';
        if($params['comments']) $bodyText .= Mage::helper('contacts')->__('Return Comments') . ": " . $params['comments'] . '<br/>';

        $mail->setBodyHtml($bodyText);
        $mail->setFrom($params['email'], $params['name']);
//        $mail->addTo('Franziska.Goldinger@henryschein.com', $storeEmailAdressName);
//        $mail->addTo('Jessica.Wirz@henryschein.com', $storeEmailAdressName);

        $mail->setSubject(Mage::helper("contacts")->__("Return"));
        $mail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);

	    $mail->addTo('bestellung@covetrus.ch', $storeEmailAdressName);

        try {
            $mail->send();
            
            Mage::getSingleton('core/session')->addSuccess(Mage::helper('contacts')->__('Thank you for your registration. Our customer service will contact you as soon as possible. Please do not send us products right away but wait for the customer service contact.'));
        }
        catch(Exception $ex) {
            Mage::getSingleton('core/session')->addError('Unable to send email. Sample of a custom notification error from Snowflake_FormHandler sendOwnbrandMailAction.');
        }

        $this->_redirectReferer();
    }

    /*public function __($param){
        return Mage::helper('core')->__($param);
    }*/
}

?>