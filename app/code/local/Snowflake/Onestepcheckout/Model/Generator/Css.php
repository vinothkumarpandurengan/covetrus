<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 13.01.2015
 * Time: 09:52
 */ 
class Snowflake_Onestepcheckout_Model_Generator_Css extends Magegiant_Onestepcheckout_Model_Generator_Css
{

    protected function _generateStoreCss($storeCode, $section)
    {
        if (!Mage::app()->getStore($storeCode)->getIsActive()) return;
        $store       = '_' . $storeCode;
        $cssFile     = $section . $store . '.css';
        $cssFileDir  = Mage::helper('onestepcheckout/generator_css')->getGeneratedCssDir() . $cssFile;
        $cssTemplate = Mage::helper('onestepcheckout/generator_css')->getTemplatePath() . $section . '.phtml';
        Mage::register('onestepcheckout_generator_css_store', $storeCode);
        try {
            Mage::getDesign()->setArea('frontend')->setPackageName((string) Mage::getConfig()->getNode('stores/default/design/package/name'));
            $cssGenerated = Mage::app()->getLayout()->createBlock('onestepcheckout/generator_css')->setData('area', 'frontend')->setTemplate($cssTemplate)->toHtml();
            if (empty($cssGenerated)) {
                throw new Exception(Mage::helper('onestepcheckout')->__("Template file is empty or doesn't exist: %s", $cssTemplate));
            }
            $varienFile = new Varien_Io_File();
            $varienFile->setAllowCreateFolders(true);
            $varienFile->open(array('path' => Mage::helper('onestepcheckout/generator_css')->getGeneratedCssDir()));
            $varienFile->streamOpen($cssFileDir, 'w+', 0777);
            $varienFile->streamLock(true);
            $varienFile->streamWrite($cssGenerated);
            $varienFile->streamUnlock();
            $varienFile->streamClose();
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('onestepcheckout')->__('Failed generating CSS file: %s in %s', $cssFile, Mage::helper('onestepcheckout/generator_css')->getGeneratedCssDir()) . '<br/>Message: ' . $e->getMessage());
            Mage::logException($e);
        }
        Mage::app()->getCacheInstance()->flush();
        Mage::unregister('onestepcheckout_generator_css_store');
    }
}