<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 04.03.2015
 * Time: 16:23
 */

class Snowflake_Onestepcheckout_Model_Observer
{
    public function checkReadOnly(){
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            if($customer->getReadOnly()) {
                Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('onestepcheckout/readonly'));
            }
        }
    }
}
  