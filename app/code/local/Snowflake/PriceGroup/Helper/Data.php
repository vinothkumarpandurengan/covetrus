<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 30.01.2015
 * Time: 12:42
 */
class Snowflake_PriceGroup_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * return date format
     *
     * @return string
     */
    public function getDateFormat()
    {
        return Mage::app()->getLocale()->getDateStrFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
    }

    /**
     * Return yes / no attribute
     *
     * @return array
     */
    public function getFinalprices()
    {
        $opt = Mage::getModel('eav/entity_attribute_source_boolean');
        return $opt->getAllOptions();
    }
}
  