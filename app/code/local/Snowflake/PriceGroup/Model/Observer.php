<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 24.04.2015
 * Time: 13:43
 */

class Snowflake_PriceGroup_Model_Observer
{
    /**
     * Create block for customer price
     *
     * @param Varien_Event_Observer $observer
     */
    public function adminhtmlCatalogProductEditPrepareForm(Varien_Event_Observer $observer)
    {
        $form = $observer->getEvent()->getForm();
        $customerPrice = $form->getElement('customer_price');
        if ($customerPrice) {
            $customerPrice->setRenderer(
                Mage::app()->getLayout()->createBlock('snowflake_pricegroup/adminhtml_catalog_product_edit_tab_price_customer')
            );
        }
    }

    /**
     * Update product final, group & tier price of customer group for minicart
     *
     * @param Varien_Event_Observer $observer
     */
    public function salesQuoteItemSetProduct($observer)
    {
        $product = $observer->getProduct();

        if ($product instanceof Mage_Catalog_Model_Product) {
            if ($groupId = Mage::getSingleton('customer/session')->getCustomerGroupId()) {
                $product->unsTierPrice();
                $product->unsMinimalPrice();

                $productPrice  = $this->_getGroupPrice($product, $groupId);
                $tierPrice     = $this->_getTierPrice($product, $groupId);
                $customerPrice = $this->_getCustomerPrice($product, $groupId);

                if ($customerPrice && is_array($customerPrice)) {
                    $tierPrice = $customerPrice;
                }

                $product->setFinalPrice($productPrice);
                $product->setPrice($productPrice);
                $product->setTierPrice($tierPrice);
            }
        }
    }

    /**
     * Update product final, group & tier price of customer group for product detail page
     *
     * @param Varien_Event_Observer $observer
     */
    public function catalogProductLoadAfter($observer)
    {
        $product = $observer->getEvent()->getProduct();

        if ($product instanceof Mage_Catalog_Model_Product) {
            if ($groupId = Mage::getSingleton('customer/session')->getCustomerGroupId()) {
                $product->unsTierPrice();
                $product->unsMinimalPrice();

                $productPrice  = $this->_getGroupPrice($product, $groupId);
                $tierPrice     = $this->_getTierPrice($product, $groupId);
                $customerPrice = $this->_getCustomerPrice($product, $groupId);

                if ($customerPrice && is_array($customerPrice)) {
                    $tierPrice = $customerPrice;
                }

                $product->setFinalPrice($productPrice);
                $product->setPrice($productPrice);
                $product->setTierPrice($tierPrice);
            }
        }
    }

    /**
     * Update product final, group & tier price of customer group for product list page
     *
     * @param Varien_Event_Observer $observer
     */
    public function catalogProductCollectionLoadAfter($observer)
    {
        $collection = $observer->getEvent()->getCollection();

        if (($collection instanceof Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection) ||
            ($collection instanceof Mage_Catalog_Model_Resource_Product_Collection)) {

            if ($groupId = Mage::getSingleton('customer/session')->getCustomerGroupId()) {
                foreach ($collection as $product)
                {
                    $product->unsTierPrice();
                    $product->unsMinimalPrice();

                    $productPrice  = $this->_getGroupPrice($product, $groupId);
                    $tierPrice     = $this->_getTierPrice($product, $groupId);
                    $customerPrice = $this->_getCustomerPrice($product, $groupId);

                    if ($customerPrice && is_array($customerPrice)) {
                        $tierPrice = $customerPrice;
                    }

                    $product->setFinalPrice($productPrice);
                    $product->setPrice($productPrice);
                    $product->setTierPrice($tierPrice);
                }
            }
        }
    }

    protected function _getTierPrice($product, $groupId)
    {
        $allGroups  = Mage_Customer_Model_Group::CUST_GROUP_ALL;
        $tierPrices = $product->getData('tier_price');

        if (is_null($tierPrices) || !is_array($tierPrices)) {
            $attribute = $product->getResource()->getAttribute('tier_price');
            if ($attribute) {
                $attribute->getBackend()->afterLoad($product);
                $tierPrices = $product->getData('tier_price');
            }
        }

        $prices = array();

        $resource = Mage::getSingleton('core/resource');

        $readConnection = $resource->getConnection('core_read');

        $tableName = $resource->getTableName('catalog_product_entity_tier_price');

        foreach ($tierPrices as $i => $data) {
            if ($data['cust_group'] != $groupId && $data['cust_group'] != $allGroups) {
                // delete tier price from other customer groups
                unset($tierPrices[$i]);
            } else {
                $startDate = $readConnection->fetchCol('SELECT `start_date` FROM ' . $tableName . ' WHERE `value_id` = ' . $data["price_id"]);

                $endDate = $readConnection->fetchCol('SELECT `end_date` FROM ' . $tableName . ' WHERE `value_id` = ' . $data["price_id"]);

                if (time() >= time($startDate) && time() <= time($endDate)) {
                    // only set tier price from date range
                    $prices[] = $data['website_price'];
                }
                else {
                    unset($tierPrices[$i]);
                }
            }
        }

        $product->setData('minimal_price', sizeof($prices) ? min($prices) : false);
        return $tierPrices;
    }

    /**
     * Get product group price
     *
     * @param Mage_Catalog_Model_Product $product
     * @param int $groupId
     * @return float
     */
    protected function _getGroupPrice($product, $groupId)
    {
        $groupPrices = $product->getData('group_price');

        if (is_null($groupPrices) || !is_array($groupPrices)) {
            $attribute = $product->getResource()->getAttribute('group_price');
            if ($attribute) {
                $attribute->getBackend()->afterLoad($product);
                $groupPrices = $product->getData('group_price');
            }
        }

        if (is_null($groupPrices) || !is_array($groupPrices)) {
            return $product->getPrice();
        }

        $matchedPrice = $product->getPrice();
        foreach ($groupPrices as $data) {
            if ($data['cust_group'] == $groupId && $data['website_price'] < $matchedPrice) {
                if (Mage::app()->getLocale()->isStoreDateInInterval($product->getStore(), $data['start_date'], $data['end_date'])) {
                    $matchedPrice = $data['website_price'];
                }
                break;
            }
        }

        return $matchedPrice;
    }

    /**
     * Get product customer price
     *
     * @param Mage_Catalog_Model_Product $product
     * @return float
     */
    protected function _getCustomerPrice($product, $groupId)
    {
        $customerPrices = $product->getData('customer_price');

        if (is_null($customerPrices) || !is_array($customerPrices)) {
            $attribute = $product->getResource()->getAttribute('customer_price');
            if ($attribute) {
                $attribute->getBackend()->afterLoad($product);
                $customerPrices = $product->getData('customer_price');
            }
        }

        $prices     = array();
        $customerId = Mage::getSingleton('customer/session')->getId();
        foreach ($customerPrices as $i => $data) {
            if ($data['customer'] != $customerId) {
                unset($customerPrices[$i]);
            } else {
                if (Mage::app()->getLocale()->isStoreDateInInterval($product->getStore(), $data['start_date'], $data['end_date'])) {
                    // only set customer price from date range
                    $customerPrice = $data['customer_price'];
                    if ($data['discount'] > 0) {
                        $customerPrice = $customerPrice - ($customerPrice * ($data['discount'] / 100));
                        $data['customer_price'] = $customerPrice;
                    }

                    $customerPrices[$i]['cust_group'] = $groupId;
                    $customerPrices[$i]['price'] = $customerPrice;
                    $customerPrices[$i]['customer_price'] = $customerPrice;
                    $customerPrices[$i]['website_price'] = $customerPrice;

                    $prices[] = $data['customer_price'];
                } else {
                    unset($customerPrices[$i]);
                }
            }
        }

        if (!$product->getMinimalPrice() || (sizeof($prices) > 0 && $product->getMinimalPrice() > min($prices, 0))) {
            $product->setData('minimal_price', sizeof($prices) ? min($prices) : false);

            return $customerPrices;
        } else {
            return false;
        }
    }
}
  