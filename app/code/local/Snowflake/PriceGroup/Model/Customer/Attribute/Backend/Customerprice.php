<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 08.10.2015
 * Time: 09:10
 */
class Snowflake_PriceGroup_Model_Customer_Attribute_Backend_Customerprice
    extends Mage_Eav_Model_Entity_Attribute_Backend_Abstract
{
    /**
     * Website currency codes and rates
     *
     * @var array
     */
    protected $_rates;

    /**
     * Retrieve resource instance
     *
     * @return Snowflake_PriceGroup_Model_Catalog_Product_Attribute_Backend_Customerprice
     */
    protected function _getResource()
    {
        return Mage::getResourceSingleton('snowflake_pricegroup/catalog_product_attribute_backend_customerprice');
    }

    /**
     * Error message when duplicates
     *
     * @return string
     */
    protected function _getDuplicateErrorMessage()
    {
        return Mage::helper('snowflake_pricegroup')->__('Duplicate website customer price, product and quantity.');
    }

    /**
     * Check customer attribute scope is global
     *
     * @return bool
     */
    protected function _isScopeGlobal()
    {
        return Mage::getModel('customer/customer')->getSharingConfig()->isGlobalScope();
    }

    /**
     * Retrieve websites currency rates and base currency codes
     *
     * @return array
     */
    protected function _getWebsiteCurrencyRates()
    {
        if (is_null($this->_rates)) {
            $this->_rates = array();
            $baseCurrency = Mage::app()->getBaseCurrencyCode();
            foreach (Mage::app()->getWebsites() as $website) {
                /* @var $website Mage_Core_Model_Website */
                if ($website->getBaseCurrencyCode() != $baseCurrency) {
                    $rate = Mage::getModel('directory/currency')
                        ->load($baseCurrency)
                        ->getRate($website->getBaseCurrencyCode());
                    if (!$rate) {
                        $rate = 1;
                    }
                    $this->_rates[$website->getId()] = array(
                        'code' => $website->getBaseCurrencyCode(),
                        'rate' => $rate
                    );
                } else {
                    $this->_rates[$website->getId()] = array(
                        'code' => $baseCurrency,
                        'rate' => 1
                    );
                }
            }
        }
        return $this->_rates;
    }

    /**
     * Get product name by id
     *
     * @return string
     */
    protected function _getProductName($id)
    {
        $_productCollection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToFilter('entity_id', array('in' => $id))
            ->addAttributeToSelect('name');

        foreach ($_productCollection as $_product) {
            $_productName = $_product->getSku() .' / ' . $_product->getName();
        }

        return $_productName;
    }

    /**
     * Add price qty to unique fields
     *
     * @param array $objectArray
     * @return array
     */
    protected function _getAdditionalUniqueFields($objectArray)
    {
        $uniqueFields = array();
        $uniqueFields['qty'] = $objectArray['price_qty'] * 1;
        return $uniqueFields;
    }

    /**
     * After load method
     *
     * @param Mage_Customer_Model_Customer $object
     * @return Snowflake_PriceGroup_Model_Customer_Attribute_Backend_Customerprice
     */
    public function afterLoad($object)
    {
        $storeId = Mage::app()->getStore()->getId();

        if ($storeId != 0) {
            return $this;
        }

        $websiteId = null;
        if ($this->_isScopeGlobal()) {
            $websiteId = 0;
        } else if ($storeId) {
            $websiteId = Mage::app()->getStore($storeId)->getWebsiteId();
        }

        $data = $this->_getResource()->loadPriceDataByCustomer($object->getId(), $websiteId);
        foreach ($data as $k => $v) {
            $data[$k]['productname']    = $this->_getProductName($v['product_id']);
            $data[$k]['customer_price'] = $v['price'];

            if ($v['start_date']) {
                $date = new Zend_Date($v['start_date']);
                $data[$k]['start_date'] = $date->toString(Mage::app()->getLocale()->getDateFormat('short'));
            }

            if ($v['end_date']) {
                $date = new Zend_Date($v['end_date']);
                $data[$k]['end_date'] = $date->toString(Mage::app()->getLocale()->getDateFormat('short'));
            }
        }

        $object->setData($this->getAttribute()->getName(), $data);
        $object->setOrigData($this->getAttribute()->getName(), $data);

        $valueChangedKey = $this->getAttribute()->getName() . '_changed';
        $object->setOrigData($valueChangedKey, 0);
        $object->setData($valueChangedKey, 0);

        return $this;
    }

    /**
     * Validate customer price data
     *
     * @param Snowflake_Customer_Model_Customer $object
     * @throws Mage_Core_Exception
     * @return bool
     */
    public function validate($object)
    {
        $attribute = $this->getAttribute();
        $priceRows = $object->getData($attribute->getName());
        if (empty($priceRows)) {
            return true;
        }

        // validate per website
        $duplicates = array();
        foreach ($priceRows as $priceRow) {
            if (!empty($priceRow['delete'])) {
                continue;
            }
            $compare = join('-', array_merge(
                array($priceRow['website_id'], $priceRow['product_id']),
                $this->_getAdditionalUniqueFields($priceRow)
            ));
            if (isset($duplicates[$compare])) {
                return $this->_getDuplicateErrorMessage();
                break;
            }
            $duplicates[$compare] = true;
        }

        // if attribute scope is website and edit in store view scope
        // add global group prices for duplicates find
        if (!$this->_isScopeGlobal() && $object->getStore()->getId()) {
            $origGroupPrices = $object->getOrigData($attribute->getName());
            foreach ($origGroupPrices as $price) {
                if ($price['website_id'] == 0) {
                    $compare = join('-', array_merge(
                        array($price['website_id'], $price['product_id']),
                        $this->_getAdditionalUniqueFields($price)
                    ));
                    $duplicates[$compare] = true;
                }
            }
        }

        // validate currency
        $baseCurrency = Mage::app()->getBaseCurrencyCode();
        $rates = $this->_getWebsiteCurrencyRates();
        foreach ($priceRows as $priceRow) {
            if (!empty($priceRow['delete'])) {
                continue;
            }
            if ($priceRow['website_id'] == 0) {
                continue;
            }

            $globalCompare = join('-', array_merge(
                array(0, $priceRow['product_id']),
                $this->_getAdditionalUniqueFields($priceRow)
            ));
            $websiteCurrency = $rates[$priceRow['website_id']]['code'];

            if ($baseCurrency == $websiteCurrency && isset($duplicates[$globalCompare])) {
                return $this->_getDuplicateErrorMessage();
                break;
            }
        }

        return true;
    }

    /**
     * After Save Attribute manipulation
     *
     * @param Snowflake_Customer_Model_Customer $object
     * @return Snowflake_PriceGroup_Model_Customer_Attribute_Backend_Customerprice
     */
    public function afterSave($object)
    {
        $websiteId  = Mage::app()->getStore($object->getStoreId())->getWebsiteId();
        $isGlobal   = $this->_isScopeGlobal() || $websiteId == 0;

        $priceRows = $object->getData($this->getAttribute()->getName());
        if (empty($priceRows)) {
            return $this;
        }

        $old = array();
        $new = array();

        // prepare original data for compare
        $origCustomerPrices = $object->getOrigData($this->getAttribute()->getName());
        if (!is_array($origCustomerPrices)) {
            $origCustomerPrices = array();
        }
        foreach ($origCustomerPrices as $data) {
            if ($data['website_id'] > 0 || ($data['website_id'] == '0' && $isGlobal)) {
                $key = join('-', array_merge(
                    array($data['website_id'], $data['product_id']),
                    $this->_getAdditionalUniqueFields($data)
                ));
                $old[$key] = $data;
            }
        }

        // prepare data for save
        foreach ($priceRows as $data) {
            $hasEmptyData = false;
            foreach ($this->_getAdditionalUniqueFields($data) as $field) {
                if (empty($field)) {
                    $hasEmptyData = true;
                    break;
                }
            }

            if ($hasEmptyData || !isset($data['product_id']) || !empty($data['delete'])) {
                continue;
            }
            if ($this->_isScopeGlobal() && $data['website_id'] > 0) {
                continue;
            }
            if (!$isGlobal && (int)$data['website_id'] == 0) {
                continue;
            }

            $key = join('-', array_merge(
                array($data['website_id'], $data['product_id']),
                $this->_getAdditionalUniqueFields($data)
            ));

            $new[$key] = array_merge(array(
                'website_id'        => $data['website_id'],
                'product'           => $data['product_id'],
                'customer_id'       => $data['customer'],
                'start_date'        => $data['customer_price_start_date'],
                'end_date'          => $data['customer_price_end_date'],
                'qty'               => $data['price_qty'],
                'value'             => $data['price'],
                'discount'          => $data['discount'],
                'final_price'       => $data['finalprice'],
            ), $this->_getAdditionalUniqueFields($data));
        }

        $delete = array_diff_key($old, $new);
        $insert = array_diff_key($new, $old);
        $update = array_intersect_key($new, $old);

        $isChanged  = false;

        if (!empty($delete)) {
            foreach ($delete as $data) {
                $this->_getResource()->deletePriceData($data['product_id'], null, $data['price_id']);
                $isChanged = true;
            }
        }

        if (!empty($insert)) {
            foreach ($insert as $data) {
                $price = new Varien_Object($data);
                $price->setEntityId($data['product']);
                $this->_getResource()->savePriceData($price);

                $isChanged = true;
            }
        }

        if (!empty($update)) {
            foreach ($update as $k => $v) {
                if ($old[$k]['price'] != $v['value']) {
                    $price = new Varien_Object(array(
                        'value_id'  => $old[$k]['price_id'],
                        'value'     => $v['value']
                    ));
                    $this->_getResource()->savePriceData($price);

                    $isChanged = true;
                }
            }
        }

        if ($isChanged) {
            $valueChangedKey = $this->getAttribute()->getName() . '_changed';
            $object->setData($valueChangedKey, 1);
        }

        return $this;
    }
}