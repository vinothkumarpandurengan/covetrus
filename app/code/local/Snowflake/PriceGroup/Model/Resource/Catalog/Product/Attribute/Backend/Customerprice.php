<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 02.10.2015
 * Time: 11:23
 */

class Snowflake_PriceGroup_Model_Resource_Catalog_Product_Attribute_Backend_Customerprice
    extends Mage_Catalog_Model_Resource_Product_Attribute_Backend_Groupprice_Abstract
{
    /**
     * Initialize connection and define main table
     *
     */
    protected function _construct()
    {
        $this->_init('snowflake_pricegroup/catalog_product_attribute_customer_price', 'value_id');
    }

    /**
     * Load Customer Prices for product
     *
     * @param int $productId
     * @param int $websiteId
     * @return Snowflake_PriceGroup_Model_Resource_Catalog_Product_Attribute_Backend_Customerprice
     */
    public function loadPriceData($productId, $websiteId = null)
    {
        $adapter = $this->_getReadAdapter();

        $columns = array(
            'price_id'      => $this->getIdFieldName(),
            'website_id'    => 'website_id',
            'price_qty'     => 'qty',
            'customer'      => 'customer_id',
            'price'         => 'value',
            'start_date'    => 'start_date',
            'end_date'      => 'end_date',
            'discount'      => 'discount',
            'finalprice'    => 'final_price',
        );

        $columns = $this->_loadPriceDataColumns($columns);

        $select  = $adapter->select()
            ->from($this->getMainTable(), $columns)
            ->where('entity_id=?', $productId);

        $this->_loadPriceDataSelect($select);

        if (!is_null($websiteId)) {
            if ($websiteId == '0') {
                $select->where('website_id = ?', $websiteId);
            } else {
                $select->where('website_id IN(?)', array(0, $websiteId));
            }
        }

        return $adapter->fetchAll($select);
    }

    /**
     * Load Customer Prices for product
     *
     * @param int $customerId
     * @param int $websiteId
     * @return Snowflake_PriceGroup_Model_Resource_Catalog_Product_Attribute_Backend_Customerprice
     */
    public function loadPriceDataByCustomer($customerId, $websiteId = null)
    {
        $adapter = $this->_getReadAdapter();

        $columns = array(
            'price_id'      => $this->getIdFieldName(),
            'website_id'    => 'website_id',
            'product_id'    => 'entity_id',
            'price_qty'     => 'qty',
            'customer'      => 'customer_id',
            'price'         => 'value',
            'start_date'    => 'start_date',
            'end_date'      => 'end_date',
            'discount'      => 'discount',
            'finalprice'    => 'final_price',
        );

        $columns = $this->_loadPriceDataColumns($columns);

        $select  = $adapter->select()
            ->from($this->getMainTable(), $columns)
            ->where('customer_id=?', $customerId);

        $this->_loadPriceDataSelect($select);

        if (!is_null($websiteId)) {
            if ($websiteId == '0') {
                $select->where('website_id = ?', $websiteId);
            } else {
                $select->where('website_id IN(?)', array(0, $websiteId));
            }
        }

        return $adapter->fetchAll($select);
    }
}