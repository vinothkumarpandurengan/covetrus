<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 02.02.2015
 * Time: 15:36
 */ 
class Snowflake_PriceGroup_Block_Adminhtml_Catalog_Product_Edit_Tab_Price_Group
    extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Price_Group
{
    /**
     * Initialize block
     */
    public function __construct()
    {
        $this->setTemplate('snowflake/catalog/product/edit/price/group.phtml');
    }

    /**
     * return date format
     *
     * @return string
     */
    public function getDateFormat()
    {
        return $this->helper('snowflake_pricegroup')->getDateFormat();
    }
}