<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 02.10.2015
 * Time: 12:47
 */

class Snowflake_PriceGroup_Block_Adminhtml_Catalog_Product_Edit_Tab_Price_Customer
    extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Price_Group_Abstract
{
    /**
     * Initialize block
     */
    public function __construct()
    {
        $this->setTemplate('snowflake/catalog/product/edit/price/customer.phtml');
    }

    /**
     * Prepare global layout
     *
     * Add "Add Customer Price" button to layout
     *
     * @return Snowflake_PriceGroup_Block_Adminhtml_Catalog_Product_Edit_Tab_Price_Customer
     */
    protected function _prepareLayout()
    {
        $button = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setData(array(
                'label' => Mage::helper('catalog')->__('Add Customer Price'),
                'onclick' => 'return customerPriceControl.addItem()',
                'class' => 'add'
            ));
        $button->setName('add_customer_price_item_button');

        $this->setChild('add_button', $button);
        return parent::_prepareLayout();
    }

    /**
     * Sort values
     *
     * @param array $data
     * @return array
     */
    protected function _sortValues($data)
    {
        usort($data, array($this, '_sortCustomerPrices'));
        return $data;
    }

    /**
     * Sort customer price values callback method
     *
     * @param array $a
     * @param array $b
     * @return int
     */
    protected function _sortCustomerPrices($a, $b)
    {
        if ($a['website_id'] != $b['website_id']) {
            return $a['website_id'] < $b['website_id'] ? -1 : 1;
        }
        if ($a['price_qty'] != $b['price_qty']) {
            return $a['price_qty'] < $b['price_qty'] ? -1 : 1;
        }

        return 0;
    }

    /**
     * Return yes / no attribute
     *
     * @return array
     */
    public function getFinalprices()
    {
        return $this->helper('snowflake_pricegroup')->getFinalprices();
    }

    /**
     * Return serialized list of all customers
     *
     * @return string
     */
    public function getAllCustomers()
    {
        $_customerCollection = Mage::getResourceModel('customer/customer_collection')
            ->addAttributeToSelect('id')
            ->addAttributeToSelect('customer_id')
            ->addNameToSelect()
            ->addAttributeToFilter('is_active', 1);

        $customers = array();
        foreach ($_customerCollection as $_customer)
        {
            $id = $_customer->getId();
            $customers[$id] = array(
                'id'    => $id,
                'label' => $_customer->getCustomerId() .' / ' . $_customer->getName()
            );
        }

        return Mage::helper('core')->jsonEncode($customers);
    }

    /**
     * return date format
     *
     * @return string
     */
    public function getDateFormat()
    {
        return $this->helper('snowflake_pricegroup')->getDateFormat();
    }
}