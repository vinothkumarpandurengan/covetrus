<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 02.10.2015
 * Time: 12:50
 */

class Snowflake_PriceGroup_Block_Adminhtml_Customer_Edit_Render_Customerprice
    extends Mage_Adminhtml_Block_Widget
    implements Varien_Data_Form_Element_Renderer_Interface
{
    /**
     * Form element instance
     *
     * @var Varien_Data_Form_Element_Abstract
     */
    protected $_element;

    /**
     * Websites cache
     *
     * @var array
     */
    protected $_websites;

    /**
     * Override parent constructor just for setting custom template
     */
    protected function _construct()
    {
        $this->setTemplate('snowflake/customer/edit/account.phtml');
    }

    /**
     * Prepare global layout
     *
     * Add "Add Customer Price" button to layout
     *
     * @return Snowflake_PriceGroup_Block_Adminhtml_Customer_Edit_Render_Customerprice
     */
    protected function _prepareLayout()
    {
        $button = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setData(array(
                'label' => Mage::helper('catalog')->__('Add Customer Price'),
                'onclick' => 'return customerPriceControl.addItem()',
                'class' => 'add'
            ));
        $button->setName('add_customer_price_item_button');

        $this->setChild('add_button', $button);
        return parent::_prepareLayout();
    }

    /**
     * Sort customer price values callback method
     *
     * @param array $a
     * @param array $b
     * @return int
     */
    protected function _sortCustomerPrices($a, $b)
    {
        if ($a['website_id'] != $b['website_id']) {
            return $a['website_id'] < $b['website_id'] ? -1 : 1;
        }
        if ($a['price_qty'] != $b['price_qty']) {
            return $a['price_qty'] < $b['price_qty'] ? -1 : 1;
        }

        return 0;
    }

    /**
     * Render HTML
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);
        return $this->toHtml();
    }

    /**
     * Set form element instance
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return Snowflake_PriceGroup_Block_Adminhtml_Customer_Edit_Render_Customerprice
     */
    public function setElement(Varien_Data_Form_Element_Abstract $element)
    {
        $this->_element = $element;
        return $this;
    }

    /**
     * Retrieve form element instance
     *
     * @return Varien_Data_Form_Element_Abstract
     */
    public function getElement()
    {
        return $this->_element;
    }

    /**
     * Prepare customer price values
     *
     * @return array
     */
    public function getValues()
    {
        $values = array();
        $data = $this->getElement()->getValue();

        if (is_array($data)) {
            $values = $this->_sortValues($data);
        }

        foreach ($values as &$value) {
            $value['readonly'] = ($value['website_id'] == 0)
                && $this->isShowWebsiteColumn()
                && !$this->isAllowChangeWebsite();
        }

        return $values;
    }

    /**
     * Sort values
     *
     * @param array $data
     * @return array
     */
    protected function _sortValues($data)
    {
        usort($data, array($this, '_sortCustomerPrices'));
        return $data;
    }

    /**
     * Show website column and switcher for group price table
     *
     * @return bool
     */
    public function isMultiWebsites()
    {
        return !Mage::app()->isSingleStoreMode();
    }

    /**
     * Retrieve allowed for edit websites
     *
     * @return array
     */
    public function getWebsites()
    {
        if (!is_null($this->_websites)) {
            return $this->_websites;
        }

        $this->_websites = array(
            0 => array(
                'name' => Mage::helper('catalog')->__('All Websites'),
                'currency' => Mage::app()->getBaseCurrencyCode()
            )
        );

        if (!$this->isScopeGlobal()) {
            $websites = Mage::app()->getWebsites(false);
            foreach ($websites as $website) {
                $this->_websites[$website->getId()] = array(
                    'name' => $website->getName(),
                    'currency' => $website->getBaseCurrencyCode()
                );
            }
        }

        return $this->_websites;
    }

    /**
     * Retrieve default value for website
     *
     * @return int
     */
    public function getDefaultWebsite()
    {
        if ($this->isShowWebsiteColumn() && !$this->isAllowChangeWebsite()) {
            return Mage::app()->getWebsite()->getId();
        }
        return 0;
    }

    /**
     * Retrieve 'add customer price item' button HTML
     *
     * @return string
     */
    public function getAddButtonHtml()
    {
        return $this->getChildHtml('add_button');
    }

    /**
     * Retrieve customized price column header
     *
     * @param string $default
     * @return string
     */
    public function getPriceColumnHeader($default)
    {
        if ($this->hasData('price_column_header')) {
            return $this->getData('price_column_header');
        } else {
            return $default;
        }
    }

    /**
     * Retrieve customized price column header
     *
     * @param string $default
     * @return string
     */
    public function getPriceValidation($default)
    {
        if ($this->hasData('price_validation')) {
            return $this->getData('price_validation');
        } else {
            return $default;
        }
    }



    /**
     * Check customer attribute scope is global
     *
     * @return bool
     */
    public function isScopeGlobal()
    {
        return Mage::getModel('customer/customer')->getSharingConfig()->isGlobalScope();
    }

    /**
     * Show group prices grid website column
     *
     * @return bool
     */
    public function isShowWebsiteColumn()
    {
        if ($this->isScopeGlobal() || Mage::app()->isSingleStoreMode()) {
            return false;
        }
        return true;
    }

    /**
     * Check is allow change website value for combination
     *
     * @return bool
     */
    public function isAllowChangeWebsite()
    {
        if (!$this->isShowWebsiteColumn() || Mage::getModel('customer/customer')->getStore()->getId()) {
            return false;
        }
        return true;
    }

    /**
     * Get customer id
     *
     * @return integer
     */
    public function getCustomerId()
    {
        return $this->getRequest()->getParam('id');
    }

    /**
     * Return yes / no attribute
     *
     * @return array
     */
    public function getFinalprices()
    {
        return $this->helper('snowflake_pricegroup')->getFinalprices();
    }

    /**
     * Return serialized list of all products
     *
     * @return json
     */
    public function getAllProducts()
    {
        $_productCollection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('id')
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('name')
            ->addAttributeToFilter('type_id', array(
                Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
                Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE,
                Mage_Catalog_Model_Product_Type::TYPE_VIRTUAL
            ));

        $products = array();
        foreach ($_productCollection as $_product)
        {
            $productId  = $_product->getId();
            $products[$productId] = array(
                'id'    => $productId,
                'label' => $_product->getSku() .' / ' . $_product->getName()
            );
        }

        return Mage::helper('core')->jsonEncode($products);
    }

    /**
     * return date format
     *
     * @return string
     */
    public function getDateFormat()
    {
        return $this->helper('snowflake_pricegroup')->getDateFormat();
    }
}