<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 05.10.2015
 * Time: 13:25
 */

$installer = $this;
/* @var $installer Mage_Eav_Model_Entity_Setup */

/* @var $eavConfig Mage_Eav_Model_Config */
$eavConfig = Mage::getSingleton('eav/config');
$store     = Mage::app()->getStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$installer->startSetup();

// update customer address user defined attributes data
$attributesCustomer = array(
    'customer_price' => array(
        'group'             => 'Account information',
        'input'             => 'text',
        'type'              => 'decimal',
        'label'             => 'Customer Price',
        'backend'           => 'snowflake_pricegroup/customer_attribute_backend_customerprice',
        'visible'           => true,
        'required'          => false,
        'sort_order'        => 20,
        'position'          => 20,
        'adminhtml_only'    => 1,
    ),
);

foreach ($attributesCustomer as $attributeCode => $data) {
    $installer->addAttribute('customer', $attributeCode, $data);

    $attribute = $eavConfig->getAttribute('customer', $attributeCode);
    $attribute->setWebsite((($store->getWebsite()) ? $store->getWebsite() : 0));

    if (false === ($attribute->getIsSystem() == 1 && $attribute->getIsVisible() == 0)) {
        if (!empty($data['adminhtml_only'])) {
            $usedInForms = array('adminhtml_customer');
        }
        $attribute->setData('used_in_forms', $usedInForms);
    }

    $attribute->save();
}

$installer->endSetup();