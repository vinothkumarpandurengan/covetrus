<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 05.10.2015
 * Time: 13:25
 */

$installer = $this;
$installer->startSetup();

$customerPriceTable = $this->getTable('snowflake_pricegroup/catalog_product_attribute_customer_price');

$installer->getConnection()->dropIndex(
    $customerPriceTable,
    $installer->getIdxName(
        $customerPriceTable,
        array('entity_id', 'qty', 'website_id'),
        Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
    )
);

$installer->getConnection()->addIndex(
    $customerPriceTable,
    $installer->getIdxName(
        $customerPriceTable,
        array('entity_id', 'customer_id', 'qty', 'website_id'),
        Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
    ),
    array('entity_id', 'customer_id', 'qty', 'website_id'),
    Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
);

$installer->endSetup();