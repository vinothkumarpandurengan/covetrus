<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 02.10.2015
 * Time: 13:17
 */

$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer->startSetup();

$installer->run("
  DROP TABLE IF EXISTS {$installer->getTable('snowflake_pricegroup/catalog_product_attribute_customer_price')};
");

/**
 * Create table 'snowflake_pricegroup/catalog_product_attribute_customer_price'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('snowflake_pricegroup/catalog_product_attribute_customer_price'))
    ->addColumn('value_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Value ID')
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
    ), 'Entity ID')
    ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
    ), 'Customer ID')
    ->addColumn('qty', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(
        'nullable'  => false,
        'default'   => '1.0000',
    ), 'QTY')
    ->addColumn('value', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(
        'nullable'  => false,
        'default'   => '0.0000',
    ), 'Value')
    ->addColumn('dicount', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
    ), 'Additional Discount')
    ->addColumn('final_price', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
        'nullable'  => false,
    ), 'No Further Discounts')
    ->addColumn('start_date', Varien_Db_Ddl_Table::TYPE_DATE, null, array(
        'nullable'  => true,
    ), 'Price Start Date')
    ->addColumn('end_date', Varien_Db_Ddl_Table::TYPE_DATE, null, array(
        'nullable'  => true,
    ), 'Price End Date')
    ->addColumn('website_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
    ), 'Website ID')
    ->addIndex(
        $installer->getIdxName(
            'snowflake_pricegroup/catalog_product_attribute_customer_price',
            array('entity_id', 'qty', 'website_id'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('entity_id', 'qty', 'website_id'),
        array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))
    ->addIndex($installer->getIdxName('snowflake_pricegroup/catalog_product_attribute_customer_price', array('entity_id')),
        array('entity_id'))
    ->addIndex($installer->getIdxName('snowflake_pricegroup/catalog_product_attribute_customer_price', array('website_id')),
        array('website_id'))
    ->addForeignKey(
        $installer->getFkName(
            'snowflake_pricegroup/catalog_product_attribute_customer_price',
            'entity_id',
            'catalog/product',
            'entity_id'
        ),
        'entity_id', $installer->getTable('catalog/product'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey(
        $installer->getFkName(
            'snowflake_pricegroup/catalog_product_attribute_customer_price',
            'website_id',
            'core/website',
            'website_id'
        ),
        'website_id', $installer->getTable('core/website'), 'website_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Catalog Product Customer Price Attribute Backend Table');
$installer->getConnection()->createTable($table);

$installer->endSetup();