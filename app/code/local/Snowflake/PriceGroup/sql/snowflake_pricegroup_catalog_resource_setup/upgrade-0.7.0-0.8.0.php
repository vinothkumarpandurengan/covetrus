<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 05.10.2015
 * Time: 13:25
 */

$installer = $this;
/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer->startSetup();

$installer->addAttribute('catalog_product', 'customer_price', array(
    'group'                      => 'Prices',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'label'                      => 'Customer Price',
    'backend'                    => 'snowflake_pricegroup/catalog_product_attribute_backend_customerprice',
    'required'                   => false,
    'sort_order'                 => 20,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
    'apply_to'                   => 'simple,configurable,virtual',

));

$installer->endSetup();