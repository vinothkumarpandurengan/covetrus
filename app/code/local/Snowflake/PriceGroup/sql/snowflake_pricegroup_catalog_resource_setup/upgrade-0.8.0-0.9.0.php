<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 05.10.2015
 * Time: 13:25
 */

$installer = $this;
/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer->startSetup();

$installer->getConnection()->dropColumn($installer->getTable('snowflake_pricegroup/catalog_product_attribute_customer_price'), 'dicount');
$installer->getConnection()->addColumn($installer->getTable('snowflake_pricegroup/catalog_product_attribute_customer_price'), 'discount', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
    'unsigned'  => true,
    'nullable'  => false,
    'default'   => 0,
    'comment'   => 'Additional Discount'
));

$installer->endSetup();