<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 02.02.2015
 * Time: 10:13
 */

/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();


$this->run("
DROP TABLE IF EXISTS {$this->getTable('snowflake_pricegroup/pricegroup')};
");

/**
 * Create table 'provet_price_group'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('snowflake_pricegroup/pricegroup'))
    ->addColumn('provet_price_group_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Customer Group Id')
    ->addColumn('provet_price_group_code', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
        'nullable'  => false,
    ), 'Price Group Code')
    ->setComment('Price Group');
$installer->getConnection()->createTable($table);

$installer->endSetup();