<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 03.03.2015
 * Time: 09:33
 */

/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$tierPriceTable  = $this->getTable('catalog_product_entity_tier_price');
$groupPriceTable = $this->getTable('catalog_product_entity_group_price');


$installer->getConnection()->dropIndex(
    $tierPriceTable,
    $installer->getIdxName(
        $tierPriceTable,
        array('entity_id', 'all_groups', 'customer_group_id', 'price_group_id', 'qty', 'website_id'),
        Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
    )
);

$installer->getConnection()->addIndex(
    $tierPriceTable,
    $installer->getIdxName(
        $tierPriceTable,
        array('entity_id', 'all_groups', 'customer_group_id', 'qty', 'website_id'),
        Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
    ),
    array('entity_id', 'all_groups', 'customer_group_id', 'qty', 'website_id'),
    Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
);

$installer->getConnection()->dropIndex(
    $groupPriceTable,
    $installer->getIdxName(
        $groupPriceTable,
        array('entity_id', 'all_groups', 'customer_group_id', 'price_group_id', 'website_id'),
        Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
    )
);

$installer->getConnection()->addIndex(
    $groupPriceTable,
    $installer->getIdxName(
        $groupPriceTable,
        array('entity_id', 'all_groups', 'customer_group_id', 'website_id'),
        Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
    ),
    array('entity_id', 'all_groups', 'customer_group_id', 'website_id'),
    Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
);

$installer->getConnection()->dropColumn($tierPriceTable, 'price_group_id');
$installer->getConnection()->dropColumn($groupPriceTable, 'price_group_id');

$installer->getConnection()->dropTable($installer->getTable('snowflake_pricegroup/pricegroup'));


$installer->endSetup();
  