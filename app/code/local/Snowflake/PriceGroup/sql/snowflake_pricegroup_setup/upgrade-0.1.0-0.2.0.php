<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 03.02.2015
 * Time: 10:35
 */

/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$tierPriceTable = $this->getTable('catalog_product_entity_tier_price');

$installer->getConnection()->addColumn(
    $tierPriceTable,
    'price_group_id',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
        'comment'   => 'Navision Price Group'
    )
);

$installer->getConnection()->addColumn(
    $tierPriceTable,
    'start_date',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_DATE,
        'nullable'  => true,
        'comment'   => 'Navision Price Start Date'
    )
);

$installer->getConnection()->addColumn(
    $tierPriceTable,
    'end_date',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_DATE,
        'nullable'  => true,
        'comment'   => 'Navision Price End Date'
    )
);

$installer->getConnection()->dropIndex(
    $tierPriceTable,
    $installer->getIdxName(
        $tierPriceTable,
        array('entity_id', 'all_groups', 'customer_group_id', 'qty', 'website_id'),
        Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
    )
);

$installer->getConnection()->addIndex(
    $tierPriceTable,
    $installer->getIdxName(
        $tierPriceTable,
        array('entity_id', 'all_groups', 'customer_group_id', 'price_group_id', 'qty', 'website_id'),
        Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
    ),
    array('entity_id', 'all_groups', 'customer_group_id', 'price_group_id', 'qty', 'website_id'),
    Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
);

$installer->endSetup();
  