<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 03.02.2015
 * Time: 10:35
 */

/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$groupPriceTable = $this->getTable('catalog_product_entity_group_price');

$installer->getConnection()->modifyColumn(
    $installer->getTable('snowflake_pricegroup/pricegroup'),
    'provet_price_group_id',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'comment' => 'Price Group Id'
    )
);

$installer->endSetup();
  