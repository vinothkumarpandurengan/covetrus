<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 28.01.2015
 * Time: 13:27
 */
class Snowflake_Customer_Model_Observer
{
    /**
     * Test if the customer account is enabled or not
     *
     * @param Varien_Event_Observer $observer
     * @throws Mage_Core_Exception
     */
    public function customerCustomerAuthenticated($observer)
    {
        $customer = $observer->getEvent()->getModel();
        // Add the inactive option
        if($customer->getIsActive () != '1' ){
            throw new Mage_Core_Exception(Mage::helper('customer')->__('This account is disabled.'), 0);
        }
    }

    /**
     * Change the attribute of username after the configuration
     * has been changed
     *
     * @param Varien_Event_Observer $observer
     */
    public function adminSystemConfigChangedSectionCustomer(Varien_Event_Observer $observer)
    {
        $minLength = Mage::getStoreConfig('customer/startup/min_length');
        $maxLength = Mage::getStoreConfig('customer/startup/max_length');
        $inputValidation = Mage::getStoreConfig('customer/startup/input_validation');

        if($minLength > $maxLength) {
            Mage::throwException(
                Mage::helper('snowflake_customer')->__('Sorry but you cannot set a minimum length value %s bigger than the maximum length value %s. Please, change the values.',
                    $minLength,
                    $maxLength)
            );
        }

        /* @var $attributeUsernameModel Mage_Customer_Model_Attribute */
        $attributeUsernameModel = Mage::getModel('customer/attribute')->loadByCode('customer', 'username');
        if($attributeUsernameModel->getId()) {
            $rules = $attributeUsernameModel->getValidateRules();
            $rules['max_text_length'] = $maxLength;
            $rules['min_text_length'] = $minLength;

            if($inputValidation != 'default' && $inputValidation != 'custom') {
                $rules['input_validation'] = $inputValidation;
            }else {
                $rules['input_validation'] = '';
            }

            $attributeUsernameModel->setValidateRules($rules);
            $attributeUsernameModel->save();
        }
    }

    /**
     * Add the username attribute to the customer collection
     *
     * @param Varien_Event_Observer $observer
     */
    public function eavCollectionAbstractLoadBefore(Varien_Event_Observer $observer)
    {
        /* @var $collection Mage_Eav_Model_Entity_Collection_Abstract */
        $collection = $observer->getEvent()->getCollection();
        $entity = $collection->getEntity();
        if (!empty($entity) && $entity->getType() == 'customer') {
            $collection->addAttributeToSelect('customer_id');
            $collection->addAttributeToSelect('username');
        }
    }

    /**
     * Add Username & Customer Number to Customer Grid
     *
     * @param Varien_Event_Observer $observer
     */
    public function coreBlockAbstractToHtmlBefore(Varien_Event_Observer $observer)
    {
        $grid = $observer->getBlock();

        /**
         * Mage_Adminhtml_Block_Customer_Grid
         */
        if ($grid instanceof Mage_Adminhtml_Block_Customer_Grid) {
            $grid->addColumnAfter(
                'customer_id',
                array(
                    'header' => Mage::helper('snowflake_customer')->__('Customer Number'),
                    'index'  => 'customer_id'
                ),
                'entity_id'
            );

            if (Mage::getStoreConfigFlag('customer/startup/grid')) {
                $grid->addColumnAfter(
                    'username',
                    array(
                        'header' => Mage::helper('snowflake_customer')->__('Username'),
                        'index'  => 'username'
                    ),
                    'customer_id'
                );
            }
        }
    }
}
  