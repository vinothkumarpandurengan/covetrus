<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 17.11.2015
 * Time: 15:49
 */
class Snowflake_Customer_Model_Attribute_Source_Discountgroup extends Mage_Eav_Model_Entity_Attribute_Source_Table
{
    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = Mage::getModel('snowflake_quantitydiscount/discount_group')
                ->getCollection()
                ->load()
                ->toOptionArray();
            array_unshift($this->_options, array('value'=> '', 'label'=> ''));
        }
        return $this->_options;
    }
}