<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 25.03.2015
 * Time: 13:14
 */
class Snowflake_Customer_Model_Export extends Mage_ImportExport_Model_Export
{
    const DEFAULT_CUSTOMER_EXPORT_PATH = 'var/export';

    /**
     * Adapter resource instance
     *
     * @var object
     */
    protected $_resource;

    //protected $_data;
    public function processCustomerExport($data)
    {
        $this->setEntity('customer');
        $this->addData($this->_prepareData($data));

        return $this->save();
    }

    /**
     * @return Varien_Io_Abstract
     */
    public function getResource()
    {
        if (!$this->_resource) {
            $this->_resource = new Varien_Io_File();
            $this->_resource->setAllowCreateFolders(true);

            try {
                $path = Mage::getBaseDir() . DS . self::DEFAULT_CUSTOMER_EXPORT_PATH;
                $this->_resource->open(array('path' => $path));
            } catch (Exception $e) {
                $message = Mage::helper('dataflow')->__('An error occurred while opening file: "%s".', $e->getMessage());
                Mage::throwException($message);
            }
        }

        return $this->_resource;
    }

    /**
     * Returns indexes of the fetched array as headers for CSV
     *
     * @param array $data
     * @return array
     */
    protected function _getCsvHeaders($data)
    {
        $headers = array_keys($data);
        return $headers;
    }

    /**
     * Save result to destination file from temporary
     *
     * @return Snowflake_Customer_Model_Export
     */
    public function save()
    {
        if (!$this->getResource()) {
            return $this;
        }

        $data = $this->getData();
        if (isset($data['entity'])) {
            unset($data['entity']);
        }

        $io = $this->getResource();

        $io->streamOpen($this->_getFileName(), 'w+');
        $io->streamLock(true);

        $io->streamWriteCsv($this->_getCsvHeaders($data), ';');
        $io->streamWriteCsv($data, ';');

        $io->streamUnlock();
        $io->streamClose();

        return $this;
    }

    /**
     * Remove unnecessary fields.
     *
     * @param array $data
     * @return array
     */
    protected function _prepareData($data)
    {
        $newData = array();
        foreach ($data as $key => $value) {
            if (($key == 'success_url') || ($key == 'error_url')) {
                continue;
            }

            if (is_array($value)) {
                foreach ($value as $num => $val) {
                    $newData[$key . '_' . $num] = $val;
                }
            } else {
                $newData[$key] = $value;
            }
        }

        return $newData;
    }

    /**
     * Return file name for downloading.
     *
     * @return string
     */
    protected function _getFileName()
    {
        return $this->getEntity() . '_' . $this->getCustomerNumber() .  '.csv';
    }
}
  