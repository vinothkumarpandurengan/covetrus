<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 28.01.2015
 * Time: 14:02
 */
class Snowflake_Customer_Model_Form extends Mage_Customer_Model_Form
{
    /**
     * Extract data from request and return associative data array
     *
     * @param Zend_Controller_Request_Http $request
     * @param string $scope the request scope
     * @param boolean $scopeOnly search value only in scope or search value in global too
     * @return array
     */
    public function extractData (Zend_Controller_Request_Http $request, $scope = null, $scopeOnly = true)
    {
        $data = parent::extractData($request, $scope, $scopeOnly);
        if(isset($data['username']) && !Mage::getStoreConfigFlag('customer/startup/case_sensitive')) {
            $filter = new Zend_Filter_StringToLower(array('encoding' => 'UTF-8'));
            $data['username'] = $filter->filter($data['username']);
        }
        return $data;
    }

    /**
     * Validate data array and return true or array of errors
     *
     * @param array $data
     * @return boolean|array
     */
    public function validateData (array $data)
    {
        $errors = array();
        foreach ($this->getAttributes() as $attribute) {
            if ($this->_isAttributeOmitted($attribute)) {
                continue;
            }
            if ($attribute->getAttributeCode() == 'customer_price') {
                continue;
            }

            $dataModel = $this->_getAttributeDataModel($attribute);
            $dataModel->setExtractedData($data);
            if (!isset($data[$attribute->getAttributeCode()])) {
                $data[$attribute->getAttributeCode()] = null;
            }
            $result = $dataModel->validateValue($data[$attribute->getAttributeCode()]);
            if ($result !== true) {
                $errors = array_merge($errors, $result);
            }
        }

        if (!empty($data['username'])) {
            $model = Mage::getModel('customer/customer');

            $customerId = Mage::app()->getFrontController()
                ->getRequest()
                ->getParam('customer_id');

            if (!$customerId) {
                $customerId = Mage::app()->getFrontController()
                    ->getRequest()
                    ->getParam('id');
            }

            if (!$customerId && !Mage::app()->getStore()->isAdmin()) {
                $customerId = Mage::getSingleton('customer/session')->getCustomer()->getId();
            }

            // Prevent possible errors
            if (empty($customerId)) {
                if (count($errors) == 0) {
                    return true;
                }

                return $errors;
            }

            if (isset($data['website_id']) && $data['website_id'] !== false) {
                $websiteId = $data['website_id'];
            } elseif ($customerId) {
                $customer = $model->load($customerId);
                $websiteId = $customer->getWebsiteId();
                //if ($customer->getUsername() == $data['username']) { // don't make any test if the user has already a username
                //    return $errors;
                //}
            } else {
                $websiteId = Mage::app()->getWebsite()->getId();
            }

            if (!is_array($errors)) {
                $errors = array();
            }

            $isCheckoutAsGuest = Mage::getSingleton('checkout/type_onepage')->getCheckoutMethod();
            if ($isCheckoutAsGuest != Mage_Checkout_Model_Type_Onepage::METHOD_GUEST && empty($data['username'])) {
                $message = Mage::helper('snowflake_customer')->__('Username is a required field.');
                $errors = array_merge($errors, array($message));
            }

            // Other rules are validated by the parent class because they are basic rules provided by Magento Core

            $inputValidation = Mage::getStoreConfig('customer/startup/input_validation');
            $useInputValidation = ($inputValidation == 'default' || $inputValidation == 'custom') ? true : false;

            switch ($inputValidation) {
                case 'default':
                    $validate = '/^[\w-]*$/';
                    break;
                case 'custom':
                    $validate = Mage::getStoreConfig('customer/startup/input_validation_custom');
                    break;
            }

            if ($useInputValidation) {
                $validate = new Zend_Validate_Regex($validate);

                if(! $validate->isValid($data['username']) ){
                    if ($inputValidation == 'custom') {
                        $message = Mage::getStoreConfig('customer/startup/input_validation_custom_message');
                    } else {
                        $message = Mage::helper('snowflake_customer')->__('Username is invalid! Only letters, digits and \'_-\' values are accepted.');
                    }
                    $errors = array_merge($errors, array($message));
                }
            }

            $result = $model->customerUsernameExists($data['username'], $websiteId);
            if ($result && $result->getId() != $customerId) {
                $message = Mage::helper('snowflake_customer')->__('Username already exists');
                $errors = array_merge($errors, array($message));
            }
        }
        if (count($errors) == 0) {
            return true;
        }
        return $errors;
    }
}
  