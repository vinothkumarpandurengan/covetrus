<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 28.01.2015
 * Time: 15:49
 */
class Snowflake_Customer_Model_Config_Source_Inputvalidation
{
    public function toOptionArray()
    {
        $helper = Mage::helper('snowflake_customer');

        return array(
            array('value'=>'default', 'label'=> $helper->__('Default (letters, digits and _- characters)')),
            array('value'=>'alphanumeric', 'label'=> $helper->__('Letters and digits')),
            array('value'=>'alpha', 'label'=> $helper->__('Letters only')),
            array('value'=>'numeric', 'label'=> $helper->__('Digits only')),
            array('value'=>'custom', 'label'=> $helper->__('Custom (PCRE Regex)')),
        );
    }
}
  