<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 28.01.2015
 * Time: 12:37
 */
class Snowflake_Customer_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Retrieve customer register form url
     *
     * @return string
     */
    public function getRegisterUrl()
    {
        return $this->_getUrl('customer/account/createRegister');
    }
}