<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 28.04.2015
 * Time: 16:17
 */

require_once Mage::getModuleDir('controllers', 'Mage_Customer').DS.'AddressController.php';
class Snowflake_Customer_AddressController extends Mage_Customer_AddressController
{
    // -- redirect to account when adding new address --
    public function newAction()
    {
        return $this->_redirect('*/account/');
    }
}
  