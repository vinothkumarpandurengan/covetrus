<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 28.01.2015
 * Time: 14:12
 */
require_once Mage::getModuleDir('controllers', 'Conlabz_CrConnect').DS.'AccountController.php';

class Snowflake_Customer_AccountController extends Conlabz_CrConnect_AccountController
{
    const XML_PATH_EMAIL_RECIPIENT  = 'customer/startup/recipient_email';
    const XML_PATH_EMAIL_SENDER     = 'customer/startup/sender_email_identity';
    const XML_PATH_EMAIL_TEMPLATE   = 'customer/startup/email_template';
    
    public function createRegisterAction()
    {
        $session = $this->_getSession();
        if ($session->isLoggedIn()) {
            $this->_redirect('*/*/');
            return;
        }
        $session->setEscapeMessages(true); // prevent XSS injection in user input

        if ($this->getRequest()->isPost()) {
            try {
                $customerData = $this->getRequest()->getParams();
                
                if (!isset($customerData["username"])) {
                    $customerData["username"] = $customerData["customer_number"];
                }
                
                if (!$this->_customerExists($customerData)) {
                    // Export CSV
                    Mage::getModel('snowflake_customer/export')->processCustomerExport($customerData);
                    
                    $postObject = new Varien_Object();
                    $postObject->setData($customerData);
                    
                    $mailTemplate = Mage::getModel('core/email_template');
                    /* @var $mailTemplate Mage_Core_Model_Email_Template */
                    $mailTemplate->setDesignConfig(array('area' => 'frontend'))
                        ->sendTransactional(
                            Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE),
                            Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
                            Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT),
                            null,
                            array('data' => $postObject)
                        );
                    
                    $mailTemplate->setDesignConfig(array('area' => 'frontend'))
                        ->sendTransactional(
                            Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE),
                            Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
                            $postObject->getEmail(),
                            null,
                            array('data' => $postObject)
                        );

                    $this->_getSession()->addSuccess(Mage::helper('snowflake_customer')->__('Your request has been sent successfully.'));
                }
            } catch (Mage_Core_Exception $e) {
                $session->setCustomerFormData($this->getRequest()->getPost());
                $message = $e->getMessage();

                $session->setEscapeMessages(false);
                $session->addError($message);
            }
        }

        $this->_redirectError($this->_getUrl('*/*/create', array('_secure' => true)));
    }

    /**
     * Rewrite to allow support of Username
     *
     */
    public function forgotPasswordPostAction()
    {
        $email = (string) $this->getRequest()->getPost('email');
        if ($email) {

            /** @var $customer Diglin_Username_Model_Customer */
            $customer = Mage::getModel('customer/customer')
                ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                ->loadByUsername($email);

            if (!$customer->getId() && !Zend_Validate::is($email, 'EmailAddress')) {
                $this->_getSession()->setForgottenEmail($email);
                $this->_getSession()->addError($this->__('Invalid email address or username.'));
                $this->_redirect('*/*/forgotpassword');
                return;
            } else if (!$customer->getId()) {
                // Load by Email if username not found and email seems to be valid
                $customer
                    ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                    ->loadByEmail($email);
            }

            if ($customer->getId()) {
                try {
                    $newResetPasswordLinkToken = $this->_getHelper('customer')->generateResetPasswordLinkToken();
                    $customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
                    $customer->sendPasswordResetConfirmationEmail();
                } catch (Exception $exception) {
                    $this->_getSession()->addError($exception->getMessage());
                    $this->_redirect('*/*/forgotpassword');
                    return;
                }
            }
            $this->_getSession()
                ->addSuccess( $this->_getHelper('customer')
                    ->__('If there is an account associated with %s you will receive an email with a link to reset your password.',
                        $this->_getHelper('customer')->escapeHtml($email)));
            $this->_redirect('*/*/');
            return;
        } else {
            $this->_getSession()->addError(Mage::helper('snowflake_customer')->__('Please enter your email or username.'));
            $this->_redirect('*/*/forgotpassword');
            return;
        }
    }

    /**
     * Check if customer already exists
     *
     * @param array $data
     * @return false|Exception
     */
    protected function _customerExists($data)
    {
        $errors = array();

        $customer = Mage::getModel('customer/customer')
            ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
            ->loadByEmail($data['email']);

        if ($customer->getId()) {
            $url = Mage::getUrl('customer/account/forgotpassword');
            throw Mage::exception('Mage_Core', $this->__('There is already an account with this email address. If you are sure that it is your email address, <a href="%s">click here</a> to get your password and access your account.', $url));
        }

        $customer
            ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
            ->loadByUsername($data['username']);

        if ($customer->getId()) {
            throw Mage::exception('Mage_Core', Mage::helper('snowflake_customer')->__('Username already exists'));
        }

        return false;
    }
}