<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 03.03.2015
 * Time: 10:07
 */

/* @var $installer Snowflake_Customer_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

$installer->removeAttribute('customer', 'provet_price_group_id');
$installer->getConnection()->dropColumn($installer->getTable('customer/entity'), 'provet_price_group_id');

$installer->endSetup();
  