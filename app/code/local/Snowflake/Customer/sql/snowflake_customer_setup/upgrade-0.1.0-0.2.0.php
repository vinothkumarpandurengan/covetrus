<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 28.01.2015
 * Time: 12:42
 */

/* @var $installer Snowflake_Customer_Model_Entity_Setup */
$installer = $this;

/* @var $eavConfig Mage_Eav_Model_Config */
$eavConfig = Mage::getSingleton('eav/config');
$store     = Mage::app()->getStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// update customer address user defined attributes data
$attributes = array(
    'provet_read_only' => array(
        'type'               => 'varchar',
        'input'              => 'select',
        'label'              => 'Read only',
        'note'               => 'If set yes, checkout is not allowed',
        'source'             => 'eav/entity_attribute_source_boolean',
        'required'           => false,
        'sort_order'         => 20,
        'position'           => 20,
        'adminhtml_only'     => 1,
    ),
    'provet_price_group_id'  => array(
        'type'               => 'static',
        'input'              => 'select',
        'label'              => 'Pricegroup',
        'note'               => 'Navision Pricegroup',
        'source'             => 'snowflake_customer/customer_attribute_source_pricegroup',
        'required'           => false,
        'sort_order'         => 22,
        'position'           => 22,
        'adminhtml_only'     => 1,
        'admin_checkout'     => 1,
    ),
    'provet_credit_limit' => array(
        'type'               => 'int',
        'input'              => 'text',
        'label'              => 'Credit limit',
        'required'           => false,
        'sort_order'         => 110,
        'position'           => 110,
        'adminhtml_only'     => 1,
    ),
);

foreach ($attributes as $attributeCode => $data) {
    $installer->addAttribute('customer', $attributeCode, $data);

    $attribute = $eavConfig->getAttribute('customer', $attributeCode);
    $attribute->setWebsite((($store->getWebsite()) ? $store->getWebsite() : 0));

    if (false === ($attribute->getIsSystem() == 1 && $attribute->getIsVisible() == 0)) {
        $usedInForms = array(
            'customer_account_create',
            'customer_account_edit',
            'checkout_register',
        );
        if (!empty($data['adminhtml_only'])) {
            $usedInForms = array('adminhtml_customer');
        } else {
            $usedInForms[] = 'adminhtml_customer';
        }
        if (!empty($data['adminhtml_checkout'])) {
            $usedInForms[] = 'adminhtml_checkout';
        }

        $attribute->setData('used_in_forms', $usedInForms);
    }
    $attribute->save();
}

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('customer/entity'), 'provet_price_group_id', array(
    'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
    'unsigned'  => true,
    'nullable'  => false,
    'default'   => '0',
    'comment' => 'Customer Price Group Id'
));

$installer->endSetup();