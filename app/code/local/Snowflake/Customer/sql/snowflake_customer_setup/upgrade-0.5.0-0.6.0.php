<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 03.03.2015
 * Time: 11:16
 */

/* @var $installer Snowflake_Customer_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

$installer->updateAttribute('customer', 'provet_discount_group', 'frontend_input', 'select');
$installer->updateAttribute('customer', 'provet_discount_group', 'is_required', true);
$installer->updateAttribute('customer', 'provet_discount_group', 'source_model', 'snowflake_customer/attribute_source_discountgroup');

$installer->getConnection()->addColumn($installer->getTable('customer/entity'), 'provet_discount_group', array(
    'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
    'unsigned'  => true,
    'nullable'  => false,
    'default'   => '0',
    'comment' => 'Customer Price Group Id'
));

$installer->endSetup();
  