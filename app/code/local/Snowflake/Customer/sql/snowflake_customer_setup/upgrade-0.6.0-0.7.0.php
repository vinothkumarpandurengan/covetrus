<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 03.03.2015
 * Time: 11:16
 */

/* @var $installer Snowflake_Customer_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

$installer->removeAttribute('customer', 'provet_matrix_group');

/* @var $eavConfig Mage_Eav_Model_Config */
$eavConfig = Mage::getSingleton('eav/config');
$store     = Mage::app()->getStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// update customer address user defined attributes data
$attributesCustomer = array(
    'provet_warenbezug' => array(
        'type'               => 'varchar',
        'input'              => 'select',
        'label'              => 'Warenbezug',
        'note'               => 'If set yes, customer saw only the NOT limited products',
        'source'             => 'eav/entity_attribute_source_boolean',
        'required'           => false,
        'sort_order'         => 170,
        'position'           => 170,
        'adminhtml_only'     => 1,
    ),
);

foreach ($attributesCustomer as $attributeCode => $data) {
    $installer->addAttribute('customer', $attributeCode, $data);

    $attribute = $eavConfig->getAttribute('customer', $attributeCode);
    $attribute->setWebsite((($store->getWebsite()) ? $store->getWebsite() : 0));

    if (false === ($attribute->getIsSystem() == 1 && $attribute->getIsVisible() == 0)) {
        $usedInForms = array(
            'customer_account_create',
            'customer_account_edit',
            'checkout_register',
        );
        if (!empty($data['adminhtml_only'])) {
            $usedInForms = array('adminhtml_customer');
        } else {
            $usedInForms[] = 'adminhtml_customer';
        }
        if (!empty($data['adminhtml_checkout'])) {
            $usedInForms[] = 'adminhtml_checkout';
        }

        $attribute->setData('used_in_forms', $usedInForms);
    }
    $attribute->save();
}

foreach ($attributesCustomer as $attributeCode => $data) {
    if (isset($data['validate_rules'])) {
        $installer->updateAttribute('customer', $attributeCode, 'validate_rules', serialize($data['validate_rules']));
    }
}

$installer->updateAttribute('customer', 'customer_id', 'backend_type', 'varchar');
$installer->updateAttribute('customer_address', 'telephone', 'is_required', false);

$installer->endSetup();
  