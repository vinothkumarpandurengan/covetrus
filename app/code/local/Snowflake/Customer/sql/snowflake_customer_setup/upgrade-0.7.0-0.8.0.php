<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 03.03.2015
 * Time: 11:16
 */

/* @var $installer Snowflake_Customer_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

$installer->updateAttribute('customer', 'lastname', 'is_required', false);
$installer->updateAttribute('customer_address', 'lastname', 'is_required', false);

$installer->endSetup();
  