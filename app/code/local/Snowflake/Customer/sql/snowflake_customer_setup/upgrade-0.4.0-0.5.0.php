<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 03.03.2015
 * Time: 11:16
 */

/* @var $installer Snowflake_Customer_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

/* @var $eavConfig Mage_Eav_Model_Config */
$eavConfig = Mage::getSingleton('eav/config');
$store     = Mage::app()->getStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// update customer address user defined attributes data
$attributesCustomer = array(
    'provet_currency_code' => array(
        'type'               => 'varchar',
        'input'              => 'text',
        'label'              => 'Währungscode',
        'required'           => true,
        'sort_order'         => 120,
        'position'           => 120,
        'adminhtml_only'     => 1,
        'validate_rules' => array(
            'max_text_length'   => 3
        ),
    ),
    'provet_billing_discount' => array(
        'type'               => 'int',
        'input'              => 'text',
        'label'              => 'Rechnungsrabatt',
        'required'           => false,
        'sort_order'         => 130,
        'position'           => 130,
        'adminhtml_only'     => 1,
        'validate_rules' => array(
            'max_text_length'   => 10
        ),
    ),
    'provet_quantity_discount' => array(
        'type'               => 'varchar',
        'input'              => 'select',
        'label'              => 'Mengenrabatte zulässig',
        'source'             => 'eav/entity_attribute_source_boolean',
        'required'           => false,
        'sort_order'         => 140,
        'position'           => 140,
        'adminhtml_only'     => 1,
    ),
    'provet_discount_group' => array(
        'type'               => 'varchar',
        'input'              => 'text',
        'label'              => 'Rabatt Gruppe',
        'required'           => false,
        'sort_order'         => 150,
        'position'           => 150,
        'adminhtml_only'     => 1,
        'validate_rules' => array(
            'max_text_length'   => 10
        ),
    ),
    'provet_tax_group' => array(
        'type'               => 'varchar',
        'input'              => 'text',
        'label'              => 'MWSt Gruppe',
        'required'           => true,
        'sort_order'         => 160,
        'position'           => 160,
        'adminhtml_only'     => 1,
        'validate_rules' => array(
            'max_text_length'   => 10
        ),
    ),
    'provet_matrix_group' => array(
        'type'               => 'varchar',
        'input'              => 'text',
        'label'              => 'Matrix Gruppe',
        'required'           => true,
        'sort_order'         => 170,
        'position'           => 170,
        'adminhtml_only'     => 1,
        'validate_rules' => array(
            'max_text_length'   => 6
        ),
    ),
    'provet_block' => array(
        'type'               => 'varchar',
        'input'              => 'select',
        'label'              => 'Sperre',
        'note'               => 'If set yes, checkout is not allowed',
        'source'             => 'eav/entity_attribute_source_boolean',
        'required'           => false,
        'sort_order'         => 180,
        'position'           => 180,
        'adminhtml_only'     => 1,
    ),
);

foreach ($attributesCustomer as $attributeCode => $data) {
    $installer->addAttribute('customer', $attributeCode, $data);

    $attribute = $eavConfig->getAttribute('customer', $attributeCode);
    $attribute->setWebsite((($store->getWebsite()) ? $store->getWebsite() : 0));

    if (false === ($attribute->getIsSystem() == 1 && $attribute->getIsVisible() == 0)) {
        $usedInForms = array(
            'customer_account_create',
            'customer_account_edit',
            'checkout_register',
        );
        if (!empty($data['adminhtml_only'])) {
            $usedInForms = array('adminhtml_customer');
        } else {
            $usedInForms[] = 'adminhtml_customer';
        }
        if (!empty($data['adminhtml_checkout'])) {
            $usedInForms[] = 'adminhtml_checkout';
        }

        $attribute->setData('used_in_forms', $usedInForms);
    }
    $attribute->save();
}

foreach ($attributesCustomer as $attributeCode => $data) {
    if (isset($data['validate_rules'])) {
        $installer->updateAttribute('customer', $attributeCode, 'validate_rules', serialize($data['validate_rules']));
    }
}

$installer->updateAttribute('customer', 'customer_id', 'backend_type', 'varchar');
$installer->updateAttribute('customer_address', 'telephone', 'is_required', false);

$installer->endSetup();
  