<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 18.02.2015
 * Time: 11:11
 */

/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$this->run("
DROP TABLE IF EXISTS {$this->getTable('snowflake_multiplewishlist/wishlist')};
DROP TABLE IF EXISTS {$this->getTable('snowflake_multiplewishlist/wishlist_items')};
");

$table = $installer->getConnection()
    ->newTable($installer->getTable('snowflake_multiplewishlist/wishlist'))
    ->addColumn('multiwishlist_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Wishlist Id')
    ->addColumn('multiwishlist_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => false,
    ), 'Wishlist Name')
    ->addColumn('multiwishlist_customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
    ), 'Wishlist Customer Id')
    ->addColumn('multiwishlist_editable', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
        'nullable'  => false,
    ), 'Wishlist Editable')
    ->addColumn('multiwishlist_is_main', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
        'nullable'  => true,
    ), 'Wishlist is Main')
    ->addForeignKey(
        'multiwishlist_customer_id',
        'multiwishlist_customer_id', $installer->getTable('customer_entity'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Multiple Wishlist');
$installer->getConnection()->createTable($table);

$table = $installer->getConnection()
    ->newTable($installer->getTable('snowflake_multiplewishlist/wishlist_items'))
    ->addColumn('item_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
    ), 'Wishlist Item Id')
    ->addColumn('multiwishlist_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
    ), 'Wishlist Id')
    ->addForeignKey(
        'FK_item_id',
        'item_id', $installer->getTable('wishlist_item'), 'wishlist_item_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey(
        'FK_multiwishlist_id',
        'multiwishlist_id', $installer->getTable('snowflake_multiplewishlist/wishlist'), 'multiwishlist_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Multiple Wishlist Item');
$installer->getConnection()->createTable($table);

$installer->endSetup();
  