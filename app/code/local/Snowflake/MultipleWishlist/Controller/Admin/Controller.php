<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 27.02.2015
 * Time: 10:44
 */
class Snowflake_MultipleWishlist_Controller_Admin_Controller extends Mage_Adminhtml_Controller_Action
{
    /**
     * Controller predispatch method
     *
     * @return Snowflake_MultipleWishlist_Controller_Admin_Controller
     */
    public function preDispatch()
    {
        return parent::preDispatch();
    }
}
  