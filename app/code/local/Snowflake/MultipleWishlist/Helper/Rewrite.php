<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 17.02.2015
 * Time: 12:21
 */
class Snowflake_MultipleWishlist_Helper_Rewrite extends Mage_Wishlist_Helper_Data
{

    /**
     * Create wishlist item collection
     *
     * @return Mage_Wishlist_Model_Resource_Item_Collection
     */
    protected function _createWishlistItemCollection() {
        return parent::_createWishlistItemCollection()->load();
    }
}
  