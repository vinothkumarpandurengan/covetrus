<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 17.02.2015
 * Time: 12:20
 */
class Snowflake_MultipleWishlist_Helper_Data extends Mage_Core_Helper_Abstract
{
    /** @var null|Snowflake_MultipleWishlist_Model_Settings */
    protected $_settings = null;
    protected $_isEnabledProductVisibilityFlag = null;

    /**
     * Get multiple wishlist settings
     *
     * @return Snowflake_MultipleWishlist_Model_Settings
     */
    public function getSettings()
    {
        if (is_null($this->_settings)) {
            $settings = Mage::getModel('snowflake_multiplewishlist/settings')->load();
            $this->_settings = $settings;
        }

        return $this->_settings;
    }

    /**
     * Check if extension is enabled
     *
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->getSettings()->getActive() && true;
    }

    public function deleteFromWishlist()
    {
        if ($this->getSettings()->getAfterAddToCart() == Snowflake_MultipleWishlist_Model_Settings::AFTER_ADD_TO_CART_LEAVE) {
            return false;
        }

        return true;
    }
}
  