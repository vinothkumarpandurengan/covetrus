<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 17.02.2015
 * Time: 12:28
 */
class Snowflake_MultipleWishlist_Block_Popup extends Mage_Core_Block_Template
{
    protected $_isRegistered = false;

    protected function _construct()
    {
        parent::_construct();
        $this->_isRegistered = Mage::helper('snowflake_multiplewishlist')->isEnabled();
    }

    /**
     * Prepare layout
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {
        if ($this->_isRegistered) {
            $head = $this->getLayout()->getBlock('head');
            if ($head) {
                $head->addJs('snowflake/multiplewishlist/popup.js');
                $head->addItem('skin_css', 'css/snowflake/multiplewishlist/popup.css');
            }
        }
        return parent::_prepareLayout();
    }

    /**
     * Only return popup template if extension is enable
     *
     * @return string
     */
    protected function _toHtml() {
        if ($this->_isRegistered) {
            return parent::_toHtml();
        }

        return '';
    }

    /**
     * Get all wishlists
     *
     * @return array
     */
    public function getWishlists()
    {
        if (Mage::getSingleton('customer/session')->getCustomer()->getId()) {
            $wishlists = Mage::getModel('snowflake_multiplewishlist/wishlist')->getWishlists(null, true);
        } else {
            $wishlists = array(
                array(
                    'multiwishlist_name'    => Mage::helper('snowflake_multiplewishlist')->getSettings()->getDefaultWishlist(),
                    'multiwishlist_id'      => 'main',
                    'multiwishlist_is_main' => true
                ),
            );
        }
        if (!is_array($wishlists)) {
            return array();
        }
        return $wishlists;
    }

    /**
     * Get json config
     *
     * @return string
     */
    public function getConfigJson()
    {
        $config = array(
            'stay_on_page'             => Mage::helper('snowflake_multiplewishlist')->getSettings()->getAfterWishlistSelect() == Snowflake_MultipleWishlist_Model_Settings::AFTER_WISHLIST_SELECTED_STAY_ON_PAGE,
            'check_wishlist_url'       => Mage::getUrl('wishlist/index/ajax/'),
            'add_product_ajax_url'     => Mage::getUrl('wishlist/ajax/addProduct/'),
            'wishlist_url'             => Mage::getUrl('wishlist'),
            'update_wishlist_link_url' => Mage::getUrl('wishlist/index/itemCount'),
            'message_empty_name'       => $this->__('Enter the name of new wishlist'),
            'message_name_exists'      => $this->__('Wishlist with such a name already exists. Please choose a different name.'),
        );

        return Zend_Json::encode($config);
    }
}
  