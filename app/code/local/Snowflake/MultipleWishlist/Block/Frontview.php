<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 18.02.2015
 * Time: 16:48
 */

class Snowflake_MultipleWishlist_Block_Frontview extends Mage_Core_Block_Template
{
    private   $_tabId = 0;
    private   $_wishlistHasItems = false;

    protected $_table = 'snowflake_wishlist_items';

    protected function _beforeToHtml()
    {
        $this->_createCollection();
        parent::_beforeToHtml();

        $this->setTemplate('snowflake/wishlist/index/wishlist.phtml');
    }

    /**
     * Create wishlist items collection
     *
     * @return void
     */
    protected function _createCollection()
    {
        $customer = Mage::getSingleton('customer/session')->getCustomer();

        $wishlist = Mage::getModel('wishlist/wishlist');
        $wishlist->loadByCustomer($customer, true);

        $items = Mage::getResourceModel('snowflake_multiplewishlist/item_collection');
        $items->addWishlistFilter($wishlist);

        $this->_prepareCollection($items);
    }

    /**
     * Prepare wishlist items collection
     *
     * @return Snowflake_MultipleWishlist_Block_Frontview
     */
    protected function _prepareCollection($collection)
    {
        $table = Mage::getSingleton('core/resource')->getTableName($this->_table);
        $collection->getSelect()->join($table, 'wishlist_item_id = '.$table.'.item_id');

        $id = $this->getRequest()->getParam('tabId');
        if (!$id) {
            $model = Mage::getModel('snowflake_multiplewishlist/wishlist');
            $customerId = Mage::getSingleton('customer/session')->getCustomer()->getId();
            $id = $model->checkMainWishlist($customerId);
        }
        $this->_tabId = $id;

        $collection->getSelect()->where('multiwishlist_id = '.$id);
        //$collection->setOrderByProductAttribute('name', 'asc');

        if (count($collection->getItems())) {
            $this->_wishlistHasItems = true;
        } else {
            $this->_wishlistHasItems = false;
        }

        $this->setWishlistItems($collection);
        return $this;
    }

    /**
     * Get current tab id
     *
     * @return int
     */
    public function getTabId()
    {
        return $this->_tabId;
    }

    /**
     * Check if current wishlist has items
     *
     * @return boolean
     */
    public function getWishlistHasItems()
    {
        return $this->_wishlistHasItems;
    }

    /**
     * Get details link html
     *
     * @return string
     */
    public function getDetailsLinkHtml($item)
    {
        $block = $this->getLayout()->createBlock('wishlist/customer_wishlist_item_options');

        if ($block) {
            $block->setItem($item);
            return $block->toHtml();
        }

        return '';
    }
}