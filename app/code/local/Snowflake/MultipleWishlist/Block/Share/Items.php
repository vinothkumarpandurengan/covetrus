<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 27.02.2015
 * Time: 15:00
 */

class Snowflake_MultipleWishlist_Block_Share_Items extends Mage_Wishlist_Block_Share_Email_Items
{

    /**
     * Get wishlist items for sharing per email
     *
     * @return Mage_Wishlist_Model_Resource_Item_Collection
     */
    public function getWishlistItems()
    {
        $shareWishlistId = Mage::getSingleton('customer/session')->getData('share_wishlist_id');

        if (is_null($this->_collection)) {
            $this->_collection = $this->_getWishlist()
                ->getItemCollection()
                ->addStoreFilter(Mage::app()->getStore()->getId());

            $this->_collection
                ->getSelect()
                ->join(array('wishlists' => Mage::getSingleton('core/resource')->getTableName('snowflake_multiplewishlist/wishlist_items')),
                    'main_table.wishlist_item_id = wishlists.item_id and wishlists.multiwishlist_id = ' . $shareWishlistId
                );

            $this->_prepareCollection($this->_collection);
        }

        return $this->_collection;
    }
}
  