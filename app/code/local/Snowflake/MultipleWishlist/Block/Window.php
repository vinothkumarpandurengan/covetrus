<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 24.02.2015
 * Time: 14:57
 */
class Snowflake_MultipleWishlist_Block_Window extends Mage_Core_Block_Template
{
    public function __construct()
    {
        parent::__construct();
        $wishlist = Mage::getModel('wishlist/wishlist')->loadByCustomer(Mage::getSingleton('customer/session')->getCustomer(), true);

        /** @var $multiWishlist Snowflake_MultipleWishlist_Model_Wishlist */
        $multiWishlist = Mage::getModel('snowflake_multiplewishlist/wishlist');

        $mainWishlistId = $multiWishlist->getMainWishlistId(Mage::getSingleton('customer/session')->getCustomer()->getId());
        $newItems       = $multiWishlist->checkingForNewItems($wishlist->getId());
        if ($newItems) {
            if (Mage::getSingleton('customer/session')->getProductParam()) {
                $this->setTemplate('snowflake/wishlist/ajax/window.phtml');
                Mage::getSingleton('customer/session')->setProductParam(null);
            } else {
                $multiWishlist->insertItemsInList($newItems, $mainWishlistId);
            }
        }

        // Set current tab id
        if (Mage::getSingleton('customer/session')->getWishlistTabId()) {
            $this->getRequest()->setParam('tabId', Mage::getSingleton('customer/session')->getWishlistTabId());
        }
    }
}