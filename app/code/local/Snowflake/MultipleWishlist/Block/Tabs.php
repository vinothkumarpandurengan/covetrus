<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 18.02.2015
 * Time: 10:51
 */
class Snowflake_MultipleWishlist_Block_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Set wishlist tabs template
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('snowflake/wishlist/ajax/tabs.phtml');
    }
}