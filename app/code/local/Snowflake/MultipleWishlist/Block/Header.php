<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 18.02.2015
 * Time: 10:51
 */
class Snowflake_MultipleWishlist_Block_Header extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Set wishlist header template
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('diagram_tab');
        $this->setDestElementId('diagram_tab_content');
        $this->setTemplate('snowflake/wishlist/index/header.phtml');
    }
}