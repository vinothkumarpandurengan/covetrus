<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 17.02.2015
 * Time: 14:11
 */
class Snowflake_MultipleWishlist_Block_Content extends Mage_Core_Block_Template
{
    /**
     * Set wishlist content template
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('snowflake/wishlist/ajax/content.phtml');
    }

    /**
     * Get wishlist header html
     *
     * @return string
     */
    public function getHeaderHtml()
    {
        $block = $this->getLayout()->createBlock('snowflake_multiplewishlist/header');
        return $block->toHtml();
    }

    /**
     * Get wishlist tabs html
     *
     * @return string
     */
    public function getTabsHtml()
    {
        $block = $this->getLayout()->createBlock('snowflake_multiplewishlist/tabs');
        return $block->toHtml();
    }
}
  