<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 27.02.2015
 * Time: 10:31
 */
class Snowflake_MultipleWishlist_Block_Admin_Wishlist_View extends Mage_Adminhtml_Block_Customer_Edit_Tab_Wishlist
{
    /**
     * Set admin wishlist template
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('snowflake/wishlist/wishlist.phtml');
    }

    /**
     * Prepare wishlist form block
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {
        $this->setChild('form',
            $this->getLayout()->createBlock('snowflake_multiplewishlist/admin_wishlist_form','wishlist.form')
        );
        return parent::_prepareLayout();
    }

    /**
     * Prepare grid collection object
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        /** @var $wishlist Mage_Wishlist_Model_Wishlist */
        $wishlist = Mage::getModel('wishlist/wishlist');

        $multiWishlist      = Mage::getSingleton('core/resource')->getTableName('snowflake_multiplewishlist/wishlist');
        $multiWishlistItems = Mage::getSingleton('core/resource')->getTableName('snowflake_multiplewishlist/wishlist_items');

        $collection = $wishlist->loadByCustomer($this->_getCustomer())
            ->setSharedStoreIds($wishlist->getSharedStoreIds(false))
            ->getItemCollection()
            ->resetSortOrder()
            ->addDaysInWishlist()
            ->addStoreData();

        $collection->getSelect()->join($multiWishlistItems, 'main_table.wishlist_item_id = ' . $multiWishlistItems . '.item_id');
        $collection->getSelect()->join($multiWishlist, $multiWishlistItems . '.multiwishlist_id = ' . $multiWishlist . '.multiwishlist_id', array(
            'wlname' => 'multiwishlist_name'));

        $this->setCollection($collection);

        return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
    }

    /**
     * Prepare Grid columns
     *
     * @return Snowflake_MultipleWishlist_Block_Admin_Wishlist_View
     */
    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        $this->addColumnAfter('wlname', array(
            'header' => Mage::helper('wishlist')->__('Whishlist name'),
            'index' => 'wlname',
            'width' => '100px'
        ), 'store');

        return Mage_Adminhtml_Block_Widget_Grid::_prepareColumns();
    }

    /**
     * Add column filter to collection
     *
     * @return Snowflake_MultipleWishlist_Block_Admin_Wishlist_View
     */
    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'store') {
            $this->getCollection()->addFieldToFilter('item_store_id', $column->getFilter()->getCondition());

            return $this;
        }

        if ($column->getId() == 'wlname') {
            $value = $column->getFilter()->getValue();
            $this->_addWishlistNameFilter($value);

            return $this;
        }

        if ($this->getCollection() && $column->getFilter()->getValue()) {
            $this->getCollection()->addFieldToFilter($column->getIndex(), $column->getFilter()->getCondition());
        }

        return $this;
    }

    /**
     * Add wishlist name as filter
     *
     * @return Netzarbeiter_GroupsCatalog2_Model_Wishlist_Resource_Item_Collection
     */
    protected function _addWishlistNameFilter($wishlistName)
    {
        $tableNames = Mage::getSingleton('core/resource')->getTableName('snowflake_multiplewishlist/wishlist');

        $collection = $this->getCollection();
        $collection->getSelect()->where($tableNames . "multiwishlist_name = '$wishlistName'");

        $this->setCollection($collection);
        return $collection;
    }
}
  