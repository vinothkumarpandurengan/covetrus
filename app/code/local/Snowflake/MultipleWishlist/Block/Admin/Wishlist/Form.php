<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 27.02.2015
 * Time: 10:51
 */
class Snowflake_MultipleWishlist_Block_Admin_Wishlist_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Set admin wishlist form template
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('snowflake/wishlist/form.phtml');
    }

    /**
     * Prepare wishlist form
     *
     * @return Snowflake_MultipleWishlist_Block_Admin_Wishlist_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();

        $products = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSort('name', 'ASC')
            ->addAttributeToFilter('visibility', array('in' => array(2, 3, 4)));

        $productToJson = array();
        foreach ($products->getItems() as $product) {
            $productToJson[] = array(
                'name' => $product->getName(),
                'id'   => $product->getId(),
                'sku'  => $product->getSku(),
            );
        }

        $this->setProducts($productToJson);

        $helper = Mage::helper('snowflake_multiplewishlist');
        if($helper->isEnabled()){
            $fieldset = $form->addFieldset('snowflake_wishlist', array('legend'=> $this->__('Manage Wishlists')));
        }

        $this->setForm($form);
        return $this;
    }

    /**
     * Get necessary translates for wishlist backend
     *
     * @return string
     */
    public function getTranslates()
    {
        /** @var $helper Snowflake_MultipleWishlist_Helper_Data */
        $helper = Mage::helper('snowflake_multiplewishlist');

        $data = array(
            'wishlistName'          => $helper->__('Wishlist Name'),
            'manageableCustomer'    => $helper->__('Manageable for Customer'),
            'addWishlist'           => $helper->__('Add Wishlist'),
            'delete'                => $helper->__('delete'),
            'deleteText'            => $helper->__('delete'),
            'edit'                  => $helper->__('edit'),
            'addProduct'            => $helper->__('add product'),
            'remove'                => $helper->__('remove'),
            'confirmRemoveProduct'  => $helper->__('Are you really want to remove this product from the wishlist?'),
            'confirmRemoveWishlist' => $helper->__('Do you really want to delete this Wishlist?'),
            'minimize'              => $helper->__('minimize'),
            'enterNewName'          => $helper->__('Please enter a new name'),
        );

        return Mage::helper('core')->jsonEncode($data);
    }

    /**
     * Get necessary urls for wishlist backend
     *
     * @return string
     */
    public function getUrls()
    {
        /** @var $helper Mage_Adminhtml_Helper_Data */
        $helper = Mage::helper('adminhtml');

        $data = array(
            'addWishlist'    => $helper->getUrl('snowflake_multiplewishlist/admin_wishlist/addWishlist'),
            'deleteWishlist' => $helper->getUrl('snowflake_multiplewishlist/admin_wishlist/deleteWishlist'),
            'renameWishlist' => $helper->getUrl('snowflake_multiplewishlist/admin_wishlist/renameWishlist'),
            'addItem'        => $helper->getUrl('snowflake_multiplewishlist/admin_wishlist/addItem'),
            'deleteItem'     => $helper->getUrl('snowflake_multiplewishlist/admin_wishlist/deleteItem'),
            'getWishlists'   => $helper->getUrl('snowflake_multiplewishlist/admin_wishlist/getWishlists'),
            'getProducts'    => $helper->getUrl('snowflake_multiplewishlist/admin_wishlist/getProducts'),
        );

        return Mage::helper('core')->jsonEncode($data);
    }
}
  