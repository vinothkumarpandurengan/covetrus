<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 18.02.2015
 * Time: 09:46
 */

class Snowflake_MultipleWishlist_Model_Settings extends Varien_Object
{
    const AFTER_ADD_TO_CART_REMOVE = 0;
    const AFTER_ADD_TO_CART_LEAVE = 1;
    const AFTER_WISHLIST_SELECTED_OPEN_WISHLIST = 0;
    const AFTER_WISHLIST_SELECTED_STAY_ON_PAGE = 1;


    /**
     * Load multiple wishlist settings
     *
     * @return Snowflake_MultipleWishlist_Model_Settings
     */
    public function load() {
        $settings = Mage::getStoreConfig('wishlist/multiple_wishlist');

        if (!isset($settings['default_wishlist']) || $settings['default_wishlist'] == '') {
            $settings['default_wishlist'] = 'Main';
        }

        $this->_saveData($settings);
        return $this;
    }

    /**
     * Save multiple wishlist settings data for further use
     *
     * @param array $settings
     * @return void
     */
    protected function _saveData($settings) {
        foreach($settings as $key => $value) {
            $this->setData($key, $value);
        }
    }
}