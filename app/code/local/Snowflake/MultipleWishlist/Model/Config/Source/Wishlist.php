<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 27.02.2015
 * Time: 18:01
 */
class Snowflake_MultipleWishlist_Model_Config_Source_Wishlist
{
    public function toOptionArray()
    {
        $helper = Mage::helper('snowflake_multiplewishlist');

        return array(
            array('value'=>'0', 'label'=> $helper->__('Redirect to wishlist')),
            array('value'=>'1', 'label'=> $helper->__('Stay on current page')),
        );
    }
}
  