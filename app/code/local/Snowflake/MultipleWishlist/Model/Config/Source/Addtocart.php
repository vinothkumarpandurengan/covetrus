<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 27.02.2015
 * Time: 18:01
 */
class Snowflake_MultipleWishlist_Model_Config_Source_Addtocart
{
    public function toOptionArray()
    {
        $helper = Mage::helper('snowflake_multiplewishlist');

        return array(
            array('value'=>'0', 'label'=> $helper->__('Remove product(s) from Wishlist')),
            array('value'=>'1', 'label'=> $helper->__('Leave product(s) in Wishlist')),
        );
    }
}
  