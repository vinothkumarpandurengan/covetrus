<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 18.02.2015
 * Time: 16:57
 */

class Snowflake_MultipleWishlist_Model_Resource_Item_Collection extends Mage_Wishlist_Model_Mysql4_Item_Collection
{
    protected $_productAttributesJoined = array();
    protected $_productFields = array('sku', 'type_id', 'created_at', 'updated_at');

    /**
     * Join product attribute db table
     *
     * @param string $attributeCode
     * @return bool|Snowflake_MultipleWishlist_Model_Resource_Item_Collection
     */
    protected function _joinProductAttributeTable($attributeCode)
    {
        if (!in_array($attributeCode, $this->_productAttributesJoined)) {
            $entityTypeId = Mage::getResourceModel('catalog/config')->getEntityTypeId();
            $attribute = Mage::getModel('catalog/entity_attribute')->loadByCode($entityTypeId, $attributeCode);

            if ($attribute->getId() && $attribute->getBackendTable()) {
                $storeId = Mage::app()->getStore()->getId();
                $attributeTableAlias = 'product_'. $attributeCode .'_table';
                $this->getSelect()
                    ->join(
                        array($attributeTableAlias => $attribute->getBackendTable()),
                        $attributeTableAlias . '.entity_id=main_table.product_id' .
                        ' AND ('. $attributeTableAlias .'.store_id=' . $storeId . ' or '. $attributeTableAlias .'.store_id=0)' .
                        ' AND '. $attributeTableAlias .'.attribute_id=' . $attribute->getId().
                        ' AND '. $attributeTableAlias .'.entity_type_id=' . $entityTypeId,
                        array()
                    );

                $this->_productAttributesJoined[] = $attributeCode;
            } else {
                return false;
            }
        }
        return $this;
    }

    /**
     * Join product db table
     *
     * @return bool|Snowflake_MultipleWishlist_Model_Resource_Item_Collection
     */
    protected function _joinProductTable()
    {
        $tableAlias = 'product_table';
        $this->getSelect()
            ->join(
                array($tableAlias => Mage::getSingleton('core/resource')->getTableName('catalog_product_entity')),
                $tableAlias . '.entity_id=main_table.product_id',
                array()
            );

        return $this;
    }

    /**
     * Join product price db table
     *
     * @return bool|Snowflake_MultipleWishlist_Model_Resource_Item_Collection
     */
    protected function _joinProductPriceTable()
    {
        $attributeTableAlias = 'product_price_table';
        $customerGroupId = (int)Mage::getSingleton('customer/session')->getCustomer()->getGroupId();
        $websiteId = (int)Mage::app()->getWebsite()->getId();
        $this->getSelect()
            ->joinLeft(
                array($attributeTableAlias => Mage::getSingleton('core/resource')->getTableName('catalog_product_index_price')),
                $attributeTableAlias . '.entity_id=main_table.product_id' .
                ' AND ' . $attributeTableAlias . '.customer_group_id=' . $customerGroupId .
                ' AND ' . $attributeTableAlias . '.website_id=' . $websiteId,
                array()
            );

        return $this;
    }

    /**
     * Set order by product attribute
     *
     * @param string $attributeCode
     * @param string $dir
     * @return Snowflake_MultipleWishlist_Model_Resource_Item_Collection
     */
    public function setOrderByProductAttribute($attributeCode, $dir)
    {
        $attributeCode = trim($attributeCode);
        if ($attributeCode == 'price') {
            $this->_joinProductPriceTable();
            $this->getSelect()->order('product_price_table.min_price ' . $dir);
        } elseif (in_array($attributeCode, $this->_productFields)) {
            $this->_joinProductTable();
            $this->getSelect()->order('product_table.' . $attributeCode . ' ' . $dir);
        } else {
            $this->_joinProductAttributeTable($attributeCode);
            $attributeTableAlias = 'product_'. $attributeCode .'_table';
            $this->getSelect()->order($attributeTableAlias . '.value ' . $dir);
        }

        return $this;
    }
}
  