<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 18.02.2015
 * Time: 10:34
 */
class Snowflake_MultipleWishlist_Model_Resource_Wishlist extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('snowflake_multiplewishlist/wishlist', 'multiwishlist_id');
    }
}