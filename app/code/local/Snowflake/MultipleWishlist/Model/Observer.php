<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 17.02.2015
 * Time: 12:17
 */
class Snowflake_MultipleWishlist_Model_Observer
{
    /**
     * Check if extension is enabled
     *
     * @return boolean
     */
    protected function _isEnabled()
    {
        return Mage::helper('snowflake_multiplewishlist')->isEnabled();
    }

    /**
     * Insert item to the multiple wishlist
     *
     * @return void
     */
    protected function _insertItemInWishlist()
    {
        $request = Mage::app()->getRequest();
        $wishlistId = $request->getParam('multiwishlist');
        if ($wishlistId) {
            $wishlist = Mage::getModel('wishlist/wishlist')->loadByCustomer(Mage::getSingleton('customer/session')->getCustomer(), true);

            /** @var $multiWishlist Snowflake_MultipleWishlist_Model_Wishlist */
            $multiWishlist = Mage::getModel('snowflake_multiplewishlist/wishlist');

            if ($request->getParam('multiwishlist_new')) {
                if ($request->getParam('multiwishlist')) {
                    $wishlistId = $multiWishlist->createWishlist($request->getParam('multiwishlist'));
                }
            }
            if ($wishlistId == 'main') {
                $wishlistId = $multiWishlist->getMainWishlistId(Mage::getSingleton('customer/session')->getCustomer()->getId());
            }
            if ($wishlistId) {
                $newItems = $multiWishlist->checkingForNewItems($wishlist->getId());
                $multiWishlist->insertItemsInList($newItems, $wishlistId);
                Mage::getSingleton('customer/session')->setWishlistTabId($wishlistId);
            }
        }
    }

    /**
     * Prepare wishlist template in my account
     *
     * @return void
     */
    public function wishlistAvailable(Varien_Event_Observer $observer)
    {
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            if ($this->_isEnabled()) {
                $controller = $observer->getControllerAction();
                $controller->loadLayout();

                $storage = Mage::getSingleton('customer/session');
                $controller->getLayout()->getMessagesBlock()->addMessages($storage->getMessages(true));

                $storage = Mage::getSingleton('customer/session');
                $controller->getLayout()->getMessagesBlock()->addMessages($storage->getMessages(true));

                $storage = Mage::getSingleton('checkout/session');
                $controller->getLayout()->getMessagesBlock()->addMessages($storage->getMessages(true));

                $storage = Mage::getSingleton('catalog/session');
                $controller->getLayout()->getMessagesBlock()->addMessages($storage->getMessages(true));

                $storage = Mage::getSingleton('wishlist/session');
                $controller->getLayout()->getMessagesBlock()->addMessages($storage->getMessages(true));

                $head = $controller->getLayout()->getBlock('head');
                $head->addItem('skin_css', 'css/snowflake/multiplewishlist/wishlist.css');
                $head->addJS('prototype/window.js');

                $wishlist = Mage::getModel('snowflake_multiplewishlist/wishlist');

                $content = $controller->getLayout()->getBlock('content');
                $content->unsetChildren();

                $contentBlock = $controller->getLayout()->createBlock('snowflake_multiplewishlist/content');
                $contentBlock->setNamesCollection($wishlist->getWishlistNames());

                $windowBlock = $controller->getLayout()->createBlock('snowflake_multiplewishlist/window');

                $content->append($windowBlock);
                $content->append($contentBlock);

                $controller->renderLayout();
                Mage::app()->getResponse()->sendResponse();
                exit;
            } else {
                return;
            }
        }
    }

    /**
     * Add product to wishlist
     *
     * @param Varien_Event_Observer $observer
     */
    public function wishlistAddItem(Varien_Event_Observer $observer)
    {
        Mage::getSingleton('customer/session')->setProductParam($observer->getProduct()->getId());
    }

    /**
     * After add product to wishlist
     * always create new item with new options if needed
     * Because Magento can use the same item for the product
     *
     * @param Varien_Event_Observer $observer
     */
    public function unsetItemId(Varien_Event_Observer $observer)
    {
        if ($this->_isEnabled()) {
            $items = $observer->getItems();
            if (Mage::registry('update_items_action')) {
                if (Mage::registry('update_items')) {
                    Mage::unregister('update_items');
                }
                Mage::register('update_items', $items);
                return;
            }
            /** @var $item Mage_Wishlist_Model_Item */
            foreach ($items as $item) {
                if ($item->getOrigData()) {
                    $item->loadWithOptions($item->getId())
                        ->setId(null);

                    $qty = Mage::app()->getRequest()->getParam('qty');
                    $item->setQty($qty ? $qty : 1)
                        ->save();

                    $options = $item->getOptions();
                    if (is_array($options)) {
                        /** @var $option Mage_Wishlist_Model_Item_Option */
                        foreach ($options as $option) {
                            $option->setId(null);
                            $option->setItem($item);
                            $option->save();
                        }
                    }
                }
            }
            $this->_insertItemInWishlist();
        }
    }

    /**
     * Set item qty
     *
     * @param Varien_Event_Observer $observer
     */
    public function setOrigItemQty(Varien_Event_Observer $observer)
    {
        if ($this->_isEnabled()) {
            /** @var $item Mage_Wishlist_Model_Wishlist */
            $item = $observer->getDataObject();
            if ($item->getIsUpdateAction() || $item->getSkipOrigSetQty()) {
                return;
            }
            $productId = Mage::app()->getRequest()->getParam('product');
            if ($item->getId() && $item->getOrigData('qty') != $item->getQty() && $item->getProductId() == $productId) {
                $item->setQty($item->getOrigData('qty'));
            }
        }
    }

    public function addProductStatusFilter(Varien_Event_Observer $observer)
    {
        $collection = $observer->getCollection();
        if (($collection instanceof Mage_Wishlist_Model_Resource_Item_Collection || $collection instanceof Mage_Wishlist_Model_Mysql4_Item_Collection)
            && $this->_isEnabled()
        ) {
            $attributeCode = 'status';
            $attributeTableAlias = 'snowflake_product_'. $attributeCode .'_table';
            $parts = $collection->getSelect()->getPart('from');
            if (isset($parts[$attributeTableAlias])) {
                return;
            }

            $entityTypeId = Mage::getResourceModel('catalog/config')->getEntityTypeId();
            $attribute    = Mage::getModel('catalog/entity_attribute')->loadByCode($entityTypeId, $attributeCode);

            if ($attribute->getId() && $attribute->getBackendTable()) {
                $storeId = Mage::app()->getStore()->getId();
                $collection->getSelect()
                    ->join(
                        array($attributeTableAlias => $attribute->getBackendTable()),
                        $attributeTableAlias . '.entity_id=main_table.product_id' .
                        ' AND ('. $attributeTableAlias .'.store_id=' . $storeId . ' or '. $attributeTableAlias .'.store_id=0)' .
                        ' AND '. $attributeTableAlias .'.attribute_id=' . $attribute->getId() .
                        ' AND '. $attributeTableAlias .'.entity_type_id=' . $entityTypeId .
                        ' AND '. $attributeTableAlias .'.value=1',
                        array()
                    );
            }
        }
    }
}
  