<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 18.02.2015
 * Time: 10:34
 */
class Snowflake_MultipleWishlist_Model_Wishlist extends Mage_Core_Model_Abstract
{
    const WISHLIST_EDITABLE = 1;
    const WISHLIST_NOT_EDITABLE = 0;

    protected $db                               = null;
    protected $itemsTable                       = 'snowflake_wishlist_items';
    protected $tableWishlist                    = 'wishlist';
    protected $tableWishlistItem                = 'wishlist_item';
    protected $tableEavAttribute                = 'eav_attribute';
    protected $tableCatalogProductEntity        = 'catalog_product_entity';
    protected $tableCatalogProductEntityVarchar = 'catalog_product_entity_varchar';

    protected function _construct()
    {
        /**@var $db Varien_Db_Adapter_Pdo_Mysql*/
        $this->db = Mage::getSingleton('core/resource')->getConnection('core_write');

        $this->_init('snowflake_multiplewishlist/wishlist');
    }

    /**
     * Check if wishlist name exist
     *
     * @param string $name
     * @return int wishlist id
     */
    protected function _isWishlistNameExists($name)
    {
        $wishlist = $this->loadByName($name);
        return isset($wishlist['multiwishlist_id']) ? $wishlist['multiwishlist_id'] : null;
    }

    /**
     * Change qty if product is in multiple wishlist
     *
     * @param int $itemId
     * @param int $wishlistId
     * @param bool $deleteItemIfExists
     * @return boolean
     */
    protected function _changeQtyIfProductInWishlist($itemId, $wishlistId, $deleteItemIfExists = true)
    {
        /** @var $itemModel Mage_Wishlist_Model_Item */
        $itemModel = Mage::getModel('wishlist/item')->loadWithOptions($itemId);
        $productId = $itemModel->getProductId();

        /** @var $wishlist Mage_Wishlist_Model_Wishlist */
        $wishlist = Mage::getModel('wishlist/wishlist');
        $wishlist->loadByCustomer(Mage::getSingleton('customer/session')->getCustomer());
        $items = $wishlist->getItemCollection()->addFieldToFilter('product_id', array('eq' => $productId));

        if ($items->getSize()) {
            foreach ($items as $item) {
                $result = $this->db->fetchRow("SELECT * FROM {$this->itemsTable} WHERE `item_id`={$item->getId()} and `multiwishlist_id`={$wishlistId}");
                if (!empty($result)) {
                    $item->loadWithOptions($item->getId());
                    $origQty = $item->getQty();
                    $item->setQty($itemModel->getQty());
                    $isExistsEqual = $this->_isRepresent($itemModel, $itemModel->getProduct(), $item->getBuyRequest());
                    $item->setQty($origQty);

                    if ($isExistsEqual) {
                        $item->setQty($item->getQty() + $itemModel->getQty());
                        $item->setSkipOrigSetQty(true);
                        $item->save();
                        if ($deleteItemIfExists) {
                            $itemModel->delete();
                        }
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Check if item represent product with buyRequest
     * (for rewrite standard magento function _compareOptions)
     *
     * @param $item
     * @param $product
     * @param $buyRequest
     * @return bool
     */
    protected function _isRepresent($item, $product, $buyRequest)
    {
        if ($item->getProductId() != $product->getId()) {
            return false;
        }

        $selfOptions = $item->getBuyRequest()->getData();

        if (empty($buyRequest) && !empty($selfOptions)) {
            return false;
        }
        if (empty($selfOptions) && !empty($buyRequest)) {
            if (!$product->isComposite()){
                return true;
            } else {
                return false;
            }
        }

        $requestArray = $buyRequest->getData();

        if(!$this->_compareOptions($requestArray, $selfOptions)){
            return false;
        }

        if(!$this->_compareOptions($selfOptions, $requestArray)){
            return false;
        }
        return true;
    }

    /**
     * Like standard item->_compareOptions
     * but added check on empty values
     *
     * @param $options1
     * @param $options2
     * @return bool
     */
    protected function _compareOptions($options1, $options2)
    {
        $skipOptions = array('id', 'qty', 'return_url');
        foreach ($options1 as $code => $value) {
            if (in_array($code, $skipOptions)) {
                continue;
            }
            if (empty($value)) {
                continue;
            }
            if (is_array($value)) {
                $allEmpty = true;
                foreach ($value as $subValue) {
                    if (!empty($subValue)) {
                        $allEmpty = false;
                        break;
                    }
                }
                if ($allEmpty) {
                    continue;
                }
            }
            if (!isset($options2[$code]) || $options2[$code] != $value) {
                return false;
            }
        }
        return true;
    }

    /**
     * Load wishlist by id
     *
     * @param int $name
     * @return array
     */
    public function loadById($id)
    {
        return $this->getWishlistById($id);
    }

    /**
     * Load wishlist by name
     *
     * @param string $name
     * @return array
     */
    public function loadByName($name)
    {
        return $this->getWishlistByName($name);
    }

    /**
     * Retrieve wishlists for current customer if customerId not specified
     * only editable wishlists if editableOnly set true
     *
     * @param int $customerId
     * @param bool $editableOnly
     * @return array
     */
    public function getWishlists($customerId = null, $editableOnly = false)
    {
        return $this->getWishlistNames($customerId, $editableOnly);
    }

    /**
     * Get all available wishlist names
     *
     * @param int $customerId
     * @param bool $editableOnly
     * @return array
     */
    public function getWishlistNames($customerId = null, $editableOnly = false)
    {
        $customerId = $customerId ? $customerId : (int) Mage::getSingleton('customer/session')->getCustomerId();
        $this->checkMainWishlist($customerId);

        $data = $this->getCollection()
            ->addFieldToFilter('multiwishlist_customer_id', $customerId);

        if ($editableOnly) {
            $data->addFieldToFilter('multiwishlist_editable', 1);
        }

        $data->setOrder('multiwishlist_name', 'ASC');
        return $data->getData();
    }

    public function getWishlistItems($id)
    {
        return $this->db->fetchAll("
            SELECT e.item_id as id, pv.value as name FROM {$this->itemsTable} as e
			INNER JOIN {$this->tableWishlistItem} as w
			ON w.wishlist_item_id = e.item_id
			INNER JOIN {$this->tableCatalogProductEntity} as p
			ON p.entity_id = w.product_id
			INNER JOIN {$this->tableEavAttribute} as a
			ON a.entity_type_id = p.entity_type_id and a.attribute_code = 'name'
			INNER JOIN {$this->tableCatalogProductEntityVarchar} as pv
			ON pv.entity_id = p.entity_id and pv.attribute_id = a.attribute_id
			WHERE e.multiwishlist_id = {$id}
			ORDER BY pv.value
		");
    }

    public function isItemInWishlist($customerId, $productId)
    {
        return $this->db->fetchOne("
            SELECT w.wishlist_item_id FROM {$this->tableWishlist} as e
            INNER JOIN {$this->tableWishlistItem} as w
            ON w.wishlist_id = e.wishlist_id and w.product_id = {$productId}
            WHERE e.customer_id = {$customerId}
		");
    }

    /**
     * Get main wishlist id
     *
     * @param int $customerId
     * @return int main wishlist id
     */
    public function getMainWishlistId($customerId)
    {
        return $this->checkMainWishlist($customerId);
    }

    /**
     * Get wishlist id from item id
     *
     * @param int $itemId
     * @return int
     */
    public function getWishlistIdByItemId($itemId)
    {
        return $this->db->fetchOne("
            SELECT multiwishlist_id FROM {$this->itemsTable}
            WHERE item_id = {$itemId}
        ");
    }

    /**
     * Get wishlist by name
     *
     * @param string $name
     * @return array
     */
    public function getWishlistByName($name)
    {
        $customerId = (int) Mage::getSingleton('customer/session')->getCustomerId();
        $wishlist = $this->getCollection()
            ->addFieldToFilter('multiwishlist_name', $name)
            ->addFieldToFilter('multiwishlist_customer_id', $customerId)
            ->getFirstItem()
            ->getData();

        return $wishlist;
    }

    /**
     * Get multiple wishlist by id
     *
     * @param int $id
     * @return array
     */
    public function getWishlistById($id)
    {
        $data = $this->getCollection()
            ->addFieldToFilter('multiwishlist_id', $id)
            ->getFirstItem()
            ->getData();

        return $data;
    }

    /**
     * Get multiple wishlist name by id
     *
     * @param int $id
     * @return string
     */
    public function getWishlistNameById($id)
    {
        $wishlist = $this->loadById($id);

        if (isset($wishlist['multiwishlist_name'])) {
            return $wishlist['multiwishlist_name'];
        }

        return null;
    }

    /**
     * Set wishlist name
     *
     * @param string $name
     * @param bool $editable
     * @param int $customerId
     * @return int
     */
    public function setName($name, $editable = Snowflake_MultipleWishlist_Model_Wishlist::WISHLIST_EDITABLE, $customerId = null)
    {
        $customerId = $customerId ? $customerId : Mage::getSingleton('customer/session')->getCustomerId();

        $this->setMultiwishlistName($name)
            ->setMultiwishlistCustomerId($customerId)
            ->setMultiwishlistEditable($editable)
            ->save();

        return $this->getId();
    }

    /**
     * Update wishlist name
     *
     * @param int $id
     * @param string $newName
     * @return Snowflake_MultipleWishlist_Model_Wishlist
     */
    public function updateName($id, $newName)
    {
        $data = array('multiwishlist_name' => $newName);

        $this->load($id)
            ->addData($data)
            ->save();

        return $this;
    }

    /**
     * Create new wishlist and return its id
     *
     * @param string $name
     * @param bool $editable
     * @param int $customerId
     * @return int
     */
    public function createWishlist($name, $editable = Snowflake_MultipleWishlist_Model_Wishlist::WISHLIST_EDITABLE, $customerId = null)
    {
        $existsWishlistId = $this->_isWishlistNameExists($name);
        if ($existsWishlistId) {
            return $existsWishlistId;
        }

        return $this->setName($name, $editable, $customerId);
    }

    /**
     * Delete wishlist
     *
     * @param int $wishlistId
     * @return void
     */
    public function deleteWishlist($wishlistId)
    {
        $wishlistId = (int) $wishlistId;

        $tableItems = $this->itemsTable;
        $wishlistItemTable = $this->tableWishlistItem;

        $this->db->query("DELETE FROM $wishlistItemTable WHERE `wishlist_item_id` in (SELECT `item_id` FROM $tableItems WHERE `multiwishlist_id` = $wishlistId)");
        $this->setId($wishlistId)->delete();

        Mage::dispatchEvent('multiple_wishlist_update', array('wishlist' => $this));
    }

    /**
     * Remove item from multiple wishlist
     *
     * @param int $itemId
     * @return void
     */
    public function removeItem($itemId) {
        $itemId = (int) $itemId;

        $wishlistItemTable = $this->tableWishlistItem;

        $this->db->query("DELETE FROM $wishlistItemTable WHERE `wishlist_item_id` = $itemId");

        Mage::dispatchEvent('multiple_wishlist_update', array('wishlist' => $this));
    }

    /**
     * Insert items to multiple wishlist
     *
     * @param int $itemId
     * @param int $listId
     * @return void
     */
    public function insertItemsInList($itemsId, $listId)
    {
        $tableItems = $this->itemsTable;
        $sqlString  = '';
        $tempArray  = array();

        if (is_array($itemsId)) {
            foreach ($itemsId as $value) {
                if (isset($value['wishlist_item_id'])) {
                    $wishlistItemId = $value['wishlist_item_id'];
                    if (!$this->_changeQtyIfProductInWishlist($wishlistItemId, $listId)) {
                        $sqlString = '(' . $wishlistItemId . ',' . $listId . ')';
                        $tempArray[] = $sqlString;
                    }
                }
            }

            $sqlString = implode(',', $tempArray);
        } else {
            if (!$this->_changeQtyIfProductInWishlist($itemsId, $listId)) {
                $itemsId = (int) $itemsId;
                $sqlString = '(' . $itemsId . ',' . $listId . ')';
            }
        }

        if (!empty($sqlString)) {
            $this->db->query("INSERT INTO $tableItems (`item_id`,`multiwishlist_id`) VALUES $sqlString");
            Mage::dispatchEvent('multiple_wishlist_update', array('wishlist' => $this));
        }
    }

    /**
     * Copy items between multiple wishlist
     *
     * @param int $itemId
     * @param int $listId
     * @return void
     */
    public function copyItemsBetweenLists($itemsId, $listId) {
        $tableItems = $this->itemsTable;
        $sqlString  = '';
        $tempArray  = array();

        if (is_array($itemsId)) {
            foreach ($itemsId as $key => $value) {
                if (!$this->_changeQtyIfProductInWishlist($value, $listId, false)) {
                    $sqlString = '(' . $value . ',' . $listId .')';
                    $tempArray[$key] = $sqlString;
                }
            }

            $sqlString = implode(',', $tempArray);
        } else {
            $itemsId = (int) $itemsId;
            if (!$this->_changeQtyIfProductInWishlist($itemsId, $listId, false)) {
                $sqlString = '(' . $itemsId .',' . $listId . ')';
            }
        }
        if (!empty($sqlString)) {
            $this->db->query("INSERT INTO $tableItems (`item_id`, `multiwishlist_id`) VALUES $sqlString");
        }
    }

    /**
     * Move items between multiple wishlist
     *
     * @param int $itemId
     * @param int $listId
     * @return void
     */
    public function moveItemsBetweenLists($itemId, $listId)
    {
        $tableItems = $this->itemsTable;
        $sqlString  = '';
        $tempArray  = array();

        if (is_array($itemId)) {
            foreach ($itemId as $key => $value) {
                if (!$this->_changeQtyIfProductInWishlist($value, $listId)) {
                    $sqlString = '(`item_id` = '.$value.')';
                    $tempArray[$key] = $sqlString;
                }
            }

            $sqlString = implode('OR', $tempArray);
        } else {
            $itemId = (int) $itemId;
            if (!$this->_changeQtyIfProductInWishlist($itemId, $listId)) {
                $sqlString = '`item_id` = '.$itemId;
            }
        }
        if (!empty($sqlString)) {
            $this->db->query("UPDATE $tableItems SET `multiwishlist_id` = $listId WHERE $sqlString");
        }

        /** @var $wishlist Mage_Wishlist_Model_Wishlist */
        $wishlist = Mage::getModel('wishlist/wishlist');
        $wishlist->loadByCustomer(Mage::getSingleton('customer/session')->getCustomer());
        $newItemsId = $this->checkingForNewItems($wishlist->getId());
        foreach ($newItemsId as $newItemId) {
            $item = Mage::getModel('wishlist/item')->load($newItemId);
            if ($item->getId()) {
                $item->delete();
            }
        }
    }

    /**
     * Check if main wishlist exist, otherwise create it
     *
     * @param int $customerId
     * @return int main wishlist id
     */
    public function checkMainWishlist($customerId)
    {
        $mainWishlist = $this->getCollection()
            ->addFieldToFilter('multiwishlist_customer_id', $customerId)
            ->addFieldToFilter('multiwishlist_is_main', 1)
            ->getFirstItem()
            ->getMultiwishlistId();

        if (empty($mainWishlist)) {
            $this->setMultiwishlistName(Mage::helper('snowflake_multiplewishlist')->getSettings()->getDefaultWishlist())
                ->setMultiwishlistCustomerId($customerId)
                ->setMultiwishlistEditable(Snowflake_MultipleWishlist_Model_Wishlist::WISHLIST_EDITABLE)
                ->setMultiwishlistIsMain(1)
                ->save();

            $mainWishlist = $this->getId();
        }

        return $mainWishlist;
    }

    /**
     * Check if the item in the wishlist is new
     *
     * @param int $wishlistId
     * @return array
     */
    public function checkingForNewItems($wishlistId)
    {
        $wishlistId = (int) $wishlistId;

        $tableItems = $this->itemsTable;
        $wishlistItemTable = $this->tableWishlistItem;

        return $this->db->fetchAll("
            SELECT `wishlist_item_id` FROM $wishlistItemTable
            WHERE `wishlist_item_id` not in (SELECT `item_id` FROM $tableItems) and `wishlist_id` = $wishlistId
        ");
    }
}