<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 23.02.2015
 * Time: 16:56
 */
require_once Mage::getModuleDir('controllers', 'Mage_Wishlist').DS.'IndexController.php';

class Snowflake_MultipleWishlist_IndexController extends Mage_Wishlist_IndexController
{
    /**
     * Ajax action for create new wishlist, add product to wishlist, remove product from wishlist
     * copy/move product to other wishlist
     *
     * Ajax request
     */
    public function ajaxAction()
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            $this->getResponse()->setBody("You are not logged in. Please login ");
        } else {
            /** @var $multiWishlist Snowflake_MultipleWishlist_Model_Wishlist */
            $multiWishlist  = $this->_getWishlistModel();
            $blockType = null;

            if (($this->getRequest()->getParam('wlname'))) {
                if ($newId = $this->_setWishlistName($this->getRequest()->getParam('wlname'))) {
                    $this->getRequest()->setParam('tabId', $newId);
                }
                $blockType = 'content';
            } else if ($this->getRequest()->getParam('newWishlistName')) {
                if ($this->_setWishlistName($this->getRequest()->getParam('newWishlistName'))) {
                    $result = $multiWishlist->getWishlistByName($this->getRequest()->getParam('newWishlistName'));

                    $wishlist = Mage::getModel('wishlist/wishlist')->loadByCustomer(Mage::getSingleton('customer/session')->getCustomer(), true);
                    $this->getRequest()->setParam('tabId', $result['multiwishlist_id']);

                    $multiWishlist->insertItemsInList($multiWishlist->checkingForNewItems($wishlist['wishlist_id']), ($result['multiwishlist_id']));
                    $blockType = 'content';
                }
            } else if ($this->getRequest()->getParam('removeWishlist')) {
                $multiWishlist->deleteWishlist($this->getRequest()->getParam('removeWishlist'));
                $blockType = 'tabs';
            } else if ($this->getRequest()->getParam('list')) {
                $blockType = 'frontview';

                if ($this->getRequest()->getParam('itemsCopy')) {
                    $newItems = array();

                    foreach ($this->getRequest()->getParam('itemsCopy') as $key => $value) {
                        $model = Mage::getModel('wishlist/item');

                        $model->loadWithOptions($value)
                            ->setId(null)
                            ->save();

                        $this->_saveItemOptions($model);
                        $newItems[$key] = $model->getId();
                    }

                    $multiWishlist->copyItemsBetweenLists($newItems, ($this->getRequest()->getParam('list')));
                } else if ($this->getRequest()->getParam('itemCopy')) {
                    $itemId = $this->getRequest()->getParam('itemCopy');
                    $model  = Mage::getModel('wishlist/item');

                    $model->loadWithOptions($itemId)
                        ->setId(null)
                        ->save();

                    $this->_saveItemOptions($model);
                    $multiWishlist->copyItemsBetweenLists($model->getId(), ($this->getRequest()->getParam('list')));
                } else if ($this->getRequest()->getParam('itemsMove')) {
                    $multiWishlist->moveItemsBetweenLists(($this->getRequest()->getParam('itemsMove')), ($this->getRequest()->getParam('list')));
                } else if ($this->getRequest()->getParam('itemMove')) {
                    $multiWishlist->moveItemsBetweenLists(($this->getRequest()->getParam('itemMove')), ($this->getRequest()->getParam('list')));
                } else if ($this->getRequest()->getParam('itemsDelete')) {
                    $items = $this->getRequest()->getParam('itemsDelete');
                    if (!is_array($items)) {
                        $items = array($items);
                    }

                    foreach ($items as $itemId) {
                        $item = Mage::getModel('wishlist/item')->load($itemId);
                        if ($item->getId()) {
                            $item->delete();
                        }
                    }
                } else if ($this->getRequest()->getParam('tabId')) {
                    $wishlist = Mage::getModel('wishlist/wishlist')->loadByCustomer(Mage::getSingleton('customer/session')->getCustomer(), true);

                    $multiWishlist->insertItemsInList($multiWishlist->checkingForNewItems($wishlist['wishlist_id']), ($this->getRequest()->getParam('list')));
                    $blockType = 'content';
                }
            } else if ($this->getRequest()->getParam('tabId')) {
                if ($this->getRequest()->getParam('remove')) {
                    $multiWishlist->removeItem($this->getRequest()->getParam('remove'));
                }
                $blockType = 'frontview';
            }
            if ($blockType) {
                $block = $this->getLayout()->createBlock('snowflake_multiplewishlist/' . $blockType);
                $this->getResponse()->setBody($block->toHtml());
            }
        }
    }

    /**
     * Add wishlist item to cart
     */
    public function cartAction()
    {
        if ($this->_getDataHelper()->isEnabled()) {
            $model  = $this->_getWishlistModel();
            $helper = $this->_getDataHelper();

            $wishlistId  = $model->getWishlistIdByItemId((int) $this->getRequest()->getParam('item'));
            $data        = $model->getWishlistById($wishlistId);

            //$deleteFromWishlist = ($data['multiwishlist_editable'] && $helper->deleteFromWishlist()) ? true : false;
            $deleteFromWishlist = false;
            $itemId = (int) $this->getRequest()->getParam('item');

            /* @var $item Mage_Wishlist_Model_Item */
            $item = Mage::getModel('wishlist/item')->load($itemId);
            if (!$item->getId()) {
                return $this->_redirect('*/*');
            }

            // Set qty
            $qty = $this->getRequest()->getParam('qty', 1);
            if ($qty) {
                $item->setQty($qty);
            }

            /* @var $session Mage_Wishlist_Model_Session */
            $session = Mage::getSingleton('wishlist/session');
            $cart    = Mage::getSingleton('checkout/cart');

            $redirectUrl = Mage::getUrl('*/*');

            try {
                $options = Mage::getModel('wishlist/item_option')->getCollection()
                    ->addItemFilter(array($itemId));
                $item->setOptions($options->getOptionsByItem($itemId));

                $buyRequest = Mage::helper('catalog/product')->addParamsToBuyRequest(
                    $this->getRequest()->getParams(),
                    array('current_config' => $item->getBuyRequest())
                );

                $item->mergeBuyRequest($buyRequest);
                if ($qty) {
                    $this->_addQtyToAssociatedProducts($item, $qty);
                }

                if ($item->addToCart($cart, $deleteFromWishlist)) {
                    $cart->save()->getQuote()->collectTotals();
                }

                if (Mage::helper('checkout/cart')->getShouldRedirectToCart()) {
                    $redirectUrl = Mage::helper('checkout/cart')->getCartUrl();
                } else if ($this->_getRefererUrl()) {
                    $redirectUrl = $this->_getRefererUrl();
                }
            } catch (Mage_Core_Exception $e) {
                if ($e->getCode() == Mage_Wishlist_Model_Item::EXCEPTION_CODE_NOT_SALABLE) {
                    $session->addError($this->__('This product(s) is currently out of stock'));
                } else if ($e->getCode() == Mage_Wishlist_Model_Item::EXCEPTION_CODE_HAS_REQUIRED_OPTIONS) {
                    Mage::getSingleton('catalog/session')->addNotice($e->getMessage());
                    $redirectUrl = Mage::getUrl('*/*/configure/', array('id' => $item->getId()));
                } else {
                    Mage::getSingleton('catalog/session')->addNotice($e->getMessage());
                    $redirectUrl = Mage::getUrl('*/*/configure/', array('id' => $item->getId()));
                }
            } catch (Exception $e) {
                Mage::logException($e);
                $session->addException($e, $this->__('Cannot add item to shopping cart'));
            }

            return $this->_redirectUrl($redirectUrl);
        } else {
            parent::cartAction();
        }
    }

    /**
     * Add all selected items from wishlist to shopping cart
     */
    public function addSelectedItemsToCartAction()
    {
        $model  = $this->_getWishlistModel();
        $helper = $this->_getDataHelper();

        $wishlist   = Mage::getModel('wishlist/wishlist')->loadByCustomer(Mage::getSingleton('customer/session')->getCustomer(), true);
        $wishlistId = (int) $this->getRequest()->getParam('wishlist_id');
        $data       = $model->getWishlistById($wishlistId);
        $isOwner    = ($data['multiwishlist_editable'] && $helper->deleteFromWishlist()) ? true : false;

        $messages   = array();
        $addedItems = array();
        $notSalable = array();
        $hasOptions = array();

        $cart       = Mage::getSingleton('checkout/cart');
        $collection = $wishlist->getItemCollection();

        $items = $this->getRequest()->getParam('items');
        if (!is_array($items)) {
            $items = array();
        }
        $items = array_map('intval', $items);

        $wishlistItems = Mage::getSingleton('core/resource')->getTableName('snowflake_multiplewishlist/wishlist_items');
        $collection->getSelect()->join($wishlistItems, "wishlist_item_id = $wishlistItems.item_id");
        $collection->getSelect()->where("multiwishlist_id = $wishlistId");

        $qtys = $this->getRequest()->getParam('qty');

        $origGroupedRequest = array();
        foreach ($collection->getItems() as $item) {
            if (!in_array($item->getId(), $items)) {
                continue;
            }

            /** @var Mage_Wishlist_Model_Item */
            try {
                $item->unsProduct();

                // Set qty
                if (isset($qtys[$item->getId()])) {
                    $qty = $this->_processLocalizedQty($qtys[$item->getId()]);
                    if ($qty) {
                        $item->setQty($qty);
                        $origGroupedRequest[$item->getId()] = $item->getBuyRequest()->getSuperGroup();
                        $this->_addQtyToAssociatedProducts($item, $qty);
                    }
                }

                // Add to cart
                if ($item->addToCart($cart, false)) {
                    $addedItems[] = $item->getProduct();
                }
            } catch (Mage_Core_Exception $e) {
                if ($e->getCode() == Mage_Wishlist_Model_Item::EXCEPTION_CODE_NOT_SALABLE) {
                    $notSalable[] = $item;
                } else if ($e->getCode() == Mage_Wishlist_Model_Item::EXCEPTION_CODE_HAS_REQUIRED_OPTIONS) {
                    $hasOptions[] = $item;
                } else {
                    $messages[] = $this->__('%s for "%s".', trim($e->getMessage(), '.'), $item->getProduct()->getName());
                }
            } catch (Exception $e) {
                Mage::logException($e);
                $messages[] = Mage::helper('wishlist')->__('Cannot add the item to shopping cart.');
            }
        }

        if ($isOwner) {
            $indexUrl = Mage::helper('wishlist')->getListUrl();
        } else {
            $indexUrl = Mage::getUrl('wishlist/shared', array('code' => $wishlist->getSharingCode()));
        }

        if (Mage::helper('checkout/cart')->getShouldRedirectToCart()) {
            $redirectUrl = Mage::helper('checkout/cart')->getCartUrl();
        } else if ($this->_getRefererUrl()) {
            $redirectUrl = $this->_getRefererUrl();
        } else {
            $redirectUrl = $indexUrl;
        }

        if ($notSalable) {
            $products = array();
            foreach ($notSalable as $item) {
                $products[] = '"' . $item->getProduct()->getName() . '"';
            }
            $messages[] = Mage::helper('wishlist')->__('Unable to add the following product(s) to shopping cart: %s.', join(', ', $products));
        }

        if ($hasOptions) {
            $products = array();
            foreach ($hasOptions as $item) {
                $products[] = '"' . $item->getProduct()->getName() . '"';
            }
            $messages[] = Mage::helper('wishlist')->__('Product(s) %s have required options. Each of them can be added to cart separately only.', join(', ', $products));
        }

        if ($messages) {
            $isMessageSole = (count($messages) == 1);
            if ($isMessageSole && count($hasOptions) == 1) {
                $item = $hasOptions[0];
                if ($isOwner) {
                    $item->delete();
                }
                $redirectUrl = $item->getProductUrl();
            } else {
                $wishlistSession = Mage::getSingleton('wishlist/session');
                foreach ($messages as $message) {
                    $wishlistSession->addError($message);
                }
                $redirectUrl = $indexUrl;
            }
        }

        if ($addedItems) {
            // save wishlist model for setting date of last update
            try {
                foreach ($collection->getItems() as $item) {
                    if (array_key_exists($item->getId(), $origGroupedRequest)) {
                        $oldBuyRequest = $item->getBuyRequest()->getData();
                        if ($origGroupedRequest[$item->getId()]) {
                            $oldBuyRequest['super_group'] = $origGroupedRequest[$item->getId()];
                        } else if (isset($oldBuyRequest['super_group'])) {
                            unset($oldBuyRequest['super_group']);
                        }
                        $sBuyRequest = serialize($oldBuyRequest);
                        $option = $item->getOptionByCode('info_buyRequest');
                        if ($option) {
                            $option->setValue($sBuyRequest);
                        } else {
                            $item->addOption(array(
                                'code'  => 'info_buyRequest',
                                'value' => $sBuyRequest
                            ));
                        }
                        
                        //$item->save();
                    }
                }
                
                //$wishlist->save();
            } catch (Exception $e) {
                Mage::getSingleton('wishlist/session')->addError($this->__('Cannot update wishlist'));
                $redirectUrl = $indexUrl;
            }

            $products = array();
            foreach ($addedItems as $product) {
                $products[] = '"' . $product->getName() . '"';
            }

            Mage::getSingleton('checkout/session')->addSuccess(
                Mage::helper('wishlist')->__('%d product(s) have been added to shopping cart: %s.', count($addedItems), join(', ', $products))
            );

            // save cart and collect totals
            $cart->save()->getQuote()->collectTotals();
        }

        Mage::helper('wishlist')->calculate();

        $this->_redirectUrl($redirectUrl);
    }

    /**
     * Add all items from wishlist to shopping cart
     */
    public function allcartAction()
    {
        if ($this->_getDataHelper()->isEnabled()) {
            $model  = $this->_getWishlistModel();
            $helper = $this->_getDataHelper();

            $wishlist   = Mage::getModel('wishlist/wishlist')->loadByCustomer(Mage::getSingleton('customer/session')->getCustomer(), true);
            $wishlistId = (int) $this->getRequest()->getParam('wishlist_id');
            $data       = $model->getWishlistById($wishlistId);
            $isOwner    = ($data['multiwishlist_editable'] && $helper->deleteFromWishlist()) ? true : false;

            $messages   = array();
            $addedItems = array();
            $notSalable = array();
            $hasOptions = array();

            $cart       = Mage::getSingleton('checkout/cart');
            $collection = $wishlist->getItemCollection()
                ->setVisibilityFilter();

            $wishlistItems = Mage::getSingleton('core/resource')->getTableName('snowflake_multiplewishlist/wishlist_items');
            $collection->getSelect()->join($wishlistItems, "wishlist_item_id = $wishlistItems.item_id");
            $collection->getSelect()->where("multiwishlist_id = $wishlistId");

            $qtys = $this->getRequest()->getParam('qty');

            $origGroupedRequest = array();
            foreach ($collection->getItems() as $item) {
                /** @var Mage_Wishlist_Model_Item */
                try {
                    $item->unsProduct();

                    // Set qty
                    if (isset($qtys[$item->getId()])) {
                        $qty = $this->_processLocalizedQty($qtys[$item->getId()]);
                        if ($qty) {
                            $item->setQty($qty);
                            $origGroupedRequest[$item->getId()] = $item->getBuyRequest()->getSuperGroup();
                            $this->_addQtyToAssociatedProducts($item, $qty);
                        }
                    }

                    // Add to cart
                    if ($item->addToCart($cart, $isOwner)) {
                        $addedItems[] = $item->getProduct();
                    }
                } catch (Mage_Core_Exception $e) {
                    if ($e->getCode() == Mage_Wishlist_Model_Item::EXCEPTION_CODE_NOT_SALABLE) {
                        $notSalable[] = $item;
                    } else if ($e->getCode() == Mage_Wishlist_Model_Item::EXCEPTION_CODE_HAS_REQUIRED_OPTIONS) {
                        $hasOptions[] = $item;
                    } else {
                        $messages[] = $this->__('%s for "%s".', trim($e->getMessage(), '.'), $item->getProduct()->getName());
                    }
                } catch (Exception $e) {
                    Mage::logException($e);
                    $messages[] = Mage::helper('wishlist')->__('Cannot add the item to shopping cart.');
                }
            }

            if ($isOwner) {
                $indexUrl = Mage::helper('wishlist')->getListUrl();
            } else {
                $indexUrl = Mage::getUrl('wishlist/shared', array('code' => $wishlist->getSharingCode()));
            }

            if (Mage::helper('checkout/cart')->getShouldRedirectToCart()) {
                $redirectUrl = Mage::helper('checkout/cart')->getCartUrl();
            } else if ($this->_getRefererUrl()) {
                $redirectUrl = $this->_getRefererUrl();
            } else {
                $redirectUrl = $indexUrl;
            }

            if ($notSalable) {
                $products = array();
                foreach ($notSalable as $item) {
                    $products[] = '"' . $item->getProduct()->getName() . '"';
                }
                $messages[] = Mage::helper('wishlist')->__('Unable to add the following product(s) to shopping cart: %s.', join(', ', $products));
            }

            if ($hasOptions) {
                $products = array();
                foreach ($hasOptions as $item) {
                    $products[] = '"' . $item->getProduct()->getName() . '"';
                }
                $messages[] = Mage::helper('wishlist')->__('Product(s) %s have required options. Each of them can be added to cart separately only.', join(', ', $products));
            }

            if ($messages) {
                $isMessageSole = (count($messages) == 1);
                if ($isMessageSole && count($hasOptions) == 1) {
                    $item = $hasOptions[0];
                    if ($isOwner) {
                        $item->delete();
                    }
                    $redirectUrl = $item->getProductUrl();
                } else {
                    $wishlistSession = Mage::getSingleton('wishlist/session');
                    foreach ($messages as $message) {
                        $wishlistSession->addError($message);
                    }
                    $redirectUrl = $indexUrl;
                }
            }

            if ($addedItems) {
                // save wishlist model for setting date of last update
                try {
                    foreach ($collection->getItems() as $item) {
                        if (array_key_exists($item->getId(), $origGroupedRequest)) {
                            $oldBuyRequest = $item->getBuyRequest()->getData();
                            if ($origGroupedRequest[$item->getId()]) {
                                $oldBuyRequest['super_group'] = $origGroupedRequest[$item->getId()];
                            } else if (isset($oldBuyRequest['super_group'])) {
                                unset($oldBuyRequest['super_group']);
                            }
                            $sBuyRequest = serialize($oldBuyRequest);
                            $option = $item->getOptionByCode('info_buyRequest');
                            if ($option) {
                                $option->setValue($sBuyRequest);
                            } else {
                                $item->addOption(array(
                                    'code'  => 'info_buyRequest',
                                    'value' => $sBuyRequest
                                ));
                            }
                            $item->save();
                        }
                    }
                    $wishlist->save();
                } catch (Exception $e) {
                    Mage::getSingleton('wishlist/session')->addError($this->__('Cannot update wishlist'));
                    $redirectUrl = $indexUrl;
                }

                $products = array();
                foreach ($addedItems as $product) {
                    $products[] = '"' . $product->getName() . '"';
                }

                Mage::getSingleton('checkout/session')->addSuccess(
                    Mage::helper('wishlist')->__('%d product(s) have been added to shopping cart: %s.', count($addedItems), join(', ', $products))
                );

                // save cart and collect totals
                $cart->save()->getQuote()->collectTotals();
            }

            Mage::helper('wishlist')->calculate();

            $this->_redirectUrl($redirectUrl);
        } else {
            parent::allcartAction();
        }
    }

    /**
     * Update wishlist item comments
     */
    public function updateAction()
    {
        if ($this->_getDataHelper()->isEnabled()) {
            $post = $this->getRequest()->getPost();
            if ($post && isset($post['description']) && is_array($post['description'])) {
                foreach ($post['description'] as $itemId => $description) {
                    $item = Mage::getModel('wishlist/item')->load($itemId);

                    // Extract new values
                    $description = (string)$description;

                    if ($description == Mage::helper('wishlist')->defaultCommentString()) {
                        $description = '';
                    } elseif (!strlen($description)) {
                        $description = $item->getDescription();
                    }

                    $qty = null;
                    if (isset($post['qty'][$itemId])) {
                        $qty = $post['qty'][$itemId];
                    }
                    if (is_null($qty)) {
                        $qty = $item->getQty();
                        if (!$qty) {
                            $qty = 1;
                        }
                    } else if (0 == $qty) {
                        try {
                            $item->delete();
                        } catch (Exception $e) {
                            Mage::logException($e);
                            Mage::getSingleton('customer/session')->addError(
                                $this->__('Can\'t delete item from wishlist')
                            );
                        }
                    }

                    // Check that we need to save
                    if (($item->getDescription() == $description) && ($item->getQty() == $qty)) {
                        continue;
                    }

                    try {
                        $item->setDescription($description)
                            ->setQty($qty)
                            ->save();
                    } catch (Exception $e) {
                        Mage::getSingleton('customer/session')->addError(
                            $this->__('Can\'t save description %s', Mage::helper('core')->escapeHtml($description))
                        );
                    }
                }
            }

            $this->_redirect('*', array('tabIdx' => $this->getRequest()->getParam('multiplewishlist_id')));
        } else {
            parent::updateAction();
        }
    }

    /**
     * Prepare wishlist for share
     */
    public function shareAction()
    {
        if ($this->_getDataHelper()->isEnabled()) {
            Mage::getSingleton('customer/session')->addData(array('share_wishlist_id' => (int) $this->getRequest()->getParam('id')));
            $this->loadLayout();
            $this->_initLayoutMessages('customer/session');
            $this->_initLayoutMessages('wishlist/session');
            $this->renderLayout();
        } else {
            parent::shareAction();
        }
    }

    /**
     * Share wishlist
     *
     * @return Mage_Core_Controller_Varien_Action|void
     */
    public function sendAction()
    {
        if ($this->_getDataHelper()->isEnabled()) {
            if (!$this->_validateFormKey()) {
                return $this->_redirect('*/*/');
            }

            $emails  = explode(',', $this->getRequest()->getPost('emails'));
            $message = nl2br(htmlspecialchars((string) $this->getRequest()->getPost('message')));
            $error   = false;
            if (empty($emails)) {
                $error = $this->__('Email address can\'t be empty.');
            } else {
                foreach ($emails as $index => $email) {
                    $email = trim($email);
                    if (!Zend_Validate::is($email, 'EmailAddress')) {
                        $error = $this->__('Please input a valid email address.');
                        break;
                    }
                    $emails[$index] = $email;
                }
            }
            if ($error) {
                Mage::getSingleton('wishlist/session')->addError($error);
                Mage::getSingleton('wishlist/session')->setSharingForm($this->getRequest()->getPost());
                $this->_redirect('*/*/share');
                return;
            }

            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);

            try {
                $customer = Mage::getSingleton('customer/session')->getCustomer();
                $wishlist = $this->_getWishlist();

                /*if share rss added rss feed to email template*/
                if ($this->getRequest()->getParam('rss_url')) {
                    $rss_url  = $this->getLayout()->createBlock('wishlist/share_email_rss')->toHtml();
                    $message .= $rss_url;
                }
                $wishlistBlock = $this->getLayout()->createBlock('snowflake_multiplewishlist/share_items')->toHtml();

                $emails = array_unique($emails);
                /* @var $emailModel Mage_Core_Model_Email_Template */
                $emailModel = Mage::getModel('core/email_template');

                $shareWishlistId = urlencode(base64_encode(Mage::getSingleton('customer/session')->getData('share_wishlist_id')));
                foreach($emails as $email) {
                    $emailModel->sendTransactional(
                        Mage::getStoreConfig('wishlist/email/email_template'),
                        Mage::getStoreConfig('wishlist/email/email_identity'),
                        $email,
                        null,
                        array(
                            'customer'      => $customer,
                            'salable'       => $wishlist->isSalable() ? 'yes' : '',
                            'items'         => $wishlistBlock,
                            'addAllLink'    => Mage::getUrl('*/shared/allcart', array(
                                'code' => $wishlist->getSharingCode(),
                                'mw'   => $shareWishlistId,
                            )),
                            'viewOnSiteLink'=> Mage::getUrl('*/shared/index', array(
                                'code' => $wishlist->getSharingCode(),
                                'mw'   => $shareWishlistId,
                            )),
                            'message'       => $message
                        ));
                }

                $wishlist->setShared(1);
                $wishlist->save();

                $translate->setTranslateInline(true);

                Mage::dispatchEvent('wishlist_share', array('wishlist'=>$wishlist));
                Mage::getSingleton('customer/session')->addSuccess(
                    $this->__('Your Wishlist has been shared.')
                );
                $this->_redirect('*/*');
            } catch (Exception $e) {
                $translate->setTranslateInline(true);

                Mage::getSingleton('wishlist/session')->addError($e->getMessage());
                Mage::getSingleton('wishlist/session')->setSharingForm($this->getRequest()->getPost());
                $this->_redirect('*/*/share');
            }
        } else {
            parent::sendAction();
        }
    }

    /**
     * Cancel adding product to multiple wishlist
     *
     * Ajax request
     */
    public function cancelAddingAction()
    {
        /** @var $multiWishlist Snowflake_MultipleWishlist_Model_Wishlist */
        $multiWishlist  = $this->_getWishlistModel();

        try {
            $wishlist = Mage::getModel('wishlist/wishlist')->loadByCustomer(Mage::getSingleton('customer/session')->getCustomer(), true);

            $newItems = $multiWishlist->checkingForNewItems($wishlist->getId());
            foreach ($newItems as $itemId) {
                $item = Mage::getModel('wishlist/item')->load($itemId['wishlist_item_id']);
                if ($item->getId()) {
                    $item->delete();
                }
            }

            $result = array('success' => true);
        } catch (Exception $e) {
            Mage::logException($e);
            $result = array('error' => 'Error');
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    /**
     * Rename wishlist
     *
     * Ajax request
     */
    public function renameWishlistAction()
    {
        $result = array();

        try {
            $wishlist    = $this->_getWishlistModel();
            $wishlistId  = $this->getRequest()->getParam('id');
            $currentName = $wishlist->loadById($wishlistId);
            if ($currentName) {
                $newName = $this->getRequest()->getParam('name');
                if ($newName && $currentName['multiwishlist_editable'] && $currentName['multiwishlist_name'] != trim($newName)) {
                    $wishlist->updateName($wishlistId, trim($newName));
                    $result['ok'] = true;
                }
            } else {
                $result['error'] = $this->__('Wishlist not found');
            }
        } catch(Exception $e) {
            $result['error'] = $this->__('Wishlist has not been renamed');
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    /**
     * @return Snowflake_MultipleWishlist_Helper_Data
     */
    protected function _getDataHelper()
    {
        return Mage::helper('snowflake_multiplewishlist');
    }

    /**
     * Get multiple wishlist model
     *
     * @return Snowflake_MultipleWishlist_Model_Wishlist
     */
    protected function _getWishlistModel()
    {
        return Mage::getModel('snowflake_multiplewishlist/wishlist');
    }

    /**
     * Get default wishlist object
     *
     * @return Mage_Wishlist_Model_Wishlist|bool
     */
    protected function _getWishlist()
    {
        try {
            $wishlist = Mage::getModel('wishlist/wishlist')->loadByCustomer(Mage::getSingleton('customer/session')->getCustomer(), true);
            Mage::register('wishlist', $wishlist, true);
        } catch (Mage_Core_Exception $e) {
            Mage::getSingleton('wishlist/session')->addError($e->getMessage());
        } catch (Exception $e) {
            Mage::getSingleton('wishlist/session')->addException($e,
                Mage::helper('wishlist')->__('Cannot create wishlist.')
            );
            return false;
        }
        return $wishlist;
    }

    /**
     * Add qty to associated products
     *
     * @param Mage_Wishlist_Model_Item $item
     * @param int $qty
     * @return Mage_Wishlist_Model_Item
     */
    protected function _addQtyToAssociatedProducts($item, $qty)
    {
        if ($item->getProduct()->getTypeId() == 'grouped') {
            $superGroup = array();
            if ($item->getBuyRequest()->getSuperGroup()) {
                $superGroup = $item->getBuyRequest()->getSuperGroup();
                if (is_array($superGroup)) {
                    foreach ($superGroup as $groupId => $groupQty) {
                        $superGroup[$groupId] = $groupQty * $qty;
                    }
                }
            } else {
                $associatedProducts = $item->getProduct()->getTypeInstance(true)->getAssociatedProducts($item->getProduct());
                /** @var $associatedProduct Mage_Catalog_Model_Product */
                foreach ($associatedProducts as $associatedProduct) {
                    if ($associatedProduct->isSalable()) {
                        $superGroup[$associatedProduct->getId()] = $qty;
                    }
                }
            }
            if (!empty($superGroup)) {
                $superGroupBuyRequest = new Varien_Object(array('super_group' => $superGroup));
                $item->mergeBuyRequest($superGroupBuyRequest);
            }
        }
        return $item;
    }

    /**
     * Set wishlist name if not exist
     *
     * @param string $wishlistName
     * @return bool||int multiple wishlist id
     */
    protected function _setWishlistName($wishlistName)
    {
        $wishlist = $this->_getWishlistModel();
        $data     = $wishlist->getWishlistByName($wishlistName);
        if (isset($data['multiwishlist_name'])) {
            Mage::getSingleton('core/session')->addError($this->__('Wishlist with such a name already exists. Please choose a different name.'));
            return false;
        } else {
            return $wishlist->setName($wishlistName);
        }
    }

    /**
     * Save item options
     *
     * @param Mage_Wishlist_Model_Item $item
     * @return void
     */
    protected function _saveItemOptions($item)
    {
        $options = $item->getOptions();
        if (is_array($options)) {
            /** @var $option Mage_Wishlist_Model_Item_Option */
            foreach ($options as $option) {
                $option->setId(null);
                $option->setItem($item);
                $option->save();
            }
        }
    }

    /**
     * Get item configure url in wishlist
     *
     * @param Mage_Catalog_Model_Product|Mage_Wishlist_Model_Item $product
     * @return string
     */
    public function getItemConfigureUrl($product)
    {
        if ($product instanceof Mage_Catalog_Model_Product) {
            $id = $product->getWishlistItemId();
        } else {
            $id = $product->getId();
        }
        $params = array('id' => $id);

        return $this->getUrl('wishlist/index/configure/', $params);
    }

    public function itemCountAction()
    {
        Mage::log('Snowflake_MultipleWishlist_IndexController itemCountAction: ');
    }

    public function updateItemOptionsAction()
    {
        Mage::log('Snowflake_MultipleWishlist_IndexController updateItemOptionsAction: ');
    }

    public function _updateItemQty($item, $qty)
    {
        Mage::log('Snowflake_MultipleWishlist_IndexController _updateItemQty: ');
    }

    public function fromcartAction()
    {
        Mage::log('Snowflake_MultipleWishlist_IndexController fromcartAction: ');
    }
}
  