<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 27.02.2015
 * Time: 10:42
 */
class Snowflake_MultipleWishlist_Admin_WishlistController extends Snowflake_MultipleWishlist_Controller_Admin_Controller
{
    /**
     * Get multiple wishlist model
     *
     * @return Snowflake_MultipleWishlist_Model_Wishlist
     */
    protected function _getWishlistModel()
    {
        return Mage::getModel('snowflake_multiplewishlist/wishlist');
    }

    /**
     * Add wishlist action
     *
     * Ajax request
     */
    public function addWishlistAction()
    {
        $wishlist   = $this->getRequest()->getParam('wishlist_name');
        $editable   = ($this->getRequest()->getParam('wishlist_editable') == 'true') ? 1 : 0;
        $customerId = (int) $this->getRequest()->getParam('customer_id');

        $wishlistModel = $this->_getWishlistModel();

        $wishlistId = $wishlistModel->setName($wishlist, $editable, $customerId);
        $result = $wishlistModel->getWishlistById($wishlistId);

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    /**
     * Delete wishlist action
     *
     * Ajax request
     */
    public function deleteWishlistAction()
    {
        $id = (int) $this->getRequest()->getParam('wishlist_id');

        $this->_getWishlistModel()->deleteWishlist($id);
        exit;
    }

    /**
     * Rename wishlist action
     *
     * Ajax request
     */
    public function renameWishlistAction()
    {
        $result = array();

        try {
            $wishlistId = (int) $this->getRequest()->getParam('wishlist_id');

            $wishlist = $this->_getWishlistModel();
            $currentName = $wishlist->loadById($wishlistId);
            if ($currentName) {
                $newName = trim($this->getRequest()->getParam('name'));
                if ($newName && $currentName['multiwishlist_name'] != $newName) {
                    $wishlist->updateName($wishlistId, $newName);
                    $result['ok'] = true;
                }
            } else {
                $result['error'] = $this->__('Wishlist not found');
            }
        } catch(Exception $e) {
            $result['error'] = $this->__('Wishlist has not been renamed');
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    /**
     * Add item to wishlist action
     *
     * Ajax request
     */
    public function addItemAction()
    {
        $productId  = (int) $this->getRequest()->getParam('product_id');
        $wishlistId = (int) $this->getRequest()->getParam('wishlist_id');
        $customerId = (int) $this->getRequest()->getParam('customer_id');

        $customer = Mage::getModel('customer/customer')->load($customerId);

        /** @var $product  Mage_Catalog_Model_Product */
        $product  = Mage::getModel('catalog/product')->load($productId);
        $storeIds = $product->getStoreIds();

        $params   = array(
            'product'  => $productId,
            'qty'      => 1,
            'store_id' => isset($storeIds[0]) ? $storeIds[0] : 0,
        );

        $product->setWishlistStoreId($params['store_id']);
        $buyRequest = new Varien_Object($params);

        $itemId = (int) $this->_getWishlistModel()->isItemInWishlist($customerId, $productId);
        if ($itemId) {
            $newItem = Mage::getModel('wishlist/item')->load($itemId);
            $newItem->setId(null);
            $newItem->setQty(1);
            $newItem->save();
        } else {
            /** @var $wishlistModel Mage_Wishlist_Model_Wishlist */
            $wishlistModel = Mage::getModel('wishlist/wishlist')->loadByCustomer($customer, true);

            $newItem = $wishlistModel->addNewItem($product, $buyRequest);
            $wishlistModel->save();
        }

        $this->_getWishlistModel()->insertItemsInList($newItem->getId(), $wishlistId);

        $result = array();
        $result[] = array(
            'id'   => $newItem->getId(),
            'name' => $product->getName(),
        );

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    /**
     * Delete item from wishlist action
     *
     * Ajax request
     */
    public function deleteItemAction()
    {
        $itemId = (int) $this->getRequest()->getParam('item_id');

        $this->_getWishlistModel()->removeItem($itemId);
        exit;
    }

    /**
     * Get all wishlist action
     *
     * Ajax request
     */
    public function getWishlistsAction()
    {
        $customerId = (int) $this->getRequest()->getParam('customer_id');
        $result = $this->_getWishlistModel()->getWishlistNames($customerId);

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    /**
     * Get products json
     *
     * Ajax request
     */
    public function getProductsAction()
    {
        $id = (int) $this->getRequest()->getParam('wishlist_id');
        try{
            $products = $this->_getWishlistModel()->getWishlistItems($id);
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($products));
    }
}
  