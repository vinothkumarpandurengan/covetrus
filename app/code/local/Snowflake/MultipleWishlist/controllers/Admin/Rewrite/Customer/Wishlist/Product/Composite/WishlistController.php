<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 27.02.2015
 * Time: 10:41
 */
require_once Mage::getModuleDir('controllers', 'Mage_Adminhtml') . DS . 'Customer' . DS . 'Wishlist' . DS . 'Product' . DS . 'Composite' . DS . 'WishlistController.php';

class Snowflake_MultipleWishlist_Admin_Rewrite_Customer_Wishlist_Product_Composite_WishlistController
    extends Mage_Adminhtml_Customer_Wishlist_Product_Composite_WishlistController
{
    public function updateAction()
    {
        Mage::log('Snowflake_MultipleWishlist_Admin_Rewrite_Customer_Wishlist_Product_Composite_WishlistController updateAction: ');
    }

    protected function updateItemId($itemId, $wishlistId)
    {
        Mage::log('Snowflake_MultipleWishlist_Admin_Rewrite_Customer_Wishlist_Product_Composite_WishlistController updateItemId: ');
    }

    public function getBeforeMycollectionId($itemId)
    {
        Mage::log('Snowflake_MultipleWishlist_Admin_Rewrite_Customer_Wishlist_Product_Composite_WishlistController getBeforeMycollectionId: ');
    }
}