<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 23.02.2015
 * Time: 16:56
 */
require_once Mage::getModuleDir('controllers', 'Mage_Wishlist').DS.'IndexController.php';

class Snowflake_MultipleWishlist_AjaxController extends Mage_Wishlist_IndexController
{
    /**
     * Set skip authentication to true
     *
     * @return Mage_Core_Controller_Front_Action|void
     */
    public function preDispatch()
    {
        $this->_skipAuthentication = true;
        parent::preDispatch();
    }

    /**
     * Ajax action for add product to wishlist and stay on the current page
     *
     * Ajax request
     */
    public function addProductAction()
    {
        $request = $this->getRequest();
        $result  = array(
            'success'  => false,
            'error'    => null,
            'redirect' => null,
        );

        /** @var $session Mage_Customer_Model_Session */
        $session = Mage::getSingleton('customer/session');
        if ($session->isLoggedIn()) {
            $errors = array();

            $wishlist = $this->_getWishlist();
            if (!$wishlist) {
                $errors[] = $this->__('Wishlist is not loaded');
            }

            if (empty($errors)) {
                $productId = (int) $request->getParam('product');
                if (!$productId) {
                    $errors[] = $this->__('Please specify product');
                }

                $product = Mage::getModel('catalog/product')->load($productId);
                if (!$product->getId() || !$product->isVisibleInCatalog()) {
                    $errors[] = $this->__('Cannot specify product.');
                }

                if (empty($errors)) {
                    try {
                        $multiWishlist   = $this->_getWishlistModel();
                        $multiWishlistId = $this->getRequest()->getParam('multiwishlist_id');
                        if ($request->getParam('is_new')) {
                            $multiWishlistId = $multiWishlist->createWishlist($multiWishlistId);
                        }

                        $params = $request->getParams();
                        if ($session->getBeforeWishlistRequest()) {
                            $params = $session->getBeforeWishlistRequest();
                            $session->unsBeforeWishlistRequest();
                        }
                        $buyRequest = new Varien_Object($params);

                        $result = $wishlist->addNewItem($product, $buyRequest);
                        if (is_string($result)) {
                            Mage::throwException($result);
                        }
                        $wishlist->save();

                        Mage::dispatchEvent('wishlist_add_product', array(
                            'wishlist'  => $wishlist,
                            'product'   => $product,
                            'item'      => $result
                        ));

                        $multiWishlist->insertItemsInList($result->getId(), $multiWishlistId);
                        Mage::helper('wishlist')->calculate();

                        $message = $this->__('%1$s has been added to your wishlist %2$s.', $product->getName(), $multiWishlist->getWishlistNameById($multiWishlistId));
                        $result['message'] = $message;
                        $result['success'] = true;
                    } catch (Mage_Core_Exception $e) {
                        $errors[] = $this->__('An error occurred while adding item to wishlist: %s', $e->getMessage());
                    } catch (Exception $e) {
                        $errors[] = $this->__('An error occurred while adding item to wishlist.');
                    }
                }
            }
            if (!empty($errors)) {
                $result['error'] = implode(' ', $errors);
            }
        } else {
            $result['redirect'] = Mage::getUrl('customer/account/login');
            if ($request->isAjax()) {
                $session->setBeforeAjaxAddReferer($this->_getRefererUrl());
            }

            $session->setBeforeAuthUrl(Mage::getUrl('*/*/*', array('_current' => true)));
            $session->setAfterAuthUrl(Mage::getUrl('*/*/*', array('_current' => true)));

            if (!$session->getBeforeWishlistUrl()) {
                $session->setBeforeWishlistUrl($this->_getRefererUrl());
            }

            $session->setBeforeWishlistRequest($this->getRequest()->getParams());
        }
        if (!$request->isAjax()) {
            if (isset($result['message'])) {
                Mage::getSingleton('core/session')->addSuccess($result['message']);
            }

            if ($result['error']) {
                Mage::getSingleton('core/session')->addError($result['error']);
            }

            $ajaxReferer = $session->getAjaxAddProductWishlistReferer(true);
            if ($ajaxReferer) {
                $this->_redirectUrl($ajaxReferer);
            } else {
                $this->_redirect('wishlist');
            }
        } else {
            $session->setAjaxAddProductWishlistReferer($this->_getRefererUrl());
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }

    /**
     * Get multiple wishlist model
     *
     * @return Snowflake_MultipleWishlist_Model_Wishlist
     */
    protected function _getWishlistModel()
    {
        return Mage::getModel('snowflake_multiplewishlist/wishlist');
    }
}
  