<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 13.03.2015
 * Time: 17:35
 */

/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('snowflake_fileattachment/attachment'), 'file_promotion', array(
    'type' => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
    'nullable'  => false,
    'default'   => 0,
    'comment' => 'Use for Promotion'
));

$installer->endSetup();
  