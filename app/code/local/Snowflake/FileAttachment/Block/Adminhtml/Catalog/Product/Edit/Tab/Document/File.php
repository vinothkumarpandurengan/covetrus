<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 13.03.2015
 * Time: 13:30
 */
class Snowflake_FileAttachment_Block_Adminhtml_Catalog_Product_Edit_Tab_Document_File
    extends Mage_Adminhtml_Block_Widget
{
    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('snowflake/fileattachment/product/edit/document/files.phtml');
    }

    /**
     * Prepare layout
     */
    protected function _prepareLayout()
    {
        $this->setChild(
            'upload_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->addData(array(
                    'id'      => '',
                    'label'   => Mage::helper('adminhtml')->__('Upload Files'),
                    'type'    => 'button',
                    'onclick' => 'SnowflakeDocument.massUploadByType(\'files\')'
                ))
        );
    }

    /**
     * Get model of the product that is being edited
     *
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct()
    {
        return Mage::registry('current_product');
    }

    /**
     * Retrieve Add Button HTML
     *
     * @return string
     */
    public function getAddButtonHtml()
    {
        $addButton = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setData(array(
                'label' => Mage::helper('snowflake_fileattachment')->__('Add New Row'),
                'id' => 'add_file_item',
                'class' => 'add',
            ));
        return $addButton->toHtml();
    }

    /**
     * Retrieve file array
     *
     * @return array
     */
    public function getFileData()
    {
        $fileArr = array();
        $model = Mage::getModel('snowflake_fileattachment/attachment')->getProductFilesCollection($this->getProduct()->getId());
        foreach ($model->getData() as $item => $value) {
            $tmpFileItem = array(
                'file_id' => $value['file_id'],
                'title' => $value['file_title'],
                'promotion' =>  $value['file_promotion'],
                'file_url' => $value['file_url'],
                'file_type' => $value['file_type'],
                'sort_order' => $value['sort_order'],
            );
            $path = Mage::getBaseDir('media') . DS . 'product_files';
            $file = Mage::helper('downloadable/file')->getFilePath(
                $path, $value['file_name']
            );
            if ($value['file_name'] && !is_file($file)) {
                Mage::helper('core/file_storage_database')->saveFileToFilesystem($file);
            }
            if ($value['file_name'] && is_file($file)) {
                $tmpFileItem['file_save'] = array(
                    array(
                        'file' => $value['file_name'],
                        'name' => Mage::helper('downloadable/file')->getFileFromPathFile($value['file_name']),
                        'size' => filesize($file),
                        'status' => 'old'
                    ));
            }
            if ($this->getProduct() && $value['file_title']) {
                $tmpFileItem['store_title'] = $value['file_title'];
            }
            $fileArr[] = new Varien_Object($tmpFileItem);
        }

        return $fileArr;
    }

    /**
     * Return yes / no attribute
     *
     * @return array
     */
    public function getPromotion()
    {
        $opt = Mage::getModel('eav/entity_attribute_source_boolean');
        return $opt->getAllOptions();
    }

    /**
     * Retrieve Upload button HTML
     *
     * @return string
     */
    public function getUploadButtonHtml()
    {
        return $this->getChild('upload_button')->toHtml();
    }

    /**
     * Retrive config json
     *
     * @return string
     */
    public function getConfigJson()
    {
        $this->getConfig()->setUrl(Mage::getModel('adminhtml/url')->addSessionParam()->getUrl('*/downloadable_file/uploadFile', array('type' => 'files', '_secure' => true)));
        $this->getConfig()->setParams(array('form_key' => $this->getFormKey()));
        $this->getConfig()->setFileField('files');
        $this->getConfig()->setFilters(array(
            'all'    => array(
                'label' => Mage::helper('adminhtml')->__('All Files'),
                'files' => array('*.*')
            )
        ));
        $this->getConfig()->setReplaceBrowseWithRemove(true);
        $this->getConfig()->setWidth('32');
        $this->getConfig()->setHideUploadButton(true);
        return Mage::helper('core')->jsonEncode($this->getConfig()->getData());
    }

    /**
     * Retrive config object
     *
     * @return Varien_Config
     */
    public function getConfig()
    {
        if(is_null($this->_config)) {
            $this->_config = new Varien_Object();
        }

        return $this->_config;
    }
}
  