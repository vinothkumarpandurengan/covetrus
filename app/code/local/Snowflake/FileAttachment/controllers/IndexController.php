<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 17.03.2015
 * Time: 16:21
 */
class Snowflake_FileAttachment_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * Function to handle request to download file for Files module
     */
    public function downloadAction()
    {
        $fileId = $this->getRequest()->getParam('id', 0);
        $file   = Mage::getModel('snowflake_fileattachment/attachment')->load($fileId, 'file_id');

        if ($file->getId())
        {
            $resource = Mage::getBaseDir('media') . DS . 'product_files' . $file->getFileName();
            try {
                $this->_processDownload($file, $resource);
                exit(0);
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError(Mage::helper('snowflake_fileattachment')->__('An error occurred while getting requested content. Please contact the store owner.'));
            }
        }
    }

    protected function _processDownload($file, $resource)
    {
        $contentDisposition = 'product/files/content_disposition';
        $fileName    = basename($file->getFileName());
        $contentType = mime_content_type($resource);

        $this->getResponse()
            ->setHttpResponseCode(200)
            ->setHeader('Pragma', 'public', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
            ->setHeader('Content-type', $contentType, true)
            ->setHeader('Content-Length', filesize($resource))
            ->setHeader('Content-Disposition', $contentDisposition . '; filename=' . $fileName);

        $this->getResponse()->clearBody();
        $this->getResponse()->sendHeaders();

        $handle = new Varien_Io_File();
        $handle->open(array('path' => Mage::getBaseDir('var')));
        if (!$handle->fileExists($resource, true)) {
            Mage::throwException(Mage::helper('snowflake_fileattachment')->__('The file does not exist.'));
        }
        $handle->streamOpen($resource, 'r');

        while ($buffer = $handle->streamRead()) {
            print $buffer;
        }
    }
}
  