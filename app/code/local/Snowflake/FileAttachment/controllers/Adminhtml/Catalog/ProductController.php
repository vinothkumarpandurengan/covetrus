<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 16.03.2015
 * Time: 12:32
 */
require_once Mage::getModuleDir('controllers', 'Mage_Adminhtml') . DS . 'Catalog' . DS . 'ProductController.php';
class Snowflake_FileAttachment_Adminhtml_Catalog_ProductController extends Mage_Adminhtml_Catalog_ProductController
{
    /**
     * Save product data action
     */
    public function saveAction()
    {
        $storeId        = $this->getRequest()->getParam('store');
        $redirectBack   = $this->getRequest()->getParam('back', false);
        $productId      = $this->getRequest()->getParam('id');
        $isEdit         = (int)($this->getRequest()->getParam('id') != null);

        $data = $this->getRequest()->getPost();
        if ($data) {
            $this->_filterStockData($data['product']['stock_data']);

            $product = $this->_initProductSave();

            try {
                $product->save();
                $productId = $product->getId();

                if (isset($data['document'])) {
                    if (count($data['document']['file']) > 0 && $product->getTypeID() != 'downloadable') {
                        $this->_saveProductFiles($data['document']['file'], $productId);
                    }
                }

                if (isset($data['copy_to_stores'])) {
                    $this->_copyAttributesBetweenStores($data['copy_to_stores'], $product);
                }

                $this->_getSession()->addSuccess($this->__('The product has been saved.'));
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage())
                    ->setProductData($data);
                $redirectBack = true;
            } catch (Exception $e) {
                Mage::logException($e);
                $this->_getSession()->addError($e->getMessage());
                $redirectBack = true;
            }
        }

        if ($redirectBack) {
            $this->_redirect('*/*/edit', array(
                'id'    => $productId,
                '_current'=>true
            ));
        } else if($this->getRequest()->getParam('popup')) {
            $this->_redirect('*/*/created', array(
                '_current'   => true,
                'id'         => $productId,
                'edit'       => $isEdit
            ));
        } else {
            $this->_redirect('*/*/', array('store'=>$storeId));
        }
    }

    /**
     * Save product files.
     *
     * @param array $files product files to save
     * @param integer $productId
     * @return $this Snowflake_FileAttachment_Adminhtml_Catalog_ProductController
     */
    protected function _saveProductFiles($files, $productId)
    {
        $model = Mage::getModel('snowflake_fileattachment/attachment');

        foreach($files as $currentFile) {
            if ($currentFile['is_delete'] == null && $currentFile['file_id'] == 0) {

                $model->setProductId($productId)
                    ->setFileTitle($currentFile['title'])
                    ->setFilePromotion($currentFile['promotion'])
                    ->setFileType($currentFile['type'])
                    ->setSortOrder($currentFile['sort_order']);

                if ($currentFile['type'] == 'file') {
                    $model
                        ->setFileName($this->_getFileName($currentFile['file']))
                        ->setFileUrl(null);
                } else {
                    $model
                        ->setFileName(null)
                        ->setFileUrl($currentFile['url']);
                }

                $model->save();
            } else if ($currentFile['is_delete'] == null && $currentFile['file_id'] != 0) {
                $model->load($currentFile['file_id'])
                    ->setProductId($productId)
                    ->setFileTitle($currentFile['title'])
                    ->setFilePromotion($currentFile['promotion'])
                    ->setFileType($currentFile['type'])
                    ->setSortOrder($currentFile['sort_order']);

                if ($currentFile['type'] == 'file') {
                    $model
                        ->setFileName($this->_getFileName($currentFile['file']))
                        ->setFileUrl(null);
                } else {
                    $model
                        ->setFileName(null)
                        ->setFileUrl($currentFile['url']);
                }

                $model->save();
            } else if($currentFile['is_delete'] == 1 && $currentFile['file_id'] != 0) {
                $model->setId($currentFile['file_id'])->delete();
            } else if($currentFile['is_delete'] == 1 && $currentFile['file_id'] == 0 && $currentFile['file'] != '[]') {
                $model->setId($currentFile['file_id'])->delete();

                $fileData = json_decode($currentFile['file']);
                $fileDir  = Mage::getBaseDir('media') . DS . 'product_files';
                $fileDir .= $fileData[0]->file;
                if (file_exists($fileDir)) {
                    unlink($fileDir);
                }
            }

            $model->unsetData();
            sleep(1);
        }

        return $this;
    }

    /**
     * Get file name from string pattern
     *
     * @return string
     */
    protected function _getFileName($string)
    {
        $string = substr($string, 1, strlen($string) - 2);
        $data   = json_decode($string, true);

        return $data['file'];
    }
}