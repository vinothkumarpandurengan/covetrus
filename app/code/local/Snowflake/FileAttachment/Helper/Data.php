<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 13.03.2015
 * Time: 12:58
 */
class Snowflake_FileAttachment_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Get product file url
     *
     * @params array $data
     * @return string
     */
    public function getProductFileUrl($data)
    {
        $result = '';

        if (!is_array($data)) {
            return $result;
        }

        if (isset($data['file_url']) && $data['file_url'] != '') {
            $parts = parse_url($data['file_url']);
            if (!isset($parts["scheme"])) {
                $result = 'http://' . $data['file_url'];
            } else {
                $result = $data['file_url'];
            }
        } else if (isset($data['file_name']) && $data['file_name'] != '') {
            $result = Mage::getUrl('files/index/download/id/'.$data['file_id']);
        }

        return $result;
    }

    /**
     * Get product file name
     *
     * @params array $data
     * @return string
     */
    public function getProductFileName($data)
    {
        $result = '';

        if (!is_array($data)) {
            return $result;
        }

        if (isset($data['file_title']) && $data['file_title'] != '') {
            $result = $data['file_title'];
        } else if (isset($data['file_url']) && $data['file_url'] != '') {
            $result = $data['file_url'];
        } else if (isset($data['file_name']) && $data['file_name'] != '') {
            $result = basename($data['file_name']);
        }

        return $result;
    }

    public function productFileIsUrl($data)
    {
        $result = false;

        if (!is_array($data)) {
            return false;
        }

        if (isset($data['file_url']) && $data['file_url'] != '') {
            $result = true;
        }

        return $result;
    }
}
  