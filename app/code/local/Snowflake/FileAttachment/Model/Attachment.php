<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 16.03.2015
 * Time: 11:20
 */
class Snowflake_FileAttachment_Model_Attachment extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        $this->_init('snowflake_fileattachment/attachment');
    }

    /**
     * Get product files collection by product id
     *
     * @params integer $productId
     * @return Snowflake_FileAttachment_Model_Resource_Attachment_Collection
     */
    public function getProductFilesCollection($productId)
    {
        $collection = Mage::getModel('snowflake_fileattachment/attachment')->getCollection()
            ->addFieldToFilter('product_id', $productId);

        return $collection;
    }

    /**
     * Get product files data for Frontend in sorted manner
     *
     * @params integer $productId
     * @return array
     */
    public function getProductFiles($productId, $promotion = 0)
    {
        $data = Mage::getModel('snowflake_fileattachment/attachment')->getCollection()
            ->addFieldToFilter('product_id', $productId)
            ->addFieldToFilter('file_promotion', $promotion)
            ->setOrder('sort_order', 'ASC')
            ->getData();

        return $data;
    }
}
  