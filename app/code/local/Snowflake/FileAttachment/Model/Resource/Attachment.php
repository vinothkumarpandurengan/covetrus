<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 16.03.2015
 * Time: 11:18
 */
class Snowflake_FileAttachment_Model_Resource_Attachment extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('snowflake_fileattachment/attachment', 'file_id');
    }
}
  