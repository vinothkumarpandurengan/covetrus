<?php

/**
 * Class Snowflake_Nettoprice_Model_Observer
 */
class Snowflake_Nettoprice_Model_Observer {

    /**
     * @param $observer
     */
    public function checkIfNettoPrice(Varien_Event_Observer $observer) {
return;
        $products = $observer->getCollection();
        foreach( $products as $product )
        {
            $originalprice = $product->getPrice();
            $customprice = $originalprice+ 10000;
            $product->setPrice($customprice);
            $product->setCustomPrice($customprice);
            $product->setOriginalCustomPrice($customprice);
            $product->setFinalPrice($customprice);
        }
       /* Zend_Debug::dump($event = $observer->getEvent());die();
        // check if are using NZD conversion
        if (!preg_match("/NZD/i", $code = Mage::app()->getStore()->getCurrentCurrencyCode())) {
            return;
        }

        $event = $observer->getEvent();
        $products = $observer->getCollection();
        foreach( $products as $product ) {
            $product->setFinalPrice($this->_getPriceLogic($product));
        }*/
    }
}