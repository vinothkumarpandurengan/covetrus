<?php
/**
 * @category   Snowflake
 * @package    Snowflake CmsRules
 * @author     Felix Elsener <felsener@snowflake.ch>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn(
    $installer->getTable('cms_page'),
    'customer_group_ids',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length'    => '64K',
        'comment'   => 'Customer Group Ids'
    )
);

$installer->endSetup();