<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 03.09.14
 * Time: 15:04
 */
class Snowflake_CmsRules_Model_Observer
{
    public function adminhtmlCmsPageEditTabContentPrepareForm(Varien_Event_Observer $observer)
    {
        /** @var $model Mage_Cms_Model_Page */
        $model = Mage::registry('cms_page');

        $form = $observer->getEvent()->getForm();

        $fieldset = $form->getElement('content_fieldset');

        // remove content
        $fieldset->removeField('content');

        /*
         * Checking if user have permissions to save information
         */
        if ($this->_isAllowedAction('save')) {
            $isElementDisabled = false;
        } else {
            $isElementDisabled = true;
        }

        // insert content again without required field
        $contentField = $fieldset->addField('content', 'editor', array(
            'name'      => 'content',
            'style'     => 'height:36em;',
            'required'  => false,
            'disabled'  => $isElementDisabled,
            'config'    => Mage::getSingleton('cms/wysiwyg_config')->getConfig()
        ));

        // Setting custom renderer for content field to remove label column
        $renderer = Mage::app()->getLayout()->createBlock('adminhtml/widget_form_renderer_fieldset_element')
            ->setTemplate('cms/page/edit/form/renderer/content.phtml');
        $contentField->setRenderer($renderer);

        $form->setValues($model->getData());
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('cms/page/' . $action);
    }
}