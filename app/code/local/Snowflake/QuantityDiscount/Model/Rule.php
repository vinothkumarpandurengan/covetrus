<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 14.10.2015
 * Time: 13:34
 */

class Snowflake_QuantityDiscount_Model_Rule
    extends Mage_Rule_Model_Abstract
{

    /**
     * Client types
     */
    const CLIENT_P = 'p';
    const CLIENT_H = 'h';
    const CLIENT_C = 'c';

    /**
     * Dicount types
     */
    const DISCOUNT_TYPE_DISCOUNT = '1';
    const DISCOUNT_TYPE_QUANTITY_BONUS  = 'b';

    /**
     * Rule type actions
     */
    const BONUS_FIXED_QTY_ACTION = 'bonus_fixed_qty';
    const BONUS_AFTER_ORDER_ACTION = 'bonus_after_order';
    const BONUS_PERCENT_ACTION   = 'bonus_percent';

    /**
     * Set resource model and Id field name
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('snowflake_quantitydiscount/rule');
        $this->setIdFieldName('rule_id');
    }

    /**
     * Reset rule combine conditions
     *
     * @param null|Mage_Rule_Model_Condition_Combine $conditions
     *
     * @return false|Mage_Rule_Model_Abstract
     */
    protected function _resetConditions($conditions = null)
    {
        if (is_null($conditions)) {
            $conditions = $this->getConditionsInstance();
        } else {
            $conditions = parent::_resetConditions($conditions);
        }

        return $conditions;
    }

    /**
     * Reset rule actions
     *
     * @param null|Mage_Rule_Model_Action_Collection $actions
     *
     * @return false|Mage_Rule_Model_Abstract
     */
    protected function _resetActions($actions = null)
    {
        if (is_null($actions)) {
            $actions = $this->getActionsInstance();
        } else {
            $actions = parent::_resetConditions($actions);
        }

        return $actions;
    }

    /**
     * We don't need rule conditions collection for this extension
     *
     * @return false
     */
    public function getConditionsInstance()
    {
        return false;
    }

    /**
     * We don't need rule actions collection for this extension
     *
     * @return false
     */
    public function getActionsInstance()
    {
        return false;
    }

    /**
     * Get catalog rule customer group Ids
     *
     * @return array
     */
    public function getDiscountGroupIds()
    {
        if (!$this->hasDiscountGroupIds()) {
            $discountGroupIds = $this->_getResource()->getDiscountGroupIds($this->getId());
            $this->setData('discount_group_ids', (array)$discountGroupIds);
        }

        return $this->_getData('discount_group_ids');
    }
}