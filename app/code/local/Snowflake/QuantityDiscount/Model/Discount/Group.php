<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 15.10.2015
 * Time: 09:36
 */

class Snowflake_QuantityDiscount_Model_Discount_Group
    extends Mage_Core_Model_Abstract
{
    const ENTITY                    = 'discount_group';

    const DISCOUNT_GROUP_CODE_MAX_LENGTH     = 32;

    protected function _construct()
    {
        $this->_init('snowflake_quantitydiscount/discount_group');
    }

    /**
     * Alias for setDiscountGroupCode
     *
     * @param string $value
     */
    public function setCode($value)
    {
        return $this->setDiscountGroupCode($value);
    }

    /**
     * Alias for getDiscountGroupCode
     *
     * @return string
     */
    public function getCode()
    {
        return $this->getDiscountGroupCode();
    }

    /**
     * Processing data save after transaction commit
     *
     * @return Snowflake_QuantityDiscount_Model_Discount_Group
     */
    public function afterCommitCallback()
    {
        parent::afterCommitCallback();
        Mage::getSingleton('index/indexer')->processEntityAction(
            $this, self::ENTITY, Mage_Index_Model_Event::TYPE_SAVE
        );
        return $this;
    }

    /**
     * Prepare data before save
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _beforeSave()
    {
        $this->_prepareData();
        return parent::_beforeSave();
    }

    /**
     * Prepare discount group data
     *
     * @return Snowflake_QuantityDiscount_Model_Discount_Group
     */
    protected function _prepareData()
    {
        $this->setCode(
            substr($this->getCode(), 0, self::DISCOUNT_GROUP_CODE_MAX_LENGTH)
        );
        return $this;
    }
}