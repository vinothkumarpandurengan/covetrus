<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 24.04.2015
 * Time: 13:43
 */

class Snowflake_QuantityDiscount_Model_Observer
{
    /*
     * After login update all out of date quote item.
     *
     * @param Varien_Event_Observer $observer
     */
    public function loadCustomerQuoteBefore($observer)
    {
        $lastQuoteId = Mage::getSingleton('checkout/session')->getQuoteId();
        if ($lastQuoteId)
        {
            $quote = Mage::getModel('sales/quote')
                ->loadByCustomer(Mage::getSingleton('customer/session')->getCustomerId());
            $quote->setQuoteId($lastQuoteId);
        } else {
            $quote = Mage::getModel('checkout/session')->getQuote();
        }

        foreach ($quote->getAllItems() as $item)
        {
            $this->_resetItem($item);
        }

        $quote->collectTotals()->save();
    }

    /*
     * Delete all free products that have been added through this module before.
     *
     * @param Varien_Event_Observer $observer
     */
    public function salesQuoteCollectTotalsBefore($observer)
    {
        $quote = $observer->getEvent()->getQuote();

        if ($quote->getIsApplied()) {
            return;
        }

        $this->_resetFreeItems($quote);
    }

    /*
     * Update quote item with free product.
     *
     * @param Varien_Event_Observer $observer
     */
    public function salesQuoteSaveBefore($observer)
    {
        $quote = $observer->getEvent()->getQuote();

        if ($quote->getIsApplied())
        {
            return;
        }

        if(Mage::getSingleton('customer/session')->isLoggedIn())
        {
            $this->_updateQuoteItem($quote);
            $quote->setIsApplied(true);
        }
    }

    /*
     * Modify quote item price with the rule discount while product is updated in the cart.
     *
     * @param Varien_Event_Observer $observer
     */
    public function salesQuoteItemSaveBefore($observer)
    {
        $this->_handleDiscount($observer->getItem());
    }

    /**
     * Adds is_free_product attribute to quote model if set to product. Relevant for reordering.
     * See also: catalogProductTypePrepareFullOptions()
     *
     * @param Varien_Event_Observer $observer
     */
    public function salesQuoteProductAddAfter($observer)
    {
        foreach ($observer->getEvent()->getItems() as $quoteItem)
        {
            $quoteItem->setIsFreeProduct($quoteItem->getProduct()->getIsFreeProduct());
        }
    }

    /*
     * Modify product price with the rule discount while added to the cart.
     *
     * @param Varien_Event_Observer $observer
     */
    public function checkoutCartProductAddAfter($observer)
    {
        $this->_handleDiscount($observer->getQuoteItem());
    }

    /*
     * Add free product to the cart and find out with rule of simple action should be use.
     *
     * @param Mage_Sales_Model_Quote $quote
     */
    protected function _updateQuoteItem(Mage_Sales_Model_Quote $quote)
    {
        if(Mage::getSingleton('customer/session')->isLoggedIn())
        {
            foreach ($quote->getAllItems() as $item)
            {
                if ($item->isDeleted()) continue;

                $rules = $this->_getRuleCollection($item);
                foreach ($rules as $rule)
                {
                    if (!Mage::app()->getLocale()->isStoreDateInInterval($item->getStore(), $rule['from_date'], $rule['to_date']))
                    {
                        continue;
                    }

                    if ($rule->getDiscountType() == Snowflake_QuantityDiscount_Model_Rule::DISCOUNT_TYPE_QUANTITY_BONUS)
                    {
                        $discountQty = 0;
                        switch ($rule->getSimpleAction()) {
                            case Snowflake_QuantityDiscount_Model_Rule::BONUS_FIXED_QTY_ACTION:
                                if ($item->getQty() >= $rule->getMinQuantity()) {
                                    $discountQty = $rule->getDiscountQty();
                                }
                                break;
                            case Snowflake_QuantityDiscount_Model_Rule::BONUS_AFTER_ORDER_ACTION:
                                if ($item->getQty() >= $rule->getMinQuantity()) {
                                    $discountQty = 1;
                                }
                                break;
                            case Snowflake_QuantityDiscount_Model_Rule::BONUS_PERCENT_ACTION:
                                if ($item->getQty() >= $rule->getMinQuantity()) {
                                    $rulePercent = min(100, $rule->getDiscountPercent());
                                    $ruleQty = floor(100 / $rulePercent);

                                    $discountQty = floor($item->getQty() / $ruleQty);
                                }
                                break;
                        }
                        $this->_handleGift($quote, $item, $rule, $discountQty);
                    }
                }
            }
        }
    }

    /*
     * Get current customer
     *
     * @return Mage_Customer_Model_Customer
     */
    protected function _getCustomer()
    {
        return Mage::getModel('customer/customer')->load(Mage::getSingleton('customer/session')->getCustomerId());
    }

    /*
     * Get quantity discount rule collection
     *
     * @param Mage_Sales_Model_Quote_Item $item
     * @return Snowflake_QuantityDiscount_Model_Resource_Rule_Collection
     */
    protected function _getRuleCollection(Mage_Sales_Model_Quote_Item $item)
    {
        $customer = $this->_getCustomer();

        $discountGroupId = $customer->getProvetDiscountGroup();
        $customerId = $customer->getCustomerId();
        $client = $customerId{0};

        $ruleCollection = Mage::getModel('snowflake_quantitydiscount/rule')->getCollection()
            ->addFieldToFilter('client', strtolower($client))
            ->addFieldToFilter('product_id', $item->getProductId())
            ->addFieldToFilter('discount_group_id', $discountGroupId)
            ->addIsActiveFilter(1);

        return $ruleCollection;
    }

    /*
     * Check if additional discounts are allowed
     *
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    protected function _stopProccessing($product)
    {
        $customerPrices = $product->getData('customer_price');

        $result = false;

        if (count($customerPrices) > 0)
        {
            $customerId = Mage::getSingleton('customer/session')->getId();

            foreach ($customerPrices as $i => $data)
            {
                if ($data['customer'] != $customerId)
                {
                    continue;
                }

                if (Mage::app()->getLocale()->isStoreDateInInterval($product->getStore(), $data['start_date'], $data['end_date'])) {
                    if ($data['finalprice']) {
                        $result = true;
                        break;
                    }
                }
            }
        }

        return $result;
    }

    /*
     * Create a free item. It has a value of CHF 0.-- in the cart, no matter what the price was
     * originally. The flag is_free_product gets saved in the buy request to read it on
     * reordering, because fieldset conversion does not work from order item to quote item.
     *
     * @param Mage_Sales_Model_Quote $quote
     * @param int $productId
     * @param int $storeId
     * @param int $qty
     * @return bool || Mage_Sales_Quote_Item
     */
    protected function _getFreeQuoteItem(Mage_Sales_Model_Quote $quote, $productId, $storeId, $qty)
    {
        if ($qty < 1)
        {
            return false;
        }

        $product = Mage::getModel('catalog/product')->load($productId);
        if ($product && $product->getId())
        {
            if (!$this->_stopProccessing($product))
            {
                $quoteItem = Mage::getModel('sales/quote_item')->setProduct($product);
                $quoteItem->setQuote($quote)
                    ->setQty($qty)
                    ->setCustomPrice(0)
                    ->setOriginalCustomPrice(0)
                    ->setIsFreeProduct(true)
                    ->setWeeTaxApplied('a:0:{}') // Set WeeTaxApplied Value by default so there are no "warnings" later on during invoice creation
                    ->setStoreId($storeId);

                $quoteItem->addOption(new Varien_Object(array(
                    'product'   => $product,
                    'code'      => 'info_buyRequest',
                    'value'     => serialize(array('qty' => $qty, 'is_free_product' => true)),
                )));

                // Add additional option to item.
                $options = array(array(
                    'label' => Mage::helper('snowflake_quantitydiscount')->__('Free Product'),
                    'value' => '',
                    'print_value' => ''
                ));

                $quoteItem->addOption(new Varien_Object(array(
                    'product'   => $product,
                    'code'      => 'additional_options',
                    'value'     => serialize($options),
                )));

                // With the freeproduct_uniqid option, items of the same free product won't get combined.
                $quoteItem->addOption(new Varien_Object(array(
                    'product'   => $product,
                    'code'      => 'freeproduct_uniqid',
                    'value'     => uniqid(null, true)
                )));

                return $quoteItem;
            }
        }

        return false;
    }

    /*
     * Set item price.
     *
     * @param Mage_Sales_Model_Quote_Item $item
     * @param float $price
     */
    protected function _setPrice(Mage_Sales_Model_Quote_Item $item, $price)
    {
        $item
            ->setPrice($price)
            ->setBasePrice($price)
            ->setCustomPrice($price)
            ->setOriginalCustomPrice($price);
    }

    /*
     * Retrieve item minimal price from finalprice and tierprice.
     *
     * @param Mage_Sales_Model_Quote_Item $item
     */
    protected function _getFinalPrice(Mage_Sales_Model_Quote_Item $item)
    {
        $product = Mage::getModel('catalog/product')->load($item->getProductId());

        $price = 0;
        if ($product && $product->getId())
        {
            $finalPrice = $product->getFinalPrice();
            $tierPrice  = $product->getTierPrice($item->getQty());

            $price = min($finalPrice, $tierPrice);
        }

        return $price;
    }

    /*
     * Get item summary qty. We cannot use $quote->getItemsSummaryQty() because of session problem
     *
     * @param Mage_Sales_Model_Quote $quote
     * @return int $qty
     */
    protected function _getItemsSummaryQty(Mage_Sales_Model_Quote $quote)
    {
        $qty = 0;
        foreach ($quote->getAllItems() as $item)
        {
            if ($item->getParentItem())
            {
                continue;
            }

            if (($children = $item->getChildren()) && $item->isShipSeparately())
            {
                foreach ($children as $child)
                {
                    $qty+= $child->getQty()*$item->getQty();
                }
            } else {
                $qty+= $item->getQty();
            }
        }

        return $qty;
    }

    /*
     * Modify item price with the rule discount.
     *
     * @param Mage_Sales_Model_Quote_Item $item
     */
    protected function _handleDiscount(Mage_Sales_Model_Quote_Item $item)
    {
        $rules = $this->_getRuleCollection($item);

        foreach ($rules as $rule)
        {
            if ($rule->getDiscountType() == Snowflake_QuantityDiscount_Model_Rule::DISCOUNT_TYPE_DISCOUNT)
            {
                if (!Mage::app()->getLocale()->isStoreDateInInterval($item->getStore(), $rule['from_date'], $rule['to_date']))
                {
                    continue;
                }

                $product = Mage::getModel('catalog/product')->load($item->getProductId());
                if ($product && $product->getId())
                {
                    $finalPrice = $product->getFinalPrice();
                    $tierPrice  = $product->getTierPrice($item->getQty());

                    $price = min($finalPrice, $tierPrice);

                    if (!$this->_stopProccessing($product))
                    {
                        if ($item->getQty() >= $rule->getMinQuantity())
                        {
                            $rulePercent = min(100, $rule->getDiscountAmount());
                            $rulePct = $rulePercent / 100;

                            $newPrice = $price - ($price * $rulePct);

                            $this->_setPrice($item, $newPrice);
                            $item->setIsDiscountProduct(true);
                        } else {
                            $this->_setPrice($item, $price);
                            $item->setIsDiscountProduct(false);
                        }
                    } else {
                        $this->_setPrice($item, $price);
                        $item->setIsDiscountProduct(false);
                    }
                }
            }
        }
    }

    /*
     * Make sure that a gift is only added once, create a free item and add it to the cart.
     *
     * @param Mage_Sales_Model_Quote $quote
     * @param Mage_Sales_Model_Quote_Item $item
     * @param Snowflake_QuantityDiscount_Model_Rule $rule
     * @param int $qty
     */
    protected function _handleGift(Mage_Sales_Model_Quote $quote, Mage_Sales_Model_Quote_Item $item, Snowflake_QuantityDiscount_Model_Rule $rule, $qty = 0)
    {
        if ($rule->getIsApplied())
        {
            return;
        }

        if ($qty)
        {
            $freeItem = $this->_getFreeQuoteItem($quote, $rule->getDiscountProductId(), $item->getStoreId(), $qty);
            if ($freeItem)
            {
                $this->_addAndApply($quote, $freeItem, $rule);
            }
        }
    }

    /*
     * Add a free item and mark that the rule was used on this item.abstract
     *
     * @param Mage_Sales_Model_Quote $quote
     * @param Mage_Sales_Model_Quote_Item $item
     * @param Snowflake_QuantityDiscount_Model_Rule $rule
     */
    protected function _addAndApply(Mage_Sales_Model_Quote $quote, Mage_Sales_Model_Quote_Item $item, Snowflake_QuantityDiscount_Model_Rule $rule)
    {
        $quote->addItem($item);
        $quote->setItemsQty($this->_getItemsSummaryQty($quote));

        $item->setApplyingRule($rule);
        $rule->setIsApplied(true);
    }

    /*
     * Reset all free & discount items which are out of date.
     *
     * @param Mage_Sales_Model_Quote_Item $item
     */
    protected function _resetItem(Mage_Sales_Model_Quote_Item $item)
    {
        $rules = $this->_getRuleCollection($item);

        foreach ($rules as $rule)
        {
            if (!Mage::app()->getLocale()->isStoreDateInInterval($item->getStore(), $rule['from_date'], $rule['to_date']))
            {
                if ($item->getIsFreeProduct())
                {
                    // delete free product
                    $item->delete();
                }

                if ($item->getIsDiscountProduct())
                {
                    // we need to set price null, otherwise update cart doesn't work
                    $this->_setPrice($item, null);
                    $item->setIsDiscountProduct(false);
                }
            }
        }
    }

    /**
     * Delete all free items from the cart.
     *
     * @param Mage_Sales_Model_Quote $quote
     */
    protected function _resetFreeItems(Mage_Sales_Model_Quote $quote)
    {
        foreach ($quote->getAllItems() as $item)
        {
            if ($item->getIsFreeProduct())
            {
                $quote->removeItem($item->getId());
            }
        }
    }
}
  