<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 14.10.2015
 * Time: 13:48
 */

class Snowflake_QuantityDiscount_Model_Resource_Rule_Collection
    extends Mage_Rule_Model_Resource_Rule_Collection_Abstract
{
    /**
     * Store associated with rule entities information map
     *
     * @var array
     */
    protected $_associatedEntitiesMap = array(
        'discount_group' => array(
            'associations_table' => 'snowflake_quantitydiscount/rule_discount_group',
            'rule_id_field'      => 'rule_id',
            'entity_id_field'    => 'discount_group_id'
        )
    );

    /**
     * Set resource model
     */
    protected function _construct()
    {
        $this->_init('snowflake_quantitydiscount/rule');
    }

    /**
     * Add discount group to collection
     *
     * @return Snowflake_QuantityDiscount_Model_Resource_Rule_Collection
     */
    public function _initSelect()
    {
        parent::_initSelect();
        $this->getSelect()
            ->joinLeft(
                array('discount_group' => $this->getTable('snowflake_quantitydiscount/rule_discount_group')),
                'main_table.rule_id = discount_group.rule_id',
                array('discount_group_id')
            );

        return $this;
    }

    /**
     * Add discount group ids to rules data
     *
     * @return Mage_Rule_Model_Resource_Rule_Collection_Abstract
     */
    protected function _afterLoad()
    {
        parent::_afterLoad();
        if ($this->getFlag('add_discount_groups_to_result') && $this->_items) {
            /** @var Mage_Rule_Model_Abstract $item */
            foreach ($this->_items as $item) {
                $item->afterLoad();
            }
        }

        return $this;
    }

    /*
     * Add product name to collection
     *
     * @return Snowflake_QuantityDiscount_Model_Resource_Rule_Collection
     */
    public function addProductData()
    {
        $attributeCode = 'name';
        $alias         = $attributeCode . '_table';

        $attribute = Mage::getSingleton('eav/config')->getAttribute(Mage_Catalog_Model_Product::ENTITY, $attributeCode);

        /** Adding eav attribute value */
        $this
            ->getSelect()
            ->join(
                array($alias => $attribute->getBackendTable()),
                "main_table.product_id = $alias.entity_id",
                array($attributeCode => 'value'))
            ->where("$alias.attribute_id = ?", $attribute->getId())
            ->where("$alias.store_id = ?", Mage::app()->getStore()->getId());

        $this->_map['fields'][$attributeCode] = 'value';

        return $this;
    }
}