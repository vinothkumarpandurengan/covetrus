<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 15.10.2015
 * Time: 09:35
 */

class Snowflake_QuantityDiscount_Model_Resource_Discount_Group
    extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Resource initialization
     */
    protected function _construct()
    {
        $this->_init('snowflake_quantitydiscount/discount_group', 'discount_group_id');
    }

    /**
     * Initialize unique fields
     *
     * @return Snowflake_QuantityDiscount_Model_Resource_Discount_Group
     */
    protected function _initUniqueFields()
    {
        $this->_uniqueFields = array(
            array(
                'field' => 'discount_group_code',
                'title' => Mage::helper('snowflake_quantitydiscount')->__('Discount Group')
            ));

        return $this;
    }
}