<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 15.10.2015
 * Time: 09:35
 */

class Snowflake_QuantityDiscount_Model_Resource_Discount_Group_Collection
    extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Resource initialization
     */
    protected function _construct()
    {
        $this->_init('snowflake_quantitydiscount/discount_group');
    }

    /**
     * Retreive option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        return parent::_toOptionArray('discount_group_id', 'discount_group_code');
    }

    /**
     * Retreive option hash
     *
     * @return array
     */
    public function toOptionHash()
    {
        return parent::_toOptionHash('discount_group_id', 'discount_group_code');
    }
}