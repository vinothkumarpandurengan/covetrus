<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 14.10.2015
 * Time: 13:41
 */

class Snowflake_QuantityDiscount_Model_Resource_Rule
    extends Mage_Rule_Model_Resource_Abstract
{
    /**
     * Product Collection object
     *
     * @var Varien_Data_Collection
     */
    protected $_productCollection = null;

    /**
     * Store associated with rule entities information map
     *
     * @var array
     */
    protected $_associatedEntitiesMap = array(
        'discount_group' => array(
            'associations_table' => 'snowflake_quantitydiscount/rule_discount_group',
            'rule_id_field'      => 'rule_id',
            'entity_id_field'    => 'discount_group_id'
        )
    );

    /**
     * Initialize main table and table id field
     */
    protected function _construct()
    {
        $this->_init('snowflake_quantitydiscount/rule', 'rule_id');
    }

    /**
     * Create product collection
     *
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getProductCollection()
    {
        $this->_productCollection = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('name');

        return $this->_productCollection;
    }

    /**
     * Add discount group ids and product name to rule data after load
     *
     * @param Mage_Core_Model_Abstract $object
     *
     * @return Snowflake_QuantityDiscount_Model_Resource_Rule
     */
    protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {
        $object->setData('discount_group_ids', (array)$this->getDiscountGroupIds($object->getId()));
        $object->setData('product', $this->getProductName($object->getProductId()));
        $object->setData('discount_product', $this->getProductName($object->getDiscountProductId()));

        parent::_afterLoad($object);
        return $this;
    }

    /**
     * Bind quantity discount rule to discount group.
     * Update products which are matched for rule.
     *
     * @param Mage_Core_Model_Abstract $object
     *
     * @return Snowflake_QuantityDiscount_Model_Resource_Rule
     */
    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {
        if ($object->hasDiscountGroupIds()) {
            $discountGroupIds = $object->getDiscountGroupIds();
            if (!is_array($discountGroupIds)) {
                $discountGroupIds = explode(',', (string)$discountGroupIds);
            }
            $this->bindRuleToEntity($object->getId(), $discountGroupIds, 'discount_group');
        }
        return parent::_afterSave($object);
    }

    /**
     * Retrieve discount group ids of specified rule
     *
     * @param int $ruleId
     * @return array
     */
    public function getDiscountGroupIds($ruleId)
    {
        return $this->getAssociatedEntityIds($ruleId, 'discount_group');
    }

    /**
    /**
     * Get product name by id
     *
     * @param int $productId
     * @return string
     */
    public function getProductName($productId)
    {
        $_productCollection = $this->getProductCollection()
            ->addAttributeToFilter('entity_id', array('in' => $productId));

        foreach ($_productCollection as $_product) {
            $_productName = $_product->getSku() .' / ' . $_product->getName();
        }

       return $_productName;
    }
}