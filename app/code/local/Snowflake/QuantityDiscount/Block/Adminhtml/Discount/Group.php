<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 15.10.2015
 * Time: 09:21
 */

class Snowflake_QuantityDiscount_Block_Adminhtml_Discount_Group
    extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Modify header & button labels
     *
     */
    public function __construct()
    {
        $this->_blockGroup = 'snowflake_quantitydiscount';
        $this->_controller = 'adminhtml_discount_group';

        $this->_headerText = Mage::helper('snowflake_quantitydiscount')->__('Discount Groups');
        $this->_addButtonLabel = Mage::helper('snowflake_quantitydiscount')->__('Add New Discount Group');

        parent::__construct();
    }

    /**
     * Redefine header css class
     *
     * @return string
     */
    public function getHeaderCssClass() {
        return 'icon-head head-discount-groups';
    }
}