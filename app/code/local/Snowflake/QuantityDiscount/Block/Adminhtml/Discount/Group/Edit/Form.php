<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 15.10.2015
 * Time: 11:03
 */

class Snowflake_QuantityDiscount_Block_Adminhtml_Discount_Group_Edit_Form
    extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare form for render
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $form = new Varien_Data_Form();
        $discountGroup = Mage::registry('current_discount_group');

        $fieldset = $form->addFieldset('base_fieldset', array('legend'=>Mage::helper('snowflake_quantitydiscount')->__('Group Information')));

        $validateClass = sprintf('required-entry validate-length maximum-length-%d',
            Snowflake_QuantityDiscount_Model_Discount_Group::DISCOUNT_GROUP_CODE_MAX_LENGTH);

        $name = $fieldset->addField('discount_group_code', 'text',
            array(
                'name'  => 'code',
                'label' => Mage::helper('snowflake_quantitydiscount')->__('Group Name'),
                'title' => Mage::helper('snowflake_quantitydiscount')->__('Group Name'),
                'note'  => Mage::helper('snowflake_quantitydiscount')->__('Maximum length must be less then %s symbols', Snowflake_QuantityDiscount_Model_Discount_Group::DISCOUNT_GROUP_CODE_MAX_LENGTH),
                'class' => $validateClass,
                'required' => true,
            )
        );

        if ($discountGroup->getId()==0 && $discountGroup->getDiscountGroupCode() ) {
            $name->setDisabled(true);
        }

        if (!is_null($discountGroup->getId())) {
            // If edit add id
            $form->addField('id', 'hidden',
                array(
                    'name'  => 'id',
                    'value' => $discountGroup->getId(),
                )
            );
        }

        if( Mage::getSingleton('adminhtml/session')->getDiscountGroupData() ) {
            $form->addValues(Mage::getSingleton('adminhtml/session')->getDiscountGroupData());
            Mage::getSingleton('adminhtml/session')->setDiscountGroupData(null);
        } else {
            $form->addValues($discountGroup->getData());
        }

        $form->setUseContainer(true);
        $form->setId('edit_form');
        $form->setAction($this->getUrl('*/*/save'));
        $this->setForm($form);
    }
}