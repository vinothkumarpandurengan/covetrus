<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 15.10.2015
 * Time: 09:22
 */

class Snowflake_QuantityDiscount_Block_Adminhtml_Discount_Group_Edit
    extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'snowflake_quantitydiscount';
        $this->_controller = 'adminhtml_discount_group';

        $this->_updateButton('save', 'label', Mage::helper('snowflake_quantitydiscount')->__('Save Discount Group'));
        $this->_updateButton('delete', 'label', Mage::helper('snowflake_quantitydiscount')->__('Delete Discount Group'));

        if(!Mage::registry('current_discount_group')->getId()) {
            $this->_removeButton('delete');
        }
    }

    public function getHeaderText()
    {
        if(!is_null(Mage::registry('current_discount_group')->getId())) {
            return Mage::helper('snowflake_quantitydiscount')->__('Edit Discount Group "%s"', $this->escapeHtml(Mage::registry('current_discount_group')->getDiscountGroupCode()));
        } else {
            return Mage::helper('snowflake_quantitydiscount')->__('New Discount Group');
        }
    }

    public function getHeaderCssClass()
    {
        return 'icon-head head-discount-groups';
    }
}