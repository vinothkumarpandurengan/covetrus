<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 15.10.2015
 * Time: 09:22
 */

class Snowflake_QuantityDiscount_Block_Adminhtml_Discount_Group_Grid
    extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('discount_group_grid');
        $this->setDefaultSort('type');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Init discount groups collection
     * @return void
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('snowflake_quantitydiscount/discount_group_collection');

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Configuration of grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('time', array(
            'header' => Mage::helper('snowflake_quantitydiscount')->__('ID'),
            'width' => '50px',
            'align' => 'right',
            'index' => 'discount_group_id',
        ));

        $this->addColumn('type', array(
            'header' => Mage::helper('snowflake_quantitydiscount')->__('Group Name'),
            'index' => 'discount_group_code',
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id'=>$row->getId()));
    }
}