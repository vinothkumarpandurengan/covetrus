<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 14.10.2015
 * Time: 09:26
 */

class Snowflake_QuantityDiscount_Block_Adminhtml_Promo_Discount_Edit
    extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Initialize form
     * Add standard buttons
     * Add "Save and Continue" button
     */
    public function __construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'snowflake_quantitydiscount';
        $this->_controller = 'adminhtml_promo_discount';

        parent::__construct();

        $this->_addButton('save_and_continue_edit', array(
            'class'   => 'save',
            'label'   => Mage::helper('snowflake_quantitydiscount')->__('Save and Continue Edit'),
            'onclick' => 'editForm.submit($(\'edit_form\').action + \'back/edit/\')',
        ), 10);
    }

    /**
     * Getter for form header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        $rule = Mage::registry('current_promo_discount_rule');
        if ($rule->getRuleId()) {
            return Mage::helper('snowflake_quantitydiscount')->__('Edit Rule');
        }
        else {
            return Mage::helper('snowflake_quantitydiscount')->__('New Rule');
        }
    }
}