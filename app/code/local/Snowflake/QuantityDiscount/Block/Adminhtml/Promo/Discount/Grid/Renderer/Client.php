<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 16.10.2015
 * Time: 13:53
 */

class Snowflake_QuantityDiscount_Block_Adminhtml_Promo_Discount_Grid_Renderer_Client
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    protected static $_clients;

    public function __construct()
    {
        self::$_clients = array(
            Snowflake_QuantityDiscount_Model_Rule::CLIENT_P => Mage::helper('snowflake_quantitydiscount')->__('P'),
            Snowflake_QuantityDiscount_Model_Rule::CLIENT_H	=> Mage::helper('snowflake_quantitydiscount')->__('H'),
            Snowflake_QuantityDiscount_Model_Rule::CLIENT_C => Mage::helper('snowflake_quantitydiscount')->__('C'),
        );
        parent::__construct();
    }

    public function render(Varien_Object $row)
    {
        return Mage::helper('snowflake_quantitydiscount')->__($this->getClients($row->getClient()));
    }

    public static function  getClients($client)
    {
        if(isset(self::$_clients[$client])) {
            return self::$_clients[$client];
        }

        return '';
    }
}