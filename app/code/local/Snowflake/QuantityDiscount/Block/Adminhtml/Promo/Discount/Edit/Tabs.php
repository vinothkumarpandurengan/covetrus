<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 14.10.2015
 * Time: 09:32
 */

class Snowflake_QuantityDiscount_Block_Adminhtml_Promo_Discount_Edit_Tabs
    extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('promo_discount_edit_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('snowflake_quantitydiscount')->__('Quantity Discount Rule'));
    }
}