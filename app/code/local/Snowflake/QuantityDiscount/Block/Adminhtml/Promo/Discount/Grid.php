<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 13.10.2015
 * Time: 17:27
 */

class Snowflake_QuantityDiscount_Block_Adminhtml_Promo_Discount_Grid
    extends Mage_Adminhtml_Block_Widget_Grid
{

    /**
     * Initialize grid
     * Set sort settings
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('promo_discount_grid');
        $this->setDefaultSort('rule_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Add websites to discount rules collection
     * Set collection
     *
     * @return Mage_Adminhtml_Block_Promo_Discount_Grid
     */
    protected function _prepareCollection()
    {
        /** @var $collection Snowflake_QuantityDiscount_Model_Resource_Rule_Collection */
        $collection = Mage::getModel('snowflake_quantitydiscount/rule')
            ->getResourceCollection()
            ->addProductData();

        $this->setCollection($collection);

        parent::_prepareCollection();
        return $this;
    }

    /**
     * Add grid columns
     *
     * @return Mage_Adminhtml_Block_Promo_Quote_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('rule_id', array(
            'header'    => Mage::helper('snowflake_quantitydiscount')->__('ID'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'rule_id',
        ));

        $this->addColumn('client', array(
            'header'    => Mage::helper('snowflake_quantitydiscount')->__('Client'),
            'align'     => 'right',
            'width'     => '50px',
            'index'     => 'client',
            'renderer'  =>  'snowflake_quantitydiscount/adminhtml_promo_discount_grid_renderer_client'
        ));

        $this->addColumn('name', array(
            'header'    => Mage::helper('snowflake_quantitydiscount')->__('Product'),
            'align'     => 'left',
            'index'     => 'product_id',
        ));

        $this->addColumn('name', array(
            'header'    => Mage::helper('snowflake_quantitydiscount')->__('Product'),
            'align'     => 'left',
            'index'     => 'name',
        ));
/*
        $this->addColumn('rule_product_discount_group', array(
            'header'    =>  Mage::helper('snowflake_quantitydiscount')->__('Product Discount Group'),
            'width'     =>  '150',
            'align'     => 'left',
            'index'     =>  'product_discount_group',
        ));
*/
        $this->addColumn('discount_type', array(
            'header'    =>  Mage::helper('snowflake_quantitydiscount')->__('Discount Type'),
            'width'     =>  '150',
            'index'     =>  'discount_type',
            'type'      =>  'options',
            'options'  => array(
                '1' => Mage::helper('snowflake_quantitydiscount')->__('Discount'),
                'b' => Mage::helper('snowflake_quantitydiscount')->__('Quantity Bonus'),
            ),
        ));

        $discountGroups = Mage::getResourceModel('snowflake_quantitydiscount/discount_group_collection')
            ->addFieldToFilter('discount_group_id', array('gt'=> 0))
            ->load()
            ->toOptionHash();

        $this->addColumn('rule_discount_group', array(
            'header'    =>  Mage::helper('snowflake_quantitydiscount')->__('Discount Group'),
            'width'     =>  '150',
            'index'     =>  'discount_group_id',
            'type'      =>  'options',
            'options'   =>  $discountGroups,
        ));

        $this->addColumn('from_date', array(
            'header'    => Mage::helper('snowflake_quantitydiscount')->__('Date Start'),
            'align'     => 'left',
            'width'     => '120',
            'type'      => 'date',
            'index'     => 'from_date',
        ));

        $this->addColumn('to_date', array(
            'header'    => Mage::helper('snowflake_quantitydiscount')->__('Date Expire'),
            'align'     => 'left',
            'width'     => '120',
            'type'      => 'date',
            'default'   => '--',
            'index'     => 'to_date',
        ));

        $this->addColumn('is_active', array(
            'header'    => Mage::helper('snowflake_quantitydiscount')->__('Status'),
            'align'     => 'left',
            'width'     => '80',
            'index'     => 'is_active',
            'type'      => 'options',
            'options'   => array(
                1 => Mage::helper('snowflake_quantitydiscount')->__('Active'),
                0 => Mage::helper('snowflake_quantitydiscount')->__('Inactive')
            ),
        ));

        $this->addColumn('sort_order', array(
            'header'    => Mage::helper('snowflake_quantitydiscount')->__('Priority'),
            'align'     => 'right',
            'index'     => 'sort_order',
            'width'     => 100,
        ));

        parent::_prepareColumns();
        return $this;
    }

    /**
     * Retrieve row click URL
     *
     * @param Varien_Object $row
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getRuleId()));
    }
}