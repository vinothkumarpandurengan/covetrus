<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 14.10.2015
 * Time: 10:14
 */

class Snowflake_QuantityDiscount_Block_Adminhtml_Promo_Discount_Edit_Tab_Main
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * Prepare content for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('snowflake_quantitydiscount')->__('Rule Information');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('snowflake_quantitydiscount')->__('Rule Information');
    }

    /**
     * Returns status flag about this tab can be showed or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Prepare form before rendering HTML
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $model = Mage::registry('current_promo_discount_rule');

        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('rule_');

        $fieldset = $form->addFieldset('base_fieldset',
            array('legend' => Mage::helper('snowflake_quantitydiscount')->__('General Information'))
        );

        if ($model->getId()) {
            $fieldset->addField('rule_id', 'hidden', array(
                'name' => 'rule_id',
            ));
        }

        $fieldset->addField('client', 'select', array(
            'name'     => 'client',
            'label'    => Mage::helper('snowflake_quantitydiscount')->__('Client'),
            'title'    => Mage::helper('snowflake_quantitydiscount')->__('Client'),
            'required' => true,
            'options'  => array(
                Snowflake_QuantityDiscount_Model_Rule::CLIENT_P => Mage::helper('snowflake_quantitydiscount')->__('P'),
                Snowflake_QuantityDiscount_Model_Rule::CLIENT_H => Mage::helper('snowflake_quantitydiscount')->__('H'),
                Snowflake_QuantityDiscount_Model_Rule::CLIENT_C => Mage::helper('snowflake_quantitydiscount')->__('C'),
            ),
        ));

        $fieldset->addField('product', 'text', array(
            'name'     => 'product',
            'required' => true,
            'class'    => 'required-entry',
            'label'    => Mage::helper('snowflake_quantitydiscount')->__('Product'),
        ));

        $fieldset->addField('product_id', 'hidden', array(
            'name' => 'product_id',
        ));

        $fieldset->addField('is_active', 'select', array(
            'label'     => Mage::helper('snowflake_quantitydiscount')->__('Status'),
            'title'     => Mage::helper('snowflake_quantitydiscount')->__('Status'),
            'name'      => 'is_active',
            'required' => true,
            'options'    => array(
                '1' => Mage::helper('snowflake_quantitydiscount')->__('Active'),
                '0' => Mage::helper('snowflake_quantitydiscount')->__('Inactive'),
            ),
        ));

        if (!$model->getId()) {
            $model->setData('is_active', '1');
        }

        $fieldset->addField('discount_group_ids', 'select', array(
            'name'      => 'discount_group_ids[]',
            'label'     => Mage::helper('snowflake_quantitydiscount')->__('Discount Groups'),
            'title'     => Mage::helper('snowflake_quantitydiscount')->__('Discount Groups'),
            'required'  => true,
            'values'    => Mage::getResourceModel('snowflake_quantitydiscount/discount_group_collection')->toOptionArray(),
        ));

        $dateFormatIso = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
        $fieldset->addField('from_date', 'date', array(
            'name'   => 'from_date',
            'label'  => Mage::helper('snowflake_quantitydiscount')->__('From Date'),
            'title'  => Mage::helper('snowflake_quantitydiscount')->__('From Date'),
            'image'  => $this->getSkinUrl('images/grid-cal.gif'),
            'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
            'format'       => $dateFormatIso
        ));
        $fieldset->addField('to_date', 'date', array(
            'name'   => 'to_date',
            'label'  => Mage::helper('snowflake_quantitydiscount')->__('To Date'),
            'title'  => Mage::helper('snowflake_quantitydiscount')->__('To Date'),
            'image'  => $this->getSkinUrl('images/grid-cal.gif'),
            'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
            'format'       => $dateFormatIso
        ));

        Mage::dispatchEvent('adminhtml_promo_discount_edit_tab_actions_prepare_form', array('form' => $form));

        $form->setValues($model->getData());

        if ($model->isReadonly()) {
            foreach ($fieldset->getElements() as $element) {
                $element->setReadonly(true, true);
            }
        }

        $this->setForm($form);

        return parent::_prepareForm();
    }
}