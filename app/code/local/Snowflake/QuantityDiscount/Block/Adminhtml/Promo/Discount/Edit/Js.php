<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 16.10.2015
 * Time: 13:08
 */

class Snowflake_QuantityDiscount_Block_Adminhtml_Promo_Discount_Edit_Js extends Mage_Adminhtml_Block_Template
{
    /**
     * Return serialized list of all products
     *
     * @return json
     */
    public function getAllProducts()
    {
        $_productCollection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('id')
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('name')
            ->addAttributeToFilter('type_id', array(
                Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
                Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE,
                Mage_Catalog_Model_Product_Type::TYPE_VIRTUAL
            ));

        $products = array();
        foreach ($_productCollection as $_product)
        {
            $productId  = $_product->getId();
            $products[$productId] = array(
                'id'    => $productId,
                'label' => $_product->getSku() .' / ' . $_product->getName()
            );
        }

        return Mage::helper('core')->jsonEncode($products);
    }
}