<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 14.10.2015
 * Time: 10:36
 */

class Snowflake_QuantityDiscount_Block_Adminhtml_Promo_Discount_Edit_Tab_Actions
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * Prepare content for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('snowflake_quantitydiscount')->__('Actions');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('snowflake_quantitydiscount')->__('Actions');
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return false
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Prepare form before rendering HTML
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $model = Mage::registry('current_promo_discount_rule');

        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('rule_');

        $fieldset = $form->addFieldset('action_fieldset', array(
                'legend' => Mage::helper('snowflake_quantitydiscount')->__('Update Prices Using the Following Information')
            )
        );

        $discountTypeField = $fieldset->addField('discount_type', 'select', array(
            'name'     => 'discount_type',
            'label'    => Mage::helper('snowflake_quantitydiscount')->__('Discount Type'),
            'title'    => Mage::helper('snowflake_quantitydiscount')->__('Discount Type'),
            //'onchange' => 'hideShowSubproductOptions(this);',
            'options'  => array(
                '1' => Mage::helper('snowflake_quantitydiscount')->__('Discount'),
                'b' => Mage::helper('snowflake_quantitydiscount')->__('Quantity Bonus'),
            ),
        ));
/*
        $productDiscountGroupField = $fieldset->addField('product_discount_group', 'text', array(
            'name'     => 'product_discount_group',
            'label'    => Mage::helper('snowflake_quantitydiscount')->__('Product Discount Group'),
            'required' => true,
            'class'    => 'required-entry',
        ));
*/
        $fieldset->addField('min_quantity', 'text', array(
            'name'     => 'min_quantity',
            'label'    => Mage::helper('snowflake_quantitydiscount')->__('Minimum Quantity'),
            'required' => true,
            'class'    => 'validate-not-negative-number',
        ));
        $model->setMinQuantity($model->getMinQuantity()*1);

        $discountAmountField = $fieldset->addField('discount_amount', 'text', array(
            'name'     => 'discount_amount',
            'label'    => Mage::helper('snowflake_quantitydiscount')->__('Discount Amount'),
            'required' => true,
            'class'    => 'validate-not-negative-number',
        ));

        $simpleActionField = $fieldset->addField('simple_action', 'select', array(
            'name'     => 'simple_action',
            'label'    => Mage::helper('snowflake_quantitydiscount')->__('Apply'),
            'title'    => Mage::helper('snowflake_quantitydiscount')->__('Apply'),
            //'onchange' => 'hideShowSubproductOptions(this);',
            'options'  => array(
                'bonus_fixed_qty' => Mage::helper('snowflake_quantitydiscount')->__('Bonus Fixed Quantity'),
                'bonus_after_order' => Mage::helper('snowflake_quantitydiscount')->__('Bonus Order'),
                'bonus_percent' => Mage::helper('snowflake_quantitydiscount')->__('Bonus Percent'),
            ),
        ));

        $discountQtyField = $fieldset->addField('discount_qty', 'text', array(
            'name'     => 'discount_qty',
            'label'    => Mage::helper('snowflake_quantitydiscount')->__('Quantity'),
            'required' => true,
            'class'    => 'validate-not-negative-number',
        ));
        $model->setDiscountQty($model->getDiscountQty()*1);

        $discountPercentField = $fieldset->addField('discount_percent', 'text', array(
            'name'     => 'discount_percent',
            'required' => true,
            'class'    => 'validate-not-negative-number',
            'label'    => Mage::helper('snowflake_quantitydiscount')->__('Percent'),
        ));

        $discountProductField = $fieldset->addField('discount_product', 'text', array(
            'name'     => 'discount_product',
            'required' => true,
            'class'    => 'required-entry',
            'label'    => Mage::helper('snowflake_quantitydiscount')->__('Bonus Product'),
        ));

        $fieldset->addField('discount_product_id', 'hidden', array(
            'name' => 'discount_product_id',
        ));

        Mage::dispatchEvent('adminhtml_promo_discount_edit_tab_main_prepare_form', array('form' => $form));

        $form->setValues($model->getData());

        if ($model->isReadonly()) {
            foreach ($fieldset->getElements() as $element) {
                $element->setReadonly(true, true);
            }
        }

        $this->setForm($form);

        // field dependencies
        $this->setChild('form_after', $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence')
            //->addFieldMap($productDiscountGroupField->getHtmlId(), $productDiscountGroupField->getName())
            //->addFieldMap($productField->getHtmlId(), $productField->getName())
            //->addFieldMap($productIdField->getHtmlId(), $productIdField->getName())
            ->addFieldMap($discountTypeField->getHtmlId(), $discountTypeField->getName())
            ->addFieldMap($discountAmountField->getHtmlId(), $discountAmountField->getName())
            ->addFieldMap($discountQtyField->getHtmlId(), $discountQtyField->getName())
            ->addFieldMap($discountPercentField->getHtmlId(), $discountPercentField->getName())
            ->addFieldMap($discountProductField->getHtmlId(), $discountProductField->getName())
            ->addFieldMap($simpleActionField->getHtmlId(), $simpleActionField->getName())
            ->addFieldDependence(
                $discountAmountField->getName(),
                $discountTypeField->getName(),
                Snowflake_QuantityDiscount_Model_Rule::DISCOUNT_TYPE_DISCOUNT)
            //->addFieldDependence(
            //    $productDiscountGroupField->getName(),
            //    $discountTypeField->getName(),
            //    Snowflake_QuantityDiscount_Model_Rule::DISCOUNT_TYPE_DISCOUNT)
            //->addFieldDependence(
            //    $productField->getName(),
            //    $discountTypeField->getName(),
            //    Snowflake_QuantityDiscount_Model_Rule::DISCOUNT_TYPE_QUANTITY_BONUS)
            //->addFieldDependence(
            //    $productIdField->getName(),
            //    $discountTypeField->getName(),
            //    Snowflake_QuantityDiscount_Model_Rule::DISCOUNT_TYPE_QUANTITY_BONUS)
            ->addFieldDependence(
                $simpleActionField->getName(),
                $discountTypeField->getName(),
                Snowflake_QuantityDiscount_Model_Rule::DISCOUNT_TYPE_QUANTITY_BONUS)
            ->addFieldDependence(
                $discountQtyField->getName(),
                $discountTypeField->getName(),
                Snowflake_QuantityDiscount_Model_Rule::DISCOUNT_TYPE_QUANTITY_BONUS)
            ->addFieldDependence(
                $discountPercentField->getName(),
                $discountTypeField->getName(),
                Snowflake_QuantityDiscount_Model_Rule::DISCOUNT_TYPE_QUANTITY_BONUS)
            ->addFieldDependence(
                $discountProductField->getName(),
                $discountTypeField->getName(),
                Snowflake_QuantityDiscount_Model_Rule::DISCOUNT_TYPE_QUANTITY_BONUS)
            ->addFieldDependence(
                $discountQtyField->getName(),
                $simpleActionField->getName(),
                Snowflake_QuantityDiscount_Model_Rule::BONUS_FIXED_QTY_ACTION)
            ->addFieldDependence(
                $discountPercentField->getName(),
                $simpleActionField->getName(),
                Snowflake_QuantityDiscount_Model_Rule::BONUS_PERCENT_ACTION)
        );

        return parent::_prepareForm();
    }
}