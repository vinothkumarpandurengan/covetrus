<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 13.10.2015
 * Time: 17:20
 */

class Snowflake_QuantityDiscount_Block_Adminhtml_Promo_Discount extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'snowflake_quantitydiscount';
        $this->_controller = 'adminhtml_promo_discount';

        $this->_headerText = Mage::helper('snowflake_quantitydiscount')->__('Quantity Discount');
        $this->_addButtonLabel = Mage::helper('snowflake_quantitydiscount')->__('Add New Rule');

        parent::__construct();
    }
}