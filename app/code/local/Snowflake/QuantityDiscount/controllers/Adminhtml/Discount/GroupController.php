<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 15.10.2015
 * Time: 09:12
 */

class Snowflake_QuantityDiscount_Adminhtml_Discount_GroupController
    extends Mage_Adminhtml_Controller_Action
{
    protected function _initDiscountGroup()
    {
        $this->_title(Mage::helper('snowflake_quantitydiscount')->__('Customers'))->_title(Mage::helper('snowflake_quantitydiscount')->__('Discount Groups'));

        Mage::register('current_discount_group', Mage::getModel('snowflake_quantitydiscount/discount_group'));
        $groupId = $this->getRequest()->getParam('id');
        if (!is_null($groupId)) {
            Mage::registry('current_discount_group')->load($groupId);
        }
    }

    /**
     * Discount groups list.
     */
    public function indexAction()
    {
        $this->_title(Mage::helper('snowflake_quantitydiscount')->__('Customers'))->_title(Mage::helper('snowflake_quantitydiscount')->__('Discount Groups'));

        $this->loadLayout();
        $this->_setActiveMenu('customer/discount_group');
        $this->_addBreadcrumb(Mage::helper('snowflake_quantitydiscount')->__('Customers'), Mage::helper('snowflake_quantitydiscount')->__('Customers'));
        $this->_addBreadcrumb(Mage::helper('snowflake_quantitydiscount')->__('Discount Groups'), Mage::helper('snowflake_quantitydiscount')->__('Discount Groups'));
        $this->renderLayout();
    }

    /**
     * create discount group action. Forward to edit action.
     */
    public function newAction()
    {
        $this->_initDiscountGroup();
        $this->loadLayout();
        $this->_setActiveMenu('customer/discount_group');
        $this->_addBreadcrumb(Mage::helper('snowflake_quantitydiscount')->__('Customers'), Mage::helper('snowflake_quantitydiscount')->__('Customers'));
        $this->_addBreadcrumb(Mage::helper('snowflake_quantitydiscount')->__('Discount Groups'), Mage::helper('snowflake_quantitydiscount')->__('Discount Groups'), $this->getUrl('*/discount_group'));

        $currentDiscountGroup = Mage::registry('current_discount_group');

        if (!is_null($currentDiscountGroup->getId())) {
            $this->_addBreadcrumb(Mage::helper('snowflake_quantitydiscount')->__('Edit Group'), Mage::helper('snowflake_quantitydiscount')->__('Edit Discount Groups'));
        } else {
            $this->_addBreadcrumb(Mage::helper('snowflake_quantitydiscount')->__('New Group'), Mage::helper('snowflake_quantitydiscount')->__('New Discount Groups'));
        }

        $this->_title($currentDiscountGroup->getId() ? $currentDiscountGroup->getCode() : $this->__('New Group'));

        $this->getLayout()->getBlock('content')
            ->append($this->getLayout()->createBlock('snowflake_quantitydiscount/adminhtml_discount_group_edit', 'discount_group')
                ->setEditMode((bool)Mage::registry('current_discount_group')->getId()));

        $this->renderLayout();
    }

    /**
     * Edit or create discount group.
     */
    public function editAction()
    {
        $this->_forward('new');
    }

    /**
     * Create or save discount group.
     */
    public function saveAction()
    {
        $discountGroup = Mage::getModel('snowflake_quantitydiscount/discount_group');
        $id = $this->getRequest()->getParam('id');
        if (!is_null($id)) {
            $discountGroup->load((int)$id);
        }

        try {
            $discountGroupCode = (string) $this->getRequest()->getParam('code');

            if (!empty($discountGroupCode)) {
                $discountGroup->setCode($discountGroupCode);
            }

            $discountGroup->save();
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('snowflake_quantitydiscount')->__('The discount group has been saved.'));
            $this->getResponse()->setRedirect($this->getUrl('*/discount_group'));
            return;
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::getSingleton('adminhtml/session')->setDiscountGroupData($discountGroup->getData());
            $this->getResponse()->setRedirect($this->getUrl('*/discount_group/edit', array('id' => $id)));
            return;
        }
    }

    /**
     * Delete discount group action
     */
    public function deleteAction()
    {
        $discountGroup = Mage::getModel('snowflake_quantitydiscount/discount_group');
        if ($id = (int)$this->getRequest()->getParam('id')) {
            try {
                $discountGroup->load($id);
                $discountGroup->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('snowflake_quantitydiscount')->__('The discount group has been deleted.'));
                $this->getResponse()->setRedirect($this->getUrl('*/discount_group'));
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->getResponse()->setRedirect($this->getUrl('*/discount_group/edit', array('id' => $id)));
                return;
            }
        }

        $this->_redirect('*/discount_group');
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('snowflake_quantitydiscount/discount_group');
    }
}