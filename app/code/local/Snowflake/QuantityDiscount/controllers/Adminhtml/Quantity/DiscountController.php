<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 13.10.2015
 * Time: 16:43
 */

class Snowflake_QuantityDiscount_Adminhtml_Quantity_DiscountController
    extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('promo/discount')
            ->_addBreadcrumb(
                Mage::helper('snowflake_quantitydiscount')->__('Promotions'),
                Mage::helper('snowflake_quantitydiscount')->__('Promotions')
            );
        return $this;
    }

    public function indexAction()
    {
        $this->_title(Mage::helper('snowflake_quantitydiscount')->__('Promotions'))->_title(Mage::helper('snowflake_quantitydiscount')->__('Quantity Discount'));

        $this->_initAction()
            ->_addBreadcrumb(Mage::helper('snowflake_quantitydiscount')->__('Catalog'), Mage::helper('snowflake_quantitydiscount')->__('Catalog'))
            ->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('snowflake_quantitydiscount/rule');

        if ($id) {
            $model->load($id);
            if (! $model->getRuleId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('snowflake_quantitydiscount')->__('This rule no longer exists.'));
                $this->_redirect('*/*');
                return;
            }
        }

        $this->_title($model->getRuleId() ? $model->getName() : $this->__('New Rule'));

        // set entered data if was error when we do save
        $data = Mage::getSingleton('adminhtml/session')->getPageData(true);
        if (!empty($data)) {
            $model->addData($data);
        }

        Mage::register('current_promo_discount_rule', $model);

        $this->_initAction()->getLayout()->getBlock('promo_discount_edit')
            ->setData('action', $this->getUrl('*/*/save'));

        $breadcrumb = $id
            ? Mage::helper('snowflake_quantitydiscount')->__('Edit Rule')
            : Mage::helper('snowflake_quantitydiscount')->__('New Rule');
        $this->_addBreadcrumb($breadcrumb, $breadcrumb)->renderLayout();
    }

    /**
     * Promo quote save action
     *
     */
    public function saveAction()
    {
        if ($this->getRequest()->getPost()) {
            try {
                /** @var $model Snowflake_QuantityDiscount_Model_Rule */
                $model = Mage::getModel('snowflake_quantitydiscount/rule');
                Mage::dispatchEvent(
                    'adminhtml_controller_discountrule_prepare_save',
                    array('request' => $this->getRequest())
                );
                $data = $this->getRequest()->getPost();
                $data = $this->_filterDates($data, array('from_date', 'to_date'));
                $id = $this->getRequest()->getParam('rule_id');
                if ($id) {
                    $model->load($id);
                    if ($id != $model->getId()) {
                        Mage::throwException(Mage::helper('snowflake_quantitydiscount')->__('Wrong rule specified.'));
                    }
                }

                $session = Mage::getSingleton('adminhtml/session');

                $validateResult = $model->validateData(new Varien_Object($data));
                if ($validateResult !== true) {
                    foreach($validateResult as $errorMessage) {
                        $session->addError($errorMessage);
                    }
                    $session->setPageData($data);
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }

                $model->loadPost($data);
                $session->setPageData($model->getData());

                $model->save();
                $session->addSuccess(Mage::helper('snowflake_quantitydiscount')->__('The rule has been saved.'));
                $session->setPageData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $id = (int)$this->getRequest()->getParam('rule_id');
                if (!empty($id)) {
                    $this->_redirect('*/*/edit', array('id' => $id));
                } else {
                    $this->_redirect('*/*/new');
                }
                return;
            } catch (Exception $e) {
                $this->_getSession()->addError(Mage::helper('snowflake_quantitydiscount')->__('An error occurred while saving the rule data. Please review the log and try again.'));

                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->setPageData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('rule_id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                /** @var $model Snowflake_QuantityDiscount_Model_Rule */
                $model = Mage::getModel('snowflake_quantitydiscount/rule');
                $model->load($id);
                $model->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('salesrule')->__('The rule has been deleted.'));
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(Mage::helper('snowflake_quantitydiscount')->__('An error occurred while deleting the rule. Please review the log and try again.'));

                Mage::logException($e);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }

        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('snowflake_quantitydiscount')->__('Unable to find a rule to delete.'));
        $this->_redirect('*/*/');
    }

    /**
     * Returns result of current user permission check on resource and privilege
     * @return boolean
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('snowflake_quantitydiscount/quantity_discount');
    }
}