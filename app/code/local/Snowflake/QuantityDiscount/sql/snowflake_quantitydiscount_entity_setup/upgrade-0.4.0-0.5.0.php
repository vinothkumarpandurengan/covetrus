<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 14.10.2015
 * Time: 12:40
 */

/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn(
    $installer->getTable('sales/quote_item'),
    'is_discount_product',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'nullable'  => false,
        'default'   => 0,
        'comment'   => 'Item is discount'
    )
);
$installer->getConnection()->addColumn(
    $installer->getTable('sales/order_item'),
    'is_discount_product',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'nullable'  => false,
        'default'   => 0,
        'comment'   => 'Item is discount'
    )
);

$installer->endSetup();