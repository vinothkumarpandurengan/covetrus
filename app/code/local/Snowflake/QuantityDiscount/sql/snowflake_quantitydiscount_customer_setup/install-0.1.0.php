<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 14.10.2015
 * Time: 12:40
 */

/* @var $installer Mage_Customer_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();


$this->run("
DROP TABLE IF EXISTS {$this->getTable('snowflake_quantitydiscount/discount_group')};
");

/**
 * Create table 'snowflake_quantitydiscount/discount_group'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('snowflake_quantitydiscount/discount_group'))
    ->addColumn('discount_group_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Customer Group Id')
    ->addColumn('discount_group_code', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
        'nullable'  => false,
    ), 'Discount Group Code')
    ->setComment('Discount Group');
$installer->getConnection()->createTable($table);

$installer->endSetup();