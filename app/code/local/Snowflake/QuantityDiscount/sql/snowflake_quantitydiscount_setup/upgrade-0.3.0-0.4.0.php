<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 14.10.2015
 * Time: 12:40
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();


$installer->getConnection()->dropForeignKey(
    $installer->getTable('snowflake_quantitydiscount/rule'),
    $installer->getFkName($installer->getTable('snowflake_quantitydiscount/rule'), 'product_id', $installer->getTable('catalog_product_entity'), 'entity_id')
);

$installer->getConnection()->addColumn(
    $installer->getTable('snowflake_quantitydiscount/rule'),
    'product_discount_group',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length'    => '20',
        'comment'   => 'Product Discount Group'
    )
);

$installer->endSetup();