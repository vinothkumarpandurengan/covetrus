<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 14.10.2015
 * Time: 12:40
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();


$this->run("
DROP TABLE IF EXISTS {$this->getTable('snowflake_quantitydiscount/rule')};
DROP TABLE IF EXISTS {$this->getTable('snowflake_quantitydiscount/rule_discount_group')};
");

/**
 * Create table 'snowflake_quantitydiscount/rule'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('snowflake_quantitydiscount/rule'))
    ->addColumn('rule_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Rule Id')
    ->addColumn('from_date', Varien_Db_Ddl_Table::TYPE_DATE, null, array(
        ), 'From Date')
    ->addColumn('to_date', Varien_Db_Ddl_Table::TYPE_DATE, null, array(
        ), 'To Date')
    ->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'default'   => 0,
        ), 'Is Active')
    ->addColumn('sort_order', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => 0,
        ), 'Sort Order')
    ->addColumn('simple_action', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
        ), 'Simple Action')
    ->addColumn('client', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
        ), 'Client')
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        ), 'Product ID')
    ->addColumn('min_quantity', Varien_Db_Ddl_Table::TYPE_DECIMAL, array(12,4), array(
        ), 'Minimum Quantity')
    ->addColumn('discount_type', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
    ), 'Discount Type')
    ->addColumn('discount_qty', Varien_Db_Ddl_Table::TYPE_DECIMAL, array(12,4), array(
        ), 'Discount Qty')
    ->addColumn('discount_amount', Varien_Db_Ddl_Table::TYPE_DECIMAL, array(4,1), array(
        'nullable'  => false,
        'default'   => 0,
        ), 'Discount Amount')
    ->addColumn('discount_percent', Varien_Db_Ddl_Table::TYPE_DECIMAL, array(4,1), array(
        'nullable'  => false,
        'default'   => 0,
        ), 'Discount Percent')
    ->addColumn('discount_product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        ), 'Discount Product ID')
    ->addIndex($installer->getIdxName('snowflake_quantitydiscount/rule', array('is_active', 'sort_order', 'to_date', 'from_date')),
        array('is_active', 'sort_order', 'to_date', 'from_date'))
    ->setComment('Quantity Discount Rule');
$installer->getConnection()->createTable($table);

/**
 * Create table 'snowflake_quantitydiscount/rule_discount_group'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('snowflake_quantitydiscount/rule_discount_group'))
    ->addColumn('rule_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true
    ), 'Rule ID')
    ->addColumn('discount_group_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true
    ), 'Discount Group ID')
    ->addIndex(
        $installer->getIdxName('snowflake_quantitydiscount/rule_discount_group', array('rule_id')),
        array('rule_id'))
    ->addIndex(
        $installer->getIdxName('snowflake_quantitydiscount/rule_discount_group', array('discount_group_id')),
        array('discount_group_id'))
    ->addForeignKey($installer->getFkName('snowflake_quantitydiscount/rule_discount_group', 'rule_id', $installer->getTable('snowflake_quantitydiscount/rule'), 'rule_id'),
        'rule_id', $installer->getTable('snowflake_quantitydiscount/rule'), 'rule_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('snowflake_quantitydiscount/rule_discount_group', 'discount_group_id', $installer->getTable('snowflake_quantitydiscount/discount_group'), 'discount_group_id'),
        'discount_group_id', $installer->getTable('snowflake_quantitydiscount/discount_group'), 'discount_group_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Quantity Discount Rules To Discount Groups Relations');
$installer->getConnection()->createTable($table);

$installer->endSetup();