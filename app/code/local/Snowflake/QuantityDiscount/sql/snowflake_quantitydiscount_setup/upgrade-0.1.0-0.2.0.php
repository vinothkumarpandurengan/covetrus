<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 14.10.2015
 * Time: 12:40
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

/**
 * Add foreign keys
 */
$installer->getConnection()->addForeignKey(
    $installer->getFkName($installer->getTable('snowflake_quantitydiscount/rule'), 'product_id', $installer->getTable('catalog_product_entity'), 'entity_id'),
    $installer->getTable('snowflake_quantitydiscount/rule'),
    'product_id',
    $installer->getTable('catalog_product_entity'),
    'entity_id'
);

$installer->endSetup();