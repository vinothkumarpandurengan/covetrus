<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 09.03.2015
 * Time: 17:24
 */ 
class Snowflake_Matrix_Model_Adminhtml_System_Config_Backend_Serialized_Array extends Mage_Adminhtml_Model_System_Config_Backend_Serialized_Array
{
    /**
     * Unset array element with '__empty' key
     */
    protected function _beforeSave()
    {
        $section = Mage::app()->getRequest()->getParam('section');
        if ($section == 'snowflake_matrix') {
            $value = $this->getValue();

            if (is_array($value)) {
                unset($value['__empty']);

                $value = array_values($value);
            }

            $this->setValue($value);
        }

        parent::_beforeSave();
    }
}