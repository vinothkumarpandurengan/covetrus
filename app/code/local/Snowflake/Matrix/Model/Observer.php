<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 10.03.2015
 * Time: 10:53
 */
class Snowflake_Matrix_Model_Observer
{
    protected $_productMatrixGroup = null;

    /**
     * Prepare product collection with matrix group.
     *
     * @param Varien_Event_Observer $observer
     * @return void
     */
    public function catalogProductCollectionLoadBefore(Varien_Event_Observer $observer)
    {
        if (Mage::getSingleton('customer/session')->isLoggedIn()){
            //$this->_getHelper()->addLimitedProductAttrToFilter($observer->getData('collection'));
            //$this->_getHelper()->addVkSperreAttrToFilter($observer->getData('collection'));
            return $this->_getHelper()->filterCustomerPermissionCollection($observer->getData('collection'));
        }
    }
    
    public function setPageSizeForCategory($observer){

        $controller = $observer->getAction();
        $fullActionName = $controller->getFullActionName();
        $id = (int)$controller->getRequest()->getParam('id');

        //check whether current page is correspond to our special category. If not, returns
        if($fullActionName == "catalog_category_view")
        {
            //check whether toolbar block exist or not
            $toolbar =  $controller->getLayout()->getBlock('product_list_toolbar');
            if($toolbar)
            {
                //sets page size to corresponding list mode
                $listMode = $toolbar->getCurrentMode();
                $toolbar = $toolbar->addPagerLimit($listMode , 15);
            }

        }

        return;
    }

    /**
     * Prepare product detailpage with matrix group.
     *
     * @param Varien_Event_Observer $observer
     * @return void
     */
    public function checkMatrixBlock(Varien_Event_Observer $observer)
    {
        if ($this->_getHelper()->isModuleEnable() && Mage::getSingleton('customer/session')->isLoggedIn())
        {
            $block = $observer->getData('block');
            if ($block instanceof Mage_Catalog_Block_Product_View)
            {
                $this->_checkMatrixProductViewBlock($block);
            }
        }
    }

    /**
     * Get Helper Data
     *
     * @return Snowflake_Matrix_Helper_Data
     */
    protected function _getHelper()
    {
        return Mage::helper('snowflake_matrix');
    }

    /**
     * Check matrix group permission on product detail page.
     *
     * @param Mage_Catalog_Block_Product_View $block
     *
     * @return void
     */
    protected function _checkMatrixProductViewBlock(Mage_Catalog_Block_Product_View $block)
    {
        $product = $block->getProduct();
        $productId = $product->getId();

        if ($this->_getHelper()->isDisabled($productId) OR $this->_getHelper()->isVkSperreActive($productId)){
            $block->setTemplate('catalog/product/empty.phtml');
        }
    }
}
  