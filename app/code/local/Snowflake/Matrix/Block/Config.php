<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 06.03.2015
 * Time: 15:39
 */
class Snowflake_Matrix_Block_Config extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    /**
     * Check if columns are defined, set matrix group template
     */
    public function __construct()
    {
        $this->addColumn('matrixCustomerGroup', array(
            'label' => Mage::helper('adminhtml')->__('Matrix customer group'),
            'size'  => 28,
        ));
        $this->addColumn('matrixProdutGroup', array(
            'label' => Mage::helper('adminhtml')->__('Matrix product group'),
            'size'  => 28
        ));
        $this->addColumn('available', array(
            'label' => Mage::helper('adminhtml')->__('Available'),
            'size'  => 28
        ));

        $this->_addAfter = false;
        if (!$this->_addButtonLabel) {
            $this->_addButtonLabel = Mage::helper('adminhtml')->__('Add Matrix Table');
        }

        if (!$this->getTemplate()) {
            $this->setTemplate('snowflake/matrix/config/form/field/matrix_group.phtml');
        }

        parent::__construct();
    }

    /**
     * Render array cell for matrix group template
     *
     * @param string $columnName
     * @return string
     */
    protected function _renderCellTemplate($columnName)
    {
        if (empty($this->_columns[$columnName])) {
            throw new Exception('Wrong column name specified.');
        }
        $column     = $this->_columns[$columnName];
        $inputName  = $this->getElement()->getName() . '[#{_id}][' . $columnName . ']';


        switch ($columnName)
        {
            case 'matrixProdutGroup':
                $name = 'provet_matrix_group';

                $entityTypeId  = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();
                $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($name)->setEntityTypeFilter($entityTypeId)->getFirstItem();
                $attributeId   = $attributeInfo->getAttributeId();
                $attribute     = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);

                $options   = $attribute->getSource()->getAllOptions(false);

                $rendered  = '<select name="' . $inputName . '">';
                $rendered .= '<option value=""></option>';
                foreach($options as $key => $data) {
                    $rendered .= '<option value="'.$data['value'].'">'.$data['label'].'</option>';
                }
                $rendered .= '</select>';

                break;


            case 'available':
                $source    = Mage::getModel('adminhtml/system_config_source_yesno')->toArray();

                $rendered  = '<select name="' . $inputName . '">';
                foreach($source as $att => $name) {
                    $rendered .= '<option value="'.$att.'">'.$name.'</option>';
                }
                $rendered .= '</select>';

                break;

            default:
                $rendered = '<input type="text" name="' . $inputName . '" value="#{' . $columnName . '}" ' . ($column['size'] ? 'size="' . $column['size'] . '"' : '') . '/>';
        }

        return $rendered;
    }
}
  