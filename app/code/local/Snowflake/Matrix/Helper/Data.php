<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 06.03.2015
 * Time: 15:23
 */
class Snowflake_Matrix_Helper_Data extends Mage_Customer_Helper_Data
{
    const PROVET_WARENBEZUG = 'provet_warenbezug';
    const PROVET_REG_NO = 'provet_reg_no';
    const PROVET_VK_SPERRE = 'provet_vksperre';

    protected $_productMatrixGroup = null;

    /**
     * Check if module is enabled
     *
     * @param int|string|Mage_Core_Model_Store $store
     * @return bool
     */
    public function isModuleEnable($store = null)
    {
        $setting = $this->_getSettings('active', $store);
        return (bool) $setting;
    }

    /**
     * Check Limited Product permission for product
     *
     * @param int $productId
     * @return bool
     */
    public function isDisabled($productId){
        if (!Mage::getSingleton('customer/session')->isLoggedIn()){
            $eavAttribute = new Mage_Eav_Model_Mysql4_Entity_Attribute();
            $keyField = 'entity_id';
            $attrCode = $eavAttribute->getIdByCode(
                'catalog_product', Snowflake_Matrix_Helper_Data::PROVET_WARENBEZUG
            );

            $attrTypeInt = Mage::getSingleton('core/resource')->getTableName('catalog/product') . '_int';


            /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
            $collection = Mage::getModel('catalog/product')->getCollection();

            $cn = $this->_getCorrelationName($collection);
            $collection->getSelect()
                ->joinLeft(
                    array('at_dis_provet_warenbezug_int' => $attrTypeInt),
                    "at_dis_provet_warenbezug_int.entity_id = {$cn}.{$keyField} AND 
                     at_dis_provet_warenbezug_int.attribute_id = {$attrCode} AND 
                     at_dis_provet_warenbezug_int.store_id = 0",
                    array()
                )
                ->where(
                    "at_dis_provet_warenbezug_int.value = 0"
                )
                ->where("{$cn}.{$keyField} = ?", $productId);

            if (!count($collection)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if vk sperre active for product
     *
     * @param int $productId
     * @return bool
     */
    public function isVkSperreActive($productId){
        $eavAttribute = new Mage_Eav_Model_Mysql4_Entity_Attribute();
        $keyField = 'entity_id';
        $attrCode = $eavAttribute->getIdByCode(
            'catalog_product', Snowflake_Matrix_Helper_Data::PROVET_VK_SPERRE
        );

        $attrTypeInt = Mage::getSingleton('core/resource')->getTableName('catalog/product') . '_int';


        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection = Mage::getModel('catalog/product')->getCollection();

        $cn = $this->_getCorrelationName($collection);
        $collection->getSelect()
            ->joinLeft(
                array('at_dis_provet_vksperre_int' => $attrTypeInt),
                "at_dis_provet_vksperre_int.entity_id = {$cn}.{$keyField} AND 
                 at_dis_provet_vksperre_int.attribute_id = {$attrCode} AND 
                 at_dis_provet_vksperre_int.store_id = 0",
                array()
            )
            ->where(
                "at_dis_provet_vksperre_int.value = 0"
            )
            ->where("{$cn}.{$keyField} = ?", $productId);

        if (!count($collection)) {
            return true;
        }

        return false;
    }

    /**
     * Add Limited Product to collection filter.
     *
     * @param Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection $collection
     * @param string $keyField
     * @return Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
     */
    public function addLimitedProductAttrToFilter($collection, $keyField = 'entity_id'){
        if (Mage::getSingleton('customer/session')->isLoggedIn()){
            $customerIsLimited = Mage::getSingleton('customer/session')->getCustomer()->getProvetWarenbezug();

            //1 = access for all article /  0 = no access for limited article
            if($customerIsLimited == false){
                $eavAttribute = new Mage_Eav_Model_Mysql4_Entity_Attribute();
                $attrCode = $eavAttribute->getIdByCode(
                    'catalog_product', Snowflake_Matrix_Helper_Data::PROVET_WARENBEZUG
                );
                $attrTypeInt = Mage::getSingleton('core/resource')->getTableName('catalog/product') . '_int';

                $cn = $this->_getCorrelationName($collection);
                $collection->getSelect()
                    ->joinLeft(
                        array('at_provet_warenbezug_int' => $attrTypeInt),
                        "at_provet_warenbezug_int.entity_id = {$cn}.{$keyField} AND 
                         at_provet_warenbezug_int.attribute_id = {$attrCode} AND 
                         at_provet_warenbezug_int.store_id = 0",
                        array()
                    )
                    ->where(
                        "at_provet_warenbezug_int.value = 0"
                    );
            }

            return $collection;
        }
    }

    /**
     * Add Vk Sperre to collection filter.
     *
     * @param Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection $collection
     * @param string $keyField
     * @return Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
     */
    public function addVkSperreAttrToFilter($collection, $keyField = 'entity_id'){
        if ($this->_alreadyProcessed($collection)) {
            return $collection;
        }

        $eavAttribute = new Mage_Eav_Model_Mysql4_Entity_Attribute();
        $attrCode = $eavAttribute->getIdByCode(
            'catalog_product', Snowflake_Matrix_Helper_Data::PROVET_VK_SPERRE
        );
        $attrTypeInt = Mage::getSingleton('core/resource')->getTableName('catalog/product') . '_int';

        $cn = $this->_getCorrelationName($collection);
        $collection->getSelect()
            ->joinLeft(
                array('at_provet_vksperre_int' => $attrTypeInt),
                "at_provet_vksperre_int.entity_id = {$cn}.{$keyField} AND
                 at_provet_vksperre_int.attribute_id = {$attrCode} AND
                 at_provet_vksperre_int.store_id = 0",
                array()
            )
            ->where(
                "at_provet_vksperre_int.value = 0"
            );

        return $collection;
    }
    
    public function filterCustomerPermissionCollection($collection) {          
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        
        $provetWarenbezug = $customer->getProvetWarenbezug();
        $allowGovernmentRestriction = $customer->getAllowGovernmentRestriction();
        $provetBlock = $customer->getProvetBlock();
        
        $eavAttribute = new Mage_Eav_Model_Mysql4_Entity_Attribute();
        $attrCode = $eavAttribute->getIdByCode(
            'catalog_product', Snowflake_Matrix_Helper_Data::PROVET_VK_SPERRE
        );
        $attrTypeInt = Mage::getSingleton('core/resource')->getTableName('catalog/product') . '_int';

        $cn = $this->_getCorrelationName($collection);
        $collection->getSelect()
            ->joinLeft(
                array('at_provet_vksperre_int' => $attrTypeInt),
                "at_provet_vksperre_int.entity_id = {$cn}.entity_id AND
                 at_provet_vksperre_int.attribute_id = {$attrCode} AND
                 at_provet_vksperre_int.store_id = 0",
                array("provet_vksperre" => "at_provet_vksperre_int.value")
            );
        
        $attrCode = $eavAttribute->getIdByCode(
            'catalog_product', Snowflake_Matrix_Helper_Data::PROVET_WARENBEZUG
        );
        $attrTypeInt = Mage::getSingleton('core/resource')->getTableName('catalog/product') . '_int';

        $cn = $this->_getCorrelationName($collection);
        $collection->getSelect()
            ->joinLeft(
                array('at_provet_warenbezug_int' => $attrTypeInt),
                "at_provet_warenbezug_int.entity_id = {$cn}.entity_id AND 
                 at_provet_warenbezug_int.attribute_id = {$attrCode} AND 
                 at_provet_warenbezug_int.store_id = 0",
                array("provet_warenbezug" => "at_provet_warenbezug_int.value")
            );
                 
        $attrCode = $eavAttribute->getIdByCode(
            'catalog_product', Snowflake_Matrix_Helper_Data::PROVET_REG_NO
        );
        $attrTypeVarchar = Mage::getSingleton('core/resource')->getTableName('catalog/product') . '_varchar';

        $cn = $this->_getCorrelationName($collection);
        $collection->getSelect()
            ->joinLeft(
                array('at_provet_reg_no_varchar' => $attrTypeVarchar),
                "at_provet_reg_no_varchar.entity_id = {$cn}.entity_id AND 
                 at_provet_reg_no_varchar.attribute_id = {$attrCode} AND 
                 at_provet_reg_no_varchar.store_id = 0",
                array("provet_reg_no" => "at_provet_reg_no_varchar.value")
            );
        
        if ($provetBlock == 1) {
            $collection->addFieldToFilter("entity_id", array('null' => true));
        }
        
        $collection->getSelect()->where(
                "at_provet_vksperre_int.value = 0"
            );
        
        if ($provetWarenbezug == 0 && $allowGovernmentRestriction == 1) {
            $collection->getSelect()->where(
                "at_provet_warenbezug_int.value = 0"
            );
        }
        else if ($provetWarenbezug == 1 && $allowGovernmentRestriction == 1) {
            
        }
        else if ($provetWarenbezug == 0 && $allowGovernmentRestriction == 0) {
            $collection->getSelect()->where(
                "at_provet_warenbezug_int.value = 0"
            );
            $collection->getSelect()->where(
                "at_provet_reg_no_varchar.value is NULL"
            );
        }
        else if ($provetWarenbezug == 1 && $allowGovernmentRestriction == 0) {
            $collection->getSelect()->where(
                "at_provet_reg_no_varchar.value is NULL"
            );
        }
        
        return $collection;
    }


    /**
     * Return a configuration setting from the snowflake_matrix/settings section.
     *
     * @param string $field
     * @param int|string|Mage_Core_Model_Store $store
     * @return mixed
     */
    protected function _getSettings($field, $store = null)
    {
        return Mage::getStoreConfig('snowflake_matrix/settings/' . $field, $store);
    }

    /**
     * @param Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection $collection
     *
     * @return string
     */
    protected function _getCorrelationName($collection)
    {
        list($correlationName) = ($collection->getSelect()->getPart(Zend_Db_Select::COLUMNS));
        return array_shift($correlationName);
    }

    /**
     * @param Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection $collection
     *
     * @return bool
     */
    protected function _alreadyProcessed($collection)
    {
        $where = $collection->getSelect()->getPart(Zend_Db_Select::WHERE);
        foreach ($where as $part) {
            if (preg_match('#at_provet_matrix_group\.value#is', $part)) {
                return true;
            }
        }
        return false;
    }
}
  