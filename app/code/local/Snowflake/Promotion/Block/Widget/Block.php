<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 16.01.2015
 * Time: 15:08
 */
class Snowflake_Promotion_Block_Widget_Block extends Mage_Catalog_Block_Product_List implements Mage_Widget_Block_Interface
{
    /**
     * Default value whether show slider or not
     */
    const DEFAULT_SHOW_SLIDER               = false;

    /**
     * Default value whether show pager or not
     */
    const DEFAULT_SHOW_PAGER                = false;

    /**
     * Default value for products per page
     */
    const DEFAULT_PRODUCTS_PER_PAGE         = 5;

    /**
     * Name of request parameter for page number value
     */
    const PAGE_VAR_NAME                     = 'p';

    /**
     * Instance of pager block
     *
     * @var Mage_Catalog_Block_Product_Widget_Html_Pager
     */
    protected $_pager;

    /**
     * Prepare collection with promotion products
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _beforeToHtml()
    {
        $this->setProductCollection($this->_getProductCollection());
        return parent::_beforeToHtml();
    }

    /**
     * Prepare and return product collection
     *
     * @return Mage_Catalog_Model_Resource_Product_Collection|Object|Varien_Data_Collection
     */
    protected function _getProductCollection()
    {
        /** @var $collection Mage_Catalog_Model_Resource_Product_Collection */
        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());

        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter()
            ->addAttributeToFilter('provet_in_action', 1)
            ->addAttributeToSort('provet_sort', 'ASC')
            ->addAttributeToSort('name', 'ASC')
            ->setPageSize($this->getProductsPerPage())
            ->setCurPage($this->getCurrentPage());

        return $collection;
    }

    /**
     * Retrieve how much products per column should be displayed
     *
     * @return int
     */
    public function getColumnCount()
    {
        if (!$this->hasData('column_count')) {
            return parent::getColumnCount();
        }
        return $this->getData('column_count');
    }

    /**
     * Get current page variable
     *
     * @return int
     */
    public function getCurrentPage()
    {
        return (isset($_REQUEST['p'])) ? intval($_REQUEST['p']) : 1;
    }

    /**
     * Retrieve how much products should be displayed
     *
     * @return int
     */
    public function getProductsPerPage()
    {
        if (!$this->hasData('products_per_page')) {
            $this->setData('products_per_page', self::DEFAULT_PRODUCTS_PER_PAGE);
        }
        return $this->getData('products_per_page');
    }

    /**
     * Return flag whether slider need to be shown or not
     *
     * @return bool
     */
    public function showSlider()
    {
        if (!$this->hasData('show_slider')) {
            $this->setData('show_slider', self::DEFAULT_SHOW_SLIDER);
        }
        return (bool)$this->getData('show_slider');
    }

    /**
     * Return flag whether pager need to be shown or not
     *
     * @return bool
     */
    public function showPager()
    {
        if (!$this->hasData('show_pager')) {
            $this->setData('show_pager', self::DEFAULT_SHOW_PAGER);
        }
        return (bool)$this->getData('show_pager');
    }

    public function getPercentage($product)
    {
        $taxHelper = $this->helper('tax');

        $regularPrice = $taxHelper->getPrice($product, $product->getPrice());
        $finalPrice   = $taxHelper->getPrice($product, $product->getFinalPrice());

        if ($regularPrice != $finalPrice) {
            $percentage      = number_format($finalPrice / $regularPrice * 100, 2);
            $finalPercentage = 100 - $percentage;
        } else {
            $finalPercentage = 0;
        }

        return number_format($finalPercentage, 0).'%';
    }

    /**
     * Render pagination HTML
     *
     * @return string
     */
    public function getPagerHtml()
    {
        if ($this->showPager()) {
            if (!$this->_pager) {
                $productCollection = $this->getProductCollection();

                $this->_pager = $this->getLayout()
                    ->createBlock('catalog/product_widget_html_pager', 'widget.promotion.product.list.pager');

                $this->_pager->setUseContainer(false)
                    ->setTemplate('snowflake/promotion/product/toolbar/pager.phtml')
                    ->setShowAmounts(false)
                    ->setShowPerPage(false)
                    ->setPageVarName(self::PAGE_VAR_NAME)
                    ->setLimit($this->getProductsPerPage())
                    ->setTotalLimit($productCollection->getSize())
                    ->setFrameLength(Mage::getStoreConfig('design/pagination/pagination_frame'))
                    ->setJump(Mage::getStoreConfig('design/pagination/pagination_frame_skip'))
                    ->setCollection($this->getProductCollection());
            }
            if ($this->_pager instanceof Mage_Core_Block_Abstract) {
                return $this->_pager->toHtml();
            }
        }

        return '';
    }
}
  