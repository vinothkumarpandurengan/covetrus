<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 18.12.2014
 * Time: 10:12
 */
class Snowflake_Teaser_Block_Category extends Mage_Catalog_Block_Product_Abstract
{

    public function getCategoryCollection()
    {
        $collection = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToFilter('is_active', 1)
            ->addAttributeToFilter('include_in_menu', 1)
            ->addAttributeToFilter('snowflake_category_teaser', 1)
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('description')
            ->addAttributeToSelect('image')
            ->addAttributeToSelect('thumbnail')
            ->setCurPage(1);

        return $collection;
    }

    public function getThumbnailUrl($category)
    {
        if  (isset($category['thumbnail']) && $category['thumbnail']) {
            return Mage::getBaseUrl('media') . 'catalog/category/' . $category->getThumbnail();
        }
    }
}
