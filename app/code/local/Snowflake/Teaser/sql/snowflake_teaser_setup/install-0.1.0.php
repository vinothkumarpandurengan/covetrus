<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 18.12.2014
 * Time: 09:29
 */

$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_category', 'snowflake_category_teaser', array(
    'group'         => 'General Information',
    'label'         => 'Show at Homepage',
    'input'         => 'select',
    'type'          => 'int',
    'source'        => 'eav/entity_attribute_source_boolean',
    'user_defined'  => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'required'      => 0,
    'visible'       => 1,
    'default'       => 0,
    'sort_order'    => 50,
));

$installer->endSetup();