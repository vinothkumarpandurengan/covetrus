<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Mode
 *
 * @author Administrator
 */
class Mdc_Wysiwyg_Model_Wysiwyg_File_Mode {
    public function toOptionArray() {
        $modes = array(
            array("value" => "new_tab", "label" => "Show in new tab"),
            array("value" => "current_tab", "label" => "Show in current tab"),
            array("value" => "force_download", "label" => "Force Download"),
        );
        
        return $modes;
    }
}
