<?php

class Mdc_Wysiwyg_Model_Wysiwyg_File extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('wysiwyg/wysiwyg_file');
    }
    
    public function save () {
        Mage::dispatchEvent('wysiwyg_file_save_before', array('file' => $this));
        
        parent::save();
        
        Mage::dispatchEvent('wysiwyg_file_save_after', array('file' => $this));
    }
    
    public function toOptionArray() {
        $list = array();
        
        $collection = $this->getCollection();
        
        if (count($collection)) {
            foreach($collection as $file) {
                $list[] = array("value" => $file->getId(), "label" => $file->getFilename());
            }
        }
        
        return $list;
    }
}
