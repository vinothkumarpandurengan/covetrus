<?php

class Mdc_Wysiwyg_Model_Mysql4_Wysiwyg_File_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('wysiwyg/wysiwyg_file');
    }
}