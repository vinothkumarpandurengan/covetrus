<?php

class Mdc_Wysiwyg_Block_Adminhtml_Wysiwyg_File_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'wysiwyg';
        $this->_controller = 'adminhtml_wysiwyg_file';

        $this->_updateButton('save', 'label', Mage::helper('wysiwyg')->__('Save File'));
        $this->_updateButton('delete', 'label', Mage::helper('wysiwyg')->__('Delete File'));

        /*$this->_addButton('saveandcontinue', array(
                'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
                'onclick' => 'saveAndContinueEdit()',
                'class' => 'save',
                    ), -100);

            $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";*/
    }

    public function getHeaderText() {
        if (Mage::registry('file_data') && Mage::registry('file_data')->getId()) {
            return Mage::helper('wysiwyg')->__("File %s", Mage::registry('file_data')->getFilename());
        } else {
            return Mage::helper('wysiwyg')->__('Upload New File');
        }
    }

}