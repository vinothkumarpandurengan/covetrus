<?php

class Mdc_Wysiwyg_Block_Adminhtml_Wysiwyg_File_Edit_Tab_General extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('general_form', array('legend' => Mage::helper('wysiwyg')->__('General Information')));
        
        $fieldset->addField('filename', 'text', array(
            'label' => Mage::helper('wysiwyg')->__('Filename'),
            'required' => true,
            'name' => 'filename'
        ));
        
        $fieldset->addField('file', 'file', array(
            'label' => Mage::helper('wysiwyg')->__('Upload File'),
            'required' => true,
            'name' => 'file'
        ));

        if (Mage::getSingleton('adminhtml/session')->getFileData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getFileData());
            Mage::getSingleton('adminhtml/session')->setFileData(null);
        } elseif (Mage::registry('file_data')) {
            $form->setValues(Mage::registry('file_data')->getData());
        }

        return parent::_prepareForm();
    }

}
