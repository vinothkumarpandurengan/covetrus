<?php

class Mdc_Wysiwyg_Block_Adminhtml_Wysiwyg_File extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_wysiwyg_file';
        $this->_blockGroup = 'wysiwyg';
        $this->_headerText = Mage::helper('wysiwyg')->__('Manage CMS Files');

        $this->_addButtonLabel = Mage::helper("wysiwyg")->__("Add New CMS File");
        
        parent::__construct();
    }

}