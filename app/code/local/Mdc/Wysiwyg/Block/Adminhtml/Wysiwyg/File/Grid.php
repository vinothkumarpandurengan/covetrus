<?php

class Mdc_Wysiwyg_Block_Adminhtml_Wysiwyg_File_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('wysiwygfileGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('wysiwyg/wysiwyg_file')->getCollection();
        
        $this->setCollection($collection);

        parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('id', array(
            'header' => Mage::helper('wysiwyg')->__('#'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'id',
        ));
        
        $this->addColumn('filename', array(
            'header' => Mage::helper('wysiwyg')->__('Filename'),
            'align' => 'left',
            'index' => 'filename'
        ));
        
        $this->addColumn('created_at', array(
            'header' => Mage::helper('wysiwyg')->__('Created At'),
            'align' => 'left',
            'index' => 'created_at',
            'type' => 'date'
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row) {
        return null;
    }

    public function getGridUrl() {
        return $this->getUrl('adminhtml/wysiwyg_file/grid', array('_current' => true));
    }

}
