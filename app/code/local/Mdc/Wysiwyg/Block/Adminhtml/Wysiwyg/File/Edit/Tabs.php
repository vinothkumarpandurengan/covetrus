<?php

class Mdc_Wysiwyg_Block_Adminhtml_Wysiwyg_File_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId('file_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('wysiwyg')->__('File Information'));
    }
    
    public function getHeaderText() {
        if (Mage::registry('file_data') && Mage::registry('file_data')->getId()) {
            return Mage::helper('wysiwyg')->__("File %s", Mage::registry('file_data')->getFilename());
        } else {
            return Mage::helper('wysiwyg')->__('Upload New File');
        }
    }

    protected function _beforeToHtml() {

        $this->addTab('general', array(
            'label' => Mage::helper('wysiwyg')->__('General Information'),
            'title' => Mage::helper('wysiwyg')->__('General Information'),
            'content' => $this->getLayout()->createBlock('wysiwyg/adminhtml_wysiwyg_file_edit_tab_general')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }

}
