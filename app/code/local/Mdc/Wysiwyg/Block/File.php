<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of File
 *
 * @author Administrator
 */
class Mdc_Wysiwyg_Block_File extends Mage_Core_Block_Abstract implements Mage_Widget_Block_Interface {

    protected function _toHtml() {
        $html = "";
        if ($this->getFile()) {
            $file = Mage::getModel("wysiwyg/wysiwyg_file")->load($this->getFile());
            if ($file->getId() && file_exists($file->getPath())) {
                $url = $this->getUrl("wysiwyg/file/download", array("id" => $file->getId(), "mode" => $this->getMode()));
                $newTab = false;
                if ($this->getMode() == "new_tab") {
                    $newTab = true;
                }
                $html = "<a ".($newTab ? "target='_BLANK'" : "")." href='".$url."'>".$file->getFilename()."</a>";
            }
        }
        return $html;
    }

}
