<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TemplateController
 *
 * @author PC
 */
class Mdc_Wysiwyg_Adminhtml_Wysiwyg_FileController extends Mage_Adminhtml_Controller_Action {
    
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('cms/file');
    }
    
    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('cms/wysiwyg')
                ->_addBreadcrumb(Mage::helper('wysiwyg')->__('Manage Files'), Mage::helper('wysiwyg')->__('Manage Files'));

        return $this;
    }

    public function indexAction() {
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('wysiwyg/adminhtml_wysiwyg_file'));
        $this->renderLayout();
    }

    public function gridAction() {
        $this->loadLayout(false);
        $block = $this->getLayout()->createBlock('wysiwyg/adminhtml_wysiwyg_file_grid');

        $this->getResponse()->setBody(
                $block->toHtml()
        );
    }
    
    public function editAction() {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('wysiwyg/wysiwyg_file')->load($id);

        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }

            Mage::register('file_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('cms/wysiwyg');

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(false);

            $this->_addContent($this->getLayout()->createBlock('wysiwyg/adminhtml_wysiwyg_file_edit'))
                    ->_addLeft($this->getLayout()->createBlock('wysiwyg/adminhtml_wysiwyg_file_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('wysiwyg')->__('File does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function saveAction() {
        if ($data = $this->getRequest()->getPost()) { 
            $file = Mage::getModel('wysiwyg/wysiwyg_file');
            if ($this->getRequest()->getParam('id')) {
                $file = $file->load($this->getRequest()->getParam('id'));
            }
            
            $path = Mage::getBaseDir("media") . DS . "mdc" . DS . "wysiwyg";
            $url = Mage::getBaseUrl('media') . 'mdc' . '/wysiwyg';
            
            $invalidFileType = null;
            
            if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != '') {
                try
                {
                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }
                    $fileName = $_FILES['file']['name'];

                    $uploader = new Varien_File_Uploader('file');
                    
                    $allowedExtensions = Mage::getStoreConfig("cms/file/allowed_extension");
                    $allowedExtensionsArray = array();
                    $allowedExtensionsArrayComplete = array();
                    
                    if ($allowedExtensions) {
                        $allowedExtensionsArray = explode(",", $allowedExtensions);
                        if (count($allowedExtensionsArray)) {
                            foreach($allowedExtensionsArray as $index => $extension) {
                                $allowedExtensionsArrayComplete[] = trim($extension);
                                $allowedExtensionsArrayComplete[] = strtoupper(trim($extension));
                            }
                        }
                    }
                    
                    $uploader->setAllowedExtensions($allowedExtensionsArrayComplete);
                    $uploader->setAllowCreateFolders(true);
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(true);
                    $return = $uploader->save($path, $fileName);
                    
                    $fileName = $return['file'];
                    
                    $fullName = $path.$fileName;
                    $urlName = $url.$fileName;
                    
                    $fileName = explode("/", $fileName);
                    
                    if (!isset($data["filename"])) {
                        $data['filename'] = $fileName[count($fileName) - 1];
                    }
                    
                    $data['path'] = str_replace("/", DS, $fullName);
                    $data['url'] = $urlName;
                    $data['extension'] = pathinfo($fullName, PATHINFO_EXTENSION);
                    $data['size'] = $return['size'];
                }
                catch (Exception $e)
                {
                    $invalidFileType = Mage::helper('wysiwyg')->__($e->getMessage());
                }
            }
            if ($invalidFileType) {
                Mage::getSingleton('adminhtml/session')->addError($fileName." ".$invalidFileType);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }

            $model = Mage::getModel('wysiwyg/wysiwyg_file');
            $model->setData($data)
                    ->setId($this->getRequest()->getParam('id'));

            try {
                if (!$this->getRequest()->getParam('id')) {
                    $model->setCreatedAt(now());
                    $model->setCreatedBy(Mage::getSingleton("admin/session")->getUser()->getUserId());
                } else {
                    $model->setUpdatedAt(now());
                    $model->setUpdatedBy(Mage::getSingleton("admin/session")->getUser()->getUserId());
                }

                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('wysiwyg')->__('File was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('wysiwyg')->__('Unable to find file to save'));
        $this->_redirect('*/*/');
    }

    public function massDeleteAction() {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('productimporter/importer')->load($this->getRequest()->getParam('id'));
                $model->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('productimporter')->__('Importer was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    protected function _sendUploadResponse($fileName, $content, $contentType = 'application/octet-stream') {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK', '');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename=' . $fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}
