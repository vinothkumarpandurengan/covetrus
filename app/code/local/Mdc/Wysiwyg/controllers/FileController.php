<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FileController
 *
 * @author Administrator
 */
class Mdc_Wysiwyg_FileController extends Mage_Core_Controller_Front_Action {
    public function downloadAction () {
        if ($this->getRequest()->getParam("id") && $this->getRequest()->getParam("mode")) {
            $file = Mage::getModel("wysiwyg/wysiwyg_file")->load($this->getRequest()->getParam("id"));
            if ($file->getId() && file_exists($file->getPath())) {
                $mode = $this->getRequest()->getParam("mode");
                if ($mode == "force_download") {
                    $filename = basename($file->getPath());
                    $this->_sendUploadResponse($filename, file_get_contents($file->getPath()));
                    return;
                }
                else {
                    $this->_redirectUrl($file->getUrl());
                    return;
                }
            }
        }
        
        $this->_redirectReferer();
        return;
    }
    
    protected function _sendUploadResponse($fileName, $content, $contentType = 'application/octet-stream') {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK', '');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename=' . $fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}
