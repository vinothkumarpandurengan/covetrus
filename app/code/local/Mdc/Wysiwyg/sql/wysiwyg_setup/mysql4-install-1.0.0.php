<?php

$installer = $this;

$installer->startSetup();

$installer->run("CREATE TABLE IF NOT EXISTS `{$this->getTable('mdc_wysiwyg_file')}` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id', 
  `filename` varchar(255) NOT NULL COMMENT 'Filename', 
  `path` varchar(255) NOT NULL COMMENT 'File path', 
  `url` varchar(255) NOT NULL COMMENT 'File url', 
  `size` varchar(255) DEFAULT '0.0000' COMMENT 'File size', 
  `extension` varchar(255) DEFAULT NULL COMMENT 'File extension', 
  `created_by` int(11) unsigned DEFAULT NULL COMMENT 'Created By Admin id', 
  `updated_by` int(11) unsigned DEFAULT NULL COMMENT 'Updated By Admin id', 
  `created_at` datetime NOT NULL COMMENT 'Created At', 
  `updated_at` datetime NOT NULL COMMENT 'Updated At', 
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;"
);

$installer->endSetup();
