<?php

class Mdc_Cleaner_Model_Processor extends Mage_Core_Model_Abstract {

    protected $__types = array(
        'tierprices'
    );
    
    public function getCleanerTypeCodes() {
        return $this->__types;
    }
    
    public function clean($type) {
        if ($type == "tierprices") {
            $this->__cleanTierPrices();
        }
    }

    protected function __cleanTierPrices() {
        $resource = Mage::getSingleton('core/resource');

        $writeConnection = $resource->getConnection('core_write');

        $tableName = $resource->getTableName('catalog_product_entity_tier_price');

        $query = 'DELETE FROM `' . $tableName . '`;';

        $writeConnection->query($query);

        $this->__reindexAll();
    }

    protected function __reindexAll() {
        $indexingProcesses = Mage::getSingleton('index/indexer')->getProcessesCollection();

        foreach ($indexingProcesses as $process) {
            $process->reindexEverything();
        }
    }

}
