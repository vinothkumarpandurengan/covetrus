<?php

$installer = $this;

$installer->startSetup();

$installer->run("CREATE TABLE IF NOT EXISTS `{$this->getTable('mdc_customer_spam')}` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `customer_id` int(11) unsigned NULL COMMENT 'Customer ID',
  `firstname` varchar(255) NOT NULL COMMENT 'Customer Firstname',
  `lastname` varchar(255) NOT NULL COMMENT 'Customer Lastname',
  `email` varchar(255) NOT NULL COMMENT 'Customer Email',
  `website_id` int(11) unsigned NOT NULL COMMENT 'Website ID',
  `group_id` int(11) unsigned NOT NULL COMMENT 'Customer Group ID',
  `customer_since` datetime NOT NULL COMMENT 'Created Creation Date', 
  `extra_info` longtext NULL COMMENT 'Customer All Infos Serialized',
  `created_at` datetime NOT NULL COMMENT 'Created At', 
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;"
);

$installer->endSetup();
