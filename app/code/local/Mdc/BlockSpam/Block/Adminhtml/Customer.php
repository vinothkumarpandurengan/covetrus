<?php

class Mdc_BlockSpam_Block_Adminhtml_Customer extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_customer';
        $this->_blockGroup = 'blockspam';
        $this->_headerText = Mage::helper('blockspam')->__('Customer Spams');

        parent::__construct();
        
        $this->removeButton("add");
    }

}