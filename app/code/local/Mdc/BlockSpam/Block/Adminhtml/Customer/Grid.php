<?php

class Mdc_BlockSpam_Block_Adminhtml_Customer_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('spamGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('blockspam/customer_spam')->getCollection();
        
        $this->setCollection($collection);

        parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('id', array(
            'header' => Mage::helper('blockspam')->__('#'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'id',
        ));
        
        $this->addColumn('name', 
            array(
                'header'       => $this->__('Name'),
                'sortable'     => true,
                'index'        => array('firstname', 'lastname'),
                'type'         => 'concat',
                'separator'    => ' ',
                'filter_index' => "CONCAT(main_table.firstname, ' ', main_table.lastname)",
            )
        );
        
        $this->addColumn('email', array(
            'header' => Mage::helper('blockspam')->__('Email'),
            'align' => 'left',
            'index' => 'email'
        ));
        
        $groups = Mage::getResourceModel('customer/group_collection')
            ->addFieldToFilter('customer_group_id', array('gt'=> 0))
            ->load()
            ->toOptionHash();

        $this->addColumn('group', array(
            'header'    =>  Mage::helper('blockspam')->__('Group'),
            'width'     =>  '100',
            'index'     =>  'group_id',
            'type'      =>  'options',
            'options'   =>  $groups,
        ));
        
        $this->addColumn('customer_since', array(
            'header' => Mage::helper('blockspam')->__('Customer Since'),
            'align' => 'left',
            'index' => 'customer_since',
            'type' => 'date'
        ));
        
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('website_id', array(
                'header'    => Mage::helper('blockspam')->__('Website'),
                'align'     => 'center',
                'width'     => '80px',
                'type'      => 'options',
                'options'   => Mage::getSingleton('adminhtml/system_store')->getWebsiteOptionHash(true),
                'index'     => 'website_id',
            ));
        }
        
        $this->addColumn('created_at', array(
            'header' => Mage::helper('blockspam')->__('Move to Spam Date'),
            'align' => 'left',
            'index' => 'created_at',
            'type' => 'date'
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row) {
        return null;
    }

    public function getGridUrl() {
        return $this->getUrl('blockspam/adminhtml_customer/grid', array('_current' => true));
    }

}
