<?php

class Mdc_BlockSpam_Model_Mysql4_Customer_Spam_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('blockspam/customer_spam');
    }
}