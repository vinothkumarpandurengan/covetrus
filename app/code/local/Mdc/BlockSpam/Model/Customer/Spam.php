<?php

class Mdc_BlockSpam_Model_Customer_Spam extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('blockspam/customer_spam');
    }
    
    public function save () {
        Mage::dispatchEvent('customer_spam_save_before', array('customer_spam' => $this));
        
        parent::save();
        
        Mage::dispatchEvent('customer_spam_save_after', array('customer_spam' => $this));
    }
    
    public function cleanupCustomers() {
        $rules = Mage::helper("blockspam")->getRules();
        
        if (count($rules)) {
            $collection = Mage::getModel("customer/customer")->getCollection()->addAttributeToSelect("*");
            
            $filters = array();
            
            foreach($rules as $rule) {
                $rule = str_replace("*", "%", $rule);
                $filters[] = array('attribute' => 'email', 'like' => $rule);
            }
            
            $collection->addAttributeToFilter($filters);
            
            if (count($collection)) {
                foreach($collection as $customer) {
                    $spam = Mage::getModel("blockspam/customer_spam");

                    $spam->setFirstname($customer->getFirstname());
                    $spam->setLastname($customer->getLastname());
                    $spam->setEmail($customer->getEmail());
                    $spam->setWebsiteId($customer->getWebsiteId());
                    $spam->setGroupId($customer->getGroupId());
                    $spam->setCustomerSince($customer->getCreatedAt());
                    $spam->setCreatedAt(now());
                    $spam->setExtraInfo(serialize($customer->getData()));
                    
                    if ($customer->getEntityId()) {
                        $spam->setCustomerId($customer->getEntityId());

                        $customer->delete();
                    }
                    
                    $spam->save();
                }
            }
        }
    }
    
    public function cleanupCustomerWithoutAddress() {
        $collection = Mage::getModel("customer/customer")->getCollection()->addAttributeToSelect("*")->joinAttribute('shipping_postcode', 'customer_address/postcode', 'default_shipping', null, 'left');
        
        $this->cleanup($collection, "address");
    }
    
    public function cleanupCustomerWithoutOrder() {
        $collection = Mage::getModel("customer/customer")->getCollection()->addAttributeToSelect("*")->joinAttribute('shipping_postcode', 'customer_address/postcode', 'default_shipping', null, 'left');
        
        $this->cleanup($collection, "order");
    }
    
    public function cleanupCustomerWithoutAddressWithoutOrder() {
        $collection = Mage::getModel("customer/customer")->getCollection()->addAttributeToSelect("*")->joinAttribute('shipping_postcode', 'customer_address/postcode', 'default_shipping', null, 'left');
        
        $this->cleanup($collection, "address_order");
    }
    
    protected function cleanup($collection, $type) {
        switch ($type) {
            case "address":
            case "address_order":
                $collection->addAttributeToFilter("default_shipping", array("null" => true));
        }

        if (count($collection)) {
            foreach($collection as $customer) {
                $delete = true;
                if ($type == "order" || $type == "address_order") {
                    $orderCollection = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('customer_id', $customer->getId());
                    if (count($orderCollection)) {
                        $delete = false;
                    }
                }
                
                if ($delete) {
                    $spam = Mage::getModel("blockspam/customer_spam");

                    $spam->setFirstname($customer->getFirstname());
                    $spam->setLastname($customer->getLastname());
                    $spam->setEmail($customer->getEmail());
                    $spam->setWebsiteId($customer->getWebsiteId());
                    $spam->setGroupId($customer->getGroupId());
                    $spam->setCustomerSince($customer->getCreatedAt());
                    $spam->setCreatedAt(now());
                    $spam->setExtraInfo(serialize($customer->getData()));
                    
                    if ($customer->getEntityId()) {
                        $spam->setCustomerId($customer->getEntityId());

                        $customer->delete();
                    }
                    
                    $spam->save();
                }
            }
        }
    }
    
    public function deleteAllCustomers() {
        $collection = Mage::getModel("customer/customer")->getCollection();
        
        if (count($collection)) {
            foreach($collection as $customer) {
                $customer->delete();
            }
        }
    }
    
    public function deleteAllOrders() {
        $collection = Mage::getModel("sales/order")->getCollection();
        
        if (count($collection)) {
            foreach($collection as $order) {
                $order->delete();
            }
        }
    }
    
    public function deleteAllProducts() {
        $collection = Mage::getModel("catalog/product")->getCollection();
        
        if (count($collection)) {
            foreach($collection as $product) {
                $product->delete();
            }
        }
    }
}
