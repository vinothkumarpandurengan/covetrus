<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Observer
 *
 * @author Administrator
 */
class Mdc_BlockSpam_Model_Observer {

    protected $_rules = array();

    public function validateCustomerRule($observer) {
        $rules = $this->getRules();

        if (count($rules)) {
            $customer = $observer->getEvent()->getCustomer();

            if (!$customer->getEmail() || Mage::helper("blockspam")->control($customer->getEmail(), $this->getRules())) {
                $spam = Mage::getModel("blockspam/customer_spam");

                if ($customer->getEntityId()) {
                    $spam->setCustomerId($customer->getEntityId());

                    $customer->delete();
                }

                $spam->setFirstname($customer->getFirstname());
                $spam->setLastname($customer->getLastname());
                $spam->setEmail($customer->getEmail());
                $spam->setWebsiteId($customer->getWebsiteId());
                $spam->setGroupId($customer->getGroupId());
                $spam->setCustomerSince($customer->getCreatedAt());
                $spam->setCreatedAt(now());
                $spam->setExtraInfo(serialize($customer->getData()));
                $spam->save();
            }
        }
    }

    protected function getRules() {
        if (!$this->_rules) {
            $this->_prepareRules();
        }
        return $this->_rules;
    }

    protected function _prepareRules() {
        if (!$this->_rules) {
            $this->_rules = Mage::helper("blockspam")->getRules();
        }
    }

    public function addButtonCleanupSpam($observer) {
        $container = $observer->getBlock();
        if (null !== $container && $container->getType() == 'adminhtml/customer') {
            $data = array(
                'label' => Mage::helper("blockspam")->__("Cleanup Spam Emails"),
                'class' => '',
                'onclick' => 'setLocation(\'' . Mage::helper("adminhtml")->getUrl('adminhtml/spam/cleanup') . '\')',
            );
            $container->addButton('cleanup_spam', $data);
            
            /*$data = array(
                'label' => Mage::helper("blockspam")->__("Cleanup Spam Test"),
                'class' => '',
                'onclick' => 'setLocation(\'' . Mage::helper("adminhtml")->getUrl('adminhtml/spam/cleanuptest') . '\')',
            );
            $container->addButton('cleanup_test_spam', $data);*/
        }

        return $this;
    }

}
