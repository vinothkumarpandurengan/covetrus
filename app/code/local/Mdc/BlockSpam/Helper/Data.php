<?php

class Mdc_BlockSpam_Helper_Data extends Mage_Core_Helper_Abstract {
    public function control($needle, $haystack) {
        foreach ($haystack as $value) {
            if (true === fnmatch($value, $needle)) {
                return true;
            }
        }
        return false;
    }
    
    public function getRules() {
        return array("*@*.ru", "*@*.ua", "*@hotmails.com", "*@yandex.com");
    }
}
