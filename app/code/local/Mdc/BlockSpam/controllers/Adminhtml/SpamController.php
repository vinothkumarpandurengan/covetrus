<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TemplateController
 *
 * @author PC
 */
class Mdc_BlockSpam_Adminhtml_SpamController extends Mage_Adminhtml_Controller_Action {
    
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('customer/blockspam');
    }
    
    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('customer/blockspam')
                ->_addBreadcrumb(Mage::helper('blockspam')->__('Customer Spam'), Mage::helper('blockspam')->__('Customer Spam'));

        return $this;
    }

    public function indexAction() {
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('blockspam/adminhtml_customer'));
        $this->renderLayout();
    }

    public function gridAction() {
        $this->loadLayout(false);
        $block = $this->getLayout()->createBlock('blockspam/adminhtml_customer_grid');

        $this->getResponse()->setBody(
                $block->toHtml()
        );
    }
    
    public function cleanupAction () {
        $model = Mage::getModel("blockspam/customer_spam");
        $model->cleanupCustomers();
        
        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('blockspam')->__('Spam has been cleaned successfully!'));
        $this->_redirect('adminhtml/customer/index');
    }
    
    /*public function cleanuptestAction () {
        $model = Mage::getModel("blockspam/customer_spam");
        $model->cleanupCustomerWithoutAddressWithoutOrder();
        
        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('blockspam')->__('Spam has been cleaned successfully!'));
        $this->_redirect('adminhtml/customer/index');
    }*/

    public function deleteAction() {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('blockspam/customer_spam')->load($this->getRequest()->getParam('id'));
                $model->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('blockspam')->__('Spam was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    protected function _sendUploadResponse($fileName, $content, $contentType = 'application/octet-stream') {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK', '');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename=' . $fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}
