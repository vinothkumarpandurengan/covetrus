<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Observer
 *
 * @author Administrator
 */
class Mdc_Import_Model_Observer {
    public function cleanupProductCount($observer) {
        //var_dump("test");exit;
        $resource = Mage::getSingleton('core/resource');
        
        $writeConnection = $resource->getConnection('core_write');
        
        $tableName1 = $resource->getTableName('catalog_product_entity');
        $tableName2 = $resource->getTableName('catalog_category_product');
        
        $query = "DELETE FROM `".$tableName2."` where product_id NOT IN (SELECT entity_id FROM (`".$tableName1."`))";
        
        $writeConnection->query($query);
        
        $categorieCollection = Mage::getModel('catalog/category')->getCollection();
        
        if (count($categorieCollection)) {
            foreach($categorieCollection as $category) {
                $category = Mage::getModel('catalog/category')->load($category->getId());
                if (strpos($category->getPath(), "1/2/13") !== FALSE && $category->getPath() != "1/2/13") {
                    if ($category->getProductCount() == 0 && $category->getIsActive() == 1) {
                        $category->setIsActive(0);
                        $category->save();
                    }
                    else if ($category->getProductCount() > 0 && $category->getIsActive() == 0) {
                        $category->setIsActive(1);
                        $category->save();
                    }
                }
                else {
                    //$category->setIsActive(1);
                    //$category->save();
                }
            }
        }
    }
}
