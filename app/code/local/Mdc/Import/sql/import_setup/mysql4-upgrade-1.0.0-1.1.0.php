<?php
/**
 * Created by PhpStorm.
 * User: Felix Elsener
 * Email: felsener@snowflake.ch
 * Date: 03.03.2015
 * Time: 11:16
 */

/* @var $installer Snowflake_Customer_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

$installer->run("UPDATE `{$this->getTable('eav_attribute')}` SET `frontend_label` = 'VetExklusivität (WarenBezug)', `note` = 'Wenn ja, dann sind für diesen Kunden Produkte mit VetExklusivität ersichtlich.' WHERE `entity_type_id` = 1 AND `attribute_code` = 'provet_warenbezug';"
);

$installer->run("UPDATE `{$this->getTable('eav_attribute')}` SET `frontend_label` = 'Kundensperre (Sperre)', `note` = 'Wenn ja, dann ist der Bestellabschluss für diesen Kunden grundsätzlich gesperrt.' WHERE `entity_type_id` = 1 AND `attribute_code` = 'provet_block';"
);

$installer->run("UPDATE `{$this->getTable('eav_attribute')}` SET `frontend_label` = 'VetExklusivität (WarenBezug)', `note` = 'Wenn ja, dann ist dies ein Produkt mit VetExklusivität und somit nur für Kunden mit entsprechender Berechtigung ersichtlich.' WHERE `entity_type_id` = 4 AND `attribute_code` = 'provet_warenbezug';"
);

$installer->run("UPDATE `{$this->getTable('eav_attribute')}` SET `frontend_label` = 'Swissmedic Reg-Nummer (RegNr)', `note` = 'Wenn Nummer enthalten, dann ist dies ein gesetzlich regulierter Artikel und somit nur für berechtigte Kunden ersichtlich.' WHERE `entity_type_id` = 4 AND `attribute_code` = 'provet_reg_no';"
);

$installer->run("UPDATE `{$this->getTable('eav_attribute')}` SET `frontend_label` = 'Verkaufssperre (VkSperre)', `note` = 'Wenn ja, dann wird dieser Produkt im Shop grundsätzlich nicht angezeigt.' WHERE `entity_type_id` = 4 AND `attribute_code` = 'provet_vksperre';"
);

$installer->endSetup();
  