if (typeof Snowflake == 'undefined') {
    Snowflake = {};
}

Snowflake.WishlistCollection = Class.create({
    wishlists: [],
    products: [],
    block: null,
    urls: {},
    addNewWishlist: {},
    customerId: 0,
    translates: null,

    initialize: function(blockId, urls, customerId, products, translates) {
        this.block = $(blockId);
        this.urls = urls;
        this.customerId = customerId;
        this.products = products;
        this.translates = translates;

        this.block.appendChild(this.createAddWishlistBlock());
        this.getWishlists();
    },

    createAddWishlistBlock: function() {
        var self = this;

        var block = self.createElement('form');
        block.id = 'add-new-wishlist';

        var labelForName = self.createElement('span');
        labelForName.update(self.translates.wishlistName + ':');
        block.appendChild(labelForName);

        block.appendChild(self.createElement('br'));

        var nameInput = self.createInputElement('text');
        nameInput.name = 'wishlist_name';
        block.appendChild(nameInput);
        self.addNewWishlist['name'] = nameInput;

        block.appendChild(self.createElement('br'));

        var checkbox = self.createInputElement('checkbox');
        checkbox.addClassName('check');
        checkbox.name = 'wishlist_editable';
        block.appendChild(checkbox);
        self.addNewWishlist['editable'] = checkbox;

        var labelForCheckbox = self.createElement('span');
        labelForCheckbox.update(self.translates.manageableCustomer);
        block.appendChild(labelForCheckbox);

        block.appendChild(self.createElement('br'));

        var buttonAdd = self.createElement('button');
        buttonAdd.update(self.translates.addWishlist);
        buttonAdd.writeAttribute('type','button');
        Event.observe(buttonAdd, 'click', self.addWishlist.bind(this, nameInput));
        block.appendChild(buttonAdd);

        return block;
    },

    createElement: function(name) {
        var element = document.createElement(name);
        Element.extend(element);
        return element;
    },

    createInputElement: function(type) {
        var self = this;

        var element = self.createElement('input');
        element.type = type;
        return element;
    },

    addWishlist: function(nameInput) {
        var self = this;

        nameInput.addClassName('required-entry');

        var validator = new Validation('add-new-wishlist');
        if (validator.validate()) {
            var preparedParams = {};
            preparedParams['wishlist_name'] = self.addNewWishlist['name'].value;
            preparedParams['wishlist_editable'] = self.addNewWishlist['editable'].checked;
            preparedParams['customer_id'] = self.customerId;

            new Ajax.Request(self.urls.addWishlist, {
                method: 'post',
                parameters: preparedParams,

                onComplete: function(response) {
                    var wishlists = [];
                    wishlists.push(response.responseText.evalJSON());
                    self.addWishlists(wishlists);
                }
            })
        }

        nameInput.removeClassName('required-entry');
    },

    addWishlists: function(wishlists) {
        var self = this;

        for (var i = 0; i < wishlists.length; i++) {
            self.wishlists.push(new Snowflake.Wishlist(wishlists[i], this));
        }
    },

    getWishlists: function() {
        var self = this;
        var preparedParams = {};
        preparedParams['customer_id'] = self.customerId;

        new Ajax.Request(self.urls.getWishlists, {
            method: 'post',
            parameters: preparedParams,

            onComplete: function(response) {
                self.addWishlists(response.responseText.evalJSON());
            }
        });
    }
});

Snowflake.Wishlist = Class.create({
    wishlistCollection: null,

    initialize: function(config, wishlistCollection) {
        this.wishlistCollection = wishlistCollection;
        this.id = config.multiwishlist_id;
        this.name = config.multiwishlist_name;
        this.editable = parseInt(config.multiwishlist_editable);
        this.isMain = config.multiwishlist_is_main;

        this.block = this.addWishlistBlock();
        this.wishlistCollection.block.appendChild(this.block);

        this.addProductBlock = this.createAddProductBlock();
        this.block.appendChild(this.addProductBlock);

        this.productsBlock = this.wishlistCollection.createElement('div');
        this.block.appendChild(this.productsBlock);
    },

    addWishlistBlock: function() {
        var self = this;

        var block = self.wishlistCollection.createElement('div');
        block.addClassName('wishlist');

        var head = self.wishlistCollection.createElement('div');
        head.addClassName('head');

        var title = self.wishlistCollection.createElement('div');
        self.wishlistNameElm = self.wishlistCollection.createElement('span');
        self.wishlistNameElm.update(self.name);
        title.appendChild(self.wishlistNameElm);
        title.addClassName('name');
        if (self.editable) {
            var editableText = self.wishlistCollection.createElement('span');
            editableText.addClassName('additional-info');
            editableText.update(' (' + self.wishlistCollection.translates.manageableCustomer + ')');

            title.appendChild(editableText);
        }

        var buttons = self.wishlistCollection.createElement('div');
        buttons.addClassName('buttons');
        if (!self.isMain) {
            var deleteButton = self.wishlistCollection.createElement('button');
            deleteButton.writeAttribute('type','button');
            deleteButton.addClassName('delete');
            deleteButton.update(self.wishlistCollection.translates.deleteText);

            buttons.appendChild(deleteButton);
            Event.observe(deleteButton, 'click', self.deleteWishlist.bind(this));
        }

        var editButton = self.wishlistCollection.createElement('button');
        editButton.writeAttribute('type','button');
        editButton.addClassName('edit');
        editButton.update(self.wishlistCollection.translates.edit);

        buttons.appendChild(editButton);
        Event.observe(editButton, 'click', self.getProducts.bind(this, editButton));

        head.appendChild(buttons);
        head.appendChild(title);
        block.appendChild(head);

        return block;
    },

    createAddProductBlock: function() {
        var self = this;
        
        var block = self.wishlistCollection.createElement('div');
        block.addClassName('add-product');

        var renameBlock = self.wishlistCollection.createElement('div');
        renameBlock.update($$('#multiplewishlist_html_templates .rename-template')[0].innerHTML);
        renameBlock.select('input')[0].value = self.name;
        Event.observe(renameBlock.select('button')[0], 'click', self.renameWishlist.bind(this, renameBlock.select('input')[0]));
        block.appendChild(renameBlock);

        var select = self.wishlistCollection.createElement('select');
        var option = null;
        for (var i = 0; i < self.wishlistCollection.products.length; i++) {
            option = self.wishlistCollection.createElement('option');
            option.value = self.wishlistCollection.products[i].id;
            option.update(self.wishlistCollection.products[i].name + ' (sku: ' + self.wishlistCollection.products[i].sku + ')');
            select.appendChild(option);
        }
        block.appendChild(select);

        var button = self.wishlistCollection.createElement('button');
        button.writeAttribute('type','button');
        button.update(self.wishlistCollection.translates.addProduct);
        block.appendChild(button);

        Event.observe(button, 'click', self.addNewProduct.bind(this, select));
        block.hide();

        return block;
    },

    addNewProduct: function(dropdown) {
        var self = this;

        if (dropdown.value) {
            var preparedParams = {};
            preparedParams['product_id']  = dropdown.value;
            preparedParams['wishlist_id'] = self.id;
            preparedParams['customer_id'] = self.wishlistCollection.customerId;

            new Ajax.Request(self.wishlistCollection.urls.addItem , {
                method : 'post',
                parameters : preparedParams,

                onComplete: function(response) {
                    var obj = response.responseText.evalJSON();

                    if (obj.error) {
                        alert(obj.error);
                    } else {
                        self.showProducts(obj);
                    }
                }
            });
        }
    },

    showProducts: function(products) {
        var self = this;

        for (var i = 0; i < products.length; i++) {
            self.productsBlock.appendChild(self.addProduct(products[i]));
        }

        self.addProductBlock.show();
    },

    addProduct: function(config) {
        var self = this;

        var block = self.wishlistCollection.createElement('div');
        block.addClassName('product');
        block.update(config.name);

        var removeLink = self.wishlistCollection.createElement('div');
        removeLink.update(self.wishlistCollection.translates.remove);
        removeLink.addClassName('remove');

        block.appendChild(removeLink);
        Event.observe(removeLink, 'click', self.deleteProduct.bind(this, config.id, block));

        return block;
    },

    deleteProduct: function(itemId, block) {
        var self = this;

        if (confirm(self.wishlistCollection.translates.confirmRemoveProduct)) {
            var preparedParams = {};
            preparedParams['item_id'] = itemId;

            new Ajax.Request(self.wishlistCollection.urls.deleteItem, {
                method : 'post',
                parameters : preparedParams,

                onComplete: function(response) {
                    block.remove();
                }
            });
        }
    },

    renameWishlist: function(newNameElm) {
        var self = this;
        var newName = newNameElm.value.strip();

        if (newName.length && newName != self.name) {
            var preparedParams = {};
            preparedParams['wishlist_id'] = self.id;
            preparedParams['name'] = newName;

            new Ajax.Request(self.wishlistCollection.urls.renameWishlist, {
                method : 'post',
                parameters : preparedParams,

                onComplete: function(response) {
                    var obj = response.responseText.evalJSON();
                    if (obj.error) {
                        alert(obj.error);
                    } else if (obj.ok) {
                        self.name = newName;
                        self.wishlistNameElm.update(newName);
                    }
                }
            });
        } else {
            alert(self.wishlistCollection.translates.enterNewName);
        }
    },

    getProducts: function(button) {
        var self = this;

        if (!self.addProductBlock.visible()) {
            var preparedParams = {};
            preparedParams['wishlist_id'] = self.id;

            new Ajax.Request(self.wishlistCollection.urls.getProducts, {
                method: 'post',
                parameters: preparedParams,

                onComplete: function(response) {
                    self.showProducts(response.responseText.evalJSON());
                    button.update(self.wishlistCollection.translates.minimize);
                }
            });
        } else {
            self.productsBlock.childElements().forEach(function(elm){
                elm.remove();
            });

            self.addProductBlock.hide();
            button.update(self.wishlistCollection.translates.edit);
        }
    },

    deleteWishlist: function() {
        var self = this;

        if (confirm(self.wishlistCollection.translates.confirmRemoveWishlist)) {
            var preparedParams = {};
            preparedParams['wishlist_id'] = self.id;

            new Ajax.Request(self.wishlistCollection.urls.deleteWishlist, {
                method : 'post',
                parameters : preparedParams,

                onComplete: function(response) {
                    self.block.remove();
                }
            });
        }
    }
});