// -------------------------------------------------------------------------
// override class MagegiantOnestepcheckoutLogin from js/magegiant/onestepcheckout/login.js
// -------------------------------------------------------------------------
Object.extend(MagegiantOnestepcheckoutLogin.prototype, {
    initialize: function (config) {
        var me = this;
        this.loginLink = $$(config.loginLink);
        this.container = $$(config.containerSelector).first();
        this.contentLogin = $$(config.contentLogin).first();
        this.contentForgotPassword = $$(config.contentForgotPassword).first();
        this.forgotPasswordLinkArray = $$(config.forgotPasswordLinkSelector);
        this.backToLoginLinkArray = $$(config.backToLoginLinkSelector);
        this.loginForm = $$(config.loginFormSelector).first();
        this.forgotPasswordForm = $$(config.forgotPasswordFormSelector).first();
        this.forgotPasswordSuccessBlock = $$(config.forgotPasswordSuccessBlockSelector).first();
        this.errorMessageBoxCssClass = config.errorMessageBoxCssClass;
        this.autoShowUp = config.autoShowUp;
        this.overlayConfig = config.overlayConfig;
        this.jsErrorMsg = config.jsErrorMsg;

        this.forgotPasswordLinkArray.each(function (link) {
            link.observe('click', me.onClickOnForgotPasswordLink.bind(me));
        });
        this.backToLoginLinkArray.each(function (link) {
            link.observe('click', me.onClickOnBackToLoginLink.bind(me));
        });

        this.loginForm = new VarienForm(this.loginForm);
        this.forgotPasswordForm = new VarienForm(this.forgotPasswordForm);

        this.loginForm.form.select('button[type=submit]').each(function (btn) {
            btn.observe('click', me.onLoginFormSubmit.bind(me));
        });
        this.forgotPasswordForm.form.select('button[type=submit]').each(function (btn) {
            btn.observe('click', me.onForgotPasswordFormSubmit.bind(me));
        });
        this.currentVisibleBlock = this.loginForm.form;
//        this.initContainerHeight();

        this.container.select('input, button, a').each(function (el) {
            el.setAttribute('tabindex', '-1');
        });

        this.currentVisibleBlock.select('input, button, a').each(function (el) {
            el.removeAttribute('tabindex');
        });
        this.popup = new MagegiantOnestepcheckoutUIPopup(config.popup);
        this.initObservers();
    },
    initObservers: function () {
        var me = this;
        if (this.loginLink) {
            this.loginLink.each(function(item) {
                item.observe('click', function (e) {
                    me.popup.showPopup();
                });
            });
        }

        if (this.autoShowUp) {
            me.popup.showPopup();
        }
    },
    onClickOnForgotPasswordLink: function (e) {
        this.currentVisibleBlock = this.forgotPasswordForm.form;

        this.contentLogin.hide();
        this.contentForgotPassword.show();
    },

    onClickOnBackToLoginLink: function (e) {
        this.currentVisibleBlock = this.loginForm.form;

        this.contentLogin.show();
        this.contentForgotPassword.hide();
    },

    _onAjaxForgotPasswordCompleteFn: function (transport) {
        try {
            var json = transport.responseText.evalJSON();
        } catch (e) {
            this.showError(this.jsErrorMsg, this.removeLoader.bind(this));
            return;
        }
        if (json.success) {
            if (json.redirect_to) {
                document.location.href = json.redirect_to;
                return;
            } else {
                this.removeLoader();
                this.moveToBlock(this.forgotPasswordSuccessBlock, true);
            }
        } else {
            var errorMsg = this.jsErrorMsg;
            if (('messages' in json) && ('length' in json.messages) && json.messages.length > 0) {
                errorMsg = json.messages;
            }
            this.showError(errorMsg, this.removeLoader.bind(this));
        }
    }
});