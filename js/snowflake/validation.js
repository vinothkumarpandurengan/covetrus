Validation.add('validate-alpha', 'Please use letters only (a-z or A-Z) in this field.', function(v) {
    return Validation.get('IsEmpty').test(v) || /^[a-zA-Z\à\é\è\ü\ö\ä\À\É\È\Ü\Ö\Ä]+$/.test(v)
});

Validation.add('validate-street', 'Please use only letters (a-z or A-Z) or numbers (0-9) or spaces and # only in this field.', function(v) {
    return Validation.get('IsEmpty').test(v) || /^[a-zA-Z\à\é\è\ü\ö\ä\À\É\È\Ü\Ö\Ä0-9 ]+$/.test(v)
});

Validation.add('validate-zip', 'Please enter a valid zip code. For example 90602 or 90602-1234.', function(v) {
    return Validation.get('IsEmpty').test(v) || /(^\d{4}$)/.test(v);
});
Validation.add('validate-zip-international', 'Please enter a valid zip code. For example 90602 or 90602-1234.', function(v) {
    return Validation.get('IsEmpty').test(v) || /(^\d{4}$)/.test(v);
});