// overwrite tinyMCE original settings
if(window.tinyMceWysiwygSetup)
{
    tinyMceWysiwygSetup.prototype.originalGetSettings = tinyMceWysiwygSetup.prototype.getSettings;
    tinyMceWysiwygSetup.prototype.getSettings = function(mode) {
        var settings = this.originalGetSettings(mode);

        //add any extra settings you'd like below
        settings.forced_root_block = false;
        return settings;
    }
}