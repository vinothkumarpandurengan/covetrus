<?php

require_once 'abstract.php';

class Snowflake_ImportExport_Shell_Orderexport extends Mage_Shell_Abstract
{
    /**
     * Run script
     *
     */
    public function run()
    {
        if ($orderId = $this->getArg('order')) {

            $order = Mage::getModel('sales/order')->load($orderId);
            if (!$order->getId()) {
                echo 'Error: Order with ID ' . $orderId . ' could not be found.';
                return;
            }

            Mage::getModel('snowflake_importexport/orderexport')->export($order);
        } else {
            echo $this->usageHelp();
        }
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f export_order.php -- [options]
        php -f export_order.php -- --order <orderId>

  --order <orderId>             Order Id to be exported
  help                          This help

USAGE;
    }
}

$shell = new Snowflake_ImportExport_Shell_Orderexport();
$shell->run();
