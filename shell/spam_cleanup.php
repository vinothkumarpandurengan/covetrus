<<<<<<< HEAD
<?php

require_once 'abstract.php';

class Mdc_BlockSpam_Shell_SpamCleanup extends Mage_Shell_Abstract
{
    /**
     * Run script
     *
     */
    public function run()
    {
        $model = Mage::getModel("blockspam/customer_spam");
        
        if ($this->getArg('type')) {
            if ($this->getArg('type') == "email") {
                $model->cleanupCustomers();
            }
            else if ($this->getArg('type') == "address") {
                $model->cleanupCustomerWithoutAddress();
            }
            else if ($this->getArg('type') == "order") {
                $model->cleanupCustomerWithoutOrder();
            }
            else if ($this->getArg('type') == "aorder") {
                $model->cleanupCustomerWithoutAddressWithoutOrder();
            }
        } else {
            echo $this->usageHelp();
        }
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f spam_cleanup.php -- [options]
        php -f spam_cleanup.php -- --type <type>

        type available: email, address, order, aorder

  help                          This help

USAGE;
    }
}

$shell = new Mdc_BlockSpam_Shell_SpamCleanup();
$shell->run();
=======
<?php

require_once 'abstract.php';

class Mdc_BlockSpam_Shell_SpamCleanup extends Mage_Shell_Abstract
{
    /**
     * Run script
     *
     */
    public function run()
    {
        $model = Mage::getModel("blockspam/customer_spam");
        
        if ($this->getArg('type')) {
            if ($this->getArg('type') == "email") {
                $model->cleanupCustomers();
            }
            else if ($this->getArg('type') == "address") {
                $model->cleanupCustomerWithoutAddress();
            }
            else if ($this->getArg('type') == "order") {
                $model->cleanupCustomerWithoutOrder();
            }
            else if ($this->getArg('type') == "aorder") {
                $model->cleanupCustomerWithoutAddressWithoutOrder();
            }
        } else {
            echo $this->usageHelp();
        }
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f spam_cleanup.php -- [options]
        php -f spam_cleanup.php -- --type <type>

        type available: email, address, order, aorder

  help                          This help

USAGE;
    }
}

$shell = new Mdc_BlockSpam_Shell_SpamCleanup();
$shell->run();
>>>>>>> 703e6d7d5f89dbc75f9ee23f619ad7a8ee6eab89
