<?php

require_once 'abstract.php';

class Snowflake_ImportExport_Shell_Test extends Mage_Shell_Abstract
{
    /**
     * Run script
     *
     */
    public function run()
    {
        /** @var Mage_Catalog_Model_Resource_Eav_Attribute $attribute */
        $attribute = Mage::getModel('catalog/resource_eav_attribute')
            ->setEntityTypeId(Mage::getModel('eav/entity')->setType(Mage_Catalog_Model_Product::ENTITY)->getTypeId());

        $attribute->loadByCode(Mage_Catalog_Model_Product::ENTITY, 'provet_webgrpcode');

        /** @var Mage_Eav_Model_Entity_Attribute_Source_Table $sourceModel */
        $sourceModel = $attribute->getSource();

        $codes  = $this->_getImportArray('WebGrpCode');

        $data   = array();
        foreach($sourceModel->getAllOptions(false, true) as $option) {
            $identifier = $option['label'];

            $data['option']['value'][$option['value']][0] = $identifier;
            // $data['option']['order'][$option['value']] = $sortOrder;

            $result = $this->multiSearch($codes, array('WebGrpCode' => $identifier));
            if ($result) {
                $data['option']['value'][$option['value']][1] = $result['Bezeichnung'];
                $data['option']['value'][$option['value']][2] = $result['Bezeichnung'];
            }
        }

        $data = $this->_filterPostData($data);
        $attribute->addData($data)->save();
    }

    /**
     * Filter post data
     *
     * @param array $data
     * @return array
     */
    protected function _filterPostData($data)
    {
        if ($data) {
            /** @var $helperCatalog Mage_Catalog_Helper_Data */
            $helperCatalog = Mage::helper('catalog');

            if (!empty($data['option']) && !empty($data['option']['value']) && is_array($data['option']['value'])) {
                foreach ($data['option']['value'] as $key => $values) {
                    $data['option']['value'][$key] = array_map(array($helperCatalog, 'stripTags'), $values);
                }
            }
        }
        return $data;
    }

    public function multiSearch($data, $searched) {
        if (empty($searched) || empty($data)) {
            return false;
        }

        foreach ($data as $key => $value) {
            $exists = true;
            foreach ($searched as $skey => $svalue) {
                $exists = ($exists && IsSet($data[$key][$skey]) && $data[$key][$skey] == $svalue);
            }
            if($exists){ return $value; }
        }

        return false;
    }

    protected function _getImportArray($type)
    {
        $filename = $this->_getFilename($type);

        $lineDataWithKeys = array();
        $fieldNames = array();
        $data = array();

        $handle = fopen($filename, 'r');
        while (($lineData = fgetcsv($handle, null, ';', '"')) !== FALSE) {
            if (!sizeof($fieldNames)) {
                $fieldNames = $lineData;
            } else {
                foreach($lineData as $key => $value) {
                    unset($lineData);
                    $lineData = null;

                    if (!isset($fieldNames[$key])) {
                        Mage::throwException('Data has more columns than the header.');
                    }
                    $lineDataWithKeys[$fieldNames[$key]] = $value;
                }
                $data[] = $lineDataWithKeys;
            }
        }

        fclose($handle);

        return $data;
    }

    /**
     * @return string
     */
    protected function _getFilename($type)
    {
        $filename = Mage::getBaseDir('var') . DS . 'import' . DS . $type . '.csv';

        if (!is_file($filename)) {
            Mage::throwException('File "' . $filename . '" does not exist.');
        }

        if (!is_readable($filename)) {
            Mage::throwException('File "' . $filename . '" is not readable.');
        }

        if (!filesize($filename)) {
            Mage::throwException('File "' . $filename . '" seems to be empty.');
        }

        return $filename;
    }
}

$shell = new Snowflake_ImportExport_Shell_Test();
$shell->run();
