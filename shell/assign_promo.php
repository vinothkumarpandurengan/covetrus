<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Shell
 * @copyright  Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

require_once 'abstract.php';

/**
 * Magento Log Shell Script
 *
 * @category    Mage
 * @package     Mage_Shell
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Shell_AssignPromo extends Mage_Shell_Abstract
{
    /**
     * Run script
     *
     */
    public function run()
    {
        $collection = Mage::getModel("catalog/product")->getCollection()->addAttributeToSelect(array("provet_in_action"))->addFieldToFilter("provet_in_action", 1);
        if (count($collection)) {
            foreach($collection as $product) {
                if ($product->getProvetInAction()) {
                    $categories = $product->getCategoryIds();
                    $categories[] = 6183;
                    $product->setCategoryIds($categories);
                    $product->save();
                }
            }
        }
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f log.php -- [options]

  help              This help

USAGE;
    }
}

$shell = new Mage_Shell_AssignPromo();
$shell->run();
