<?php

require_once 'abstract.php';

class Snowflake_ImportExport_Shell_Ftpexport extends Mage_Shell_Abstract
{
    /**
     * Run script
     *
     */
    public function run()
    {
        Mage::getModel('snowflake_importexport/ftp_export')->transferFiles();
    }
}

$shell = new Snowflake_ImportExport_Shell_Ftpexport();
$shell->run();
