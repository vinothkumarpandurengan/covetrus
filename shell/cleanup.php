<?php
/**
 * Created by PhpStorm.
 * User: felsener
 * Date: 08.02.2016
 * Time: 09:22
 */

require_once 'abstract.php';

class Snowflake_ImportExport_Shell_Cleanup extends Mage_Shell_Abstract
{
    /**
     * Run script
     *
     */
    public function run()
    {
        Mage::getModel('snowflake_importexport/backup_cleanup')->run();
    }
}

$shell = new Snowflake_ImportExport_Shell_Cleanup();
$shell->run();