<?php

require_once 'abstract.php';

/**
 * @category   AvS
 * @package    AvS_FastSimpleImport
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software Licence 3.0 (OSL-3.0)
 * @author     Andreas von Studnitz <avs@avs-webentwicklung.de>
 */
class Snowflake_ImportExport_Shell_Import extends Mage_Shell_Abstract
{
    /**
     * Run script
     *
     */
    public function run()
    {
        if ($importTypeCode = $this->getArg('type')) {

            try {
                $importMethods = $this->_getImportMethods();

                if (!isset($importMethods[$importTypeCode])) {
                    Mage::throwException('Please give a valid import type code: product, stock, tierprice, groupprice, customer, matrix.');
                    return;
                }

                echo "FastSimpleImport is running...". "\n";

                //initialize the translations so that we are able to translate things.
                Mage::app()->loadAreaPart(
                    Mage_Core_Model_App_Area::AREA_ADMINHTML,
                    Mage_Core_Model_App_Area::PART_TRANSLATE
                );

                $importMethod = $importMethods[$importTypeCode];

                $importType = '_import' . ucfirst($importTypeCode);
                $this->$importType($importMethod);
            } catch (Exception $e) {
                echo 'Error: ' . $e->getMessage() . "\n";
            }
        } else {
            echo $this->usageHelp();
        }
    }

    /**
     * Import products & categories
     *
     * @params array $importMethods
     */
    protected function _importProduct($importMethods, $dropdownAttributes = array())
    {
        $i = 1;

        while(file_exists(Mage::getBaseDir("var") . DS . "import" . DS . "Artikel_".$i.".csv") || file_exists(Mage::getBaseDir("var") . DS . "import" . DS . "Artikel_".$i."..csv")) {
            unlink(Mage::getBaseDir("var") . DS . "import" . DS . "artikel.csv");
            
            if (file_exists(Mage::getBaseDir("var") . DS . "import" . DS . "Artikel_".$i."..csv")) {
                rename(Mage::getBaseDir("var") . DS . "import" . DS . "Artikel_".$i."..csv", Mage::getBaseDir("var") . DS . "import" . DS . "artikel.csv");
            }
            else {
                rename(Mage::getBaseDir("var") . DS . "import" . DS . "Artikel_".$i.".csv", Mage::getBaseDir("var") . DS . "import" . DS . "artikel.csv");
            }
            
            $model = Mage::getModel('snowflake_importexport/productimport');

            $model->importProducts(1);
            
            $i++;
        }
        return;
        
        $data  = $this->_getImportArray('artikel');
        $model = Mage::getModel('snowflake_importexport/productimport');
        
        $model->importProducts(1);
        return;

        $model->setProductImportData($data);

        /** @var $import AvS_FastSimpleImport_Model_Import */
        $import = Mage::getModel('fastsimpleimport/import');

        foreach ($importMethods as $importMethod) {
            if ($importMethod == 'processProductImport') {
                $data = $model->getProductData();
                
                echo "Start processing of " . count($data) . " products";

                $import
                    ->setUseNestedArrays(true)
    //                ->setDropdownAttributes(array(
    //                    'provet_matrix_group',
    //                    'color', // Configurable Product
    //                    'size', // Configurable Product
    //                ))
                    ->setMultiselectAttributes(array(
                        'provet_webgrpcode',
                    ))
                    //->dryrunProductImport($data);
                    ->processProductImport($data);
            } else {
                $data = $model->getCategoryData();
            }

            $import->$importMethod($data);

            if ($importMethod == 'processProductImport') {
                echo $import->getEntityAdapter()->getProcessedRowsCount() . ' rows with ' . $import->getEntityAdapter()->getProcessedEntitiesCount() . ' entities have been imported successfully.' . "\n";
            }
        }
    }

    /**
     * Import product stocks
     *
     * @params array $importMethods
     */
    protected function _importStock($importMethods)
    {
        $data  = $this->_getImportArray('ampel');
        $model = Mage::getModel('snowflake_importexport/productimport');

        $model->setProductStockImportData($data);

        /** @var $import AvS_FastSimpleImport_Model_Import */
        $import = Mage::getModel('fastsimpleimport/import');
        foreach ($importMethods as $importMethod) {
            $import
                ->$importMethod($model->getProductStocksData());
        }

        echo $import->getEntityAdapter()->getProcessedRowsCount() . ' rows with ' . $import->getEntityAdapter()->getProcessedEntitiesCount() . ' entities have been imported successfully.' . "\n";
    }

    /**
     * Import product tier prices
     *
     * @params array $importMethods
     */
    protected function _importTierprice($importMethods)
    {
        $i = 1;

        while(file_exists(Mage::getBaseDir("var") . DS . "import" . DS . "artikel_Sp_".($i < 10 ? "0" . $i : $i).".csv")) {
            unlink(Mage::getBaseDir("var") . DS . "import" . DS . "artikel_Sp.csv");
			
			if ($i < 10) {
				$i = "0" . $i;
			}
            
            rename(Mage::getBaseDir("var") . DS . "import" . DS . "artikel_Sp_".$i.".csv", Mage::getBaseDir("var") . DS . "import" . DS . "artikel_Sp.csv");
            
            $model = Mage::getModel('snowflake_importexport/productimport');

            $model->importTierPrices(1);
            
            $i++;
        }
        return;
        
        $data = $this->_getImportArray('artikel_sp');
        $model = Mage::getModel('snowflake_importexport/productimport');

        $model->setTierPriceImportData($data);

        /** @var $import AvS_FastSimpleImport_Model_Import */
        $import = Mage::getModel('fastsimpleimport/import');
        foreach ($importMethods as $importMethod) {
            $import
                ->setDropdownAttributes(array(
                    'provet_matrix_group',
                ))
                ->setMultiselectAttributes(array(
                    'provet_webgrpcode',
                ))
                ->$importMethod($model->getTierPriceData());
        }

        echo $import->getEntityAdapter()->getProcessedRowsCount() . ' rows with ' . $import->getEntityAdapter()->getProcessedEntitiesCount() . ' entities have been imported successfully.' . "\n";
    }

    /**
     * Import product group prices
     *
     * @params array $importMethods
     */
    protected function _importGroupprice($importMethods)
    {
        $data = $this->_getImportArray('artikel_vp');
        $model = Mage::getModel('snowflake_importexport/productimport');

        $model->setGroupPriceImportData($data);

        /** @var $import AvS_FastSimpleImport_Model_Import */
        $import = Mage::getModel('fastsimpleimport/import');
        foreach ($importMethods as $importMethod) {
            $import
                ->setDropdownAttributes(array(
                    'provet_matrix_group',
                ))
                ->setMultiselectAttributes(array(
                    'provet_webgrpcode',
                ))
                ->$importMethod($model->getGroupPriceData());
        }

        echo $import->getEntityAdapter()->getProcessedRowsCount() . ' rows with ' . $import->getEntityAdapter()->getProcessedEntitiesCount() . ' entities have been imported successfully.' . "\n";
    }

    /**
     * Import matrix table
     *
     * @params array $importMethods
     */
    protected function _importMatrix($importMethods)
    {
        $data = $this->_getImportArray('kunden_artikel_matrix');
        $model = Mage::getModel('snowflake_importexport/productimport');

        $model->setMatrixData($data);

        foreach ($importMethods as $importMethod) {
            $this->$importMethod($model->getMatrixData());
        }

        echo sizeof($model->getMatrixData()) . ' entities have been imported successfully.' . "\n";
    }

    /**
     * Import customers
     *
     * @params array $importMethods
     */
    protected function _importCustomer($importMethods)
    {
        $data = $this->_getImportArray('kunden');
        $model = Mage::getModel('snowflake_importexport/customerimport');

        $model->setCustomerImportData($data);

        /** @var $import AvS_FastSimpleImport_Model_Import */
        $import = Mage::getModel('fastsimpleimport/import');
        foreach ($importMethods as $importMethod) {
            $import->$importMethod($model->getCustomerData());
        }

        // send welcome email to new customers
        foreach($model->getNewCustomers() as $customer) {
            Mage::helper('snowflake_importexport')->sendWelcomeEmail($customer['email']);
        }

        echo $import->getEntityAdapter()->getProcessedRowsCount() . ' rows with ' . $import->getEntityAdapter()->getProcessedEntitiesCount() . ' entities have been imported successfully.' . "\n";
    }

    /**
     * Import customer price
     *
     * @params array $importMethods
     */
    protected function _importCustomerprice($importMethods)
    {
        $data  = $this->_getImportArray('kunden_ap');
        $model = Mage::getModel('snowflake_importexport/customerimport');

        $model->setCustomerPriceImportData($data);

        /** @var $import AvS_FastSimpleImport_Model_Import */
        $import = Mage::getModel('fastsimpleimport/import');
        foreach ($importMethods as $importMethod) {
            $import
                ->setDropdownAttributes(array(
                    'provet_matrix_group',
                ))
                ->setMultiselectAttributes(array(
                    'provet_webgrpcode',
                ))
                ->$importMethod($model->getCustomerPriceData());
        }

        echo $import->getEntityAdapter()->getProcessedRowsCount() . ' rows with ' . $import->getEntityAdapter()->getProcessedEntitiesCount() . ' entities have been imported successfully.' . "\n";
    }

    /**
     * Import quantity discount
     *
     * @params array $importMethods
     */
    protected function _importDiscount($importMethods)
    {
        $data  = $this->_getImportArray('mengenrabatte');
        $model = Mage::getModel('snowflake_importexport/customerimport');

        $model->setDiscountImportData($data);
        foreach ($importMethods as $importMethod) {
            $this->$importMethod($model->getDiscountImportData());
        }

        echo sizeof($model->getDiscountImportData()) . ' entities have been imported successfully.' . "\n";
    }

    /**
     * Save matrix table to configuration settings
     *
     * @params array $data
     */
    protected function processMatrixImport($data)
    {
        if (is_array($data)) {
            Mage::getConfig()->saveConfig('snowflake_matrix/settings/config', serialize($data));
        }
    }

    /**
     * Save quantity discount settings to db
     *
     * @params array $discounts
     */
    protected function processDiscountImport($discounts)
    {
        foreach($discounts as  $rowNum => $rowData) {
            $discountModel = Mage::getModel('snowflake_quantitydiscount/rule');

            $validateResult = $discountModel->validateData(new Varien_Object($rowData));
            if ($validateResult !== true) {
                Mage::throwException('test blabla', $rowNum);
            }

            $discountCheck = Mage::getModel('snowflake_quantitydiscount/rule')->getCollection()
                ->addFieldToFilter('client', $rowData['client'])
                ->addFieldToFilter('product_id', $rowData['product_id'])
                ->addFieldToFilter('discount_group_id', $rowData['discount_group_ids'])
                ->load();

            $discountCheckArr = $discountCheck->getData();
            if(count($discountCheckArr) > 0) {
                foreach($discountCheckArr as $data) {
                    $discountModel = $discountModel->load($data['rule_id']);
                    $discountModel->addData($rowData);
                }
            } else {
                $discountModel->setData($rowData);
            }

            $discountModel->loadPost($discountModel->getData());
            $discountModel->save();
        }
    }

    /**
     * @return array All available import type codes (keys) with according methods (values)
     */
    protected function _getImportMethods()
    {
        return array(
            'product'       => array('processCategoryImport', 'processProductImport'),
            'stock'         => array('processProductImport'),
            'tierprice'     => array('processProductImport'),
            'groupprice'    => array('processProductImport'),
            'matrix'        => array('processMatrixImport'),
            'customer'      => array('processCustomerImport'),
            'customerprice' => array('processProductImport'), //-> because of the same structure as tierprice & groupprice
            'discount'      => array('processDiscountImport'),
        );
    }

    /**
     * Read import csv file and transform to array format
     *
     * @return array
     */
    protected function _getImportArray($type)
    {
        $filename = $this->_getFilename($type);

        $lineDataWithKeys = array();
        $fieldNames = array();
        $data = array();

        $handle = fopen($filename, 'r');
        while (($lineData = fgetcsv($handle, null, ';', '"')) !== FALSE) {
            if (!sizeof($fieldNames)) {
                $fieldNames = $lineData;
            } else {
                foreach($lineData as $key => $value) {
                    unset($lineData);
                    $lineData = null;

                    if (!isset($fieldNames[$key])) {
                        Mage::throwException('Data has more columns than the header.');
                    }
                    $lineDataWithKeys[$fieldNames[$key]] = $value;
                }
                $data[] = $lineDataWithKeys;
            }
        }

        fclose($handle);

        return $data;
    }

    /**
     * @return string
     */
    protected function _getFilename($type)
    {
        $filename = Mage::getBaseDir('var') . DS . 'import' . DS . $type . '.csv';

        if (!is_file($filename)) {
            Mage::throwException('File "' . $filename . '" does not exist.');
        }

        if (!is_readable($filename)) {
            Mage::throwException('File "' . $filename . '" is not readable.');
        }

        if (!filesize($filename)) {
            Mage::throwException('File "' . $filename . '" seems to be empty.');
        }

        return $filename;
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f import_provet.php -- [options]
        php -f import_provet.php -- --type <code>

  --type <code>                 Import type: product, stock, tierprice, groupprice, matrix, customer, customerprice or discount
  help                          This help

USAGE;
    }
}

$shell = new Snowflake_ImportExport_Shell_Import();
$shell->run();
