<?php

require_once 'abstract.php';

class Snowflake_ImportExport_Shell_Ftpimport extends Mage_Shell_Abstract
{
    /**
     * Run script
     *
     */
    public function run()
    {
        if ($importType = $this->getArg('type')) {
            /** @var $import AvS_FastSimpleImport_Model_Import */
            $model = Mage::getModel('snowflake_importexport/ftp_import');
            $actionMethod = 'import' . ucfirst($importType);

            if (method_exists($model, $actionMethod)) {
                $model->$actionMethod();
            } else {
                echo "Action $actionMethod not found!\n";
                echo $this->usageHelp();
                exit(1);
            }
        } else {
            echo $this->usageHelp();
        }
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f ftp_import.php -- [options]
        php -f ftp_import.php -- --type <code>

  --type <code>                 Import type: product, stock, customer, webgroupcode
  help                          This help

USAGE;
    }
}

$shell = new Snowflake_ImportExport_Shell_Ftpimport();
$shell->run();
