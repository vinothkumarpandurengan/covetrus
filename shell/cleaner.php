<?php

require_once 'abstract.php';

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    MDC
 * @package     Mdc_Cleaner
 * @copyright   Copyright (c) 2019 MDC ERP Software (https://mdcerpsoftware.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Mdc_Cleaner_Shell_Processor extends Mage_Shell_Abstract
{    
    /**
     * Run script
     *
     */
    public function run()
    {
        $model = Mage::getModel("cleaner/processor");
        
        if ($cleanTypeCode = $this->getArg('type')) {
            $list = $model->getCleanerTypeCodes();
            
            if (!in_array($cleanTypeCode, $list)) {
                Mage::throwException('Please give a valid clean type code: tierprices.');
                return;
            }
            
            echo "Cleanup is starting". "\n";
            
            $model->clean($cleanTypeCode);
            
            echo "Cleanup is terminated". "\n";
        }
        else {
            echo $this->usageHelp();
        }
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f cleaner.php -- --type <code>

  --type <code>                 Clean type: tierprice
  help                          This help

USAGE;
    }
}

$shell = new Mdc_Cleaner_Shell_Processor();
$shell->run();
