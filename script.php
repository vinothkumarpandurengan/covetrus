<?php
 
//Load Magento API
require_once 'app/Mage.php';
Mage::app();
 
//First we load the model
$model = Mage::getModel('snowflake_importexport/ftp_import');
 
//Then execute the task
$model->importCustomer();
